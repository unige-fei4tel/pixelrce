#!/bin/bash  
export LD_LIBRARY=''
export PATH=/usr/lib/qt-3.3/bin:/bin:/sbin:/usr/sbin/:/usr/local/bin:/usr/bin:/usr/bin/X11:/usr/sue/bin:/usr/X11R6/bin
export LINUXVERS=SLC`cat /etc/redhat-release | sed -e 's#[0-9])##g' -e 's#[^\.0-9]##g' -e 's#\.[0-9]##'`
if [  -n $LD_LIBRARY_PATH ] ;  then
  export LD_LIBRARY_PATH=''
fi

export ORBHOST=''
export ORBHOST=`/sbin/ifconfig  | grep "inet addr:1[79]2" | head -1 | gawk -F: '{print $2}' | awk '{print $1}'`
if [ -n $ORBHOST  ] ; then
  export ORBendPoint="giop:tcp:${ORBHOST}:0"
else
  unset ORBHOST
fi

export TDAQ_IPC_INIT_REF="file:${HOME}/ipc_root.ref"
export HOST_ARCH=i686-slc5-gcc43-opt
export TDAQ_HOST_ARCH=i686-slc5-gcc43-opt

export RCE_ARCH=ppc-rtems-rce405-opt
export RCE=${HOME}/daq/rce
export ARCH=$HOST_ARCH

#setup python
if [ -e  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt ]; then
export  LD_LIBRARY_PATH=/daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/lib:${LD_LIBRARY_PATH}
export  PATH=/daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/bin:${PATH}
else
  echo Error: Python is not installed
fi

if [ -e /daq/slc5/opt/rtems-4.9.2 ]; then
  export RTEMS=/daq/slc5/opt/rtems-4.9.2
else
  echo Error: RTEMS is not installed
fi

#setup rtems cross compilers
RTEMS_GCC=''
if [ -e  /daq/slc5/opt/powerpc-rtems49-gcc432 ] ; then
  RTEMS_GCC=/daq/slc5/opt/powerpc-rtems49-gcc432
else 
  echo "rtems gcc not found"�
fi
if [ -n $RTEMS_GCC ] ; then
export LD_LIBRARY_PATH=$RTEMS_GCC/lib:${LD_LIBRARY_PATH}
export PATH=$RTEMS_GCC/bin:${PATH}
fi

#setup ROOT
export ROOTSYS=""
if [ -e /daq/slc5/sw/lcg/app/releases/ROOT/5.30.05/${TDAQ_HOST_ARCH} ]; then
  export ROOTSYS=/daq/slc5/sw/lcg/app/releases/ROOT/5.30.05/${TDAQ_HOST_ARCH}/root
  # this needs the TDAQ gcc
  if [ `uname -m` = 'x86_64' ] ; then
    .  /daq/slc5/sw/lcg/contrib/gcc/4.3.5/x86_64-slc5-gcc43-opt/setup.sh
    export LD_LIBRARY_PATH=/daq/slc5/sw/lcg/contrib/gcc/4.3/x86_64-slc5-gcc43-opt/lib:${LD_LIBRARY_PATH}
   else
     .  /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34/setup.sh
     export  LD_LIBRARY_PATH=/daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc43-opt/lib:${LD_LIBRARY_PATH}
   fi
else
  echo "TDAQ gcc not installed"
fi
if [ -n $ROOTSYS ] ; then
  export PATH=$ROOTSYS/bin:${PATH}
  export LD_LIBRARY_PATH=$ROOTSYS/lib:${LD_LIBRARY_PATH}
fi

#setup RCE client software 
export PATH=${RCE}/build/rceis/bin/${HOST_ARCH}:${RCE}/build/rceipc/bin/${HOST_ARCH}:${RCE}/build/rcecalib/bin/${HOST_ARCH}:${RCE}/rcecalib/scripts:${PATH}
export  LD_LIBRARY_PATH=${RCE}/build/rcecalib/lib/${HOST_ARCH}:${RCE}/build/rceers/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceipc/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceoh/lib/${HOST_ARCH}:${RCE}/build/rceis/lib/${HOST_ARCH}:${LD_LIBRARY_PATH}

export RELEASE=${RCE}
export RCE_BIN=${RCE}/build/rcecalib/bin/${RCE_ARCH}
export RCE_MOD=${RCE}/build/rcecalib/mod/${RCE_ARCH}

export XMD_INI=${RCE}/rcecalib/xmd.ini
export TDAQ_IPC_INIT_REF=file:/${HOME}/ipc_root.ref
export SVNROOT=svn+ssh://svn.cern.ch/reps/RceCimDev
export TDAQ_PARTITION=rcetest_${USER}

alias rce_ipc_server='ipc_server -p $TDAQ_PARTITION'
alias rce_is_server='is_server -p  $TDAQ_PARTITION -n RceIsServer'
alias rce_ipc_ls='ipc_ls ; ipc_ls -p $TDAQ_PARTITION'
function rce_load () { echo -e "reboot\nsetenv TDAQ_PARTITION $TDAQ_PARTITION\nsetenv TDAQ_IS_COMPRESSION_THRESHOLD 100000000\n" | host_bootloader -r $1 -l $RCE_MOD/calibservermod.1.0.prod.so ; } 
alias rce_killall='pkill -u $USER ipc_server; pkill -u $USER is_server;'
#fix HOSTTYPE on bash
export HOSTTYPE=`uname -m`-linux


if [ ! -d ~/calibData ] ; then
mkdir ~/calibData 
fi
#tdaq setup
export TDAQ_INST_PATH=/daq/slc5/tdaq/tdaq-04-00-01/installed
export TDAQC_INST_PATH=/daq/slc5/tdaq-common/tdaq-common-01-18-04/installed
export TDAQC_EXT_PATH=/daq/slc5/tdaq-common/tdaq-common-01-18-04/external
export TDAQ_BOOST=/daq/slc5/sw/lcg/external/Boost/1.44.0_python2.6
export TDAQ_MYSQL=/daq/slc5/sw/lcg/external/mysql/5.5.14/
export PATH=$TDAQ_INST_PATH/$HOST_ARCH/bin:${PATH}
export  LD_LIBRARY_PATH=$TDAQ_INST_PATH/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
export  PATH=$TDAQC_INST_PATH/$HOST_ARCH/bin:${PATH}
export  LD_LIBRARY_PATH=$TDAQC_INST_PATH/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
export  PATH=$TDAQC_EXT_PATH/$HOST_ARCH/bin:${PATH}
export  LD_LIBRARY_PATH=$TDAQC_EXT_PATH/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
export  PATH=$TDAQ_INST_PATH/share/bin:${PATH}
export  LD_LIBRARY_PATH=$TDAQ_INST_PATH/share/lib:${LD_LIBRARY_PATH}
export  PYTHONPATH=$TDAQ_INST_PATH/$HOST_ARCH/lib
export PIXLIBINTERFACE=${RCE}/../PixLibInterface 
export USE_RCE=yes
export TDAQ_VERSION=4
export RCE_CORE_VERSION=1.4
