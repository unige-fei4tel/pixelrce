#!/bin/csh
setenv LD_LIBRARY_PATH
setenv PATH /usr/lib/qt-3.3/bin:/bin:/sbin:/usr/sbin/:/usr/local/bin:/usr/bin:/usr/bin/X11:/usr/sue/bin:/usr/X11R6/bin
setenv LINUXVERS SLC`cat /etc/redhat-release | sed -e 's#[0-9])##g' -e 's#[^\.0-9]##g' -e 's#\.[0-9]##'`
setenv TDAQ_VERSION 4

setenv ORBHOST ''
setenv ORBHOST `/sbin/ifconfig | grep "inet addr:1[79]2" | head -1 | gawk -F: '{print $2}' | awk '{print $1}'`
#setenv ORBHOST `/sbin/ifconfig | grep "inet addr:192.168.1" | head -1 | gawk -F: '{print $2}' | awk '{print $1}'`
if ( $ORBHOST != '' ) then
setenv ORBendPoint giop:tcp:${ORBHOST}:0
else
unsetenv ORBHOST
endif



#override for now - x86_64 build is broken
set DBG_FLAG=opt
if ( "$1" == "debug" ) then
set DBG_FLAG=dbg
endif

setenv HOST_ARCH i686-slc5-gcc43-${DBG_FLAG}
setenv TDAQ_HOST_ARCH i686-slc5-gcc43-${DBG_FLAG}

setenv RCE_ARCH ppc-rtems-rce405-opt
setenv RCE ${HOME}/daq/rce


#setup python
if ( -e  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt ) then
setenv LD_LIBRARY_PATH  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/lib:${LD_LIBRARY_PATH}
setenv PATH  /daq/slc5/sw/lcg/external/Python/2.6.5/i686-slc5-gcc43-opt/bin:${PATH}
else
  echo Error: Python is not installed
endif 

if ( -e /daq/slc5/opt/rtems-4.9.2 ) then
  setenv RTEMS /daq/slc5/opt/rtems-4.9.2
else
  echo Error: RTEMS is not installed
endif

#setup rtems cross compilers
set RTEMS_GCC=''

if ( -e  /daq/slc5/opt/powerpc-rtems49-gcc432 ) then
  set RTEMS_GCC=/daq/slc5/opt/powerpc-rtems49-gcc432
else
  echo "rtems gcc not found"
endif

if ( $RTEMS_GCC != '' ) then
setenv LD_LIBRARY_PATH $RTEMS_GCC/lib:${LD_LIBRARY_PATH}
setenv PATH $RTEMS_GCC/bin:${PATH}
endif

#setup ROOT
setenv ROOTSYS ''
if ( -e /daq/slc5/sw/lcg/app/releases/ROOT/5.30.05/i686-slc5-gcc43-opt ) then
  setenv ROOTSYS /daq/slc5/sw/lcg/app/releases/ROOT/5.30.05/i686-slc5-gcc43-opt/root
# this needs the TDAQ gcc
  if ( `uname -m` == 'x86_64') then
    source  /daq/slc5/sw/lcg/contrib/gcc/4.3.5/x86_64-slc5-gcc43-opt/setup.csh
    setenv LD_LIBRARY_PATH /daq/slc5/sw/lcg/contrib/gcc/4.3/x86_64-slc5-gcc43-opt/lib:${LD_LIBRARY_PATH}
  else
    source  /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34/setup.csh
    setenv  LD_LIBRARY_PATH /daq/slc5/sw/lcg/contrib/gcc/4.3/slc4_ia32_gcc34-opt/lib:${LD_LIBRARY_PATH}
  endif
else
  echo "TDAQ gcc not installed"
endif
if ( $ROOTSYS != '' ) then
setenv PATH $ROOTSYS/bin:${PATH}
setenv LD_LIBRARY_PATH $ROOTSYS/lib:${LD_LIBRARY_PATH}
endif

#setup RCE client software 
setenv PATH  ${RCE}/build/rceis/bin/${HOST_ARCH}:${RCE}/build/rceipc/bin/${HOST_ARCH}:${RCE}/build/rcecalib/bin/${HOST_ARCH}:${RCE}/rcecalib/scripts:${PATH}
setenv LD_LIBRARY_PATH  ${RCE}/build/rcecalib/lib/${HOST_ARCH}:${RCE}/build/rceers/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceipc/lib/${HOST_ARCH}:${RCE}/build/rceowl/lib/${HOST_ARCH}:${RCE}/build/rceoh/lib/${HOST_ARCH}:${RCE}/build/rceis/lib/${HOST_ARCH}:${LD_LIBRARY_PATH}

setenv RELEASE ${RCE}
setenv RCE_BIN ${RCE}/build/rcecalib/bin/${RCE_ARCH}
setenv RCE_MOD ${RCE}/build/rcecalib/mod/${RCE_ARCH}

setenv XMD_INI ${RCE}/rcecalib/xmd.ini
setenv TDAQ_IPC_INIT_REF file:/${HOME}/ipc_root.ref
setenv SVNROOT svn+ssh://svn.cern.ch/reps/RceCimDev
setenv TDAQ_PARTITION rcetest_${USER}

alias rce_ipc_server 'ipc_server -p $TDAQ_PARTITION'
alias rce_is_server  'is_server -p  $TDAQ_PARTITION -n RceIsServer'
alias rce_ipc_ls     'ipc_ls ; ipc_ls -p $TDAQ_PARTITION'
alias rce_load       'echo "reboot\nsetenv TDAQ_PARTITION $TDAQ_PARTITION\nsetenv TDAQ_IS_COMPRESSION_THRESHOLD 100000000\n" | host_bootloader -r \!:1 -l $RCE_MOD/calibservermod.1.0.prod.so' 
alias rce_killall    'pkill -u $USER ipc_server; pkill -u $USER is_server;'

if ( ! -d ~/calibData ) then
mkdir ~/calibData
endif

#tdaq setup
setenv TDAQ_INST_PATH /daq/slc5/tdaq/tdaq-04-00-01/installed
setenv TDAQC_INST_PATH  /daq/slc5/tdaq-common/tdaq-common-01-18-04/installed
setenv TDAQC_EXT_PATH /daq/slc5/tdaq-common/tdaq-common-01-18-04/external
setenv TDAQ_BOOST /daq/slc5/sw/lcg/external/Boost/1.44.0_python2.6
setenv TDAQ_MYSQL /daq/slc5/sw/lcg/external/mysql/5.5.14
setenv PATH             $TDAQ_INST_PATH/$HOST_ARCH/bin:${PATH}
setenv LD_LIBRARY_PATH  $TDAQ_INST_PATH/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
setenv LD_LIBRARY_PATH  $TDAQ_BOOST/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
setenv PATH             $TDAQC_INST_PATH/$HOST_ARCH/bin:${PATH}
setenv LD_LIBRARY_PATH  $TDAQC_INST_PATH/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
setenv PATH             $TDAQC_EXT_PATH/$HOST_ARCH/bin:${PATH}
setenv LD_LIBRARY_PATH  $TDAQC_EXT_PATH/$HOST_ARCH/lib:${LD_LIBRARY_PATH}
setenv PATH             $TDAQ_INST_PATH/share/bin:${PATH}
setenv LD_LIBRARY_PATH  $TDAQ_INST_PATH/share/lib:${LD_LIBRARY_PATH}
setenv PYTHONPATH $TDAQ_INST_PATH/$HOST_ARCH/lib:${PATH}
setenv ARCH $HOST_ARCH
rehash
setenv PIXLIBINTERFACE $RCE/../PixLibInterface
setenv USE_RCE yes
setenv RCE_CORE_VERSION 1.4




