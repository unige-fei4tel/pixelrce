#!/bin/bash
#change default paths, if necessary
export TDAQ_VERSION=6
export LCG_INST_PATH=/sw/atlas
export SDK_ROOT=/opt/AtlasRceSdk/V0.11.1


source $LCG_INST_PATH/sw/lcg/contrib/gcc/4.9.3/x86_64-slc6/setup.sh
source $SDK_ROOT/setup.sh

pushd .
debug="Release"
version=""
if  [ $# -gt 0 ] && [  $1 = "TDAQ" ] ; then
  if [ $2 = "debug" ] ; then 
     debug="Debug"
     version="-dbg"
  fi
  rcf=''
  arm="build.arm-tdaq6.1.0$version"
  slc="build.slc6-tdaq6.1.0$version" 
  source $TDAQ_INST_PATH/setup.sh
  source $LCG_INST_PATH/sw/lcg/Python/2.7.9.p1-df007/x86_64-slc6-gcc49-opt/Python-env.sh
else
  if [  $# -eq 1 ] && [ $1 = "debug" ]  ; then
     debug="Debug"  
     version="-dbg"
  fi
  rcf='-DMDW=RCF'
  arm="build.arm-rcf$version"
  slc="build.slc6-rcf$version" 
fi
export ROOTSYS=$LCG_INST_PATH/sw/lcg/LCG_81b/ROOT/6.04.12/x86_64-slc6-gcc49-opt
export TDAQ_INST_PATH=/sw/atlas/tdaq/tdaq-06-01-00/installed
set --
if [ -d ~/daq/rce/scripts ]; then
  if [ ! -d ~/daq/rce/$arm ]; then
     echo Creating ~/daq/rce/$arm
     mkdir ~/daq/rce/$arm
  fi
  if [ ! -d ~/daq/rce/$slc ]; then
     echo Creating ~/daq/rce/$slc
     mkdir ~/daq/rce/$slc
  fi
#  source /sw/atlas/setup.sh
  export MAKEFLAGS="-j12 QUICK=1"
  echo cd ~/daq/rce/$arm
  cd ~/daq/rce/$arm
  cmake  -DCMAKE_BUILD_TYPE="$debug"    -DCMAKE_TOOLCHAIN_FILE=~/daq/rce/pixelrce/toolchain/arm-archlinux ~/daq/rce/pixelrce $rcf
  echo cd ~/daq/rce/$slc
  cd ~/daq/rce/$slc
  cmake  -DCMAKE_BUILD_TYPE="$debug"   -DCMAKE_TOOLCHAIN_FILE=~/daq/rce/pixelrce/toolchain/tdaq-linux ~/daq/rce/pixelrce $rcf

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/daq/rce/${slc}/lib
export PATH=${PATH}:~/daq/rce/${slc}/bin
export TDAQ_PARTITION=rce_$USER
export TDAQ_IPC_INIT_REF=file:~/daq/ipc_root.ref
export ORBHOST=192.168.1.1
export ORBendPoint=giop:tcp:${ORBHOST}:0

alias rce_ipc_server='ipc_server -p $TDAQ_PARTITION'
alias rce_is_server='is_server -p  $TDAQ_PARTITION -n RceIsServer'
alias rce_ipc_ls='ipc_ls ; ipc_ls -p $TDAQ_PARTITION'
alias rce_killall='pkill -u $USER ipc_server; pkill -u $USER is_server;'

else 
  echo "~/daq/rce/scripts does not exist"
fi
popd
