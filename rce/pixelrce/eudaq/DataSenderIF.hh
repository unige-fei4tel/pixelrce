#ifndef DATASENDER_HH
#define DATASENDER_HH

  /*
A static interface to the DataSender eudaq class so that one instance may be easily shared across an 
entire object-oritented application.
  */

namespace eudaq
{
  class Event;
  class DataSender;
}

class DataSenderIF
{
  public:
    static void SendEvent(eudaq::Event * event);
    static void SendEvent(unsigned char *data, int size);
    static void setDataSender(eudaq::DataSender * dataSender);
    static bool hasInstance() {return m_dataSender != 0;}
  
  private:
    static eudaq::DataSender * m_dataSender;
};

#endif
