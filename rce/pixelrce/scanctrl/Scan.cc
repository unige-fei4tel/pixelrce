
#include "scanctrl/Scan.hh"
#include "scanctrl/ActionFactory.hh"
#include "dataproc/AbsDataHandler.hh"
#include "scanctrl/LoopFactory.hh"
#include "scanctrl/RceCallback.hh"
#include "util/RceName.hh"
#include "util/RceMutex.hh"
#include <pthread.h>
#include <stdio.h> /* for printf */
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/foreach.hpp>

#include "util/HistoManager.hh"

#include <string>
#include <iostream>
#include "HW/SerialIF.hh"

#ifdef __rtems__
extern "C"{
  #include <rtems/malloc.h>
}
#endif

Scan::Scan():
  m_state(IDLE), m_pause_cond (&m_pause_mutex), 
  m_i(0), m_handler(0),m_timeout_seconds(0),m_timeout_nanoseconds(0), m_failed(false), m_timeouts(0){}

Scan::~Scan(){
}

int Scan::configureScan(boost::property_tree::ptree *scanOptions, ActionFactory &af, AbsDataHandler* par){
  m_handler=par;
  if(m_state!=IDLE && m_state!=CONFIGURED){
    std::cout<<"Warning: Scan was not in idle state. Trying to clean up..."<<std::endl;
    // perhaps there is an old scan hanging in the main loop
    abort();
    sleep(2);
    if(m_state!=IDLE){
      std::cout<<"Could not recover situation. Starting anyway."<<std::endl;
      m_state=IDLE;
    }else{
      std::cout<<"Cleanup successful. Starting the run now"<<std::endl;
    }
  }
  // in case we got stuck in the waiting state somehow in a previous run
  stopWaiting();
  m_handler->timeoutOccurred();
  int retval=0;
  try{
    // set up loops
    //setup contains the code to build the concrete nested loop 
    LoopFactory lf;
    std::string type = scanOptions->get<std::string>("scanType");
    std::cout<<"Scan Type "<<type<<std::endl;
    LoopSetup* setup=lf.createLoopSetup(type.c_str());
    //m_loops gets configured
    setup->setupLoops(m_loops,scanOptions, &af);
    m_timeout_seconds=scanOptions->get<unsigned>("Timeout.Seconds");
    m_timeout_nanoseconds=scanOptions->get<unsigned>("Timeout.Nanoseconds");
    m_allowedTimeouts=scanOptions->get<int>("Timeout.AllowedTimeouts");
    m_state=CONFIGURED;
    // get rid of the setup object
    //    m_loops.print();
    delete setup;
    // histograms
    int nHistoNames=scanOptions->get<int>("nHistoNames");
    m_histolist.clear();
    if(nHistoNames>0){
      BOOST_FOREACH(boost::property_tree::ptree::value_type &v, scanOptions->get_child("HistoNames")){
	std::string data = boost::lexical_cast<std::string>(v.second.data());
	std::cout<<"Histogram "<<data<<std::endl;
	m_histolist.push_back(data);
      }
    }
    
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  return retval;
}

int Scan::waitForData(){
  if(m_dataCond.waitingForData!=false)return 1;// only makes sense if running
  m_dataCond.mutex.lock();
  m_dataCond.waitingForData=true;
  return 0;
}

int Scan::stopWaiting(){
  if(m_dataCond.waitingForData!=true)return 1;// cannot go on unless waiting
  rce_mutex_lock pl( m_dataCond.mutex );
  m_dataCond.waitingForData=false;
  m_dataCond.cond.signal();
  return 0;
}

int Scan::pause(){
  if(m_state!=RUNNING)return 1;// only pause if running
  rce_mutex_lock pl( m_pause_mutex );
  m_state=PAUSED;
  return 0;
}
int Scan::resume(){
  if(m_state!=PAUSED)return 1;// cannot resume unless paused
  rce_mutex_lock pl( m_pause_mutex );
  m_state=RUNNING;
  m_pause_cond.signal();
  return 0;
}

int Scan::abort(){
  // abort must wake up the main thread when it is paused
  rce_mutex_lock pl( m_pause_mutex );
  m_state=ABORT;
  m_pause_cond.signal();
  stopWaiting();
  return 0;
}

int Scan::startScan(){
  std::cout<<"Starting Scan"<<std::endl;
  if(m_state!=IDLE && m_state!=CONFIGURED){ 
    std::cout<<"Bad state "<<m_state<<std::endl;
    return 1;
  }
  int timeoutcount=0;
  while(m_state!=CONFIGURED){
    if(timeoutcount>=CONFIG_TIMEOUT){
      std::cout<<"Configure timed out"<<std::endl;
      return 1; //configure timed out
    }
    timeoutcount++;
    usleep(10000);
  }
  std::cout<<"Running"<<std::endl;
  m_state=RUNNING;
  // m_loops.print();
  m_loops.reset();
#ifdef __rtems__
  struct sched_param    param;
  int                   policy;
  pthread_getschedparam(pthread_self(), &policy, &param);
  param.sched_priority=254;
  pthread_setschedparam(pthread_self(), policy, &param);
  rtems_malloc_statistics_t stats;      	
  malloc_get_statistics(&stats);
  printf("*** malloc statistics\n");
  printf("space available: %uk\n",(unsigned int)stats.space_available/1024);
  printf("space allocated: %uk\n",(unsigned int)(stats.lifetime_allocated-stats.lifetime_freed)/1024);
  //malloc_report_statistics(); 
#endif
  m_timeouts=0;
  m_failed=false;
  do{
    //    std::cout<<"pre event"<<std::endl;

    //Set the state to WAITINGFORDATA until the data is processed.
    //The actual waiting happens at m_data_cond.wait()
    //waitForData();
    m_loops.next();
    //std::cout<<"post event"<<std::endl;
    //if(m_loops.done())stopWaiting(); //there is no data to wait for
    if(!m_loops.done())waitForData(); //there is no data to wait for
    //std::cout<<"before pause"<<std::endl;
    //if paused go to sleep until resume() wakes the thread up
    if(m_dataCond.waitingForData==true) {
      int32_t abs_sec,abs_nsec;
      rce_get_time(&abs_sec,&abs_nsec,m_timeout_seconds,m_timeout_nanoseconds);
      int signalled=m_dataCond.cond.timedwait(abs_sec,abs_nsec);
      m_dataCond.waitingForData=false;
      if(signalled==0){ //timeout. Something went wrong with the data.
	if(m_allowedTimeouts!=-1){ //-1 means any number is allowed
	  m_timeouts++;
	  if(m_timeouts>m_allowedTimeouts){
	    std::cout<<"Timeout limit reached. Aborting scan"<<std::endl;
	    setFailed();
	    abort();
	  }
	}
	std::cout<<"Timeout"<<std::endl;
	m_handler->timeoutOccurred();
      }else{
	//std::cout<<"Was restarted"<<std::endl;
      }
    }
    m_dataCond.mutex.unlock();
    //std::cout<<"Unlocked data mutex "<<m_i++<<std::endl;
      
    m_pause_mutex.lock();
    if(m_state==PAUSED) m_pause_cond.wait();
    m_pause_mutex.unlock();
    
    //std::cout<<"loop"<<std::endl;
  }while(!m_loops.done() && m_state!=ABORT);
  SerialIF::setChannelOutMask(0);
  if(!failed()){
    std::cout<<"Publish histos "<<m_i<<std::endl;
    ipc::CallbackParams cbp;
    cbp.rce=RceName::getRceNumber();
    cbp.status=ipc::DOWNLOADING;
    cbp.maskStage=-1;
    cbp.loop0=-1;
    cbp.loop1=-1;
    RceCallback::sendMsg(ipc::HIGH,&cbp);
    publishHistos();
    std::cout<<"Done "<<m_i++<<std::endl;
    m_state=CONFIGURED;
  }else{
    std::cout<<"Failed"<<std::endl;
    m_state=IDLE;
  }
  RceCallback::shutdown();
  return 0;
}

void Scan::publishHistos(){
  // this scan has no histograms
  if(m_histolist.size()==0)return;
  for (size_t k=0;k<m_histolist.size();k++){
    HistoManager::instance()->publish(m_histolist[k].c_str());
  }
}

void Scan::setFailed(){
  m_failed=true;
  ipc::CallbackParams cbp;
  cbp.rce=RceName::getRceNumber();
  cbp.status=ipc::FAILED;
  cbp.maskStage=-1;
  cbp.loop0=-1;
  cbp.loop1=-1;
  RceCallback::sendMsg(ipc::HIGH,&cbp);
}
  
