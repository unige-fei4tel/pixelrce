#ifndef DISABLEALLCHANNELSEOLACTION_HH
#define DISABLEALLCHANNELSEOLACTION_HH

#include "scanctrl/EndOfLoopAction.hh"
#include "config/ConfigIF.hh"

class DisableAllChannelsEoLAction: public EndOfLoopAction{
public:
  DisableAllChannelsEoLAction(std::string name, ConfigIF* cif) :
    EndOfLoopAction(name),m_configIF(cif){}
  int execute(){
    m_configIF->disableAllChannels();
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
