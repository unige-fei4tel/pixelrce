#ifndef RCFSCANROOT_HH
#define RCFSCANROOT_HH

#include "scanctrl/ScanRoot.hh"
#include "rcf/ScanOptions.hh"
#include "rcf/RCFScanRootAdapter.hh"

class ConfigIF;
class Scan;
class RCFRceCallback;
namespace RCF{
  class RcfServer;
}

class RCFScanRoot: public RCFScanRootAdapter, public ScanRoot {
public:
  RCFScanRoot(RCF::RcfServer &server, ConfigIF* cif, Scan* scan);
  ~RCFScanRoot();
  int32_t RCFconfigureScan(ipc::ScanOptions options);    
  std::vector<std::string> RCFgetHistoNames(std::string reg);
  std::vector<std::string> RCFgetPublishedHistoNames();
  uint32_t RCFnEvents();
  void RCFresynch();
  void RCFsetRceNumber(int32_t rce);
  void RCFconfigureCallback(ipc::Priority pr);
private:
  RCFRceCallback* m_cb;

};
  

#endif
