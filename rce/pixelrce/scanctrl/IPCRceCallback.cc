#include "scanctrl/IPCRceCallback.hh"
#include <iostream>

IPCRceCallback::IPCRceCallback(): RceCallback(){
}

void IPCRceCallback::configure(ipc::Callback_ptr cb, ipc::Priority pr){
  m_cb = ipc::Callback::_duplicate( cb );
  m_pr=pr;
  m_configured=true;
}

void IPCRceCallback::SendMsg(ipc::Priority pr, const ipc::CallbackParams *msg){
  if(m_configured){
    if(pr>=m_pr){
      try {
	m_cb->notify( *msg );
      }
      catch(...) {
	std::cout << "callback : notification fails" << std::endl;
      }
    }
  }
}
void IPCRceCallback::Shutdown(){
  if(m_configured){
    try {
      m_cb->stopServer();
    }
    catch(...) {
      std::cout << "callback : notification fails" << std::endl;
    }
    m_configured=false;
  }
}
