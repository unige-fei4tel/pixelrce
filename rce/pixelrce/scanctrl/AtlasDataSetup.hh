#ifndef ATLASDATASETUP_HH
#define ATLASDATASETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class AtlasDataSetup: public LoopSetup{
public:
  AtlasDataSetup():LoopSetup(){};
  virtual ~AtlasDataSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
