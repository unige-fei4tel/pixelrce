#ifndef SETUPMASKSTAGEACTION_HH
#define SETUPMASKSTAGEACTION_HH


#include "scanctrl/LoopAction.hh"
#include "config/ConfigIF.hh"
#include "util/RceName.hh"
#include "dataproc/AbsDataProc.hh"
#include <boost/property_tree/ptree.hpp>
#include <string>
#include <stdio.h>
#include "scanctrl/RceCallback.hh"

class SetupMaskStageAction: public LoopAction{
public:
  SetupMaskStageAction(std::string name, ConfigIF* cif, AbsDataProc* proc, boost::property_tree::ptree *pt):
    LoopAction(name),m_configIF(cif),m_dataProc(proc){
    try{
      m_firstStage=pt->get<int>("firstStage");
      m_stepStage=pt->get<int>("stepStage");
    }
    catch(boost::property_tree::ptree_bad_path ex){
      std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    }
  }
  int execute(int i){
    int stage=m_firstStage+i*m_stepStage;
    //std::cout<<"Stage "<<stage<<std::endl;
    m_dataProc->setMaskStage(stage);
    ipc::CallbackParams cbp;
    cbp.rce=RceName::getRceNumber();
    cbp.status=ipc::SCANNING;
    cbp.maskStage=stage;
    cbp.loop0=-1;
    cbp.loop1=-1;
    ipc::Priority pr=ipc::LOW;
    if(stage%100==0)pr=ipc::HIGH;
    else if(stage%10==0)pr=ipc::MEDIUM;
    RceCallback::sendMsg(pr,&cbp);
    return m_configIF->setupMaskStage(stage);
    //std::cout<<"Post after SetupMaskStageAction"<<std::endl;
  }
private: 
  ConfigIF* m_configIF;
  AbsDataProc* m_dataProc;
  unsigned short m_firstStage;
  unsigned short m_stepStage;
};

#endif
