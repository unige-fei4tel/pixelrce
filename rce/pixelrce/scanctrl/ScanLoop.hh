#ifndef SCANLOOP_HH
#define SCANLOOP_HH

// Implements a nested for loop. Each loop object has a pointer to the next outer loop to call its next function.
// setupLoop is called every time the loop counter is incremented. endOfLoopAction is called at the end of the loop.
// next returns the return value of the next outer loop. The outermost loop returns if it's done (i.e. the nested 
// loop is over.
// This class is the replacement for the NewDsp LoopCtrl structure.

// C 2009 SLAC Author: Martin Kocian 

#include <string.h>
#include <vector>
#include <list>

#include "scanctrl/LoopAction.hh"
#include "scanctrl/EndOfLoopAction.hh"

class ScanLoop {

public:
  ScanLoop(const char* name, unsigned n);
  ~ScanLoop();
  bool next();
  void reset(){m_i=0;};
  void firstEvent();
  void addLoopAction(LoopAction* la);
  void addEndOfLoopAction(EndOfLoopAction* ea);
  const char* name(){return m_name;}
  void print(const char* prefix);
  
protected:
  void setNextOuterLoop(ScanLoop*);
  void loopAction(unsigned);
  void endOfLoopAction();

  char m_name[128];
  unsigned m_nPoints;
  ScanLoop* m_nextOuterLoop;
  unsigned m_i;
  std::vector<int> m_values; 
  std::list<LoopAction*> m_loopActions;
  std::list<EndOfLoopAction*> m_endOfLoopActions;
  
  friend class NestedLoop;
};

#endif
