#ifndef IPCRCECALLBACK_HH
#define IPCRCECALLBACK_HH

#include "scanctrl/RceCallback.hh"
#include <omnithread.h>

class IPCRceCallback: public RceCallback{
public:
  IPCRceCallback();
  void SendMsg(ipc::Priority pr, const ipc::CallbackParams *msg);
  void Shutdown();
  void configure( ipc::Callback_ptr cb, ipc::Priority pr);
private:
  ipc::Callback_var m_cb;
  
};

#endif
