#ifndef NOISESCANSETUP_HH
#define NOISESCANSETUP_HH

#include "scanctrl/NestedLoop.hh"
#include "scanctrl/LoopSetup.hh"
#include "scanctrl/ActionFactory.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "config/ConfigIF.hh"

class NoiseScanSetup: public LoopSetup{
public:
  NoiseScanSetup():LoopSetup(){};
  virtual ~NoiseScanSetup(){}
  int setupLoops(NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af);
};
  

#endif
