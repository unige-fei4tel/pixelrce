#ifndef IPCSCAN_HH
#define IPCSCAN_HH

#include "scanctrl/Scan.hh"
#include "ipc/object.h"
#include "ScanOptions.hh"
#include "IPCScanAdapter.hh"

class IPCPartition;

template <class TP = ipc::single_thread>
class IPCScan: public IPCNamedObject<POA_ipc::IPCScanAdapter,TP>, public Scan {
public:
  IPCScan(IPCPartition & p, const char * name);
  ~IPCScan();
  void IPCpause();
  void IPCresume();
  void IPCwaitForData();
  void IPCstopWaiting();
  void IPCabort();
  void IPCstartScan();
  CORBA::Long IPCgetStatus();
  void shutdown();
  static void *startScanStatic(void *arg);

};
  

#endif
