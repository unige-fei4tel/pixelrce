#ifndef PROVIDER_HH
#define PROVIDER_HH
#include "ipc/partition.h"
#include "oh/OHRawProvider.h"
#include "util/RceHistoAxis.hh"

class OHBins;
template<class T> class OHRawProvider;

class Provider {
public:
  Provider(IPCPartition& p, const char* servername, const char* providername){
    m_provider=new OHRawProvider<>(p, servername, providername, 0);
  }
  ~Provider(){
    delete m_provider;
  }
  double dsw(float inval){
#ifdef __rtems__
    return (double)inval;
#else
    double invald=(double)inval;
    unsigned long long iv=*(unsigned long long*)&invald;
    unsigned long long ov=iv<<32 | iv>>32;
    return *(double*)&ov;
#endif
  }
  template<class TC,class TE>
  void publish(const std::string & name,
		const std::string & title,
		RceHistoAxis & xaxis,
		const TC * bins,
		const TE * errors,
		bool hasOverflowAndUnderflow,
		int tag  ,
		const std::vector< std::pair<std::string,std::string> > & annotations) ;
  template<class TC,class TE>
  void publish(const std::string & name,
		const std::string & title,
		RceHistoAxis & xaxis,
		RceHistoAxis & yaxis,
		const TC * bins,
		const TE * errors,
		bool hasOverflowAndUnderflow,
		int tag ,
		const std::vector< std::pair<std::string,std::string> > & annotations ) ;
private:
  OHRawProvider<OHBins>* m_provider;
};

#include "util/IPCProvider.i"

#endif
