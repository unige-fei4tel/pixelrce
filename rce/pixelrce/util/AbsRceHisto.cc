#include "util/AbsRceHisto.hh"
#include "util/HistoManager.hh"
#include <assert.h>

AbsRceHisto::AbsRceHisto(const char* name, const char* title, int ndim):m_ndim(ndim), m_name(name), m_title(title){
  HistoManager* mgr=HistoManager::instance();
  assert(mgr!=0);
  mgr->addToInventory(this);
}
AbsRceHisto::~AbsRceHisto(){
  HistoManager* mgr=HistoManager::instance();
  assert(mgr!=0);
  mgr->removeFromInventory(this);
}

void AbsRceHisto::setAxisTitle(int axis, const char* title){
  if(axis>1)return;
  m_axisTitle[axis]=title;
}
const char* AbsRceHisto::axisTitle(int axis){
  if(axis>1)return 0;
  return m_axisTitle[axis].c_str();
} 
  
