// 
// IS monitoring
// 
// Martin Kocian, SLAC, 2/11/2016
//

#ifndef RCEMONITORING_HH
#define RCEMONITORING_HH

#include "config/FWRegisters.hh"


  class Monitoring{ 
  public:
    static void destroy();
    static void publish();
    static void reset();
    static void setOccupancy(int fe, float occ);
    static void setAverageOccupancy(float occ);
    static void setNumberOfEvents(unsigned nevt);
    static void setNMissed(unsigned nevt);
    static void setDisabledMask(unsigned nevt);
    static void setDisabledInRunMask(unsigned nevt);
    static void setErrorCounter(int fe, unsigned nevt, FWRegisters::EFBCOUNTER counter);
    static unsigned getErrorCounter(int fe, FWRegisters::EFBCOUNTER counter);
    static void setTtcClashCounter(unsigned nevt, FWRegisters::CLASH counter);
    static int  getRce();
    Monitoring();
    virtual ~Monitoring(){}
    virtual void Publish(){}
    virtual void Reset(){}
    virtual void SetOccupancy(int fe, float occ){}
    virtual void SetAverageOccupancy(float occ){}
    virtual void SetNumberOfEvents(unsigned nevt){}
    virtual void SetNMissed(unsigned nevt){}
    virtual void SetDisabledMask(unsigned nevt){}
    virtual void SetDisabledInRunMask(unsigned nevt){}
    virtual void SetErrorCounter(int fe, unsigned nevt, FWRegisters::EFBCOUNTER counter){}
    virtual unsigned GetErrorCounter(int fe, FWRegisters::EFBCOUNTER counter){return 0;}
    virtual void SetTtcClashCounter(unsigned nevt, FWRegisters::CLASH counter){}
    virtual int  GetRce(){return 0;}
  protected:
    static Monitoring* m_instance;
  };
    
#endif
