#ifndef RCEHISTOAXIS_HH
#define RCEHISTOAXIS_HH

#include <string>

class RceHistoAxis{
public:
  RceHistoAxis( const std::string & label, size_t bincount, float low, float width ):
    label_(label), bincount_(bincount), low_(low), width_(width){};
  std::string label(){return label_;}
  size_t bincount(){return bincount_;}
  float low(){return low_;}
  float width(){return width_;}
private: 
    std::string		label_;
    size_t		bincount_;
    float		low_;
    float		width_;
};
  

#endif
