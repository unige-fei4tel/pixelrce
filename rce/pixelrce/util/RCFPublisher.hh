#ifndef RCFPUBLISHER_HH
#define RCFPUBLISHER_HH

#include <RCF/RCF.hpp>
#include <SF/string.hpp>
#include <SF/vector.hpp>
#include <SF/utility.hpp>
class RCFRceHistoAxis{
public:
  RCFRceHistoAxis(){}
  RCFRceHistoAxis( const std::string label, size_t bincount, float low, float width ):
    label_(label), bincount_(bincount), low_(low), width_(width){};
  std::string label(){return label_;}
  size_t bincount(){return bincount_;}
  float low(){return low_;}
  float width(){return width_;}
  void serialize(SF::Archive &ar){
    ar & label_ & bincount_ & low_ & width_;
  }
private: 
    std::string		label_;
    uint32_t		bincount_;
    float		low_;
    float		width_;
};

RCF_BEGIN(I_RCFPublisher, "I_RCFPublisher")
	     
RCF_METHOD_V7(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCF::ByteBuffer, RCF::ByteBuffer, int, int)
RCF_METHOD_V7(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCF::ByteBuffer, RCF::ByteBuffer, float, float)
RCF_METHOD_V7(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCF::ByteBuffer, RCF::ByteBuffer, short, short)
RCF_METHOD_V8(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCFRceHistoAxis, RCF::ByteBuffer, RCF::ByteBuffer, int, int)
RCF_METHOD_V8(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCFRceHistoAxis, RCF::ByteBuffer, RCF::ByteBuffer, char, char)
RCF_METHOD_V8(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCFRceHistoAxis, RCF::ByteBuffer, RCF::ByteBuffer, short, short)
RCF_METHOD_V8(void, publishHisto, std::string, std::string, RCFRceHistoAxis, 
	       RCFRceHistoAxis, RCF::ByteBuffer, RCF::ByteBuffer, float, float)
RCF_END(I_RCFPublisher)
#endif
