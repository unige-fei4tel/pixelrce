#include "util/Monitoring.hh"
#include <assert.h>

Monitoring* Monitoring::m_instance=0;

Monitoring::Monitoring(){
  m_instance=this;
}
void Monitoring::destroy(){
  assert(m_instance!=0);
  delete m_instance;
  m_instance=0;
}
void Monitoring::publish(){
  assert(m_instance!=0);
  m_instance->Publish();
}
void Monitoring::reset(){
  assert(m_instance!=0);
  m_instance->Reset();
}
void Monitoring::setOccupancy(int fe, float occ){
  assert(m_instance!=0);
  m_instance->SetOccupancy(fe, occ);
}
void Monitoring::setAverageOccupancy(float occ){
  assert(m_instance!=0);
  m_instance->SetAverageOccupancy(occ);
}
void Monitoring::setNumberOfEvents(unsigned nevt){
  assert(m_instance!=0);
  m_instance->SetNumberOfEvents(nevt);
}
void Monitoring::setNMissed(unsigned nevt){
  assert(m_instance!=0);
  m_instance->SetNMissed(nevt);
}
void Monitoring::setDisabledMask(unsigned mask){
  assert(m_instance!=0);
  m_instance->SetDisabledMask(mask);
}
void Monitoring::setDisabledInRunMask(unsigned mask){
  assert(m_instance!=0);
  m_instance->SetDisabledInRunMask(mask);
}
void Monitoring::setErrorCounter(int fe, unsigned nevt, FWRegisters::EFBCOUNTER counter){
  assert(m_instance!=0);
  m_instance->SetErrorCounter(fe, nevt, counter);
}
unsigned Monitoring::getErrorCounter(int fe, FWRegisters::EFBCOUNTER counter){
  assert(m_instance!=0);
  return m_instance->GetErrorCounter(fe, counter);
}
void Monitoring::setTtcClashCounter( unsigned nevt, FWRegisters::CLASH counter){
  assert(m_instance!=0);
  m_instance->SetTtcClashCounter(nevt, counter);
}
int Monitoring::getRce(){
  assert(m_instance!=0);
  return m_instance->GetRce();
}
