#include "util/IsMonitoring.hh"
#include "util/RceMonitoring.hh"
#include "util/RceName.hh"
#include <ipc/partition.h>
#include <is/infoT.h>
#include <is/infodictionary.h>

IsMonitoring::IsMonitoring(IPCPartition &p, const char* servername, int rce): Monitoring(), m_dict(new ISInfoDictionary(p)),
									      m_mon(new RceMonitoring), m_rce(rce){
  sprintf(m_monname, "%s.RceMonitoring_RCE%d", servername, rce);
}
IsMonitoring::~IsMonitoring(){
  delete m_dict;
  delete m_mon;
}

void IsMonitoring::Publish(){
  m_dict->checkin(m_monname, *m_mon);
}

void IsMonitoring::Reset(){
  m_mon->Occ_Average=0;
  m_mon->NEvt=0;
  m_mon->Mon_Missed=0;
  m_mon->Disabled=0;
  m_mon->DisabledRun=0;
  for(int i=0;i<MAXFE;i++){
    m_mon->Occupancy[i]=0;
    m_mon->Timeout[i]=0;
    m_mon->TooManyHeaders[i]=0;
    m_mon->SkippedTriggers[i]=0;
    m_mon->BadHeaders[i]=0;
    m_mon->MissingHeaders[i]=0;
    m_mon->DataNoHeader[i]=0;
    m_mon->Desynched[i]=0;
    m_mon->EcrResets[i]=0;
  }
  for(int i=0;i<3;i++)m_mon->TtcClash[i]=0;
}

void IsMonitoring::SetOccupancy(int fe, float occ){
  if(fe<MAXFE)m_mon->Occupancy[fe]=occ;
}
void IsMonitoring::SetAverageOccupancy(float occ){
  m_mon->Occ_Average=occ;
}
void IsMonitoring::SetNumberOfEvents(unsigned nevt){
  m_mon->NEvt=nevt;
}
void IsMonitoring::SetNMissed(unsigned nevt){
  m_mon->Mon_Missed=nevt;
}
void IsMonitoring::SetDisabledMask(unsigned mask){
  m_mon->Disabled=mask;
}
void IsMonitoring::SetDisabledInRunMask(unsigned mask){
  m_mon->DisabledRun=mask;
}
void IsMonitoring::SetErrorCounter(int fe, unsigned nevt, FWRegisters::EFBCOUNTER counter){
  if(fe<MAXFE){
    if(counter==FWRegisters::TIMEOUT)m_mon->Timeout[fe]=nevt;
    else if(counter==FWRegisters::TOOMANYHEADERS)m_mon->TooManyHeaders[fe]=nevt;
    else if(counter==FWRegisters::SKIPPEDTRIGGERS)m_mon->SkippedTriggers[fe]=nevt;
    else if(counter==FWRegisters::BADHEADERS)m_mon->BadHeaders[fe]=nevt;
    else if(counter==FWRegisters::MISSINGTRIGGERS)m_mon->MissingHeaders[fe]=nevt;
    else if(counter==FWRegisters::DATANOHEADER)m_mon->DataNoHeader[fe]=nevt;
    else if(counter==FWRegisters::DESYNCHED)m_mon->Desynched[fe]=nevt;
    else if(counter==FWRegisters::ECRRESET)m_mon->EcrResets[fe]=nevt;
  }
}
unsigned IsMonitoring::GetErrorCounter(int fe, FWRegisters::EFBCOUNTER counter){
  if(fe<MAXFE){
    if(counter==FWRegisters::TIMEOUT)return m_mon->Timeout[fe];
    else if(counter==FWRegisters::TOOMANYHEADERS)return m_mon->TooManyHeaders[fe];
    else if(counter==FWRegisters::SKIPPEDTRIGGERS)return m_mon->SkippedTriggers[fe];
    else if(counter==FWRegisters::BADHEADERS)return m_mon->BadHeaders[fe];
    else if(counter==FWRegisters::MISSINGTRIGGERS)return m_mon->MissingHeaders[fe];
    else if(counter==FWRegisters::DATANOHEADER)return m_mon->DataNoHeader[fe];
    else if(counter==FWRegisters::DESYNCHED)return m_mon->Desynched[fe];
    else if(counter==FWRegisters::ECRRESET)return m_mon->EcrResets[fe];
  }
  return 0;
}
void IsMonitoring::SetTtcClashCounter(unsigned nevt, FWRegisters::CLASH counter){
  m_mon->TtcClash[(int)counter]=nevt;
}
