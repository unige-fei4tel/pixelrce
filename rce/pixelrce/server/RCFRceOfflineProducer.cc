#include "server/RceOfflineProducer.hh"
#include "server/RCFController.hh"
#include "util/HistoManager.hh"
#include <boost/program_options.hpp>
#include <netdb.h>


int main ( int argc, char ** argv )
{
  int rce;
  std::string hostname;
  std::string orbhost;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("rce,r", boost::program_options::value<int>(&rce)->required(), "RCE to connect to")
    ("interface,i", boost::program_options::value<std::string>(&orbhost)->required(), "IP address of the host interface that the RCE is connected to")
    ("runcontrol,d", boost::program_options::value<std::string>(&hostname)->required(), "Runcontrol hostname");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  try{
    boost::program_options::notify(vm);
  }catch(boost::program_options::error& e){
    std::cout<<std::endl<<"ERROR: "<<e.what()<<std::endl<<std::endl;
    std::cout<<desc<<std::endl;
    exit(0);
  }

  setenv("ORBHOST", orbhost.c_str(), 1);
  std::string rchost;
  hostent * ipAddrContainer = gethostbyname(hostname.c_str());
  if (ipAddrContainer != 0) {
    int nBytes;
    if (ipAddrContainer->h_addrtype == AF_INET) nBytes = 4;
    else if (ipAddrContainer->h_addrtype == AF_INET6) nBytes = 6;
    else {
      std::cout << "Unrecognized IP address type. Run not started." 
		<< std::endl;
      exit(0);
    }
    std::stringstream ss("tcp://");
    ss << "tcp://"; 
    for (int i = 0, curVal; i < nBytes; i++)
      {
	curVal = static_cast<int>(ipAddrContainer->h_addr[i]);
	if (curVal < 0) curVal += 256;
	ss << curVal;
	if (i != nBytes - 1) ss << ".";
      }
    ss<<":44000";
    rchost=ss.str();
  }else{
    std::cout<<"Bad IP address. Exiting."<<std::endl;
    exit(0);
  }
  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  new HistoManager(0);
  RceOfflineProducer producer("RceOfflineProducer", rchost.c_str(), &controller, rce);
  sleep(100000000);
}
