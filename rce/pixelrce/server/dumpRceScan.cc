#include <iostream>
#include <boost/regex.hpp>
#include <sys/stat.h> 
#include <string>
#include "PixScan.hh"
#include "ScanOptions.hh"
#include "PixScanBase.h"
#include "PixEnumBase.h"
#include <sstream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "config/ConfigIF.hh"
#include "scanctrl/Scan.hh"
#include "dataproc/DataProcFactory.hh"
#include "scanctrl/ActionFactory.hh"
#include "scanctrl/LoopFactory.hh"
#include "scanctrl/NestedLoop.hh"
#include <boost/program_options.hpp>
int configureScan(boost::property_tree::ptree *scanOptions){
  //std::cout<<"Start ScanRoot configureScan"<<std::endl;
  int retval=0;
  Scan * m_scan=new Scan();
  //print out configuration
  //write_info(std::cout,*scanOptions);

  // set up pixel modules
  //  retval+=m_configIF->configureScan(scanOptions);
  // set up data processor
  printf("configIF configured\n");
  DataProcFactory dfac;
 ConfigIF* m_configIF=0;
 AbsDataProc* m_dataProc=0;
 AbsFormatter *m_formatter=0;
  AbsDataHandler* m_handler=0;
    AbsReceiver* m_receiver=0;
 NestedLoop m_loops;
  try{ //catch bad scan option parameters
    //    delete m_dataProc;
    printf("Deleted old dataproc\n");
    m_dataProc=0;
    std::string dataProc = scanOptions->get<std::string>("DataProc");
    //   m_dataProc=dfac.createDataProcessor(dataProc.c_str(),m_configIF, scanOptions);
    printf("Created dataproc of type %s\n", dataProc.c_str());
    std::string ptype = scanOptions->get<std::string>("DataHandler");
    delete m_handler;
    m_handler=0;
    //  m_handler=dfac.createDataHandler(ptype.c_str(),m_dataProc, m_scan->getDataCond(), m_configIF, scanOptions);
    printf("Created data handler of type %s\n", ptype.c_str());
    std::string rtype = scanOptions->get<std::string>("Receiver");
    delete m_receiver;
    m_receiver=0;
    //    m_receiver=dfac.createReceiver(rtype.c_str(),m_handler, scanOptions);
    printf("Created receiver of type %s\n", rtype.c_str());
    //    assert(m_handler!=0);

  // set up Scan Loops
    // The action factory contains all available loop and end-of-loop actions.
    // These can depend on m_config, m_dataProc, m_scan. Therefore created here.
    ActionFactory af(m_configIF, m_dataProc, m_scan);
    //  m_scan->configureScan(scanOptions, af, m_handler);
    LoopFactory lf;
    std::string type = scanOptions->get<std::string>("scanType");
    std::cout<<"Scan Type "<<type<<std::endl;
    LoopSetup* setup=lf.createLoopSetup(type.c_str());
    //m_loops gets configured
    setup->setupLoops(m_loops,scanOptions, &af);
  m_loops.print();
    std::cout<<"Setup done"<<std::endl;
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  //  assert(m_dataProc!=0);
  delete m_scan;
  return retval;
}




void scanconf(const ipc::ScanOptions &options) {
  //PixScan::dump(options);
  //std::cout<<"Start IPCScanRoot "<<std::endl;
  //convert struct into property tree
  boost::property_tree::ptree *pt=new boost::property_tree::ptree;
  //Top level 
  pt->put("Name",options.name); // scan name. Can be used as a file name, for example.
  pt->put("scanType",options.scanType); // scan type. Determines what loop setup to use
  pt->put("Receiver",options.receiver); 
  pt->put("DataHandler",options.dataHandler);
  pt->put("DataProc",options.dataProc);
  pt->put("Timeout.Seconds",options.timeout.seconds);
  pt->put("Timeout.Nanoseconds",options.timeout.nanoseconds);
  pt->put("Timeout.AllowedTimeouts",options.timeout.allowedTimeouts);
  pt->put("runNumber", options.runNumber);
  pt->put("stagingMode",options.stagingMode);
  //maskStages is not used in NewDsp, only nMaskStages
  pt->put("maskStages", options.maskStages);
  pt->put("nMaskStages",options.nMaskStages);
  pt->put("firstStage",options.firstStage);
  pt->put("stepStage",options.stepStage);
  pt->put("nLoops",options.nLoops);
  //Loops
  for(int i=0;i<options.nLoops;i++){
    char loopname[128];
    sprintf (loopname,"scanLoop_%d.",i);
    pt->put(std::string(loopname)+"scanParameter", options.scanLoop[i].scanParameter);
    pt->put(std::string(loopname)+"nPoints",options.scanLoop[i].nPoints);
    pt->put(std::string(loopname)+"endofLoopAction.Action", options.scanLoop[i].endofLoopAction.Action);
    pt->put(std::string(loopname)+"endofLoopAction.fitFunction", options.scanLoop[i].endofLoopAction.fitFunction);
    pt->put(std::string(loopname)+"endofLoopAction.targetThreshold", options.scanLoop[i].endofLoopAction.targetThreshold);
    if(options.scanLoop[i].dataPoints.size()>0){
      char pointname[10];
      for(unsigned int j=0;j<options.scanLoop[i].dataPoints.size();j++){
        sprintf(pointname,"P_%d",j);
        pt->put(std::string(loopname)+"dataPoints."+pointname,options.scanLoop[i].dataPoints[j]);
      }
    }
  }
  //Trigger options
  pt->put("trigOpt.nEvents",options.trigOpt.nEvents);
  pt->put("trigOpt.triggerMask",options.trigOpt.triggerMask);
  pt->put("trigOpt.moduleTrgMask",options.trigOpt.moduleTrgMask);
  pt->put("trigOpt.triggerDataOn",options.trigOpt.triggerDataOn);
  pt->put("trigOpt.deadtime",options.trigOpt.deadtime);
  pt->put("trigOpt.nL1AperEvent",options.trigOpt.nL1AperEvent);
  pt->put("trigOpt.nTriggersPerGroup",options.trigOpt.nTriggersPerGroup);
  pt->put("trigOpt.nTriggers",options.trigOpt.nTriggers);
  pt->put("trigOpt.Lvl1_Latency",options.trigOpt.Lvl1_Latency);
  pt->put("trigOpt.Lvl1_Latency_Secondary",options.trigOpt.Lvl1_Latency_Secondary);
  pt->put("trigOpt.strobeDuration",options.trigOpt.strobeDuration);
  pt->put("trigOpt.strobeMCCDelay",options.trigOpt.strobeMCCDelay);
  pt->put("trigOpt.strobeMCCDelayRange",options.trigOpt.strobeMCCDelayRange);
  pt->put("trigOpt.CalL1ADelay",options.trigOpt.CalL1ADelay);
  pt->put("trigOpt.eventInterval",options.trigOpt.eventInterval);
  pt->put("trigOpt.injectForTrigger",options.trigOpt.injectForTrigger);
  pt->put("trigOpt.vcal_charge",options.trigOpt.vcal_charge);
  pt->put("trigOpt.threshold",options.trigOpt.threshold);
  pt->put("trigOpt.hitbusConfig",options.trigOpt.hitbusConfig);

  // each option gets an entry in the tree below optionsMask with value 1
  for (unsigned int i=0;i<options.trigOpt.optionsMask.size();i++){
    pt->put(std::string("trigOpt.optionsMask.")+std::string(options.trigOpt.optionsMask[i]),1);
  }
  pt->put("trigOpt.triggerMode", options.trigOpt.triggerMode);

  // front-end options
  pt->put("feOpt.phi",options.feOpt.phi);
  pt->put("feOpt.totThresholdMode",options.feOpt.totThresholdMode);
  pt->put("feOpt.totMinimum",options.feOpt.totMinimum);
  pt->put("feOpt.totTwalk",options.feOpt.totTwalk);
  pt->put("feOpt.totLeMode",options.feOpt.totLeMode);
  pt->put("feOpt.hitbus",options.feOpt.hitbus);

  pt->put("nHistoNames",options.histos.size()); 
  if(options.histos.size()>0){
    char pointname[10];
    for(unsigned int j=0;j<options.histos.size();j++){
      sprintf(pointname,"Name_%d",j);
      pt->put(std::string("HistoNames.")+pointname,options.histos[j]);
    }
  }
  //Trigger options
  //call the real initScan function
  //std::cout<<"Calling ScanRoot "<<std::endl;
  configureScan(pt);

  std::stringstream ss;
  boost::property_tree::json_parser::write_json_internal(ss,*pt,"  ",true);
  std::string s = ss.str();
  std::cout <<s <<std::endl;
	 
  
  delete pt;
}

int main(int argc,char *argv[])
{
 
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("help,h", "produce help message")
    ("list,l", "list defined scans")
    ("name,n", boost::program_options::value<std::string>(), "name of scan.")
    ("flavor,f", boost::program_options::value<std::string>(), "module flavor to use.");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  const char *name=0;
  const char *flavor=0;
  int scanindex=-1;
  if(vm.count("name"))name=vm["name"].as<std::string>().c_str();
  if(vm.count("flavor"))flavor=vm["flavor"].as<std::string>().c_str();
  bool list=false;
  if(vm.count("list")) list=true;


  PixLib::EnumFEflavour::FEflavour f; 
  f  =  PixLib::EnumFEflavour::PM_FE_I4A;
  if(!list) {
    if(!strcmp(flavor,"PM_FE_I4A"))  f =  PixLib::EnumFEflavour::PM_FE_I4A; 
    else if(!strcmp(flavor,"PM_FE_I4B"))  f  =  PixLib::EnumFEflavour::PM_FE_I4B  ;
    else if(!strcmp(flavor,"PM_FE_I2"))  f  =  PixLib::EnumFEflavour::PM_FE_I2  ;
    else if(!list) {std::cout << "unkown flavor " << flavor << std::endl;return 1;}
  }
  PixLib::EnumScanType scantype;
  std::map<std::string, int> scannames=scantype.EnumScanTypeMap();
  for (std::map <std::string, int>::const_iterator it = scannames.begin(); it != scannames.end(); ++it){
    std::string scanname=it->first;
    scanindex=it->second;
    if(name) if(std::string((const char*)name)==scanname) break;
    if(list)  std::cout << scanname << std::endl;
  }
  if(list) return 0;
  std::cout <<"Flavor: " <<flavor << std::endl;
  std::cout <<"ScanName: " <<name << std::endl; 
  std::cout <<"ScanIndex: "<< scanindex<< std::endl; 
 ipc::ScanOptions scanopts;
 RCE::PixScan scan((RCE::PixScan::ScanType) scanindex, f);
 // if(scan.presetRCE((RCE::PixScan::ScanType) scanindex, f)==true) {
   
   scan.convertScanConfig(scanopts);
   scan.dump(std::cout,scanopts);	      
   scan.toJSON(scanopts);
   //}
 
 scanconf(scanopts);



  return 0;


}
