
#include "server/RCFHistoController.hh"
#include "RCFScanRootAdapter.hh"
#include "util/RceName.hh"
#include <boost/regex.hpp>
#include "TDirectory.h"
#include "TROOT.h"
#include "TKey.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH2C.h"
#include "TH2S.h"

#include <iostream>

RCFHistoController::RCFHistoController(){
  m_server=new RCF::RcfServer(RCF::TcpEndpoint("0.0.0.0", RceName::SUBPORT));
  m_server->start();
}
RCFHistoController::~RCFHistoController(){
  clear();
  m_server->stop();
}
void RCFHistoController::addRce(int rce){
  for(size_t i=0;i<m_rces.size();i++)if(m_rces[i]==rce)return; //RCE has already been added.
  m_rces.push_back(rce);
  RCF::SubscriptionParms subParams;
  char rcename[32];
  sprintf(rcename, RCFHOST"%d", rce);
  subParams.setPublisherEndpoint(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
  subParams.setOnSubscriptionDisconnect(&RCFHistoController::onSubDisconnect);
  m_subscriptions.push_back(m_server->createSubscription<I_RCFPublisher>(*this, subParams));
  std::cout<<"Created subscription"<<std::endl;
}
void RCFHistoController::removeAllRces(){
  for(size_t i=0;i<m_subscriptions.size();i++)m_subscriptions[i]->close();
  m_subscriptions.clear();
  m_rces.clear();
}

std::vector<TH1*> RCFHistoController::getHistos(const char* reg){
  int n=getPublishedHistoNames().size();
  while(m_histos.size()!=n){
    std::cout<<"Got "<<m_histos.size()<<" histograms. Expecting "<<n<<"."<<std::endl;
    std::cout<<"Waiting for histograms to appear."<<std::endl;
    usleep(100000);
  }
  std::vector<TH1*> retvec;
  boost::regex re(reg);
  for(int i=0;i<m_histos.size();i++){
    boost::cmatch matches;
    std::string histname=m_histos[i]->GetName();
    std::string tn=histname.substr(0,histname.size()-1);
    if(boost::regex_search(tn.c_str(), matches, re)){
      TH1* hist=(TH1*)m_histos[i]->Clone(tn.c_str());
      hist->SetDirectory(0);
      retvec.push_back(hist);
    }
  } 
  return retvec;
}

std::vector<std::string> RCFHistoController::getHistoNames(const char* reg){
  std::vector<std::string> retvect;
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, RCFHOST"%d", m_rces[i]);
    std::vector<std::string> stringvect;
    try {
      RcfClient<I_RCFScanRootAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      stringvect=client.RCFgetHistoNames(reg);
      retvect.insert(retvect.end(), stringvect.begin(), stringvect.end());
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
  return retvect;
}

std::vector<std::string> RCFHistoController::getPublishedHistoNames(){
  std::vector<std::string> retvect;
  for(size_t i=0;i<m_rces.size();i++){
    char rcename[32];
    sprintf(rcename, RCFHOST"%d", m_rces[i]);
    std::vector<std::string> stringvect;
    try {
      RcfClient<I_RCFScanRootAdapter> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
      stringvect=client.RCFgetPublishedHistoNames();
      retvect.insert(retvect.end(), stringvect.begin(), stringvect.end());
    }
    catch(const RCF::Exception & ex) {
      std::cerr<<"RCF error: "<<ex.getErrorString()<<std::endl;
    }
  }
  return retvect;
}

bool RCFHistoController::clear()
{
  for(size_t i=0;i<m_histos.size();i++)delete m_histos[i];
  m_histos.clear();
  return 1;
}

void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, int, int){
  TH1I *histo=new TH1I((name+"R").c_str(), title.c_str(), xaxis.bincount(), 
		      xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width());
  int* p=(int*)bins.getPtr();
  int* ep=(int*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    histo->SetBinContent(i+1, p[i]);
    if(!errors.isEmpty()) histo->SetBinError(i+1, ep[i]);
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}

void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, float, float){
  TH1F *histo=new TH1F((name+"R").c_str(), title.c_str(), xaxis.bincount(), 
		       xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width());
  float* p=(float*)bins.getPtr();
  float* ep=(float*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    histo->SetBinContent(i+1, p[i]);
    if(!errors.isEmpty()) histo->SetBinError(i+1, ep[i]);
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}

void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, short, short){
  TH1S *histo=new TH1S((name+"R").c_str(), title.c_str(), xaxis.bincount(), 
		       xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width());
  short* p=(short*)bins.getPtr();
  short* ep=(short*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    histo->SetBinContent(i+1, p[i]);
    if(!errors.isEmpty()) histo->SetBinError(i+1, ep[i]);
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}

void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCFRceHistoAxis yaxis, 
				      RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, int, int){
  TH2I *histo=new TH2I((name+"R").c_str(), title.c_str(), 
		       xaxis.bincount(), xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width(),
		       yaxis.bincount(), yaxis.low(), yaxis.low()+yaxis.bincount()*yaxis.width() );
  int* p=(int*)bins.getPtr();
  int* pe=(int*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    for(int j=0;j<yaxis.bincount();j++){
      histo->SetBinContent(i+1,j+1, p[j*xaxis.bincount()+i]);
      if(!errors.isEmpty()) histo->SetBinError(i+1, j+1, pe[j*xaxis.bincount()+i]);
    }
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}
void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCFRceHistoAxis yaxis, 
				      RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, char, char){
  TH2C *histo=new TH2C((name+"R").c_str(), title.c_str(), 
		       xaxis.bincount(), xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width(),
		       yaxis.bincount(), yaxis.low(), yaxis.low()+yaxis.bincount()*yaxis.width() );
  char* p=(char*)bins.getPtr();
  char* pe=(char*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    for(int j=0;j<yaxis.bincount();j++){
      histo->SetBinContent(i+1,j+1, p[j*xaxis.bincount()+i]);
      if(!errors.isEmpty()) histo->SetBinError(i+1, j+1, pe[j*xaxis.bincount()+i]);
    }
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}
void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCFRceHistoAxis yaxis, 
				      RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, short, short){
  TH2S *histo=new TH2S((name+"R").c_str(), title.c_str(), 
		       xaxis.bincount(), xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width(),
		       yaxis.bincount(), yaxis.low(), yaxis.low()+yaxis.bincount()*yaxis.width() );
  short* p=(short*)bins.getPtr();
  short* pe=(short*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    for(int j=0;j<yaxis.bincount();j++){
      histo->SetBinContent(i+1,j+1, p[j*xaxis.bincount()+i]);
      if(!errors.isEmpty()) histo->SetBinError(i+1, j+1, pe[j*xaxis.bincount()+i]);
    }
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}
void RCFHistoController::publishHisto(std::string name, std::string title, 
				      RCFRceHistoAxis xaxis, RCFRceHistoAxis yaxis, 
				      RCF::ByteBuffer bins, 
				      RCF::ByteBuffer errors, float, float){
    TH2F *histo=new TH2F((name+"R").c_str(), title.c_str(), 
		      xaxis.bincount(), xaxis.low(), xaxis.low()+xaxis.bincount()*xaxis.width(),
		      yaxis.bincount(), yaxis.low(), yaxis.low()+yaxis.bincount()*yaxis.width() );
  float* p=(float*)bins.getPtr();
  float* pe=(float*)errors.getPtr();
  for(int i=0;i<xaxis.bincount();i++){
    for(int j=0;j<yaxis.bincount();j++){
      histo->SetBinContent(i+1,j+1, p[j*xaxis.bincount()+i]);
      if(!errors.isEmpty()) histo->SetBinError(i+1, j+1, pe[j*xaxis.bincount()+i]);
    }
  }
  histo->SetDirectory(0);
  m_histos.push_back(histo);
}

void RCFHistoController::onSubDisconnect(RCF::RcfSession & session){
  std::cout<<"Subscription was disconnected"<<std::endl;
}
