#include "server/PrimListGui.hh"
#include "server/PrimList.hh"
#include "server/PixScan.hh"
#include "server/ConfigGui.hh"
#include "ScanOptions.hh"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <algorithm>
using namespace RCE;

PrimListGui::PrimListGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options):

  TGVerticalFrame(p,w,h,options), m_pList(new PrimList)
{
 
  m_name=new TGLabel(this,name);
  AddFrame(m_name,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 10, 0));
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_name->SetTextFont(labelfont);

  TGHorizontalFrame *btnFrame = new TGHorizontalFrame(this, 2,2 );
  AddFrame(btnFrame,new TGLayoutHints(kLHintsExpandX ));
  TGTextButton* loadBtn =new TGTextButton(btnFrame,"Load Primlist");
  loadBtn->SetFont(labelfont);
  loadBtn->SetMargins(5,10,5,5);
  loadBtn->Connect("Clicked()", "PrimListGui", this,"load()");
  btnFrame->AddFrame(loadBtn,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));

  TGTextButton* clearBtn =new TGTextButton(btnFrame,"Clear Primlist");
  clearBtn->SetFont(labelfont);
  clearBtn->SetMargins(5,10,5,5);
  clearBtn->Connect("Clicked()", "PrimListGui", this,"clear()");
  btnFrame->AddFrame(clearBtn,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  plStatusLabel = new TGLabel(btnFrame, "                                                                          ");
  FontStruct_t statusfont= gClient->GetFontByName("-adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-iso8859-1");
  plStatusLabel->SetTextFont(statusfont);
  btnFrame->AddFrame(plStatusLabel, new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 20, 2, 2, 0));
  
}
  



PrimListGui::~PrimListGui(){
  Cleanup();
}

void PrimListGui::updateText(TGLabel* label, const char* newtext){
  //unsigned len=strlen(label->GetText()->GetString());
  label->SetText(newtext);
  //if (strlen(newtext)>len)Layout();
  Layout();
}

void PrimListGui::disableControls(){

}
void PrimListGui::enableControls(){

}
  
void PrimListGui::printFromGui(){
 // print(std::cout);
}

void PrimListGui::print(std::ostream &os){

}

RCE::PrimList* PrimListGui::getPrimList(){
  return m_pList;
}
int PrimListGui::loadPrimList(const char* fPath)//Name,Flavor,Threshold,Charge,ToT
{std::cout << "LOADING" << std::endl;
        int loaded=0;
	std::ifstream file;
	file.open(fPath);
	 if (file.is_open())
  	{
		std::string line; 
		int i = 1;
 		while (file.good() )
   		 {
   		   std::getline(file,line);
		  if(line.compare("")== 0)
			break;
  		   int err = parseScan(line);
		   if (err == 0)
			{
				loaded++;
			}
	        	else
			{			
				//m_pList=new PrimList();
			        std::string failSt = "Failed to load PrimList on item " + i;
				updateStatus(failSt);		
				std::cout << "NULL SCAN" << line << " error "<<err<<std::endl;
				clear();
				return 0;
			}
		
    		}
		
    		file.close();
  	}
	//check to make sure scannames are good (DEBUG LATER)
/*
	std::vector<std::string>::iterator itr;
	for ( itr = m_scannames.begin(); itr != m_scannames.end(); ++itr )
	{
		int str = m_scantypes.find(*itr)->second;
		std::cout << "NAME " << *itr << str << std::endl;
		//std::map <std::string, int>::const_iterator itr2;
		if (m_scantypes.find(*itr) == m_scantypes.end())
		{
			std::string failSt = "Bad scan name: ";// + str + "!";
			updateStatus(failSt);
			m_pList = new PrimList();
			return -1;
		}
	}*/
	return loaded;
        
}

void PrimListGui::setScanTypes (std::map<int, std::string>  pscantypes)
{
	std::map<std::string,int> revMap;
  	std::map<int,std::string>::iterator it;
	for ( it=pscantypes.begin() ; it != pscantypes.end(); it++ )
	{
		std::transform((*it).second.begin(), (*it).second.end(), (*it).second.begin(), ::toupper);	
		revMap.insert(std::pair<std::string, int>((*it).second, (*it).first));
	}
	m_scantypes = revMap;
	
}

void PrimListGui::setFlavor(std::string pFlavor)
{
	flavor = pFlavor;
}
int PrimListGui::parseScan(std::string line)//Name,Flavor,Options
{
	PixScan* scan;	
	std::string scnName, scnFlavour, tmp;
	std::istringstream buf(line);
	getline(buf, scnName, ',');
	getline(buf, scnFlavour, ',');
	//to uppercase for user error
	std::transform(scnName.begin(), scnName.end(), scnName.begin(), ::toupper);
	std::transform(scnFlavour.begin(), scnFlavour.end(), scnFlavour.begin(), ::toupper);
     	//remove accidental spaces
	std::string::iterator end_pos = std::remove(line.begin(), line.end(), ' ');
	line.erase(end_pos, line.end());
	if (scnName == NULL ||  scnName == "" || scnFlavour == NULL || scnFlavour == "" )
	{
	  return 1; //error
	}
	PixLib::EnumFEflavour::FEflavour fl=PixLib::EnumFEflavour::PM_FE_I4A;
 	if(scnFlavour=="FEI3")fl=PixLib::EnumFEflavour::PM_FE_I2;
 	else if (scnFlavour=="FEI4A")fl=PixLib::EnumFEflavour::PM_FE_I4A;
  	else if (scnFlavour=="FEI4B")fl=PixLib::EnumFEflavour::PM_FE_I4B;
	if(m_scantypes.find(scnName)==m_scantypes.end())return 2; //error
	scan = new PixScan((PixScan::ScanType) m_scantypes[scnName],fl);
	std::string preScript="";
	std::string postScript="";
	std::string topConfig="";
	bool updateConfig=true;
	std::vector<std::string> enables;
	std::vector<std::string> disables;
	int pause=0;
	//Look for optional scan parameters
	std::string optPar;
	while (getline(buf, optPar, ','))
	{
		std::string paramName, paramVal;
		std::istringstream buf2 (optPar);
		getline(buf2, paramName, '=');
		getline(buf2, paramVal);
		int err=setScanParam(paramName, paramVal, scan, preScript, postScript, enables, disables, 
				     updateConfig, topConfig, pause);
		if(err!=0){
		  delete scan;
		  return 3;
		}
	}
	m_pList->addScan(scnName, scan, preScript, postScript, enables, disables, updateConfig, topConfig, pause);
	

	return 0;
	
}
int PrimListGui::toInt(std::string str)
{
	int result;
	std::stringstream(str) >> result;
	return result;
}

void PrimListGui::load()
{

  TGFileInfo fileinfo;
  new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
  if(!fileinfo.fFilename){
    printf("Scheisse\n");
    return;
  }
 int load = loadPrimList(fileinfo.fFilename);
 std::stringstream buf;
 if(load != 0 && isLoaded()) {
   buf << "Loaded "<< load << " items from " << fileinfo.fFilename<<". "<<getNumScans()<<" items total.";
   updateStatus(buf.str());
 }
 currentScan=0;
}
void PrimListGui::clear()
{
  currentScan=0;
  m_pList->clearScans();
  std::stringstream buf;
  buf << "No primlist loaded.";
  updateStatus(buf.str());
}
int PrimListGui::setScanParam(std::string name, std::string value, PixScan* pScan, std::string &preScript, 
			      std::string &postScript, std::vector<std::string>  &enabled, std::vector<std::string> &disabled, 
			      bool &updateConfig, std::string &topConfig, int &pause)
{
	//convert name to uppercase to avoid any case issue
		
	std::transform(name.begin(), name.end(), name.begin(), ::toupper);
	if(name.compare("TOPCONFIG")==0 )
	{
		topConfig=value;
		return 0;		
	}
	if(name.compare("SCRIPT")==0 || name.compare("PRESCRIPT")==0 )//Don't want to change case!
	{
		preScript=value;
		return 0;		
	}
        if(name.compare("POSTSCRIPT")==0)
	{
               postScript=value;
	       return 0;
	}
	std::transform(value.begin(), value.end(), value.begin(), ::toupper);
	if(name.compare("UPDATECONFIG")==0)
	{
		updateConfig=toBool(value);
	}
	else if(name.compare("PAUSE")==0)
	{
		pause=toInt(value);
	}
	else if(name.compare("MODSCANCONCURRENT")==0)
	{
		pScan->setModScanConcurrent(toBool(value));
	}	
	else if(name.compare("MASKSTAGESTEPS")==0)
	{
		pScan->setMaskStageSteps(toInt(value));
	}
	else if (name.compare("TUNINGSTARTFROMCONFIGVALUE")==0)
	{	 
		pScan->tuningStartsFromCfgValues(toBool(value));
	}
	else if (name.compare("REPETITIONS")==0)
	{	 
		pScan->setRepetitions(toInt(value));
	}
	else if (name.compare("SELFTRIGGER")==0)
	{	 
		pScan->setSelfTrigger(toBool(value)) ;
	}
	else if (name.compare("STROBELVL1DELAYOVERRIDE")==0)
	{	 
		pScan->setStrobeLVL1DelayOveride(toBool(value)) ;
	}
	else if (name.compare("STROBELVL1DELAY")==0)
	{	 
		pScan->setStrobeLVL1Delay(toInt(value)) ;
	}
	else if (name.compare("LVL1LATENCY")==0)
	{	 
		pScan->setLVL1Latency(toInt(value)) ;
	}
	else if (name.compare("STROBEMCCDELAY")==0)
	{	 
		pScan->setStrobeMCCDelay(toInt(value)) ;
	}
	else if (name.compare("STROBEMCCDELAYRANGE")==0)
	{	 
		pScan->setStrobeMCCDelayRange(toInt(value)) ;
	}
	else if (name.compare("STROBEDURATION")==0)
	{	 
		pScan->setStrobeDuration(toInt(value)) ;
	}
	else if (name.compare("CLEARMASKS")==0)
	{	 
		pScan->setClearMasks(toInt(value)) ;
	}
	else if (name.compare("MODULEMASK")==0)//format MODULEMASK=Group;Mask
	{	 
		std::vector <std::string> values = toVector(value);
		int group = toInt(values[0]);
		int mask = toInt(values[1]);		
		pScan->setModuleMask(group,mask);
	}
	else if (name.compare("CONFIGENABLED")==0)//format Group;Enabled
	{	 
		std::vector <std::string> values = toVector(value);
		int group = toInt(values[0]);
		bool ena = toBool(values[1]);		
		pScan->setConfigEnabled(group,ena);
	}
	else if (name.compare("TRIGGERENABLED")==0)//format Group;Enabled
	{	 
		std::vector <std::string> values = toVector(value);
		int group = toInt(values[0]);
		bool ena = toBool(values[1]);		
		pScan->setTriggerEnabled(group,ena);
	}
	else if (name.compare("STROBEENABLED")==0)//format Group;Enabled
	{	 
		std::vector <std::string> values = toVector(value);
		int group = toInt(values[0]);
		bool ena = toBool(values[1]);		
		pScan->setStrobeEnabled(group,ena);
	}
	else if (name.compare("MODULEMASK")==0)
	{	 
		pScan->setStrobeMCCDelayRange(toInt(value)) ;
	}
	else if (name.compare("FEVCAL")==0)
	{	 
		pScan->setFeVCal(toInt(value)) ;
	}
	else if (name.compare("THRESHOLDTARGETVALUE")==0)
	{
		if(toInt(value)>0)//If value is 0, should just use default		
		pScan->setThresholdTargetValue(toInt(value));
	}
	else if (name.compare("TOTTARGETCHARGE")==0)
	{
		if(toInt(value)>0)//If value is 0, should just use default		
		pScan->setTotTargetCharge(toInt(value));
	}
	else if (name.compare("TOTTARGETVALUE")==0)
	{
		if(toInt(value)>0)//If value is 0, should just use default		
			pScan->setTotTargetValue(toInt(value));
	}
	else if (name.compare("STEPSTAGE")==0)
	{
		pScan->setStepStage(toInt(value));
	}
	else if (name.compare("SETUPTHRESHOLD")==0)
	{
		pScan->setSetupThreshold(toBool(value));
	}
	else if (name.compare("THRESHOLD")==0)
	{
		pScan->setThreshold(toInt(value));
	}
	else if (name.compare("TIMEOUTSECONDS")==0)
	{
		pScan->setTimeoutSeconds(toInt(value));
	}
	else if (name.compare("LVL1LATENCY")==0)
	{
		pScan->setLVL1Latency(toInt(value));
	}
	else if (name.compare("STROBEDURATION")==0)
	{
		pScan->setStrobeDuration(toInt(value));
	}

	else if (name.compare("MODCONFIGTYPE")==0)
	{
		PixLib::EnumModConfigType enumObj;		
		std::map<std::string, int> enummap = enumObj.EnumModConfigTypeMap();
		pScan->setModConfig((PixLib::EnumModConfigType::ModConfigType)enummap[value]);
	}
	else if (name.compare("MASKSTAGEMODE")==0)
	{
		PixLib::EnumMaskStageMode enumObj;
		std::map<std::string, int> enummap = enumObj.EnumMaskStageModeMap();
		pScan->setMaskStageMode((PixLib::EnumMaskStageMode::MaskStageMode)enummap[value]);
	
	}
	else if (name.compare("MASKSTAGETOTALSTEPS")==0)
	{
		PixLib::EnumMaskSteps enumObj;		
		std::map<std::string, int> enummap = enumObj.EnumMaskStageStepsMap();
		pScan->setMaskStageTotalSteps((PixLib::EnumMaskSteps::MaskStageSteps)enummap[value]);
	}
        else if (name.compare("MODULETRGMASK")==0)
	{
		pScan->setModuleTrgMask(toInt(value));
	}
	//Loop Variable Values
	else if (name.compare("LOOPVARVALUES")==0)
	{
		int index;
		std::string tmp;
		std::istringstream buf (value);
		getline(buf, tmp, ';');
		index = toInt(tmp);
		size_t begOfVector = value.find('[');
		if(begOfVector == std::string::npos)//then format index;startVal;endVal;nSteps
		{
			int nSteps;
			double startVal, endVal;
			getline(buf, tmp, ';');
			startVal=toDouble(tmp);
			getline(buf, tmp, ';');
			endVal=toDouble(tmp);
			getline(buf, tmp, ';');
			nSteps=toInt(tmp);
			pScan->setLoopVarValues(index, startVal, endVal, nSteps);
		}
		else //then format [val1;val2;val3;...]
		{
			std::vector <float> values;
			size_t endOfVector = value.find(']');
			value = value.substr(begOfVector+1, endOfVector-begOfVector-1); //trim off index[, ]
			new (&buf) std::istringstream (value);
			while (getline(buf, tmp, ';'))
			{
				values.push_back(toDouble(tmp));	
			}
			pScan->setLoopVarValues(index, values);

		}

	}
	else if (name.compare("ENABLEFES")==0 || name.compare("DISABLEFES")==0 )
	  {
	    value.erase (std::remove (value.begin(), value.end(), ' '), value.end()); 
	    std::string tmp;
	    std::istringstream buf (value);
	    std::vector <float> values;
	   while (getline(buf, tmp, ';'))
	      {
		if(name.compare("ENABLEFES")==0)enabled.push_back(tmp);
		else disabled.push_back(tmp);
	      }
	}
	else 
	{
	  std::string st = "Unrecognized scan parameter name: " + name;
	  updateStatus(st);
	  return 1;
	}
	return 0;
		
}
std::vector <std::string> PrimListGui::toVector(std::string value)
{
	std::vector <std::string> values;	
	std::istringstream buf (value);
	std::string tmp;
	while (getline(buf, tmp, ';'))
	{
		values.push_back(tmp);	
	}
	return values;
}
double PrimListGui::toDouble(std::string val)
{
	double result;
	std::stringstream(val) >> result;
	return result;
}  
bool PrimListGui::toBool(std::string val)
{
	bool result;
	std::stringstream(val) >> result;
	return result;
}

bool PrimListGui::isLoaded()
{
  return m_pList->nScans() > 0;
}
void PrimListGui::updateStatus(std::string str)
{
	updateText(plStatusLabel, str.c_str());
}
int PrimListGui::getNumScans()
{
  return m_pList->nScans();
}
std::string PrimListGui::getCurrScanName()
{
	return m_pList->getScanName(currentScan);

}
RCE::PixScan* PrimListGui::getCurrentScan()
{
	return getPrimList()->getScan(currentScan);
}
std::string PrimListGui::getCurrentPreScript()
{
	return m_pList->getPreScript(currentScan);
}
std::string PrimListGui::getCurrentPostScript()
{
	return m_pList->getPostScript(currentScan);
}
int PrimListGui::getCurrentPause()
{
        return m_pList->getPause(currentScan);
}
std::string PrimListGui::getCurrentTopConfig()
{
	return m_pList->getTopConfig(currentScan);
}
ipc::ScanOptions* PrimListGui::getCurrentScanConfig()
{
  return getPrimList()->getScanOptions(currentScan);
}
std::vector<std::string>& PrimListGui::getCurrentDisabledList(){
  return m_pList->getDisabledList(currentScan);
}
std::vector<std::string>& PrimListGui::getCurrentEnabledList(){
  return m_pList->getEnabledList(currentScan);
}
void PrimListGui::setRunNumber(int runnum){
  getCurrentScan()->setRunNumber(runnum);
  getCurrentScanConfig()->runNumber=runnum;
}

bool PrimListGui::getCurrentUpdateConfig()
{
	return m_pList->getUpdateConfig(currentScan);
}

void PrimListGui::changeIncludes(ConfigGui* cfg[]){
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(cfg[i]->isIncluded()){
      std::vector<std::string> dis=getCurrentDisabledList();
      for(size_t j=0;j<dis.size();j++){
	if (cfg[i]->getName()==dis[j]){
	  cfg[i]->setIncluded(false);
	  std::cout<<"Disabled FE "<<dis[j]<<std::endl;
	  break;
	}
      }
    }else if (cfg[i]->isValid()){
      std::vector<std::string> en=getCurrentEnabledList();
      for(size_t j=0;j<en.size();j++){
	if (cfg[i]->getName()==en[j]){
	  cfg[i]->setIncluded(true);
	  std::cout<<"Enabled FE "<<en[j]<<std::endl;
	  break;
	}
      }
    }
  }
}
    


  

