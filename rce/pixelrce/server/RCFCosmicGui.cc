
#include "server/CosmicGui.hh"
#include "server/RCFController.hh"
#include "util/HistoManager.hh"
#include "TApplication.h"
#include <TROOT.h>
#include <TStyle.h>
#include <boost/program_options.hpp>
#include <stdlib.h>

int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  bool start;
  std::string orbhost;
  boost::program_options::options_description desc("Allowed options");
  desc.add_options()
    ("interface,i", boost::program_options::value<std::string>(&orbhost)->required(), "IP address of the host interface that the RCE is connected to")
    ("start,s", boost::program_options::value<bool>(&start)->default_value(false), "Start run when GUI comes up")
      ("help,h", "produce help message");
  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);
  if(vm.count("help")){
    std::cout<<desc<<std::endl;
    exit(0);
  }
  try{
    boost::program_options::notify(vm);
  }catch(boost::program_options::error& e){
    std::cout<<std::endl<<"ERROR: "<<e.what()<<std::endl<<std::endl;
    std::cout<<desc<<std::endl;
    exit(0);
  }
  setenv("ORBHOST", orbhost.c_str(), 1);
  RCF::RcfInitDeinit rcfInit;
  RCFController controller;
  AbsController &acontroller(controller);
  new HistoManager(0);
  
  gROOT->SetStyle("Plain");
  TApplication theapp("app",&argc,argv);
  new CosmicGui(acontroller, start, gClient->GetRoot(),800, 735);
  theapp.Run();
  return 0;
}


