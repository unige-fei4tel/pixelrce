#ifndef IPCCALLBACK_HH
#define IPCCALLBACK_HH

#include "ipc/object.h"
#include "Callback.hh"
#include <boost/thread/mutex.hpp>

class IPCCallback : public IPCObject<POA_ipc::Callback>
{
public:
  IPCCallback(): m_rce(0){}
  void notify( const ipc::CallbackParams &msg )=0;
  void stopServer()=0;
  void addRce(){m_rce++;}
  void stop(){
    boost::mutex::scoped_lock pl( m_mutex );
    m_cond.notify_one();
  }
  void run(){
    boost::mutex::scoped_lock pl(m_mutex);
    m_cond.wait(pl);
  }
    
protected:
  int m_rce;
  boost::mutex m_mutex;
  boost::condition_variable m_cond;
};

#endif
