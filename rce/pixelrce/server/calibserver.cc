#include <stdio.h>
#include <unistd.h>

#include <ers/ers.h>

#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <ipc/server.h>

#include <owl/timer.h>
#include <owl/semaphore.h> 

#include <boost/regex.hpp>
#include <fstream>

#include "scanctrl/IPCScan.cc"
#include "scanctrl/IPCScanRoot.cc"
#include "config/IPCConfigIF.cc"
#include "HW/SerialHexdump.hh"
#include "HW/SerialPgp.hh"
#include "HW/SerialPgpBw.hh"
#include "HW/SerialPgpFei4.hh"
#include "config/IPCModuleFactory.hh"
#include "util/RceName.hh"
#include "util/IPCProvider.hh"
#include "util/HistoManager.hh"

#include "server/calibserver.hh"


#include <is/infoT.h>
#include <is/infodictionary.h>
#include <ipc/core.h>
#include <signal.h>

static IPCServer ipcServer;




//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////


void calibserver::run(bool ismodule, char* partition){

const char *cc_argv[] = 
{
	"calibserver",              /* always the name of the program */
	"-p",
	"rcetest",
        //"-ORBtraceLevel",       /* trace level */
	//"25",
	//"-ORBtraceInvocations",
	//"1",
	//"-ORBtraceTime",
	//"1",
	//"-ORBtraceThreadId",
	//"1",
	"-ORBgiopMaxMsgSize", 
	"33554422",       /* max message size 32 MB */
	"-ORBmaxServerThreadPerConnection", "1"
	// "-ORBendPoint",
	//" giop:tcp:192.168.1.35:"
};


 if(partition)cc_argv[2]=partition;
int cc_argc = sizeof( cc_argv ) / sizeof( cc_argv[ 0 ]  ); 
//   PROFILE_THREAD_SCOPED();
 
 
    try {
      IPCCore::init( cc_argc, (char**)cc_argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return;
    }
   
       // Declare command object and its argument-iterator
   IPCPartition p((const char*)cc_argv[2]);
   printf("Partition is %s\n",cc_argv[2]);
   char name[128];
   sprintf(name, "configIF_RCE%d", RceName::getRceNumber());
   p.isObjectValid<ipc::IPCConfigIFAdapter>(name);
   
   //Serial IF
   new SerialPgpFei4;

    // set big endianness for pgp 2
   unsigned value;
   asm volatile("mfdcr %0, %1" : "=r"(value) : "i"(0x2e0));
   value|=1<<13;
   asm volatile("mtdcr %0, %1" : : "i"(0x2e0), "r"(value));
   



   //Module Factory

   ModuleFactory *moduleFactory=new IPCModuleFactory(p);
   
   // Config IF
   
   IPCConfigIF<ipc::single_thread> *cif;
   IPCScan<ipc::multi_thread> *scan;   
   IPCScanRoot<ipc::single_thread> * scanroot;   

   try{
     cif=new IPCConfigIF<ipc::single_thread>(p, name, moduleFactory);
     sprintf(name, "scanCtrl_RCE%d", RceName::getRceNumber());
     scan = new IPCScan<ipc::multi_thread>( p, name);   
     sprintf(name, "scanRoot_RCE%d", RceName::getRceNumber());
     scanroot = new IPCScanRoot<ipc::single_thread>( p, name, (ConfigIF*)cif, (Scan*)scan);   
   }catch(...){
     std::cout<<"Could not add IPC objects (ipc_server not running? Several servers in the same partition?)."<<std::endl;
     assert(0);
   }
   Provider *ipcprov;
   try{
     sprintf(name, "RCE%d",RceName::getRceNumber());
     ipcprov=new Provider(p,"RceIsServer", name);
     new HistoManager(ipcprov);
   }catch(...){
     std::cout<<"Could not start histogram manager (is_server not running? Another server running on the same partition?)."<<std::endl;
     assert(0);
   }
   printf("ipc_test_server has been started.\n");
   
   if(ismodule==true){
     kill(getpid(),SIGUSR1);
     pause();
   }else{
     ipcServer.run();
   }
   scan->_destroy();
   scanroot->_destroy();
   cif->_destroy();
   IPCCore::shutdown();
   delete ipcprov;
   
   std::cout << "Test successfully completed." << std::endl;
}
