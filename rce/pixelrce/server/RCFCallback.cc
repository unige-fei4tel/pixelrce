
#include "util/RceName.hh"
#include "server/RCFCallback.hh"
#include "server/CallbackInfo.hh"
#include <iostream>

RCFCallback::RCFCallback(CallbackInfo *cb): m_rce(0), m_cbinfo(cb){
  m_callbackServer= new RCF::RcfServer(RCF::TcpEndpoint("0.0.0.0", RceName::CBPORT));
  m_callbackServer->bind<I_RCFCallback>(*this);
  m_callbackServer->start();
}
RCFCallback::~RCFCallback(){
  m_callbackServer->stop();
  delete m_callbackServer;
} 
void RCFCallback::addRce(int rce){
  m_rce++;
  char rcename[32];
  sprintf(rcename, RCFHOST"%d", rce);
  RcfClient<I_RCFCallback> client(RCF::TcpEndpoint(rcename, RceName::MAINPORT));
  RCF::createCallbackConnection(client, *m_callbackServer);
}

void RCFCallback::notify(ipc::CallbackParams msg){
  if(msg.status==ipc::SCANNING){
    m_cbinfo->setStage(msg.maskStage);
    m_cbinfo->addToMask(CallbackInfo::NEWSTAGE);
    std::cout<<"RCE "<<msg.rce<<": Mask Stage "<<msg.maskStage<<std::endl;
  }else if(msg.status==ipc::FITTING){
    std::cout<<"RCE "<<msg.rce<<": Fitting"<<std::endl;
    m_cbinfo->addToMask(CallbackInfo::FIT);
  }else if(msg.status==ipc::DOWNLOADING){
    std::cout<<"RCE "<<msg.rce<<": Downloading"<<std::endl;
    m_cbinfo->addToMask(CallbackInfo::DOWNLOAD);
  }else if(msg.status==ipc::FAILED){
    m_cbinfo->addToMask(CallbackInfo::FAILED);
    std::cout<<"RCE "<<msg.rce<<": Failed"<<std::endl;
  }
}

void RCFCallback::stopServer(){
  m_rce--;
  if(m_rce<=0){
    m_cbinfo->addToMask(CallbackInfo::STOP);
    boost::mutex::scoped_lock pl( m_mutex );
    m_cond.notify_one();
  }
}

void RCFCallback::run(){
  boost::mutex::scoped_lock pl(m_mutex);
  m_cond.wait(pl);
}

