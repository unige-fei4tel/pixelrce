
#include "server/IPCGuiCallback.hh"
#include "server/CallbackInfo.hh"

void IPCGuiCallback::notify(const ipc::CallbackParams& msg){
  if(msg.status==ipc::SCANNING){
    m_cbinfo->setStage(msg.maskStage);
    m_cbinfo->addToMask(CallbackInfo::NEWSTAGE);
    std::cout<<"RCE "<<msg.rce<<": Mask Stage "<<msg.maskStage<<std::endl;
  }else if(msg.status==ipc::FITTING){
    std::cout<<"RCE "<<msg.rce<<": Fitting"<<std::endl;
    m_cbinfo->addToMask(CallbackInfo::FIT);
  }else if(msg.status==ipc::DOWNLOADING){
    std::cout<<"RCE "<<msg.rce<<": Downloading"<<std::endl;
    m_cbinfo->addToMask(CallbackInfo::DOWNLOAD);
  }else if(msg.status==ipc::FAILED){
    m_cbinfo->addToMask(CallbackInfo::FAILED);
    std::cout<<"RCE "<<msg.rce<<": Failed"<<std::endl;
  }
}

void IPCGuiCallback::stopServer(){
  m_rce--;
  if(m_rce<=0){
    m_cbinfo->addToMask(CallbackInfo::STOP);
    stop();
  }
}

