#ifndef RCFCALLBACK_HH
#define RCFCALLBACK_HH

#include "rcf/Callback.hh"
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

class CallbackInfo;

class RCFCallback : public Callback
{
public:
  RCFCallback(CallbackInfo* cbinfo);
  ~RCFCallback();
  void notify(ipc::CallbackParams msg);
  void stopServer();
  void addRce(int rce);
  void run();
    
protected:
  int m_rce;
  boost::mutex m_mutex;
  boost::condition_variable m_cond;
  CallbackInfo* m_cbinfo;
  RCF::RcfServer *m_callbackServer;
};

#endif
