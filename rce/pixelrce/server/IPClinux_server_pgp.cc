//
//      linux_server_pgp.cc
//
//      IPC based calibration executable for Linux.
//
//      Martin Kocian
/////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include "util/IPCProvider.hh"
#include "util/HistoManager.hh"
#include "scanctrl/IPCScan.cc"
#include "scanctrl/IPCScanRoot.cc"
#include "config/IPCConfigIF.cc"
#include "HW/SerialHexdump.hh"
#include "HW/SerialPgpFei4.hh"
#include "config/IPCModuleFactory.hh"
#include "util/RceName.hh"
#include "util/IsMonitoring.hh"
#include "scanctrl/Scan.hh"
#include "server/PgpModL.hh"
boost::mutex mutex;
boost::condition_variable cond;


void sig_handler( int sig )
{    
  std::cout << " :: [IPCServer::sigint_handler] the signal " << sig << " received - exiting ... "<<std::endl;
  boost::mutex::scoped_lock pl( mutex );
  cond.notify_one();
}


//////////////////////////////////////////
//
// Main function
//
//////////////////////////////////////////


int main ( int argc, char ** argv )
{
 
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
        return 1;
    }
   
    
   signal( SIGINT , sig_handler );
   signal( SIGTERM, sig_handler );
   

  const char *p_name=getenv("TDAQ_PARTITION");
  if(p_name==NULL) p_name="rcetest";
   IPCPartition p((const char*)p_name);

   //Serial IF
   PgpModL pgp;
   pgp.open();
   //new SerialHexdump;
   new SerialPgpFei4;

   new IsMonitoring(p, "RceIsServer", RceName::getRceNumber());

   //Module Factory

   ModuleFactory *moduleFactory=new IPCModuleFactory(p);
   
   char name[128];
   sprintf(name, "configIF_RCE%d", RceName::getRceNumber());
   // Config IF
   IPCConfigIF<ipc::single_thread> *cif=new IPCConfigIF<ipc::single_thread>(p, name, moduleFactory);
        
   sprintf(name, "scanCtrl_RCE%d", RceName::getRceNumber());
   IPCScan<ipc::multi_thread> *scan = new IPCScan<ipc::multi_thread>( p, name);   
   sprintf(name, "scanRoot_RCE%d", RceName::getRceNumber());
   IPCScanRoot<ipc::single_thread> * scanroot = new IPCScanRoot<ipc::single_thread>( p, name, (ConfigIF*)cif, (Scan*)scan);   

   sprintf(name, "RCE%d", RceName::getRceNumber());
   Provider* ipcprov=new Provider(p,"RceIsServer", name);
   new HistoManager(ipcprov);
    //
   std::cout << "ipc_test_server has been started." << std::endl;

   boost::mutex::scoped_lock pl(mutex);
   cond.wait(pl);
   std::cout << "Shutdown." << std::endl;
  
   cif->_destroy();
   scanroot->_destroy();
   scan->_destroy();
   
   return 0;
}
