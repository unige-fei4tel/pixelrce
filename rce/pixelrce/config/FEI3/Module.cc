#include "config/FEI3/Module.hh"
#include "config/MaskStageFactory.hh"
#include "HW/BitStream.hh"
#include "config/FEI3/FECommands.hh"
#include "HW/SerialIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <stdio.h>

namespace FEI3{

  Module::Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt)
    :AbsModule(name, id,inLink, outLink, fmt), m_maskStaging(0){
    //    std::cout<<"Module"<<std::endl;
        for (int i=0;i<N_FRONTENDS;i++){
          m_frontend[i]=new Frontend(i);
        }
  }
  Module::~Module(){
    for (int i=0;i<N_FRONTENDS;i++){
      delete m_frontend[i];
    }
    delete m_maskStaging;
  }
  void Module::configureHW(){
    //std::cout<<"Configuring "<<m_inLink<<std::endl;
    resetHW();
    m_mcc.configureHW();
    for (int i=0;i<N_FRONTENDS;i++){
      m_frontend[i]->configureHW();
    }
  }

  void Module::resetFE(){
    resetHW(true);
  }
  void Module::resetHW(bool resetfe){
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    FECommands::resetMCC(bs);
    if(resetfe)FECommands::resetFE(bs,FECommands::FE_CMD_SOFT_RESET);
    SerialIF::send(bs);
    delete bs;
  }
  void Module::enableDataTakingHW(){
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    FECommands::sendECR(bs);
    FECommands::sendBCR(bs);
    SerialIF::send(bs);
    BitStreamUtils::prependZeros(bs);
    m_mcc.writeRegister(bs, Mcc::MCC_CNT);
    m_mcc.writeRegister(bs, Mcc::MCC_FEEN);
    FECommands::enableDataTaking(bs);
    SerialIF::send(bs);
    delete bs;
  }

  void Module::setupMaskStageHW(int maskStage) {
    m_maskStaging->setupMaskStageHW(maskStage);
  }
    
  //set the value in the register but not in the frontend
  Module::PAR Module::setParameter(const char* name, int val){ 
    if(std::string(name)=="CHARGE"){
      for (int i=0;i<N_FRONTENDS;i++){
	//std::cout<<"Fronted "<<i<<" dac setting "<<m_frontend[i]->electronsToDac(val)<<std::endl;
	m_frontend[i]->setGlobalRegField("dacVCAL", m_frontend[i]->electronsToDac(val));
      }
      return SPECIAL;
    }
    if (std::string(name)=="NO_PAR"){
      return SPECIAL;
    }
    PixelRegister::Field fpar=PixelRegister::lookupParameter(name);
    if (fpar!=PixelRegister::not_found){
      for (int i=0;i<N_FRONTENDS;i++){
	m_frontend[i]->setPixelParameter(fpar, val);
      }
      return PIXEL;
    }
    std::string par=GlobalRegister::lookupParameter(name);
    if(par!=""){ //it's a global variable
      for (int i=0;i<N_FRONTENDS;i++){
	m_frontend[i]->setGlobalRegField(par.c_str(), val);
      }
      return GLOBAL;
    }
    // wasn't a global variable, should be MCC then.
    int rt=m_mcc.setParameter(name, val);
    if(rt)return MCC;
    char error_message[128];
    printf("parameter %s was not found.",name);
    return NOT_FOUND;
 }
  // set a parameter (global or MCC) in the frontend
  int Module::setupParameterHW(const char* name, int val){
    int retval=1;
    //first set the appropriate internal register
    PAR p=setParameter(name,val);
    // now send the config to the frontend
    if(p!=NOT_FOUND){
      retval=0;
      resetHW(false); //don't reset FE, only MCC
      m_mcc.configureHW();
      for (int i=0;i<N_FRONTENDS;i++){
	m_frontend[i]->writeGlobalRegisterHW();
      }
    }
    return retval;
  }

  int Module::configureScan(boost::property_tree::ptree *scanOptions){
    int retval=0;
    //std::cout<<"FEI3::Module::configureScan"<<std::endl;
    try{
      // MCC
      setParameter("trigOpt.nL1AperEvent",scanOptions->get<int>("trigOpt.nL1AperEvent"));
      setParameter("bandwidth",0);
      setParameter("trigOpt.strobeMCCDelayRange",scanOptions->get<int>("trigOpt.strobeMCCDelayRange"));
      setParameter("trigOpt.strobeMCCDelay",scanOptions->get<int>("trigOpt.strobeMCCDelay"));
      setParameter("trigOpt.strobeDuration",scanOptions->get<int>("trigOpt.strobeDuration"));
      //setParameter("enables",0xffff);
      // Global regs
      int diginject=scanOptions->get("trigOpt.optionsMask.DIGITAL_INJECT", 0);
      if(diginject)setParameter("enableDigitalInject",1);
      else setParameter("enableDigitalInject",0);
      setParameter("latency",scanOptions->get<int>("trigOpt.Lvl1_Latency"));
      int chigh=scanOptions->get("trigOpt.optionsMask.USE_CHIGH", 0);
      if(chigh)setParameter("enableCinjHigh",1);
      else setParameter("enableCinjHigh",0);
      std::string stagingMode=scanOptions->get<std::string>("stagingMode");
      std::string maskStages=scanOptions->get<std::string>("maskStages");
      delete m_maskStaging;
      MaskStageFactory msf;
      m_maskStaging=msf.createMaskStaging(this, maskStages.c_str(), stagingMode.c_str());
      if(chigh){
	m_maskStaging->setClow(0);
	m_maskStaging->setChigh(1);
      }else{
	m_maskStaging->setClow(1);
	m_maskStaging->setChigh(0);
      }
      setParameter("doMux",8); // event data
      // set Vcal for each FE 
      std::string par="dacVCAL";
      if(scanOptions->get("trigOpt.optionsMask.SPECIFY_CHARGE_NOT_VCAL", 0)==1)par="CHARGE";
      setParameter(par.c_str(),scanOptions->get<int>("trigOpt.vcal_charge"));
    }
    catch(boost::property_tree::ptree_bad_path ex){
      retval=1;
      std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    }
    //    std::cout<<"FEI3::Module::configureScan end"<<std::endl;
    return retval;
  }
  void Module::destroy(){
    delete this;
  }
  ModuleInfo Module::getModuleInfo(){
    return ModuleInfo(m_name, m_id,m_inLink, m_outLink,Module::N_FRONTENDS,Frontend::N_ROWS,Frontend::N_COLS, m_formatter);
  }
  const float Module::dacToElectrons(int fe, int dac){
    if(fe>=N_FRONTENDS)return -1;
    return m_frontend[fe]->dacToElectrons(dac);
  }
};

