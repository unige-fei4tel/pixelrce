#ifndef FEI3REGULARMASKSTAGING_HH
#define FEI3REGULARMASKSTAGING_HH

#include "config/Masks.hh"
#include "config/MaskStaging.hh"

namespace FEI3{
  class Module;
  
  class RegularMaskStaging: public MaskStaging<FEI3::Module>{
  public:
    RegularMaskStaging(FEI3::Module* module, Masks masks, std::string type);
    void setupMaskStageHW(int maskStage);
  private:
    int m_nStages;
  };
  
}
#endif
