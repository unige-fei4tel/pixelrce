#ifndef TRIGGERRECEIVERIF_HH
#define TRIGGERRECEIVERIF_HH

class TriggerReceiverIF{
public:
  TriggerReceiverIF();
  virtual ~TriggerReceiverIF();
  static void receive(unsigned *data, int size);
  virtual void Receive(unsigned* data, int size)=0;
private:
  static TriggerReceiverIF* s_rec;
};
#endif
