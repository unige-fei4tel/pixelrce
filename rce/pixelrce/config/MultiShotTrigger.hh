#ifndef MULTISHOTTRIGGER_HH
#define MULTISHOTTRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "config/AbsTrigger.hh"
#include "HW/BitStream.hh"


  class MultiShotTrigger: public AbsTrigger{
  public:
    MultiShotTrigger();
    ~MultiShotTrigger(){};
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
    void setBit(std::vector<unsigned> &vec, int bit);
  private:
    BitStream m_triggerStream;
    int m_ntrig;
    int m_interval;
    int m_leading0;
  };


#endif
