#ifndef FEI4__FEI4BMODULE_HH
#define FEI4__FEI4BMODULE_HH

#include "config/FEI4/Module.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <string>

class AbsFormatter;

namespace FEI4{

  class FEI4BModule: public Module{
  public:
    FEI4BModule(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter *fmt);
    virtual ~FEI4BModule();
    void resetHW();
    int verifyModuleConfigHW();
    virtual void destroy();
    void setupGlobalPulseHW(int pulsereg);

};

};
#endif
