#include "config/FEI4/AnalogColprMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  AnalogColprMaskStaging::AnalogColprMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type), m_oldDcolStage(-1){
    m_nStages=strtoul(type.substr(12).c_str(),0,10);
    m_colpr_mode=strtoul(type.substr(10,1).c_str(),0,10);
    if(m_colpr_mode==0)m_nColStages=40;
    else if(m_colpr_mode==1)m_nColStages=4;
    else if(m_colpr_mode==2)m_nColStages=8;
    else m_nColStages=1;
    std::cout<<"Set up COLPR mask staging with "<<m_nColStages<<" col stages and "<< m_nStages<<" row stages"<<std::endl;
  }
  
  void AnalogColprMaskStaging::setupMaskStageHW(int maskStage){
    int dcolStage=maskStage/m_nStages;
    unsigned stage=maskStage%m_nStages;
    if(m_initialized==false || maskStage==0 || dcolStage!=m_oldDcolStage){
      clearBitsHW();
      m_initialized=true;
      m_oldDcolStage=dcolStage;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    global->setField("Colpr_Mode", m_colpr_mode, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){
	pixel->setupMaskStageCol(dcolStage*2+1, i, stage, m_nStages);
	m_module->writeDoubleColumnHW(i, i, dcolStage, dcolStage); 
	int seconddcol=dcolStage-1<0? m_nColStages-1 : dcolStage-1;
	pixel->setupMaskStageCol(seconddcol*2+2, i, stage, m_nStages);
	m_module->writeDoubleColumnHW(i, i, seconddcol, seconddcol); 
	if(dcolStage==0 || dcolStage==m_nColStages-1){
	  if(dcolStage==0){
	    pixel->setBitCol(80, i, 0);
	  }else{
	    pixel->setupMaskStageCol(79, i, stage, m_nStages);
	    pixel->setupMaskStageCol(80, i, stage, m_nStages);
	  }
	  global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
	  m_module->writeDoubleColumnHW(i, i, 39, 39); 
	  global->setField("Colpr_Mode", m_colpr_mode, GlobalRegister::SW); 
	}
      }
    }
    global->setField("Colpr_Mode", m_colpr_mode, GlobalRegister::SW); 
    global->setField("Colpr_Addr", dcolStage, GlobalRegister::HW); 
  }
}
