
#include "config/FEI4/FECommands.hh"

namespace FEI4{

  void FECommands::writeGlobalRegister(BitStream *bs, int reg, unsigned short value){
    unsigned cmd=0x5a0800; /* header (10110) + field2 (1000) + field3 (0010) */;
    cmd|=((m_chipAddr|m_broadcasting)&0xf)<<6; // chip id
    cmd|=reg&0x3f; // register address
    bs->push_back(cmd);
    bs->push_back(value<<16);  // 16 bit register value
    bs->push_back(0); // padding a la NewDsp
  }
  void FECommands::readGlobalRegister(BitStream *bs, int reg){
    unsigned cmd=0x5a0400; /* header (10110) + field2 (1000) + field3 (0001) */;
    cmd|=((m_chipAddr|m_broadcasting)&0xf)<<6; // chip id
    cmd|=reg&0x3f; // register address
    bs->push_back(cmd);
    bs->push_back(0); // padding a la NewDsp
  }
  void FECommands::globalReset(BitStream *bs){
    unsigned cmd=0x16880;
    cmd|=(m_chipAddr|m_broadcasting)&0xf;
    bs->push_back(cmd);
    bs->push_back(0); // padding a la NewDsp
  }
  void FECommands::globalPulse(BitStream *bs, unsigned width) {
    unsigned cmd=0x5a2400; /* header (10110) + field2 (1000) + field3 (1001) */;
    cmd|=((m_chipAddr|m_broadcasting)&0xf)<<6; // chip id
    cmd|=width&0x3f; // pulse width
    bs->push_back(cmd);
    bs->push_back(0);
  }
  void FECommands::switchMode(BitStream *bs, MODE m) {
    unsigned cmd=0x5a2800; /* header (10110) + field2 (1000) + field3 (1010) */;
    cmd|=((m_chipAddr|m_broadcasting)&0xf)<<6; // chip id
    cmd|=m&0x3f; // mode (conf or run)
    bs->push_back(cmd);
    bs->push_back(0);
  }
  void FECommands::L1A(BitStream *bs){
    bs->push_back(0xE8000000);
  }
  void FECommands::sendECR(BitStream *bs){
    bs->push_back(0);
    bs->push_back(0x00162000); /* MCC ECR command */
    bs->push_back(0);
  }
  void FECommands::sendBCR(BitStream *bs){
    bs->push_back(0);
    bs->push_back(0x00161000); /* MCC BCR command */
    bs->push_back(0);
  }
  void FECommands::feWriteCommand(BitStream *bs){
    bs->push_back(0);
    unsigned cmd=0x5a1000;
    cmd|=((m_chipAddr|m_broadcasting)&0xf)<<6;
    bs->push_back(cmd); 
  }

};
