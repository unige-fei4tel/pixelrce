#include "config/FEI4/DiffusionMaskStaging.hh"
#include "config/FEI4/Module.hh"

namespace FEI4{
  
  DiffusionMaskStaging::DiffusionMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    m_nStages=16;
  }
  
  void DiffusionMaskStaging::setupMaskStageHW(int maskStage){
  
    //    std::cout<<"diffusion stage = "<<maskStage<<std::endl;
    
    GlobalRegister* global=m_module->global();
    
    //std::cout<<"Setting colpr_mode to 3"<<std::endl;
    global->setField("Colpr_Mode", 3, GlobalRegister::SW); 
    
    //the "double column" for enabling strobe, which has skewed addressing
    //the strobe double columns 0,1,2.....38,39 correspond to columns: 
    //(0),(1,2),(3,4).....(75,76),(77,78,79)
    int strobeDCol = 39;   //it doesn't matter what value we set this to, but we do need to set it.
    global->setField("Colpr_Addr", strobeDCol, GlobalRegister::HW); 
    
  }

}
