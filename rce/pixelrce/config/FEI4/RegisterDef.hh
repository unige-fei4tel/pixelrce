#ifndef FEI4__REGISTERDEF_HH
#define FEI4__REGISTERDEF_HH

namespace FEI4{

  struct FieldParams{
    int reg;
    int bitpos;
    int SRABbitpos;
    int SRCbitpos;
    int width;
    bool reverse;
  };
  
  struct RegisterDef{
    int LOW_REG, N_REG_REGS, N_W_REGS, N_R_REGS, SRAB_BITS, SRC_BITS;
    std::map<std::string, FieldParams> m_fields;
    std::map<std::string, std::string> m_parameters;
    std::string m_cachedName, m_cachedField; 
    std::map<int, SerialIF::Rate> m_rate;
    FieldParams m_params8b10b;
    FieldParams m_paramsDrate;
    int m_colpr_reg;
    unsigned short m_colpr_disable;
  };
}


#endif
