#ifndef PIXEL_REGISTER_FEI4_HH
#define PIXEL_REGISTER_FEI4_HH

#include <vector>
#include <map>
#include "config/FEI4/Utils.hh"
#include <iostream>

namespace FEI4{
  class FECommands;

  namespace PixelRegisterFields{
    static const unsigned fieldPos[]={0,1,6,7,8,9,13,14};
    static const unsigned fieldMask[]={1,0x1f,1,1,1,0xf,1};
  } 
  class PixelRegister{
  public:
    enum{N_COLS=80, N_ROWS=336 ,N_PIXEL_REGISTER_BITS=14, DROW_WORDS=N_ROWS*2/32};
    enum Field{enable, tdac, largeCap, smallCap, hitbus, fdac, diginj, Nfields, not_found};
    PixelRegister(FECommands* commands); 
    ~PixelRegister();
    unsigned getBit(unsigned bit, unsigned row, unsigned col){
      int drow;
      int dcol=(col-1)/2;// column numbering starts with 1, array with 0. dcol is double column
      if(col%2==0){
	drow = row+335;
      }else{
	drow = 336-row;
      }
      return (m_pixreg[bit][dcol][DROW_WORDS-1-drow/32] >> (drow%32))&0x1;
    }
    
    void setBit(unsigned bit, unsigned row, unsigned col, bool val){
      int drow;
      int dcol=(col-1)/2;// column numbering starts with 1, array with 0. Array is by double columns.
      if(col%2==0){
	drow = row+335; //even column is in forward order 336 - 671
      }else{
	drow = 336-row; //odd column is in backward order 0 - 335
      }
      if(val){
	m_pixreg[bit][dcol][DROW_WORDS-1-drow/32] |= (1<<(drow%32));
      }else{
	m_pixreg[bit][dcol][DROW_WORDS-1-drow/32] &= ~(1<<(drow%32));
      }
    }
    void setFieldAll(Field f, unsigned val);
    void setField(Field f, unsigned row, unsigned col, unsigned val){
      unsigned bitpos=PixelRegisterFields::fieldPos[f];
      unsigned mask=PixelRegisterFields::fieldMask[f];
      unsigned v=val;
      if(f==tdac)v=flipBits(5, val);// tdac has reverse bit order
      while(mask&1){
	setBit(bitpos, row, col, v&0x1);
	mask>>=1;
	v>>=1;
	bitpos++;
      }
    }
    unsigned getField(Field f, unsigned row, unsigned col){
      unsigned bitpos=PixelRegisterFields::fieldPos[f+1]-1;
      unsigned mask=PixelRegisterFields::fieldMask[f];
      unsigned val=0;
      while(mask&1){
	unsigned bit=getBit(bitpos, row, col);
	mask>>=1;
	val<<=1;
	val|=bit;
	bitpos--;
      }
      if(f==tdac)return flipBits(5, val); // word is flipped, reverse for fdac
      else return val;
    }
    unsigned *getDoubleColumn(unsigned bit, unsigned dcol){
      if(bit<N_PIXEL_REGISTER_BITS && dcol<N_COLS/2)return m_pixreg[bit][dcol];
      else return 0;
    }
    void setDoubleColumn(unsigned bit, unsigned dcol, unsigned* data);
    unsigned writeDoubleColumnHW(unsigned bit, unsigned dcol);
    unsigned writeDoubleColumnHW(const unsigned *data, std::vector<unsigned>& readback);
    void dumpPixelRegister(std::ostream &os);
    void dumpDoubleColumn(unsigned bit, unsigned dcol, std::ostream &os);
    unsigned decodePixelRecord(std::vector<unsigned char>& shiftin, std::vector<unsigned>& readback);
    int verifyDoubleColumnHW(int bit, unsigned dcol, std::vector<unsigned>& readback);
    void setBitAll(int bit, int on);
    void setBitDcol(unsigned dcol, int bit, int on);
    void setBitCol(unsigned col, int bit, int on);
    void setupMaskStage(int bit, int maskStage, unsigned nMaskStages);
    void setupMaskStageCol(unsigned col, int bit, int maskStage, unsigned nMaskStages);
    static Field lookupParameter(const char* name);
    static void setHitBusMode(int value);

  private:
    static std::string m_cachedName; 
    static Field m_cachedField;
    static std::map<std::string, Field> m_parameters;
    static bool m_initialized;
    static void initialize();
    static int m_hitbusMode;  //0 for monleak scan, 1 for hitbus scan 
    FECommands* m_commands;
    unsigned m_pixreg[N_PIXEL_REGISTER_BITS][N_COLS/2][DROW_WORDS]; // by double column
  };

};

#endif
