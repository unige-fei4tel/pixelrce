
#include "config/MultiShotTrigger.hh"
#include "config/FEI3/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "HW/SerialIF.hh"
#include <iostream>


MultiShotTrigger::MultiShotTrigger():AbsTrigger(),m_ntrig(0), m_interval(0){
  m_parameters.push_back("TRIGGER_DELAY");
}
int MultiShotTrigger::configureScan(boost::property_tree::ptree* scanOptions){
  int retval=0;
  m_i=0; //reset the number of triggers
  try{
    m_ntrig = scanOptions->get<int>("trigOpt.nTriggersPerGroup");
    m_leading0 = scanOptions->get<int>("trigOpt.CalL1ADelay");
    m_interval=scanOptions->get<int>("trigOpt.Lvl1_Latency_Secondary");
    std::cout<<"Setting up "<<m_ntrig<<" Triggers with an interval of "<<m_interval<<std::endl;
    setupTriggerStream();
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    retval=1;
  }
  return retval;
}
int MultiShotTrigger::setupParameter(const char* name, int val){
  return 0;
}

void MultiShotTrigger::setupTriggerStream(){
  m_triggerStream.clear();
  int totallength=m_leading0+m_ntrig*5+(m_ntrig-1)*(m_interval-5);
  for(int i=0;i<totallength/32;i++)m_triggerStream.push_back(0);
  if(totallength%32!=0)m_triggerStream.push_back(0);
  for(int i=0;i<5;i++){
    if(i==3)continue;
    for(int j=0;j<m_ntrig;j++){
      int bit=m_leading0+i+j*m_interval;
      setBit(m_triggerStream,bit);
    }
  }
  SerialIF::writeRegister(19,0);
  for (size_t i=0;i<m_triggerStream.size();i++){
    SerialIF::writeRegister(18,m_triggerStream[i]);
    std::cout<<std::hex<<m_triggerStream[i]<<std::dec<<std::endl;
  }
  SerialIF::writeRegister(27,1);
}
void MultiShotTrigger::setBit(std::vector<unsigned> &vec, int bit){
  int wordpos=bit/32;
  int bitpos=31-bit%32;
  if(vec.size()<=(unsigned)wordpos)return;
  vec[wordpos]|=1<<bitpos;
}

int MultiShotTrigger::sendTrigger(){
  //std::cout<<"Trigger::Trigger "<<m_i<<std::endl;
  SerialIF::send(&m_triggerStream,SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA);
  m_i++;
  return 0;
}

int MultiShotTrigger::enableTrigger(bool on){
  return SerialIF::enableTrigger(on);
  return 0;
}
int MultiShotTrigger::resetCounters(){
  BitStream *bs=new BitStream;
  BitStreamUtils::prependZeros(bs);
  FEI3::FECommands::sendECR(bs);
    FEI3::FECommands::sendBCR(bs);
  SerialIF::send(bs,SerialIF::WAITFORDATA);
  delete bs;
  return 0;
}
