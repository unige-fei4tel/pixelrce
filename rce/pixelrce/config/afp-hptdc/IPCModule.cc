#ifndef AFPHPTDC__IPCMODULE_CC
#define AFPHPTDC__IPCMODULE_CC
#include "util/RceName.hh"
#include "config/afp-hptdc/IPCModule.hh"
#include "ipc/partition.h"
#include <boost/property_tree/ptree.hpp>
#include "ers/ers.h"
#include <boost/lexical_cast.hpp>

namespace afphptdc{

template <class TP>
IPCModule<TP>::IPCModule(IPCPartition & p, const char * name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
  IPCNamedObject<POA_ipc::IPCAFPHPTDCAdapter, TP>( p, std::string(boost::lexical_cast<std::string>(id)+"_RCE"+boost::lexical_cast<std::string>(RceName::getRceNumber())).c_str()) , 
  Module(name, id, inlink, outlink, fmt){
  //  std::cout<<"IPCModule"<<std::endl;
  try {
    IPCNamedObject<POA_ipc::IPCAFPHPTDCAdapter,TP>::publish();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
}

template <class TP>
IPCModule<TP>::~IPCModule(){
  try {
    IPCNamedObject<POA_ipc::IPCAFPHPTDCAdapter,TP>::withdraw();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::warning( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
}

template <class TP>
CORBA::Long IPCModule<TP>::IPCdownloadConfig(const ipc::AFPHPTDCModuleConfig &config){
  setFpgaField("test", config.test);
  setFpgaField("tdcControl", config.tdcControl);
  setFpgaField("run", config.run);
  setFpgaField("bypassLut", config.bypassLut);
  setFpgaField("localClockEn", config.localClockEn);
  setFpgaField("calClockEn", config.calClockEn);
  setFpgaField("refEn", config.refEn);
  setFpgaField("hitTestEn", config.hitTestEn);
  setFpgaField("inputSel", config.inputSel);
  setFpgaField("address", config.address);
  setFpgaField("fanspeed", config.fanspeed);
  setFpgaField("channelEn", config.channelEn);

  for(int i=0;i<3;i++){
    for (int j=31;j>=0;j--){
      char offs[32];
      sprintf(offs, "offset%d", j);
      setTdcField(offs, i, config.offset[j][i]);
    }
    setTdcField("test_select", i, config.test_select[i]);
    setTdcField("enable_error_mark", i, config.enable_error_mark[i]);
    setTdcField("enable_error_bypass", i, config.enable_error_bypass[i]);
    setTdcField("enable_error", i, config.enable_error[i]);
    setTdcField("readout_single_cycle_speed", i, config.readout_single_cycle_speed[i]);
    setTdcField("serial_delay", i, config.serial_delay[i]);
    setTdcField("strobe_select", i, config.strobe_select[i]);
    setTdcField("readout_speed_select", i, config.readout_speed_select[i]);
    setTdcField("token_delay", i, config.token_delay[i]);
    setTdcField("enable_local_trailer", i, config.enable_local_trailer[i]);
    setTdcField("enable_local_header", i, config.enable_local_header[i]);
    setTdcField("enable_global_trailer", i, config.enable_global_trailer[i]);
    setTdcField("enable_global_header", i, config.enable_global_header[i]);
    setTdcField("keep_token", i, config.keep_token[i]);
    setTdcField("master", i, config.master[i]);
    setTdcField("enable_bytewise", i, config.enable_bytewise[i]);
    setTdcField("enable_serial", i, config.enable_serial[i]);
    setTdcField("enable_jtag_readout", i, config.enable_jtag_readout[i]);
    setTdcField("tdc_id", i, config.tdc_id[i]);
    setTdcField("select_bypass_inputs", i, config.select_bypass_inputs[i]);
    setTdcField("readout_fifo_size", i, config.readout_fifo_size[i]);
    setTdcField("reject_count_offset", i, config.reject_count_offset[i]);
    setTdcField("search_window", i, config.search_window[i]);
    setTdcField("match_window", i, config.match_window[i]);
    setTdcField("leading_resolution", i, config.leading_resolution[i]);
    setTdcField("fixed_pattern", i, config.fixed_pattern[i]);
    setTdcField("enable_fixed_pattern", i, config.enable_fixed_pattern[i]);
    setTdcField("max_event_size", i, config.max_event_size[i]);
    setTdcField("reject_readout_fifo_full", i, config.reject_readout_fifo_full[i]);
    setTdcField("enable_readout_occupancy", i, config.enable_readout_occupancy[i]);
    setTdcField("enable_readout_separator", i, config.enable_readout_separator[i]);
    setTdcField("enable_overflow_detect", i, config.enable_overflow_detect[i]);
    setTdcField("enable_relative", i, config.enable_relative[i]);
    setTdcField("enable_automatic_reject", i, config.enable_automatic_reject[i]);
    setTdcField("event_count_offset", i, config.event_count_offset[i]);
    setTdcField("trigger_count_offset", i, config.trigger_count_offset[i]);
    setTdcField("enable_set_counters_on_bunch_reset", i, config.enable_set_counters_on_bunch_reset[i]);
    setTdcField("enable_master_reset_code", i, config.enable_master_reset_code[i]);
    setTdcField("enable_master_reset_code_on_event_reset", i, config.enable_master_reset_code_on_event_reset[i]);
    setTdcField("enable_reset_channel_buffer_when_separator", i, config.enable_reset_channel_buffer_when_separator[i]);
    setTdcField("enable_separator_on_event_reset", i, config.enable_separator_on_event_reset[i]);
    setTdcField("enable_separator_on_bunch_reset", i, config.enable_separator_on_bunch_reset[i]);
    setTdcField("enable_direct_event_reset", i, config.enable_direct_event_reset[i]);
    setTdcField("enable_direct_bunch_reset", i, config.enable_direct_bunch_reset[i]);
    setTdcField("enable_direct_trigger", i, config.enable_direct_trigger[i]);
    setTdcField("coarse_count_offset", i, config.coarse_count_offset[i]);
    setTdcField("dll_tap_adjust3_0", i, config.dll_tap_adjust3_0[i]);
    setTdcField("dll_tap_adjust7_4", i, config.dll_tap_adjust7_4[i]);
    setTdcField("dll_tap_adjust11_8", i, config.dll_tap_adjust11_8[i]);
    setTdcField("dll_tap_adjust15_12", i, config.dll_tap_adjust15_12[i]);
    setTdcField("dll_tap_adjust19_16", i, config.dll_tap_adjust19_16[i]);
    setTdcField("dll_tap_adjust23_20", i, config.dll_tap_adjust23_20[i]);
    setTdcField("dll_tap_adjust27_24", i, config.dll_tap_adjust27_24[i]);
    setTdcField("dll_tap_adjust31_28", i, config.dll_tap_adjust31_28[i]);
    setTdcField("rc_adjust", i, config.rc_adjust[i]);
    setTdcField("not_used", i, config.not_used[i]);
    setTdcField("low_power_mode", i, config.low_power_mode[i]);
    setTdcField("width_select", i, config.width_select[i]);
    setTdcField("vernier_offset", i, config.vernier_offset[i]);
    setTdcField("dll_control", i, config.dll_control[i]);
    setTdcField("dead_time", i, config.dead_time[i]);
    setTdcField("test_invert", i, config.test_invert[i]);
    setTdcField("test_mode", i, config.test_mode[i]);
    setTdcField("enable_trailing", i, config.enable_trailing[i]);
    setTdcField("enable_leading", i, config.enable_leading[i]);
    setTdcField("mode_rc_compression", i, config.mode_rc_compression[i]);
    setTdcField("mode_rc", i, config.mode_rc[i]);
    setTdcField("dll_mode", i, config.dll_mode[i]);
    setTdcField("pll_control", i, config.pll_control[i]);
    setTdcField("serial_clock_delay", i, config.serial_clock_delay[i]);
    setTdcField("io_clock_delay", i, config.io_clock_delay[i]);
    setTdcField("core_clock_delay", i, config.core_clock_delay[i]);
    setTdcField("dll_clock_delay", i, config.dll_clock_delay[i]);
    setTdcField("serial_clock_source", i, config.serial_clock_source[i]);
    setTdcField("io_clock_source", i, config.io_clock_source[i]);
    setTdcField("core_clock_source", i, config.core_clock_source[i]);
    setTdcField("dll_clock_source", i, config.dll_clock_source[i]);
    setTdcField("roll_over", i, config.roll_over[i]);
    setTdcField("enable_matching", i, config.enable_matching[i]);
    setTdcField("enable_pair", i, config.enable_pair[i]);
    setTdcField("enable_ttl_serial", i, config.enable_ttl_serial[i]);
    setTdcField("enable_ttl_control", i, config.enable_ttl_control[i]);
    setTdcField("enable_ttl_reset", i, config.enable_ttl_reset[i]);
    setTdcField("enable_ttl_clock", i, config.enable_ttl_clock[i]);
    setTdcField("enable_ttl_hit", i, config.enable_ttl_hit[i]);
  }
  AbsFormatter* fmt=getFormatter();
  if(fmt){
    // configure the formatter with the calibration constants
    boost::property_tree::ptree *pt=new boost::property_tree::ptree;
    for(int i=0;i<2;i++){
      for(int j=0;j<12;j++){
	for(int k=0;k<1024;k++){
	  char name[32];
	  sprintf(name, "c_%d_%d_%d", i, j, k);
	  pt->put(name, config.calib[i][j][k]);
	}
      }
    }
    std::cout<<"%%%%%%%%% Configuring HPTDC Formatter"<<std::endl;
    fmt->configure(pt);
    delete pt;
  }
  return 0;
}

template <class TP>
void IPCModule<TP>::shutdown(){
  std::cout<<"Shutdown"<<std::endl;
}

template <class TP>
void IPCModule<TP>::destroy(){
  this->_destroy();
}

  

};

#endif
