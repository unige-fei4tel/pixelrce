#ifndef MEASUREMENTTRIGGER_HH
#define MEASUREMENTTRIGGER_HH

#include "config/AbsTrigger.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <string>

  class MeasurementTrigger: public AbsTrigger{
  public:
    MeasurementTrigger();
    ~MeasurementTrigger(){};
    int sendTrigger();
    int enableTrigger(bool on){return 0;}
    int configureScan(boost::property_tree::ptree* scanOptions);
  };


#endif
