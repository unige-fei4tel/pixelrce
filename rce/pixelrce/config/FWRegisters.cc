#include "config/FWRegisters.hh"
#include <iostream>

void FWRegisters::setChannelmask(int rce, unsigned mask){
  writeRegister(rce, CHANNELMASK, mask);
}
void FWRegisters::setMode(int rce, opmode md){
  writeRegister(rce, MODE, md);
}
void FWRegisters::setChannelOutmask(int rce, unsigned mask){
  writeRegister(rce, CHANNELOUTMASK, mask);
}
void FWRegisters::setDiscDelay(int rce, int channel, int delay){
  resetDelays(rce);
  if(delay>MAXDELAY){
    std::cout<<"Discriminator delay exceeds "<<MAXDELAY<<"."<<std::endl;
    return;
  }
  for(int i=0;i<delay;i++) writeRegister(rce, INCDISCDELAY, 1<<channel);
}
void FWRegisters::setDiscOpMode(int rce, unsigned val){
  writeRegister(rce, DISCOP, val);
}
void FWRegisters::resetDelays(int rce){
  writeRegister(rce, RESETDELAYS, 1);
}
void FWRegisters::setTriggermask(int rce, unsigned mask){
  writeRegister(rce, TRIGGERMASK, mask);
}
void FWRegisters::setDeadtime(int rce, unsigned deadtime){
  writeRegister(rce, DEADTIME, deadtime);
}
void FWRegisters::setEncoding(int rce, streamencoding enc){
  writeRegister(rce, ENCODING, enc);
}
void FWRegisters::setHitbusOp(int rce, unsigned val){
  writeRegister(rce, HITBUSOP, val);
}
void FWRegisters::setL1Type(int rce, routing val){
  writeRegister(rce, L1ROUTE, val);
}
void FWRegisters::setHitbusDataDelay(int rce, unsigned delay){
  writeRegister(rce, HBDELAY, delay);
}
void FWRegisters::setHitbusDataNegativeDelay(int rce, unsigned delay){
  writeRegister(rce, HBDELAYNEG, delay);
}
void FWRegisters::setTelescopeOp(int rce, unsigned op){
  writeRegister(rce, TELESCOPEOP, op);
}
void FWRegisters::setTemperatureReadoutFrequency(int rce, unsigned ticks){
  writeRegister(rce, TEMPFREQ, ticks);
}
void FWRegisters::setTemperatureReadoutEnable(int rce, bool on){
  writeRegister(rce, TEMPENABLE, on);
}
void FWRegisters::setNumberofFeFramesPgp(int rce, unsigned num){
  writeRegister(rce, MAXBUFLENGTH, num);
}
void FWRegisters::setOutputDelay(int rce, unsigned inlink, unsigned val){
  writeRegister(rce, OUTPUTDELAYS+inlink, val);
}
void FWRegisters::setRcePresent(int rce){
  sendCommand(rce, CMD_PRESENT);
} 
void FWRegisters::setHitDiscConfig(int rce, int chan, unsigned val){
  writeRegister(rce, HITDISCCONFIG+chan, val);
}
void FWRegisters::setNExp(int rce, unsigned val){
  unsigned rval= val==0? 16 : val;
  writeRegister(rce, NEXP, rval);
}
void FWRegisters::setEfbTimeout(int rce, unsigned val){
  writeRegister(rce, EFBTIMEOUT, val);
}
void FWRegisters::setEfbTimeoutFirst(int rce, unsigned val){
  writeRegister(rce, EFBTIMEOUTFIRST, val);
}
void FWRegisters::setEfbMissingHeaderTimeout(int rce, unsigned val){
  writeRegister(rce, EFBMISSINGHEADERTIMEOUT, val);
}
void FWRegisters::setRunNumber(int rce, unsigned val){
  writeRegister(rce, RUNNUMBER, val);
}
void FWRegisters::selectClock(int rce, CLK clk){
  writeRegister(rce, CLKSEL, clk);
}
bool FWRegisters::atlasClockPresent(int rce){
  return readRegister(rce, CLKRB)&2;
}
bool FWRegisters::externalClockSelected(int rce){
  return !(readRegister(rce, CLKRB)&1);
}
void FWRegisters::setOccNormalization(int rce, unsigned val){
  writeRegister(rce, NUMMON, val);
}
unsigned FWRegisters::getOccNormalization(int rce){
  return readRegister(rce, NUMMON);
}
void FWRegisters::enableMonitoring(int rce, unsigned val){
  writeRegister(rce, MONENABLED, val);
}
void FWRegisters::enableDatapath(int rce, unsigned val){
  writeRegister(rce, DATAPATH, val);
}
void FWRegisters::enableTtcSim(int rce, unsigned val){
  writeRegister(rce, TTCSIM, val);
}
void FWRegisters::enableEcrReset(int rce, bool on){
  writeRegister(rce, ENABLEECRRESET, on);
}
void FWRegisters::enableBcrBusy(int rce, bool on){
  writeRegister(rce, ENABLEBCRVETO, on);
}
void FWRegisters::setBcrBusyParams(int rce, unsigned first, unsigned num){
  writeRegister(rce, FIRSTVETOBCID, first);
  writeRegister(rce, NUMVETOBCID, num);
}
void FWRegisters::enableComplexDeadtime(int rce, bool on){
  writeRegister(rce, CDTENABLE, on);
}
void FWRegisters::setComplexDeadtimeParams(int rce, unsigned nbuffers, unsigned window){
  writeRegister(rce, CDTNBUF, nbuffers);
  writeRegister(rce, CDTWINDOW, window);
}
void FWRegisters::setBcidOffset(int rce, unsigned val){
  writeRegister(rce, BCIDOFFSET, val);
}
void FWRegisters::disableInternalBusy(int rce, bool on){
  writeRegister(rce, VETODISABLE, on);
}
void FWRegisters::blockEcrBcr(int rce, unsigned val){
  writeRegister(rce, BLOCKECRBCR, val);
}
void FWRegisters::enableSLinkBlowoff(int rce, bool on){
  writeRegister(rce, BLOWOFF, on);
}
void FWRegisters::outputBusy(int rce, bool on){
  writeRegister(rce, IGNOREBUSY, !on);
}
void FWRegisters::resetTtc(int rce){
  writeRegister(rce, RESETTTC, 0);
}
void FWRegisters::resetL1id(int rce){
  writeRegister(rce, RESETL1ID, 0);
}
void FWRegisters::presetECR(int rce, unsigned val){
  writeRegister(rce, PRESETECR, val);
}
void FWRegisters::pauseECR(int rce, bool on){
  writeRegister(rce, PAUSEECR, on);
}
unsigned FWRegisters::getECR(int rce){
  return readRegister(rce, READECR);
}
unsigned FWRegisters::getNumberOfEvents(int rce){
  return readRegister(rce, NEVT);
}
unsigned FWRegisters::getNumberOfMonMissed(int rce){
  return readRegister(rce, NEVTNOMON);
}
unsigned FWRegisters::getDisabledMask(int rce){
  return readRegister(rce, DISABLED);
}
unsigned FWRegisters::getDisabledInRunMask(int rce){
  return readRegister(rce, DISABLEDINRUN);
}
unsigned FWRegisters::getEfbCounter(int rce, int chan, EFBCOUNTER cnt){
  return readRegister(rce, EFBCOUNTERS+16*(int)cnt+chan);
}
unsigned FWRegisters::getTtcClashCounter(int rce, CLASH cnt){
  return readRegister(rce, CLASHCOUNTERS+cnt);
}
