#include <new>

namespace PgpTrans{

  class PgpHeader {
  public:
    enum VChannel {VC0, VC1, VC2, VC3};
    PgpHeader() {}
    PgpHeader(VChannel vc, unsigned dest, unsigned tid) {_data = (tid<<8) | 
	(dest&1)<<2 | vc;}
    unsigned tid() const {return (_data&0xffffff00)>>8;}
    VChannel vc() const {return (VChannel)(_data&0x3);}
    unsigned const destination() {return ((_data&4)>>2);}
  private:
    unsigned _data;
  };
  
  class RegHeader {
  public:
    RegHeader() {}
    enum Opcode  {Read, Write, Set, Clear};
    RegHeader(unsigned address, Opcode oc) {_data = (oc<<30) | address;}
    RegHeader(unsigned address, Opcode oc, unsigned writeData) {
      new(this) RegHeader(address, oc);
      data = writeData;
    }
    unsigned address() {return _data&0xffffff;}
    Opcode opcode() {return (Opcode)((_data>>30)&0x3);}
    unsigned timeout() {return _status&0x20000;}
    unsigned fail() {return _status&0x10000;}
    unsigned status() {return _status;}
  private:
    unsigned _data;
  public:
    unsigned data;
  private:
    unsigned _status;
  };

  class RegTxHeader {
  public:
    RegTxHeader() {}
    PgpHeader pgpHeader;
    RegHeader  regHeader;
    RegTxHeader(unsigned tid, unsigned addr, RegHeader::Opcode oc) :
      pgpHeader(PgpHeader::VC1, 0, tid), regHeader(addr, oc) {}
    RegTxHeader(unsigned tid, unsigned addr, RegHeader::Opcode oc,
		unsigned data) {
      new(this) RegTxHeader(tid, addr, oc);
      regHeader.data=data;
    }
  };
  
  class RegRxHeader {
  public:
    RegRxHeader() {}
    PgpHeader pgpHeader;
    RegHeader regHeader;
  };
  class CmdHeader {
  public:
    CmdHeader() {}
    CmdHeader( unsigned char oc) {_data = (unsigned)oc;}
    unsigned char opcode() {return (unsigned char)(_data&0xff);}
  private:
    unsigned _data;
  };

  class CmdTxHeader {
  public:
    CmdTxHeader() {}
    PgpHeader pgpHeader;
    CmdHeader  cmdHeader;
    CmdTxHeader( unsigned char oc, unsigned tid=0) :
      pgpHeader(PgpHeader::VC0, 0, tid), cmdHeader(oc) {}
  };
  class BlockWriteHeader{
  public:
    BlockWriteHeader(bool handshake){
      _data= handshake ? 1: 0;
    }
  private:
    unsigned _data;
  };
  class BlockWriteTxHeader {
  public:
    PgpHeader pgpHeader;
    BlockWriteHeader blockHeader;
    BlockWriteTxHeader(bool handshake ) :
      pgpHeader(PgpHeader::VC3, 0, 0),blockHeader(handshake){}
  };
  
}

