#ifndef SERIALPGP_HH
#define SERIALPGP_HH

#include "HW/SerialIF.hh"
#include <fstream>

class SerialPgp: public SerialIF {
public:
  SerialPgp();
private:
  void Send(BitStream* bs, int opt);
  void SetChannelMask(unsigned linkmask);
  void SetChannelInMask(unsigned linkmask);
  void SetChannelOutMask(unsigned linkmask);
  int EnableTrigger(bool on);
};


#endif
