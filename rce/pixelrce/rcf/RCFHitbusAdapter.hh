#ifndef RCFHITBUSADAPTER_HH
#define RCFHITBUSADAPTER_HH

#include <stdint.h>
#include <RCF/RCF.hpp>
#include "rcf/HitbusModuleConfig.hh"

RCF_BEGIN(I_RCFHitbusAdapter, "I_RCHitbusAdapter")
RCF_METHOD_R1(int32_t, RCFdownloadConfig, ipc::HitbusModuleConfig)
RCF_END(I_RCFHitbusAdapter)

    class RCFHitbusAdapter 
    {
      virtual int32_t RCFdownloadConfig(ipc::HitbusModuleConfig config)=0;

    };

#endif
