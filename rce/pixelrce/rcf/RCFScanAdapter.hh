#ifndef RCFSCANADAPTER_HH
#define RCFSCANADAPTER_HH

#include <RCF/RCF.hpp>

RCF_BEGIN(I_RCFScanAdapter, "I_RCFScanAdapter")
RCF_METHOD_V0(void, RCFstartScan)
RCF_METHOD_V0(void, RCFpause)
RCF_METHOD_V0(void, RCFresume)
RCF_METHOD_V0(void, RCFwaitForData)
RCF_METHOD_V0(void, RCFstopWaiting)
RCF_METHOD_V0(void, RCFabort)
RCF_METHOD_R0(int32_t, RCFgetStatus)
RCF_END(I_RCFScanAdapter)

class RCFScanAdapter {
public:
  virtual void RCFstartScan()=0;
  virtual void RCFpause()=0;
  virtual void RCFresume()=0;
  virtual void RCFwaitForData()=0;
  virtual void RCFstopWaiting()=0;
  virtual int32_t RCFgetStatus()=0;
  virtual void RCFabort()=0;
};

#endif
