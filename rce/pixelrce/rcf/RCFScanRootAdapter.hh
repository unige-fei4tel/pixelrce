#ifndef RCFSCANROOTADAPTER_HH
#define RCFSCANROOTADAPTER_HH

#include "rcf/Callback.hh"
#include "rcf/ScanOptions.hh"
#include <vector>
#include <string>

RCF_BEGIN(I_RCFScanRootAdapter, "I_RCFScanRootAdapter")
RCF_METHOD_R1(int32_t, RCFconfigureScan, ipc::ScanOptions )
RCF_METHOD_R1(std::vector<std::string> , RCFgetHistoNames, std::string )
RCF_METHOD_R0(std::vector<std::string> , RCFgetPublishedHistoNames)
RCF_METHOD_R0(uint32_t , RCFnEvents)
RCF_METHOD_V0(void , RCFresynch)
RCF_METHOD_V1(void , RCFconfigureCallback, ipc::Priority)
RCF_METHOD_V1(void , RCFsetRceNumber, int32_t)
RCF_END(I_RCFScanRootAdapter)

class RCFScanRootAdapter{
public:
  virtual int32_t RCFconfigureScan(ipc::ScanOptions options)=0;
  virtual std::vector<std::string> RCFgetHistoNames(std::string reg)=0;
  virtual std::vector<std::string> RCFgetPublishedHistoNames()=0;
  virtual uint32_t RCFnEvents()=0;
  virtual void RCFresynch()=0;
  virtual void RCFsetRceNumber(int32_t)=0;
  virtual void RCFconfigureCallback(ipc::Priority pr)=0;
};

#endif
