#ifndef TOTCALIB_ANALYSIS_HH
#define TOTCALIB_ANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include "analysis/CfgFileWriter.hh"
#include "server/PixScan.hh"

class TFile;
class TH2;
namespace RCE{
  class PixScan;
}
class ConfigGui;

class TotCalibAnalysis: public CalibAnalysis{
public:
  TotCalibAnalysis(): CalibAnalysis(){}
  ~TotCalibAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
