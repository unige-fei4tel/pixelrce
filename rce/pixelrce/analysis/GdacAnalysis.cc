#include "analysis/GdacAnalysis.hh"
#include "server/PixScan.hh"
#include "server/ConfigGui.hh"
#include "config/FEI3/Module.hh"
#include "config/FEI4/Module.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"

void GdacAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  gStyle->SetOptFit(111);
  std::map<int, std::vector<TH1D*> > histmap;
  file->cd("loop2_0");
  TIter nextkey(gDirectory->GetListOfKeys()); // GDAC settings
  TKey *key;
  int index=0;
  while ((key=(TKey*)nextkey())) {
    gDirectory->cd(key->GetName());


    TIter snextkey(gDirectory->GetListOfKeys()); // histograms
    TKey *skey;
    while ((skey=(TKey*)snextkey())) {
      std::string name(skey->GetName());
      boost::cmatch matches;
      boost::regex re("_(\\d+)_Mean");
      boost::regex re2("Mean");

      if(boost::regex_search(name.c_str(), matches, re)){
	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	int id=strtol(match.c_str(),0,10);
	std::string chi2HistoName = boost::regex_replace (name, re2, "ChiSquare");
	TH2* histo = (TH2*)skey->ReadObj();
	TH1* chi2Histo=(TH1*)gDirectory->Get(chi2HistoName.c_str());
	assert(chi2Histo!=0);
	TH1* gdacValHisto=(TH1*)gDirectory->Get(Form("GDAC_settings_Mod_%d_it_%d", id, index)); 
	int numfrontend=0;
	if(findFEType(cfg, id)=="FEI3") numfrontend = FEI3::Module::N_FRONTENDS;
	else numfrontend = FEI4::Module::N_FRONTENDS;
	
	char name[128];
	char title[128];
	int gdac=0;
	if(histmap.find(id)==histmap.end()){
	  for (int k=0;k<numfrontend;k++){
	    if(numfrontend==1){
	      sprintf(name, "GDAC_mod_%d", id);
	      sprintf(title, "GDAC vs charge module %d at %s", id, findFieldName(cfg, id));
	    }else{
	      sprintf(name, "GDAC_mod_%d_frontent_%i", id, k);
	      sprintf(title, "GDAC vs charge module %d frontend %i at %s", id, k, findFieldName(cfg, id));
	    }
	    TH1D *hold =new TH1D (name, title, 256, -.5, 255.5);
	    histmap[id].push_back(hold);
	  }
	}
	for (int k=0;k<numfrontend;k++){

	  if(gdacValHisto!=0)gdac=gdacValHisto->GetBinContent(k+1);
	  else gdac=int(scan->getLoopVarValues(1)[index]);

	  if(numfrontend==1){
	    sprintf(name, "threshold_id_%d_step_%d_gdac_%d", id, index, gdac);
	    sprintf(title, "Threshold id=%d at %s step=%d gdac=%d", id, findFieldName(cfg, id), index, gdac);
	  }else{
	    sprintf(name, "threshold_id_%d_frontend_%d_step_%d_gdac_%d", id, k, index, gdac);
	    sprintf(title, "Threshold id=%d at %s frontend=%d step=%d gdac=%d", id, findFieldName(cfg, id), k, index, gdac);
	  }
	  TH1D *fithist = new TH1D(name, title, 100, 0, 10000);
	  
	  int histnum = histo->GetNbinsX()/numfrontend;
	  for (int i=histnum*k+1;i<=histnum*(k+1);i++){
	    for(int j=1;j<=histo->GetNbinsY();j++){
	      if(chi2Histo->GetBinContent(i,j)!=0&&chi2Histo->GetBinContent(i,j)<5){
		fithist->Fill(histo->GetBinContent(i,j));
	      }
	    }
	  }
	  fithist->Fit("gaus","lq");
	  double val=fithist->GetFunction("gaus")->GetParameter(1);
	  double err=fithist->GetFunction("gaus")->GetParError(1);
	  if(val>100 && val<30000 && err>0 && err<5000 && fithist->GetEntries()>100){ //arbitrary criteria
	    histmap[id][k]->SetBinContent(gdac+1, val);
	    histmap[id][k]->SetBinError(gdac+1, err);
	  }
	  anfile->cd();
	  fithist->Write();
	  fithist->SetDirectory(gDirectory);
	  file->cd("loop2_0");
	  gDirectory->cd(key->GetName());
	}
      }
    }
    file->cd("loop2_0");
    index++;
  }
  anfile->cd();
  for(std::map<int, std::vector<TH1D*> >::iterator it=histmap.begin();it!=histmap.end();it++){
    int id=it->first;
    PixelConfig *confb=findConfig(cfg, id);
    int numfrontend=0;
    if(findFEType(cfg, id)=="FEI3") numfrontend = FEI3::Module::N_FRONTENDS;
    else numfrontend = FEI4::Module::N_FRONTENDS;
    for (int k=0;k<numfrontend;k++){
      unsigned short confval=interpolate(id, scan->getThresholdTargetValue(), histmap[id][k]);
      confb->setGDac(k, confval);
      histmap[id][k]->Write();
      histmap[id][k]->SetDirectory(gDirectory);
    }
    if(scan->getScanType()==RCE::PixScan::GDAC_TUNE){
      std::vector<float> varValues=scan->getLoopVarValues(2);
      if(varValues.size()>=1){
	int val=(int)varValues[0];
	int ncol, nrow, nchip;
	if(confb->getType()=="FEI3"){
	  ncol=18;
	  nrow=160;
	  nchip=16;
	}else{
	  ncol=80;
	  nrow=336;
	  nchip=1;
	}
	for (int chip=0;chip<nchip;chip++){
	  for (int col=0;col<ncol;col++){
	    for (int row=0;row<nrow;row++){
	      confb->setThresholdDac(chip, col, row, val);
	    }
	  }
	}
	
      }else{
	std::cout<<"No outer loop for GDAC tuning. Not setting up TDAC values"<<std::endl;
      }
    }
    writeConfig(anfile,runno);
  }
  
  if(configUpdate()){
    writeTopFile(cfg, anfile, runno);
  }
}
 
unsigned short GdacAnalysis::interpolate(int id, int target, TH1D* histo){
  double lower=-1,higher=-1;
  int lowerbin=0, higherbin=0;
  int firstbin=-1, lastbin=-1;
  for(int i=1;i<=histo->GetNbinsX();i++){
    if(histo->GetBinContent(i)!=0){
      if(firstbin==-1)firstbin=i-1;
      lastbin=i-1;
      if(target>=histo->GetBinContent(i)){
	lower=histo->GetBinContent(i);
	lowerbin=i-1;
      }else{
	higher=histo->GetBinContent(i);
	higherbin=i-1;
	break;
      }
    }
  }
  unsigned short confval=0;
  if(lower==-1){
    std::cout<<"Target is lower than lowest scan point "<<firstbin<<" for module id="<<id<<std::endl;
    confval=firstbin;
  }else if (higher==-1){
    std::cout<<"Target is larger than highest scan point "<<lastbin<<" for module id="<<id<<std::endl;
    confval=lastbin;
  }else{
    double m=(higher-lower)/(higherbin-lowerbin);
    if(m==0){
      std::cout<<"Slope is 0, no interpolation possible for module id="<<id<<std::endl;
      confval=lowerbin;
    }else{
      double n=higher-m*higherbin;
      double x=(target-n)/m;
      std::cout<<"vthin_altFine for module id="<<id<<" at a target threshold of "<<target<<" electrons is "<<x<<std::endl;
      confval=x;
    }
  }
  return confval;
}
