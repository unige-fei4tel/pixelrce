#ifndef MULTITRIGNOISEANALYSIS_HH
#define MULTITRIGNOISEANALYSIS_HH

#include "analysis/CalibAnalysis.hh"
#include <map>

class ConfigGui;
class TFile;
class TH2;
class TH1D;

namespace RCE{
  class PixScan;
}

class MultiTrigNoiseAnalysis: public CalibAnalysis{
public:
  MultiTrigNoiseAnalysis(): CalibAnalysis(){}
  ~MultiTrigNoiseAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
