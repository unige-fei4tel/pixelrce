#ifndef DELAYSCANDATAHANDLER_HH
#define DELAYSCANDATAHANDLER_HH

#include <vector>
#include <map>
#include <assert.h>

#include "dataproc/AbsDataHandler.hh"
#include <boost/property_tree/ptree_fwd.hpp>
class DataCond;


class DelayScanDataHandler: public AbsDataHandler{
public:
  DelayScanDataHandler(AbsDataProc* dataproc, DataCond& datacond);
  virtual ~DelayScanDataHandler(){}
  void handle(unsigned link, unsigned* data, int size);
};
#endif
