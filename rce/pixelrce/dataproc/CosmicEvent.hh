#ifndef COSMICEVENT_HH
#define COSMICEVENT_HH

#include <vector>
#include <stdlib.h>
#include <iostream>
#include <fstream>

namespace eudaq{
  class DetectorEvent;
  class RawDataEvent;
  class FileSerializer;
}

class CosmicEvent{
public:
  CosmicEvent(unsigned runno, unsigned l1a, unsigned nMod, std::vector<int>& rces);
  ~CosmicEvent();
  void appendCtelData(unsigned *data, unsigned size);
  void appendDutData(unsigned *data, unsigned size, unsigned rceIndex);
  void setTdcData(unsigned firstword, unsigned *data, unsigned rceIndex);
  void incrementTrigger(unsigned mod, int ntrg, unsigned bxfirst, unsigned bxlast, unsigned rceIndex);

  bool consistent(CosmicEvent* ev);

  unsigned getL1A(){return m_l1a;}
  int getNmod(){ return m_nMod;}
  unsigned getRunNo(){ return m_runno;}
  unsigned getFirstModWithRceIndex(unsigned rceIndex);


  CosmicEvent* makeDummyEvent(int runno, int tlu, int evtno);
  void writeEvent(bool monitor, eudaq::FileSerializer *efile, std::ofstream* pfile, unsigned& evtno); 
  void print();
  static void synch(bool on){m_synch=on;};

private:

  bool m_tdctrig;
  std::vector<int> m_ntrig;
  std::vector<unsigned> m_bx;
  std::vector<unsigned> m_bxlast;
  std::vector<unsigned> m_modRceIndex;
  eudaq::RawDataEvent* m_ctel;
  eudaq::RawDataEvent* m_dut;
  unsigned m_l1a;
  unsigned m_nMod;
  std::vector<unsigned long long> m_timestamp;
  std::vector<unsigned> m_tdcbx;
  unsigned m_runno;
  std::vector<int> m_rces;
  unsigned m_nRce;
  static int m_lastTLU;
  static bool m_synch;

};

#endif
