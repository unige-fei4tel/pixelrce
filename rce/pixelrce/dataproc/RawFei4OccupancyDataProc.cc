#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/RawFei4OccupancyDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"
#include "dataproc/fit/FitFactory.cc"
#include "config/FEI4/FEI4ARecord.hh"

int RawFei4OccupancyDataProc::fit(std::string fitfun) {

  FitFactory<char, char> dfac; //template parameters are the type of the histogram and error
  std::cout << "Running: " << fitfun << std::endl;
  if(fitfun.size()>=6 && fitfun.substr(0,6)=="SCURVE") {
    //    m_fit=dfac.createFit("ScurveLikelihoodFloat",m_configIF, m_histo_occ,m_vcal,m_nTrigger);
    m_fit=dfac.createFit("ScurveLikelihoodFastInt",m_configIF, m_histo_occ,m_vcal,m_nTrigger);
    int opt=0;
    if(fitfun.size()>=13&&fitfun.substr(7,6)=="NOCONV")opt=AbsFit::NOCONV;
    if(fitfun.size()>=12&&fitfun.substr(7,5)=="XTALK")opt=AbsFit::XTALK;
    std::cout<<"Scurve option is "<<opt<<std::endl;
    m_fit->doFit(opt);
  }
  return 0;
}

int RawFei4OccupancyDataProc::processData(unsigned link, unsigned *buffer, int buflen){
  //m_timer.Start();
  if(m_counter++==0)usleep(1);
  if(buflen==0)return 0;
  int nL1A=0;
  int module=m_linkToIndex[link];
  RceHisto2d<char, char>* histo=m_histo_occ[module][m_currentBin];
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  ::FEI4::FEI4ARecord rec;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if(rec.isData()){ //includes empty record type
      //there are potentially 2 hits in one data record
      //fprintf(m_flog, "%x\n", rec.getUnsigned());
      if(((rec.getTotBottom())>>1)!=0x7){ //above threshold
	unsigned int row=rec.getRow()-1;
	unsigned int col=rec.getColumn()-1;
	if(row<N_ROW && col<N_COL) {
	  histo->incrementFast(row,col); 
	  //std::cout<<"Hit at "<<row<<" "<<col<<std::endl;
	}
      }
      if(((rec.getTotTop())>>1)!=0x7){ //above threshold
	unsigned int row=rec.getRow();
	unsigned int col=rec.getColumn()-1;
	if(row<N_ROW && col<N_COL) {
	  histo->incrementFast(row,col); 
	  //std::cout<<"Hit at "<<row<<" "<<col<<std::endl;
	}
      }
    } else if(rec.isDataHeader()){
      //fprintf(m_flog, "%x\n", rec.getUnsigned());
      nL1A++;
    }else if(rec.isServiceRecord()){
      unsigned int moduleId=m_info[module].getId();
      //if(rec.getErrorCode()!=10 && (rec.getErrorCode()!=16 || (rec.getErrorCount()&0x200)) )
	 printf("Service record FE %d . Error code: %d. Count: %d \n", moduleId, rec.getErrorCode(), rec.getErrorCount());
      if( rec.getErrorCode()<32)m_errhist[module]->fill(rec.getErrorCode(), rec.getErrorCount());
      //header=false;
    }else if(rec.isValueRecord()){ //val rec without addr rec
      //      printf("Value record: %04x.\n",rec.getValue());
    }else if(rec.isAddressRecord()){ // address record
      std::cout<<"FE "<<m_info[module].getId()<<": Address record for ";
      if(rec.isGlobal())std::cout<<" global register ";
      else std::cout<<" shift register ";
      std::cout<<rec.getAddress()<<std::endl;
      std::cout<<"This should not happen."<<std::endl;
    }else{
      std::cout<<"FE "<<m_info[module].getId()<<": Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      //return FEI4::FEI4ARecord::BadRecord;
      return 0;
    }
    bytepointer+=3;
  }
  //m_timer.Stop();
  return nL1A;
}

RawFei4OccupancyDataProc::RawFei4OccupancyDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif),m_fit(0), m_counter(0) {
  std::cout<<"Raw FEI4 Occupancy Data Proc"<<std::endl;
  //m_flog=fopen("/nfs/dbg.txt","w");
  try{ //catch bad scan option parameters
    m_nLoops = scanOptions->get<int>("nLoops");
    /* there is at least one parameter loop */
    /* TODO: fix in scan control */
    m_nPoints=1;
    if(m_nLoops>0){         
      m_nPoints=scanOptions->get<int>("scanLoop_0.nPoints");
      for(int i=0;i<m_nPoints;i++) {
	char pointname[10];
	sprintf(pointname,"P_%d",i);
	int vcal=scanOptions->get<int>(std::string("scanLoop_0.dataPoints.")+pointname);
	//std::cout << "point vcal " << vcal << std::endl;
	m_vcal.push_back(vcal);
      }
    }
    m_nTrigger=scanOptions->get<int>("trigOpt.nEvents");

    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      std::vector<RceHisto2d<char, char>* > vh;
      m_histo_occ.push_back(vh);
      char name[128];
      char title[128];
      RceHisto2d<char, char> *histo;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      /* retrieve scan points - Vcal steps in this case */
      sprintf(name, "Mod_%d_FEI4_Errors_Proc", moduleId);
      sprintf(title, "Module %d at %s FEI4 Errors", moduleId, moduleName.c_str());
      m_errhist.push_back(new RceHisto1d<int, int>(name, title, 32, -.5, 31.5));
      std::cout<<"Creating Occupancy histograms."<<std::endl;
      for (int point=0;point<m_nPoints;point++){
	sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());
	sprintf(name,"Mod_%d_Occupancy_Point_%03d", moduleId,point);
	histo=new RceHisto2d<char, char>(name,title,rows,0,rows,cols,0,cols, true);
	if(m_info[module].getNFrontends()==1)histo->setAxisTitle(1,"Column");
	else histo->setAxisTitle(1,"FE*N_COL+Column");
	histo->setAxisTitle(0, "Row");
	m_histo_occ[module].push_back(histo);
      }
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
  //m_timer.Reset();
}

RawFei4OccupancyDataProc::~RawFei4OccupancyDataProc(){
  //fclose(m_flog);
  if(m_fit) delete m_fit;
  for (size_t module=0;module<m_histo_occ.size();module++){
    delete m_errhist[module];
    for(size_t i=0;i<m_histo_occ[module].size();i++){
      delete m_histo_occ[module][i];
    }
  }

  //m_timer.Print("RawFei4OccupancyDataProc");
}
  

