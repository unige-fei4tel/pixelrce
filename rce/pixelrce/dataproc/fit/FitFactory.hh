#ifndef FITFACTORY_HH
#define FITFACTORY_HH

#include "dataproc/fit/AbsFit.hh"
#include "util/RceHisto2d.cc"
#include "util/RceHisto1d.cc"


template<typename H, typename HE>
class FitFactory{
public:
  FitFactory(){}

  AbsFit *createFit(const char *type,ConfigIF *cif,std::vector<std::vector<RceHisto2d<H, HE>*> > &histo,std::vector<int> &vcal,int nTrigger,const char* name="");
  AbsFit *createFit(const char *type,ConfigIF *cif,std::vector<std::vector<RceHisto1d<H, HE>*> > &histo,std::vector<int> &vcal,int nTrigger,const char* name="");

};
#endif
