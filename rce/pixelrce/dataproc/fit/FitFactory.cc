#ifndef FITFACTORY_CC
#define FITFACTORY_CC

#include "dataproc/fit/FitFactory.hh"

#ifdef INCLUDE_FLOAT_FIT
#include "dataproc/fit/FitScurveLikelihoodFloat.cc"
#endif
#include "dataproc/fit/FitScurveLikelihoodInt.cc"
#include "dataproc/fit/FitScurveLikelihoodFastInt.cc"

template<typename H, typename HE>
AbsFit *FitFactory<H, HE>::createFit(const char * type,ConfigIF *cif, std::vector<std::vector<RceHisto2d<H, HE>*> > &histo, std::vector<int> &vcal,int nTrigger, const char* name)
{ 
#ifdef INCLUDE_FLOAT_FIT
  if(std::string(type)=="ScurveLikelihoodFloat") return new FitScurveLikelihoodFloat<H, HE>(cif,histo,vcal,nTrigger);
#endif
  if(std::string(type)=="ScurveLikelihoodFastInt") return new FitScurveLikelihoodFastInt<H, HE>(cif,histo,vcal,nTrigger,name);
  else if (std::string(type)=="ScurveLikelihoodInt") return new FitScurveLikelihoodInt<H, HE>(cif,histo,vcal,nTrigger,name);
  else
    return 0;
}
template<typename H, typename HE>
AbsFit *FitFactory<H, HE>::createFit(const char * type,ConfigIF *cif, std::vector<std::vector<RceHisto1d<H, HE>*> > &histo, std::vector<int> &vcal,int nTrigger, const char* name){
  return 0;
}

#endif
