#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "dataproc/NoiseOccupancyDataProc.hh"
#include "config/ConfigIF.hh"
#include "config/FormattedRecord.hh"


int NoiseOccupancyDataProc::processData(unsigned link, unsigned *data, int size){
  //std::cout<<"Process data of size "<<size<<std::endl;
  if(m_counter++==0)usleep(1);
  int module=m_linkToIndex[link];
  for (int i=0;i<size;i++){
    FormattedRecord current(data[i]);
    if (current.isHeader()){
      unsigned l1id = current.getL1id();
      //printf("bcid : %x \n", bcid);
      //printf("l1id : %x \n", l1id);
      if (l1id != m_l1id_last[module])
	{
	  m_l1id_last[module] = l1id;
	  m_histo_eventCount[module]->incrementFast(0);
	}
      //printf("bcidafter : %x \n", bcid);
    }
    if (current.isData()){
      unsigned int chip=current.getFE();
      unsigned int col=current.getCol();
      unsigned int row=current.getRow();
      //unsigned int tot=current.getToT();
      //printf("Hit col=%d row=%d tot=%d\n",col, row, tot);
      if(chip==0){
	if((row<(unsigned)m_info[module].getNRows()) && (col<(unsigned)m_info[module].getNColumns())) {
	  m_histo_occ[module]->incrementFast(col,row);
	}
      }else{
	if(chip<(unsigned)m_info[module].getNFrontends() && 
	   (row<(unsigned)m_info[module].getNRows()) && 
	   (col<(unsigned)m_info[module].getNColumns())) {
	  m_histo_occ[module]->incrementFast(chip*m_info[module].getNColumns()+col,row);
	}
      }
    }
  }
  return 0;
}

NoiseOccupancyDataProc::NoiseOccupancyDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif), m_counter(0) {
  std::cout<<"Occupancy Data Proc"<<std::endl;
  try{ //catch bad scan option parameters
    m_l1id_last = new unsigned[m_configIF->getNmodules()];
    for(unsigned i=0;i<m_configIF->getNmodules();i++)m_l1id_last[i]=0xffff;

    for (unsigned int module=0;module<m_configIF->getNmodules();module++){
      char name[128];
      char title[128];
      RceHisto2d<int, int> *histo;
      RceHisto1d<int, int> *histo_ec;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();
      std::cout<<"Creating Occupancy histograms."<<std::endl;
      sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_NoiseOccupancy", moduleId);
      histo=new RceHisto2d<int, int>(name,title,cols,0,cols,rows,0,rows);
      if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
      else histo->setAxisTitle(0,"FE*N_COL+Column");
      histo->setAxisTitle(1, "Row");
      m_histo_occ.push_back(histo);
      
      sprintf(title,"Number of Events Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_nEvents", moduleId);
      histo_ec=new RceHisto1d<int,int>(name, title, 1,0,1);
      histo_ec->setAxisTitle(0, "n Event Bin");
      m_histo_eventCount.push_back(histo_ec);
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    std::cout<<"ERROR: Bad ptree param "<<ex.what()<<std::endl;
    assert(0);
  }
}

NoiseOccupancyDataProc::~NoiseOccupancyDataProc(){
  delete [] m_l1id_last;
  for (size_t module=0;module<m_histo_occ.size();module++)delete m_histo_occ[module];
  for (size_t module=0;module<m_histo_occ.size();module++)delete m_histo_eventCount[module];

}
  

