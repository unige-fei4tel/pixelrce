#ifndef DATAPROCFACTORY_HH
#define DATAPROCFACTORY_HH

#include "dataproc/AbsDataProc.hh"
#include "dataproc/AbsReceiver.hh"
#include "dataproc/AbsDataHandler.hh"
#include "config/ConfigIF.hh"
#include <boost/property_tree/ptree_fwd.hpp>

class DataCond;

class DataProcFactory{
public:
  DataProcFactory(){};
  AbsDataProc* createDataProcessor(const char* type, ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  AbsDataHandler* createDataHandler(const char* type, AbsDataProc *dataproc, DataCond& datacond,
			       ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  AbsReceiver *createReceiver(const char* type, AbsDataHandler* handler, boost::property_tree::ptree* scanOptions);


};
  

#endif
