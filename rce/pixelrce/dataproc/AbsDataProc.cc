#include "dataproc/AbsDataProc.hh"
#include "config/ConfigIF.hh"
#include <stdio.h>

AbsDataProc::AbsDataProc(ConfigIF* cif):m_configIF(cif){
  m_currentMaskStage=0;
  m_currentBin=0;
  m_info.clear();
  int maxlink=0;
  m_nEvents=0;
  for (unsigned int i=0;i<m_configIF->getNmodules();i++){
    m_info.push_back(m_configIF->getModuleInfo(i));
    if(m_info[i].getOutLink()>maxlink)maxlink=m_info[i].getOutLink();
  }
  maxlink++;
  m_linkToIndex=new int[maxlink];
  for (unsigned int i=0;i<m_configIF->getNmodules();i++){
    m_linkToIndex[m_info[i].getOutLink()]=i;
  }
  
}

AbsDataProc::~AbsDataProc(){
  delete [] m_linkToIndex;
}
int AbsDataProc::changeBin(int i){
  m_currentBin=i;
  return 0;
}

int AbsDataProc::fit(std::string fitfunc){
  return 0;
}
int AbsDataProc::setMaskStage(int stage){
  m_currentMaskStage=stage;
  return 0;
}

int AbsDataProc::processData(unsigned link, unsigned* data, int size){
  return 0;
}

