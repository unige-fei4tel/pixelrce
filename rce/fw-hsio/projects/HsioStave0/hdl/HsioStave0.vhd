-------------------------------------------------------------------------------
-- Title         : BNL ASIC Test FGPA, Top Level
-- Project       : LCLS Detector, BNL ASIC
-------------------------------------------------------------------------------
-- File          : HsioCosmic.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 07/21/2008
-------------------------------------------------------------------------------
-- Description:
-- Top level logic for BNL ASIC test FPGA.
-------------------------------------------------------------------------------
-- Copyright (c) 2008 by Ryan Herbst. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
USE ieee. std_logic_1164.ALL;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
USE work.ALL;

entity HsioStave0 is 
   port ( 

      -- PGP Crystal Clock Input, 156.25Mhz
      iPgpRefClkP   : in    std_logic;
      iPgpRefClkM   : in    std_logic;

      -- System clock 125 MHz clock input
      iMainClkP     : in    std_logic;
      iMainClkN     : in    std_logic;

      -- PGP Rx/Tx Lines
      iMgtRxN       : in    std_logic;
      iMgtRxP       : in    std_logic;
      oMgtTxN       : out   std_logic;
      oMgtTxP       : out   std_logic;
      iMgtRxN2      : in    std_logic;
      iMgtRxP2      : in    std_logic;
      oMgtTxN2      : out   std_logic;
      oMgtTxP2      : out   std_logic;

     -- ATLAS Pixel module pins
      serialin     : in std_logic_vector(15 downto 0);
      serialout    : out std_logic_vector(15 downto 0);
      serialout_p  : out std_logic_vector(15 downto 0);
      serialout_n  : out std_logic_vector(15 downto 0);
      serialin_p   : in std_logic_vector(15 downto 0);
      serialin_n   : in std_logic_vector(15 downto 0);
      discinP      : in std_logic_vector(3 downto 0);
      HITOR        : in std_logic;
--      discinN    : in std_logic_vector(1 downto 0);
      clkout40     : out std_logic;
      refclk_p     : out std_logic;
      refclk_n     : out std_logic;
      xclk_p       : out std_logic;
      xclk_n       : out std_logic;

      -- Reset button
      iResetInL     : in    std_logic;
      -- Reload firmware
      oReload       : out std_logic;
      iExtreload    : in std_logic;
      -- LED Display
      oDispClk      : out   std_logic;
      oDispDat      : out   std_logic;
      oDispLoadL    : out   std_logic_vector(1 downto 0);
      oDispRstL     : out   std_logic;

      -- Debug
      oDebug        : out std_logic_vector(7 downto 0);

      -- Eudet trigger
      iExttrigger     : in std_logic;
      iExtrst         : in std_logic;
      oExtbusy        : out std_logic;
      oExttrgclk      : out std_logic;

      --HSIO trigger IF
      iHSIOtrigger    : in std_logic_vector(1 downto 0);
      oHSIOtrigger    : out std_logic;
      oHSIObusy       : out std_logic;
      iHSIObusy       : in std_logic;

      oExtrstP        : out std_logic;
      oExtrstN        : out std_logic;
      oExttriggerP    : out std_logic;
      oExttriggerN    : out std_logic;
      

      -- Misc Signals
      oPdBuff0      : out   std_logic;
      oPdBuff1      : out   std_logic;
      oPdBuff3      : out   std_logic;
      oPdBuff4      : out   std_logic;
      oLemoA        : out   std_logic;
      iLemoB        : in    std_logic;

      -- Transmitter enable
      transDis1   : out   std_logic;
      transDis2   : out   std_logic;

      -- Doric resets
      doricreset1:   out std_logic;
      doricreset2:   out std_logic;
      --unused_p   : out std_logic_vector(3 downto 0);
      --unused_n   : out std_logic_vector(3 downto 0);

      -- Fiber modules
      TX_EN      : out std_logic;
      TX_DIS     : out std_logic;
      TX_RESET   : out std_logic;
      TX_FAULT   : in std_logic;
      RX0_RX_EN  : out std_logic;
      RX0_EN_SD  : out std_logic;
      RX0_SD     : in std_logic;
      RX0_SQ_EN  : out std_logic;
      RX1_RX_EN  : out std_logic;
      RX1_EN_SD  : out std_logic;
      RX1_SD     : in std_logic;
      RX1_SQ_EN  : out std_logic
   );
end HsioStave0;


-- Define architecture for top level module
architecture HsioStave0 of HsioStave0 is 

   -- Synthesis control attributes
   attribute syn_useioff    : boolean;
   attribute syn_useioff    of HsioStave0 : architecture is true;
   attribute xc_fast_auto   : boolean;
   attribute xc_fast_auto   of HsioStave0 : architecture is false;
   attribute syn_noclockbuf : boolean;
   attribute syn_noclockbuf of HsioStave0 : architecture is true;

   -- IO Pad components
   component IBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   component OBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   component OBUFDS
     generic( IOSTANDARD: STRING:= "LVDS_25";
              SLEW: STRING:="FAST");
     port ( O : out std_logic; OB : out std_logic; I  : in std_logic );
   end component;

   -- Input LVDS with termination
   component IBUFDS 
      generic ( DIFF_TERM : boolean := TRUE;
                IOSTANDARD: STRING := "LVDS_25"); 
      port    ( O : out std_logic; I  : in  std_logic; IB : in std_logic ); 
   end component;

   -- Xilinx global clock buffer component
   component BUFGMUX 
      port ( 
         O  : out std_logic; 
         I0 : in  std_logic;
         I1 : in  std_logic;  
         S  : in  std_logic 
      ); 
   end component;

   component IDELAYCTRL
   port ( RDY    : out std_logic;
          REFCLK : in std_logic;
          RST    : in std_logic
        );
   end component;

component IDELAY
        generic (IOBDELAY_TYPE : string := "DEFAULT"; --(DEFAULT, FIXED, VARIABLE)
                 IOBDELAY_VALUE : integer := 0 --(0 to 63)
                 );
        port (
              O : out STD_LOGIC;
              I : in STD_LOGIC;
              C : in STD_LOGIC;
              CE : in STD_LOGIC;
              INC : in STD_LOGIC;
              RST : in STD_LOGIC
             );
end component;
--component ila
   --PORT (
     --CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
     --CLK : IN STD_LOGIC;
     --DATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
     --TRIG0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0));
--
--end component;
 --component icon
   --PORT (
     --CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
--
 --end component;


   -- Local signals
   signal resetInL     : std_logic;
   signal tmpClk250    : std_logic;
   signal sysClk125    : std_logic;
   signal sysRst125    : std_logic;
   signal sysRst250    : std_logic;
   signal sysClk250    : std_logic;
   signal refClock     : std_logic;
   signal pgpClk       : std_logic;
   signal pgpClk90     : std_logic;
   signal pgpReset     : std_logic;
   signal clk320       : std_logic;
   signal oClk320       : std_logic;
   signal reload1      : std_logic;
   signal reload2      : std_logic;
   signal reload       : std_logic;
   signal resetOut     : std_logic;
   signal resetOut1    : std_logic;
   signal resetOut2    : std_logic;
   signal mgtRxN       : std_logic;
   signal mgtRxP       : std_logic;
   signal mgtTxN       : std_logic;
   signal mgtTxP       : std_logic;
   signal mgtRxN2      : std_logic;
   signal mgtRxP2      : std_logic;
   signal mgtTxN2      : std_logic;
   signal mgtTxP2      : std_logic;
   signal dispClk      : std_logic;
   signal dispDat      : std_logic;
   signal dispLoadL    : std_logic_vector(1 downto 0);
   signal dispRstL     : std_logic;
   signal debug        : std_logic_vector(7 downto 0);
   signal sysClk125i   : std_logic;
   signal mainClk      : std_logic;
   signal clk0         : std_logic;
   signal clkin        : std_logic;
   signal halfclock    : std_logic;
   signal quarterclock : std_logic;
   signal exttrigger   : std_logic;
   signal extrst       : std_logic;
   signal exttriggero  : std_logic;
   signal extrsto      : std_logic;
   signal extbusy      : std_logic;
   signal exttrgclk    : std_logic;
   signal hsiobusyb    : std_logic;
   signal hsiobusyinb  : std_logic;
   signal hsiobusyb1   : std_logic;
   signal hsiobusyb2   : std_logic;
   signal hsiotriggerb : std_logic_vector(1 downto 0);
   signal doricresetb1 : std_logic;
   signal doricresetb2 : std_logic;
   signal serialoutb   : std_logic_vector(7 downto 0);
   signal serialoutb1  : std_logic_vector(15 downto 0);
   signal serialoutb2  : std_logic_vector(15 downto 0);
   signal serialinb    : std_logic_vector(15 downto 0);
   signal serialinb1   : std_logic_vector(15 downto 0);
   signal serialinb2   : std_logic_vector(15 downto 0);
   signal serialininv  : std_logic_vector(15 downto 0);
   signal discinb      : std_logic_vector(3 downto 0);
   signal ccontrol: std_logic_vector(35 downto 0);
   signal cdata: std_logic_vector(31 downto 0);
   signal ctrig: std_logic_vector(0 downto 0);
   signal clockb          : std_logic_vector(7 downto 0);
   signal extreload       : std_logic;
   signal dispDatA        : std_logic;
   signal dispDatB        : std_logic;
   signal pgpDispA        : std_logic_vector(7 downto 0);
   signal pgpDispB        : std_logic_vector(7 downto 0);
   signal dispDigitA      : std_logic_vector(7 downto 0);
   signal dispDigitB      : std_logic_vector(7 downto 0);
   signal dispDigitC      : std_logic_vector(7 downto 0);
   signal dispDigitD      : std_logic_vector(7 downto 0);
   signal dispDigitE      : std_logic_vector(7 downto 0);
   signal dispDigitF      : std_logic_vector(7 downto 0);
   signal dispDigitG      : std_logic_vector(7 downto 0);
   signal dispDigitH      : std_logic_vector(7 downto 0);
   signal dispStrobe      : std_logic;
   signal dispUpdateA     : std_logic;
   signal dispUpdateB     : std_logic;
   signal sysClkCnt       : std_logic_vector(15 downto 0);
   signal lockedid        : std_logic;
   signal oldlockedid     : std_logic;
   signal holdrst         : std_logic;
   signal hitorb          : std_logic;

   signal txfault         : std_logic;
   signal rx0sd          : std_logic;
   signal rx1sd          : std_logic;

   signal pgpClkUnbuf    : std_logic;
   signal pgpClk90Unbuf  : std_logic;
   signal sysClkUnbuf    : std_logic;
   signal disc           : std_logic_vector(4 downto 0);

   signal calibmode1    : std_logic_vector(1 downto 0);
   signal calibmode2    : std_logic_vector(1 downto 0);
   signal calibmode     : std_logic_vector(1 downto 0);
   signal eudaqdone     : std_logic;
   signal eudaqtrgword  : std_logic_vector(14 downto 0);
   signal trigenabled1  : std_logic;
   signal trigenabled2  : std_logic;
   signal trigenabled   : std_logic;
   signal paused1       : std_logic;
   signal paused2       : std_logic;
   signal paused        : std_logic;
   signal triggermask1  : std_logic_vector(15 downto 0);
   signal triggermask2  : std_logic_vector(15 downto 0);
   signal triggermask   : std_logic_vector(15 downto 0);
   signal resetdelay1   : std_logic;
   signal resetdelay2   : std_logic;
   signal resetdelay    : std_logic;
   signal incrementdelay1: std_logic_vector(4 downto 0); 
   signal incrementdelay2: std_logic_vector(4 downto 0); 
   signal incrementdelay: std_logic_vector(4 downto 0); 
   signal discop1       : std_logic_vector(15 downto 0); 
   signal discop2       : std_logic_vector(15 downto 0); 
   signal discop        : std_logic_vector(15 downto 0); 
   signal telescopeop1 : std_logic_vector(2 downto 0);
   signal telescopeop2 : std_logic_vector(2 downto 0);
   signal telescopeop   : std_logic_vector(2 downto 0);
   signal period1       : std_logic_vector(31 downto 0);
   signal period2       : std_logic_vector(31 downto 0);
   signal period        : std_logic_vector(31 downto 0);
   signal fifothresh   : std_logic;
   signal fifothresh1  : std_logic;
   signal fifothresh2  : std_logic;
   signal serbusy1      : std_logic;
   signal serbusy2      : std_logic;
   signal serbusy       : std_logic;
   signal tdcreadoutbusy1: std_logic;
   signal tdcreadoutbusy2: std_logic;
   signal tdcreadoutbusy: std_logic;
   signal tcounter1     : std_logic_vector(31 downto 0);
   signal tcounter2     : std_logic_vector(31 downto 0);
   signal l1a1          : std_logic;
   signal l1a2          : std_logic;
   signal l1a           : std_logic;
   signal triggerword   : std_logic_vector(7 downto 0);
   signal busy          : std_logic;
   signal rstFromCore1  : std_logic;
   signal rstFromCore2  : std_logic;
   signal rstFromCore   : std_logic;
   signal hitbus        : std_logic_vector(1 downto 0);
   signal present       : std_logic_vector(1 downto 0);
   signal phaseclksel   : std_logic;
   signal phaseclksel1  : std_logic;
   signal phaseclksel2  : std_logic;
   signal startmeas     : std_logic;
   signal startmeas1    : std_logic;
   signal startmeas2    : std_logic;
   signal hitbusout     : std_logic;
   signal hitbusoutgated: std_logic;
   signal phasebusySelc : is array (1 downto 0) of std_logic_vector(1 downto 0);
   signal phasebusySel  : std_logic_vector(1 downto 0);
   signal phasebusyEnc  : std_logic_vector(1 downto 0);
   signal phasebusyEn   : std_logic;

   -- Register delay for simulation
   constant tpd:time := 0.5 ns;
   signal holdctr         : std_logic_vector(24 downto 0);
   signal clockidctrl     : std_logic;
   signal idctrlrst       : std_logic;
   signal idcounter       : std_logic_vector(2 downto 0);
  component ODDR port ( 
         Q  : out std_logic;
         CE : in std_logic;
         C  : in std_logic;
         D1 : in std_logic;
         D2 : in std_logic;
         R  : in std_logic;
         S  : in std_logic
      );
  end component;

   -- Black Box Attributes
--   attribute syn_noprune : boolean;
--   attribute syn_noprune of chipscope : label is true;
--   attribute syn_noprune of chipscopeicon : label is true;
   attribute syn_black_box : boolean;
   attribute syn_noprune   : boolean;
   attribute syn_black_box of IBUF    : component is TRUE;
   attribute syn_noprune   of IBUF    : component is TRUE;
   attribute syn_black_box of OBUF    : component is TRUE;
   attribute syn_noprune   of OBUF    : component is TRUE;
   attribute syn_black_box of IBUFDS  : component is TRUE;
   attribute syn_noprune   of IBUFDS  : component is TRUE;
   attribute syn_black_box of OBUFDS  : component is TRUE;
   attribute syn_noprune   of OBUFDS  : component is TRUE;
   attribute syn_black_box of BUFGMUX : component is TRUE;
   attribute syn_noprune   of BUFGMUX : component is TRUE;
   attribute syn_hier: string;
   attribute syn_hier of HsioStave0: architecture is "hard";
   attribute xc_props : string;
   attribute xc_props of HsioStave0 :  architecture is "KEEP_HIERARCHY=TRUE";
   attribute syn_noprune of U_idelayctrl: label is true;
begin

   -- Reset input
   U_ResetIn: IBUF port map ( I => iResetInL, O => resetInL );

   -- PGP Clock Generator
   U_PgpClkGen: entity work.PgpClkGen generic map (
         RefClkEn1  => "ENABLE",
         RefClkEn2  => "DISABLE",
         DcmClkSrc  => "RefClk1",
         UserFxDiv  => 4,
         UserFxMult => 2
      ) port map (
         pgpRefClkInP  => iPgpRefClkP,
         pgpRefClkInN  => iPgpRefClkM,
         ponResetL     => resetInL,
         locReset      => resetOut,
         pgpRefClk1    => refClock,
         pgpRefClk2    => open,
         pgpClk        => pgpClk,
         pgpClk90      => pgpClk90,
         pgpReset      => pgpReset,
         clk320        => clk320,
         pgpClkIn      => pgpClk,
         userClk       => sysClk125,
         userReset     => sysRst125,
         userClkIn     => sysClk125,
         pgpClkUnbuf   => pgpClkUnbuf,
         pgpClk90Unbuf => pgpClk90Unbuf,
         locClkUnbuf   => sysClkUnbuf
         
      );

   -- Generate Divided Clock, sample reset
   process ( pgpClk ) begin
      if rising_edge(pgpClk) then
         tmpClk250 <= not tmpClk250 after tpd;
         sysRst250 <= sysRst125     after tpd;
      end if;
   end process;

   -- Global Buffer For 125Mhz Clock
   U_CLK125: BUFGMUX port map (
      O  => sysClk250,
      I0 => tmpClk250,
      I1 => '0',
      S  => '0'
   );

   -- No Pads for MGT Lines
   mgtRxN  <= iMgtRxN;
   mgtRxP  <= iMgtRxP;
   oMgtTxN <= mgtTxN;
   oMgtTxP <= mgtTxP;
   mgtRxN2  <= iMgtRxN2;
   mgtRxP2  <= iMgtRxP2;
   oMgtTxN2 <= mgtTxN2;
   oMgtTxP2 <= mgtTxP2;

   -- fiber modules
   U_TX_EN    : OBUF   port map ( I  => '1'      , O => TX_EN      );
   U_TX_DIS   : OBUF   port map ( I  => '0'      , O => TX_DIS     );
   U_TX_RESET : OBUF   port map ( I  => '1'      , O => TX_RESET   );
   U_RX0_RX_EN: OBUF   port map ( I  => '1'      , O => RX0_RX_EN  );
   U_RX0_EN_SD: OBUF   port map ( I  => '0'      , O => RX0_EN_SD  );
   U_RX0_SQ_EN: OBUF   port map ( I  => '0'      , O => RX0_SQ_EN  );
   U_RX1_RX_EN: OBUF   port map ( I  => '1'      , O => RX1_RX_EN  );
   U_RX1_EN_SD: OBUF   port map ( I  => '0'      , O => RX1_EN_SD  );
   U_RX1_SQ_EN: OBUF   port map ( I  => '0'      , O => RX1_SQ_EN  );
   U_TX_FAULT:  IBUF   port map ( O => txfault   , I => TX_FAULT );
   U_RX0_SD:    IBUF   port map ( O => rx0sd     , I => RX0_SD );
   U_RX1_SD:    IBUF   port map ( O => rx1sd     , I => RX1_SD );
   -- LED Display
   U_DispClk    : OBUF   port map ( I  => dispClk      , O => oDispClk      );
   U_DispDat    : OBUF   port map ( I  => dispDat      , O => oDispDat      );
   U_DispLoadL1 : OBUF   port map ( I  => dispLoadL(1) , O => oDispLoadL(1) );
   U_DispLoadL0 : OBUF   port map ( I  => dispLoadL(0) , O => oDispLoadL(0) );
   U_DispRstL   : OBUF   port map ( I  => dispRstL     , O => oDispRstL     );
   U_reload     : OBUF   port map ( I  => reload       , O => oReload     );
   U_clkout40   : OBUF   port map ( I  => sysClk125i   , O => clkout40     );
   U_refclk     : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => sysClk125i   , O => refclk_p , OB => refclk_n  );
   U_xclk       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => sysClk125i   , O => xclk_p , OB => xclk_n  );

--       U_Clk320 : ODDR port map ( 
--          Q  => oClk320,
--          CE => '1',
--          C  => clk320,
--          D1 => '0',      
--          D2 => '1',      
--          R  => '0',      
--          S  => '0'
--       );
--    U_clk320buf       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
--                            port map ( I  => oClk320   , O => oDebug(4) , OB => oDebug(5)  );

--    U_bit0out       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
--                            port map ( I  => debug(7)   , O => oDebug(6) , OB => oDebug(7)  );

   --UNUSED_IO_DATA:
   --for I in 0 to 3 generate
    --U_unused       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25")
     --                       port map ( I  => '0'   , O => unused_p(I) , OB => unused_n(I)  );
   --end generate UNUSED_IO_DATA;
   -- Debug
   U_Debug0     : OBUF   port map ( I  => debug(0)     , O => oDebug(0)   );
   U_Debug1     : OBUF   port map ( I  => debug(1)     , O => oDebug(1)   );
   U_Debug2     : OBUF   port map ( I  => debug(2)     , O => oDebug(2)   );
   U_Debug3     : OBUF   port map ( I  => debug(3)     , O => oDebug(3)   );
   U_Debug4     : OBUF   port map ( I  => debug(4)     , O => oDebug(4)   );
   U_Debug5     : OBUF   port map ( I  => debug(5)     , O => oDebug(5)   );
   U_Debug6     : OBUF   port map ( I  => debug(6)     , O => oDebug(6)   );
   U_Debug7     : OBUF   port map ( I  => debug(7)     , O => oDebug(7)   );

   U_transdis     : OBUF   port map ( I  => '0'     , O => transDis1   );
   U_transdis2    : OBUF   port map ( I  => '0'     , O => transDis2   );

   U_rstb_1     : OBUF   port map ( I  => doricresetb1     , O => doricreset1   );
   U_rstb_2     : OBUF   port map ( I  => doricresetb2     , O => doricreset2   );

   U_exttrgclk  : OBUF   port map ( I  => exttrgclk     , O => oExttrgclk   );

   U_extbusy  : OBUF   port map ( I  => extbusy     , O => oExtBusy   );

   U_exttrigger  : IBUF   port map ( O  => exttrigger       , I => iExttrigger );

   U_extrst  : IBUF   port map ( O  => extrst       , I => iExtrst );

   --U_exttriggero  : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
   --                        port map ( I  => exttriggero   , O => oExttriggerP , OB => oExttriggerN  );
   U_exttriggero  : OBUF   port map ( I  => exttriggero     , O => oExttriggerP   );

   U_extrsto    : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => extrsto   , O => oExtrstP , OB => oExtrstN  );

   U_HSIObusy   : OBUF   port map ( I  => hsiobusyb              , O => oHSIObusy);
   hitbusoutgated <= hitbusout and trigenabled and not paused;
   U_HSIOtriggero  : OBUF   port map ( I  => hitbusoutgated         , O => oHSIOtrigger);
   U_HSIOtrigger  : IBUF   port map ( O  => hsiotriggerb(0)       , I => iHSIOtrigger(0) );
   U_HSIOtrigger2 : IBUF   port map ( O  => hsiotriggerb(1)       , I => iHSIOtrigger(1) );
   U_hsiobusyin  : IBUF   port map ( O  => hsiobusyinb       , I => iHSIObusy );
   U_extreload  : IBUF   port map ( O  => extreload       , I => iExtreload );

   SERIALOUT_IO_DATA:
   for I in 0 to 7 generate
     U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                               port map ( I  => serialoutb(I)   , O => serialout_p(I*2) , OB => serialout_n(I*2)  );
     U_Clk320 : ODDR port map ( 
       Q  => clockb(I),
       CE => '1',
       C  => sysClk125,
       D1 => '0',      
       D2 => '1',      
       R  => '0',      
       S  => '0'
     );
     U_serialoutclock_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                               port map ( I  => clockb(I)   , O => serialout_p(I*2+1) , OB => serialout_n(I*2+1)  );
   end generate SERIALOUT_IO_DATA;

   SERIALIN_IO_DATA:
   for I in 0 to 15 generate
     U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                              port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialininv(I)   );
     serialinb(I)<=not serialininv(I);

   end generate SERIALIN_IO_DATA;


   --DISC_1: IBUFDS port map ( O => discinb(0), I => discinP(0) , IB => discinN(0) );
   --DISC_2: IBUFDS port map ( O => discinb(1), I => discinP(1) , IB => discinN(1) );
   DISC_1: IBUF port map ( O => discinb(0), I => discinP(0) );
   DISC_2: IBUF port map ( O => discinb(1), I => discinP(1) );
   DISC_3: IBUF port map ( O => discinb(2), I => discinP(2) );
   DISC_4: IBUF port map ( O => discinb(3), I => discinP(3) );
   DISC_5: IBUF port map ( O => hitorb, I => HITOR);

      -- Display Controller A
   U_DispCntrlA: entity work.DisplayControl port map (
      sysClk     => sysClk125,   sysRst     => sysRst125,
      dispStrobe => dispStrobe,  dispUpdate => dispUpdateA,
      dispRotate => "01",        dispDigitA => dispDigitA,
      dispDigitB => dispDigitB,  dispDigitC => dispDigitC,
      dispDigitD => dispDigitD,  dispClk    => dispClk,
      dispDat    => dispDatA,    dispLoadL  => dispLoadL(0),
      dispRstL   => dispRstL
   );

   -- Display Controller B
   U_DispCntrlB: entity work.DisplayControl port map (
      sysClk     => sysClk125,   sysRst     => sysRst125,
      dispStrobe => dispStrobe,  dispUpdate => dispUpdateB,
      dispRotate => "01",        dispDigitA => dispDigitE,
      dispDigitB => dispDigitF,  dispDigitC => dispDigitG,
      dispDigitD => dispDigitH,  dispClk    => open,
      dispDat    => dispDatB,    dispLoadL  => dispLoadL(1),
      dispRstL   => open
   );

   -- Output LED Data
   dispDat    <= dispDatA or dispDatB;
   -- Generate display strobe (200ns) and update control
   process ( sysClk125, sysRst125 ) begin
      if sysRst125 = '1' then
         sysClkCnt   <= (others=>'0') after tpd;
         dispStrobe  <= '0'           after tpd;
         dispUpdateA <= '0'           after tpd;
         dispUpdateB <= '0'           after tpd;
      elsif rising_edge(sysClk125) then

         -- Display strobe, 320ns
         dispStrobe <= sysClkCnt(4) after tpd;

         -- Update Display 0
         if sysClkCnt(15 downto 0) = x"8000" then
            dispUpdateA <= '1' after tpd;
         else
            dispUpdateA <= '0' after tpd;
         end if;

         -- Update Display B
         if sysClkCnt(15 downto 0) = x"0000" then
            dispUpdateB <= '1' after tpd;
         else
            dispUpdateB <= '0' after tpd;
         end if;

         -- Update counter
         sysClkCnt <= sysClkCnt + 1 after tpd;


      end if;
   end process;


   --debug(7 downto 0)<=serialinb(7 downto 0);
  paused <= paused1 or paused2;
  resetdelay <= resetdelay1 or resetdelay2;
  incrementdelay <= incrementdelay1 or incrementdelay2;
  serbusy <= (serbusy1 and present(0)) or (serbusy2 and present(1));
  tdcreadoutbusy<= tdcreadoutbusy1 or tdcreadoutbusy2;
  rstFromCore <= rstFromCore1 or rstFromCore2;

  phasebusyEn <= phasebusyEnc(0) or phasebusyEnc(1);
  phasebusySel <= phasebusySelc(0) or phasebusySelc(1);

  fifothresh <= (fifothresh1 and present(0)) or (fifothresh2 and present(1));
  trigenabled <= (trigenabled1 or not present(0)) and (trigenabled2 or not present(1)) and (present(0) or present(1));
  l1a1 <= l1a and present(0);
  l1a2 <= l1a and present(1);

  with present select discop <= discop2 when "10", discop1 when others;
  with present select calibmode <= calibmode2 when "10", calibmode1 when others;
  with present select triggermask <= triggermask2 when "10", triggermask1 when others;
  with present select period <= period2 when "10", period1 when others;
  with present select telescopeop <= telescopeop2 when "10", telescopeop1 when others;
  with present select phaseclksel <= phaseclksel2 when "10", phaseclksel1 when others;
  with present select startmeas <= startmeas2 when "10", startmeas1 when others;


  U_triggerlogic: entity work.triggerlogic 
    port map(
      clk => sysClk125,
      rst => rstFromCore,
      clk160 => pgpClk,
      rst160 => pgpReset,
      -- hardware inputs
      discin => discinb,
      hitbusin => hitbus,
      -- HSIO trigger
      HSIObusy => hsiobusyb,
      HSIOtrigger => hsiotriggerb,
      HSIObusyin => hsiobusyinb,
      hitbusout => hitbusout,
      
      -- eudet trigger
      exttrigger => exttrigger,
      extrst => extrst,
      extbusy => extbusy,
      exttrgclk => exttrgclk,
      
      calibmode => calibmode,
      startmeas => startmeas,
      eudaqdone => eudaqdone,
      eudaqtrgword => eudaqtrgword,
      tcounter1 => tcounter1,
      tcounter2 => tcounter2,
      
      trigenabled => trigenabled,
      paused => paused,
      triggermask => triggermask,
      resetdelay => resetdelay,
      incrementdelay => incrementdelay,
      discop => discop,
      telescopeop => telescopeop,
      period => period,
      fifothresh => fifothresh,
      serbusy => serbusy,
      tdcreadoutbusy => tdcreadoutbusy,
      phaseclksel => phaseclksel,
      phasebusyEn => phasebusyEn,
      phasebusySel => phasebusySel,
      l1a => l1a,
      triggerword => triggerword,
      busy =>busy,
      coincd =>open
);
   -- FPGA Core
   U_HsioStave0Core1: entity work.HsioPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 7,
                  rawFirstChannel => 11,
                  rawLastChannel => 10,
                  buffersizefe => 4096,
                  buffersizetdc => 16384,
                  encodingDefault => "00",
                  hitbusreadout => '1')
     port map (
      sysClk250  => sysClk250,  sysRst250 => sysRst250,
      sysClk125  => sysClk125,  sysRst125 => sysRst125,
      refClock   => refClock,   pgpClk    => pgpClk,
      pgpClk90 => pgpClk90, pgpReset   => pgpReset,
      clk320 => clk320, reload => reload1,
      mgtRxN    => mgtRxN,
      mgtRxP     => mgtRxP,     mgtTxN    => mgtTxN,
      mgtTxP     => mgtTxP,     serialin  => serialinb1,
      serialout  => serialoutb1, clock160 => mainClk, 
      clock80   => halfclock, clock40 => quarterclock,
      resetOut  => resetOut1, l1a => l1a1,
      latchtriggerword => triggerword, tcounter1 => tcounter1,
      tcounter2 => tcounter2, busy => busy,
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      calibmodeout => calibmode1, startmeas => startmeas1,
      pausedout => paused1, present => present(0),
      trgenabledout => trigenabled1, rstFromCore => rstFromCore1, 
      fifothresh => fifothresh1, triggermask => triggermask1,
      discop => discop1, period => period1,
      telescopeop => telescopeop1, resetdelay => resetdelay1,
      incrementdelay => incrementdelay1,
      sbusy => serbusy1, tdcbusy => tdcreadoutbusy1,
      phaseclksel  => phaseclksel1 , hitbus => hitbus(0),
      debug     => debug, 
      exttriggero => exttriggero, extrsto => extrsto,
      doricreset => doricresetb1,
      phasebusyEn => phasebusyEnc(0), phasebusySel => phasebusySelc(0),
      dispDigitA => dispDigitE, dispDigitB => dispDigitF,
      dispDigitC => open, dispDigitD => dispDigitH,
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      pgpClkUnbuf => pgpClkUnbuf, pgpClk90Unbuf => pgpClk90Unbuf,
      sysClkUnbuf => sysClkUnbuf, trigAdc => open,
      sendAdcData => '0', adcData => (others => (others => '0'))
   );
    dispDigitC<="0000000"&present(0);
    dispDigitG<="0000000"&present(1);
    U_HsioStave0Core2: entity work.HsioPixelCore
      generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 7,
                  rawFirstChannel => 11,
                  rawLastChannel => 10,
                  buffersizefe => 4096,
                  buffersizetdc => 16384,
                  encodingDefault => "00",
                  hitbusreadout => '1')
      port map (
       sysClk250  => sysClk250,  sysRst250 => sysRst250,
       sysClk125  => sysClk125,  sysRst125 => sysRst125,
       refClock   => refClock,   pgpClk    => pgpClk,
       pgpClk90 => pgpClk90, pgpReset   => pgpReset,
       clk320 => clk320, reload => reload2,
       mgtRxN    => mgtRxN2,
       mgtRxP     => mgtRxP2,     mgtTxN    => mgtTxN2,
       mgtTxP     => mgtTxP2,     serialin  => serialinb2,
       serialout  => serialoutb2, clock160 => mainClk, 
       clock80   => halfclock, clock40 => quarterclock,
       resetOut  => resetOut2, l1a => l1a2, 
       latchtriggerword => triggerword, tcounter1=>tcounter1, 
       tcounter2=>tcounter2, busy =>busy,
       eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
       calibmodeout => calibmode2, startmeas => startmeas2,
       pausedout => paused2, present => present(1),
       trgenabledout => trigenabled2, rstFromCore => rstFromCore2,
       fifothresh => fifothresh2, triggermask => triggermask2,
       discop => discop2, period => period2,
       telescopeop => telescopeop2, resetdelay => resetdelay2,
       incrementdelay => incrementdelay2,
       sbusy => serbusy2, tdcbusy => tdcreadoutbusy2,
       phaseclksel=>phaseclksel2, hitbus => hitbus(1),
       debug     => open,
       exttriggero => open, extrsto => open,
       doricreset => doricresetb2,
       phasebusyEn => phasebusyEnc(1), phasebusySel => phasebusySelc(1),
       dispDigitA => dispDigitA, dispDigitB => dispDigitB,
       dispDigitC => open, dispDigitD => dispDigitD,
       dispDigitE => open, dispDigitF => open,
       dispDigitG => open, dispDigitH => open,
       pgpClkUnbuf => pgpClkUnbuf, pgpClk90Unbuf => pgpClk90Unbuf,
       sysClkUnbuf => sysClkUnbuf, trigAdc => open,
       sendAdcData => '0', adcData => (others => (others => '0'))
    );
    serialoutb<= serialoutb2(3 downto 0) & serialoutb1(3 downto 0);
    serialinb1(7 downto 0)<=serialinb(7 downto 0);
    serialinb2(7 downto 0)<=serialinb(15 downto 8);
    --dispDigitC<="0000000"&txfault;
--    serialout(15 downto 8) <= "00000000";
   
   --dispDigitD<=x"02";
   
   sysClk125i <= not sysClk125;
   reload <= reload1 and reload2 and extreload;       -- active low
   resetOut <= resetOut1 or resetOut2;
   --sysClk125i<=debug(0);
   U_clock160: entity work.clock160 port map(
     CLKIN_N_IN => iMainClkN,
     CLKIN_P_IN => iMainClkP,
     RST_IN => sysRst125,
     CLKFX_OUT => mainClk,
     CLKIN_IBUFGDS_OUT => clkin,
     CLK0_OUT => clk0,
     LOCKED_OUT => open);
  
   process(mainClk)
   begin
     if(mainClk'event and mainClk='1') then
       halfclock<= not halfclock;
     end if;
   end process;
   process(halfclock)
   begin
     if(halfclock'event and halfclock='1') then
       quarterclock<= not quarterclock;
     end if;
   end process;
 --cdata(7 downto 0)<=serialinb(7 downto 0);
 --cdata(31 downto 8) <=(others=>'0');
 --ctrig(0)<='0';
 --chipscope : ila
   --port map (
     --CONTROL => ccontrol,
     --CLK => mainClk,
     --DATA => cdata,
     --TRIG0 => ctrig);
 --chipscopeicon : icon
   --port map (
     --CONTROL0 => ccontrol);
     U_idelctrlclk: entity work.clock200 port map(
       CLKIN_IN  => mainClk,
       RST_IN    => holdrst,
       CLKFX_OUT => clockidctrl,
       CLK0_OUT  => open,
       LOCKED_OUT => lockedid);  
     U_idelayctrl: IDELAYCTRL port map(
       RDY     => open,
       REFCLK  => clockidctrl,
       RST     => idctrlrst);

     process(sysRst125, sysClk125) -- clock interface
     begin
       if (sysRst125='1') then
         holdctr<=(others=>'1');
         holdrst<='1';
       elsif(sysClk125'event and sysClk125='1') then
         if (holdctr(24)='0') then
           holdrst<='0';
         end if;
         holdctr<=unsigned(holdctr)-1;
       end if;
     end process;

     process(sysClk125, sysRst125) -- reset logic for IDELAYCTRL
     begin
       if(sysRst125='1') then
         idctrlrst<='0';
         idcounter<="000";
         oldlockedid<='0';
       elsif(sysClk125'event and sysClk125='1') then
         if(lockedid='1' and oldlockedid='0')then
           idcounter<="111";
           idctrlrst<='1';
         elsif(unsigned(idcounter)>0)then
           idcounter<=unsigned(idcounter)-1;
         else
           idctrlrst<='0';
         end if;
         oldlockedid<=lockedid;
       end if;
     end process;


end HsioStave0;
