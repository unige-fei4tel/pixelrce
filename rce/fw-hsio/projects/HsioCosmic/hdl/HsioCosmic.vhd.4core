-------------------------------------------------------------------------------
-- Title         : HsioCosmic, Top Level
-- Project       : ATLAS Pixel Detector
-------------------------------------------------------------------------------
-- File          : HsioCosmic.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Created       : 2009
-------------------------------------------------------------------------------
-- Description:
-- Top level logic for cosmic interface board
-------------------------------------------------------------------------------
-- Copyright (c) 2009 by Martin Kocian. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
Library Unisim;
USE ieee. std_logic_1164.ALL;
use ieee. std_logic_arith.all;
use ieee. std_logic_unsigned.all;
USE work.ALL;
use work.StdRtlPkg.all;

entity HsioCosmic is 
   port ( 

      -- PGP Crystal Clock Input, 156.25Mhz
      iPgpRefClkP   : in    std_logic;
      iPgpRefClkM   : in    std_logic;

      -- System clock 125 MHz clock input
      iMainClkP     : in    std_logic;
      iMainClkN     : in    std_logic;

      -- PGP Rx/Tx Lines
      iMgtRxN       : in    std_logic_vector(3 downto 0);
      iMgtRxP       : in    std_logic_vector(3 downto 0);
      oMgtTxN       : out   std_logic_vector(3 downto 0);
      oMgtTxP       : out   std_logic_vector(3 downto 0);

     -- ATLAS Pixel module pins
      serialin     : in std_logic_vector(15 downto 0);
      serialout    : out std_logic_vector(15 downto 0);
      serialout_p  : out std_logic_vector(15 downto 0);
      serialout_n  : out std_logic_vector(15 downto 0);
      serialin_p   : in std_logic_vector(15 downto 0);
      serialin_n   : in std_logic_vector(15 downto 0);
      discinP      : in std_logic_vector(3 downto 0);
--      discinN    : in std_logic_vector(1 downto 0);
      clkout40     : out std_logic;
      refclk_p     : out std_logic;
      refclk_n     : out std_logic;
      xclk_p       : out std_logic;
      xclk_n       : out std_logic;

      -- Reset button
      iResetInL     : in    std_logic;
      -- Reload firmware
      oReload       : out std_logic;
      iExtreload    : in std_logic;
      -- LED Display
      oDispClk      : out   std_logic;
      oDispDat      : out   std_logic;
      oDispLoadL    : out   std_logic_vector(1 downto 0);
      oDispRstL     : out   std_logic;

      -- Temperature ADC
      adcI2cScl: inout std_logic;
      adcI2cSda: inout std_logic;
      adcAS: out std_logic;

      -- Debug
      oDebug        : out std_logic_vector(7 downto 0);

      -- Eudet trigger
      iExttriggerP    : in std_logic;
      iExttriggerN    : in std_logic;
      iExtrstP        : in std_logic;
      iExtrstN        : in std_logic;
      oExtbusyP       : out std_logic;
      oExtbusyN       : out std_logic;
      oExttrgclkP     : out std_logic;
      oExttrgclkN     : out std_logic;

      --HSIO trigger IF
      iHSIOtrigger    : in std_logic_vector(1 downto 0);
      oHSIOtrigger    : out std_logic;
      oHSIObusy       : out std_logic;
      iHSIObusy       : in std_logic;

      -- Eudet test
      oExtrstP        : out std_logic;
      oExtrstN        : out std_logic;
      oExttriggerP    : out std_logic;
      oExttriggerN    : out std_logic;
      

      -- Misc Signals
      oPdBuff0      : out   std_logic;
      oPdBuff1      : out   std_logic;
      oPdBuff3      : out   std_logic;
      oPdBuff4      : out   std_logic;
      oLemoA        : out   std_logic;
      iLemoB        : in    std_logic;

      -- Transmitter enable
      transDis   : out   std_logic_vector(3 downto 0);
      -- CMOS chip I/O
      RD2           : out std_logic;
      AuxClk        : out std_logic;
      RA            : out std_logic;
      RD1           : out std_logic;
      IOMXIN        : out std_logic_vector(3 downto 0);
      IOMXSEL       : out std_logic_vector(2 downto 0);
      HITOR         : in std_logic;
      IOMXOUT       : in std_logic_vector(2 downto 0);
      SELALTBUS     : out std_logic;
      REGABDACLD    : out std_logic;
      REGABSTBLD    : out std_logic;
      SELCMD        : out std_logic;
      CMDEXTTRIGGER : out std_logic;
      CMDALTPLS     : out std_logic
   );
end HsioCosmic;


-- Define architecture for top level module
architecture HsioCosmic of HsioCosmic is 

   -- Synthesis control attributes
   attribute syn_useioff    : boolean;
   attribute syn_useioff    of HsioCosmic : architecture is true;
   attribute xc_fast_auto   : boolean;
   attribute xc_fast_auto   of HsioCosmic : architecture is false;
   attribute syn_noclockbuf : boolean;
   attribute syn_noclockbuf of HsioCosmic : architecture is true;

   -- IO Pad components
   component IBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   component OBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   component OBUFDS
     generic( IOSTANDARD: STRING:= "LVDS_25";
              SLEW: STRING:="FAST");
     port ( O : out std_logic; OB : out std_logic; I  : in std_logic );
   end component;

   -- Input LVDS with termination
   component IBUFDS 
      generic ( DIFF_TERM : boolean := TRUE;
                IOSTANDARD: STRING := "LVDS_25"); 
      port    ( O : out std_logic; I  : in  std_logic; IB : in std_logic ); 
   end component;

   -- Xilinx global clock buffer component
   component BUFGMUX 
      port ( 
         O  : out std_logic; 
         I0 : in  std_logic;
         I1 : in  std_logic;  
         S  : in  std_logic 
      ); 
   end component;

   component IDELAYCTRL
   port ( RDY    : out std_logic;
          REFCLK : in std_logic;
          RST    : in std_logic
        );
   end component;

component IDELAY
        generic (IOBDELAY_TYPE : string := "DEFAULT"; --(DEFAULT, FIXED, VARIABLE)
                 IOBDELAY_VALUE : integer := 0 --(0 to 63)
                 );
        port (
              O : out STD_LOGIC;
              I : in STD_LOGIC;
              C : in STD_LOGIC;
              CE : in STD_LOGIC;
              INC : in STD_LOGIC;
              RST : in STD_LOGIC
             );
end component;


   -- Local signals
   signal resetInL       : std_logic;
   signal tmpClk250      : std_logic;
   signal sysClk125      : std_logic;
   signal sysRst125      : std_logic;
   signal sysRst250      : std_logic;
   signal sysClk250      : std_logic;
   signal refClock       : std_logic;
   signal pgpClk         : std_logic;
   signal pgpClk90       : std_logic;
   signal pgpReset       : std_logic;
   signal clk320         : std_logic;
   signal reload         : std_logic;
   signal reloadc        : std_logic_vector(3 downto 0);
   signal extreload      : std_logic;
   signal resetOut       : std_logic;
   signal resetOutc      : std_logic_vector(3 downto 0);
   signal dispClk        : std_logic;
   signal dispDat        : std_logic;
   signal dispLoadL      : std_logic_vector(1 downto 0);
   signal dispRstL       : std_logic;
   signal debug          : std_logic_vector(7 downto 0);
   signal sysClk125i     : std_logic;
   signal mainClk        : std_logic;
   signal clk0           : std_logic;
   signal clkin          : std_logic;
   signal halfclock      : std_logic;
   signal quarterclock   : std_logic;
   signal clockidctrl    : std_logic;
   signal idctrlrst      : std_logic;
   signal RD2b           : std_logic;
   signal AuxClkb        : std_logic;
   signal RAb            : std_logic;
   signal RD1b           : std_logic;
   signal IOMXINb        : std_logic_vector(3 downto 0);
   signal IOMXSELb       : std_logic_vector(2 downto 0);
   signal HITORb         : std_logic;
   signal HITORout       : std_logic;
   signal IOMXOUTb       : std_logic_vector(2 downto 0);
   signal SELALTBUSb     : std_logic;
   signal REGABDACLDb    : std_logic;
   signal REGABSTBLDb    : std_logic;
   signal SELCMDb        : std_logic;
   signal CMDEXTTRIGGERb : std_logic;
   signal CMDALTPLSb     : std_logic;
   signal exttrigger     : std_logic;
   signal exttriggerinv  : std_logic;
   signal extrst         : std_logic;
   signal extrstinv      : std_logic;
   signal exttriggero    : std_logic;
   signal extrsto        : std_logic;
   signal extbusy        : std_logic;
   signal extbusyinv     : std_logic;
   signal exttrgclk      : std_logic;
   signal exttrgclkinv   : std_logic;
   signal hsiobusyb      : std_logic;
   signal hsiobusyinb    : std_logic;
   signal hsiotriggerb   : std_logic_vector(1 downto 0);
   signal serialoutb     : std_logic_vector(15 downto 0);
   signal serialoutm1    : std_logic_vector(15 downto 0);
   signal serialoutm2    : std_logic_vector(15 downto 0);
   signal serialinb      : std_logic_vector(15 downto 0);
   signal serialinm1     : std_logic_vector(15 downto 0);
   signal serialinm2     : std_logic_vector(15 downto 0);
   signal discinb        : std_logic_vector(3 downto 0);
   signal ccontrol       : std_logic_vector(35 downto 0);
   signal cdata          : std_logic_vector(31 downto 0);
   signal ctrig          : std_logic_vector(0 downto 0);
   signal opgpClk        : std_logic;
   signal dispDatA        : std_logic;
   signal dispDatB        : std_logic;
   signal pgpDispA        : std_logic_vector(7 downto 0);
   signal pgpDispB        : std_logic_vector(7 downto 0);
   signal dispDigitA      : std_logic_vector(7 downto 0);
   signal dispDigitB      : std_logic_vector(7 downto 0);
   signal dispDigitC      : std_logic_vector(7 downto 0);
   signal dispDigitD      : std_logic_vector(7 downto 0);
   signal dispDigitE      : std_logic_vector(7 downto 0);
   signal dispDigitF      : std_logic_vector(7 downto 0);
   signal dispDigitG      : std_logic_vector(7 downto 0);
   signal dispDigitH      : std_logic_vector(7 downto 0);
   signal dispStrobe      : std_logic;
   signal dispUpdateA     : std_logic;
   signal dispUpdateB     : std_logic;
   signal sysClkCnt       : std_logic_vector(15 downto 0);
   signal lockedid        : std_logic;
   signal oldlockedid     : std_logic;
   signal holdrst         : std_logic;
   signal holdctr         : std_logic_vector(24 downto 0);
   signal idcounter       : std_logic_vector(2 downto 0);

   signal pgpClkUnbuf    : std_logic;
   signal pgpClk90Unbuf  : std_logic;
   signal sysClkUnbuf    : std_logic;
   signal disc           : std_logic_vector(4 downto 0);

   signal calibmodec    : Slv2Array(0 to 3);
   signal calibmode     : std_logic_vector(1 downto 0);
   signal eudaqdone     : std_logic;
   signal eudaqtrgword  : std_logic_vector(14 downto 0);
   signal trigenabledc  : std_logic_vector(3 downto 0);
   signal trigenabled   : std_logic;
   signal pausedc       : std_logic_vector(3 downto 0);
   signal paused        : std_logic;
   signal triggermaskc  : Slv16Array(0 to 3);
   signal triggermask   : std_logic_vector(15 downto 0);
   signal resetdelayc   : std_logic_vector(3 downto 0);
   signal resetdelay    : std_logic;
   signal incrementdelayc: Slv5Array(0 to 3);
   signal incrementdelay: std_logic_vector(4 downto 0); 
   signal discopc       : Slv16Array(0 to 3);
   signal discop        : std_logic_vector(15 downto 0); 
   signal telescopeopc : Slv3Array(0 to 3);
   signal telescopeop   : std_logic_vector(2 downto 0);
   signal periodc       : Slv32Array(0 to 3);
   signal period        : std_logic_vector(31 downto 0);
   signal fifothreshc  : std_logic_vector(3 downto 0);
   signal fifothresh   : std_logic;
   signal serbusyc      : std_logic_vector(3 downto 0);
   signal serbusy       : std_logic;
   signal tdcreadoutbusyc: std_logic_vector(3 downto 0);
   signal tdcreadoutbusy: std_logic;
   signal tcounter1     : std_logic_vector(31 downto 0);
   signal tcounter2     : std_logic_vector(31 downto 0);
   signal l1ac          : std_logic_vector(3 downto 0);
   signal l1a           : std_logic;
   signal triggerword   : std_logic_vector(7 downto 0);
   signal busy          : std_logic;
   signal rstFromCorec  : std_logic_vector(3 downto 0);
   signal rstFromCore   : std_logic;
   signal hitbus        : std_logic_vector(3 downto 0);
   signal present       : std_logic_vector(3 downto 0);
   signal phaseclkselc  : std_logic_vector(3 downto 0);
   signal phaseclksel   : std_logic;
   signal startmeasc    : std_logic_vector(3 downto 0);
   signal startmeas     : std_logic;
   signal dispDigitAA   : Slv8Array(0 to 3);
   signal dispDigitBB   : Slv8Array(0 to 3);
   signal dispDigitCC   : Slv8Array(0 to 3);
   signal dispDigitDD   : Slv8Array(0 to 3);
   signal transdissig   : std_logic_vector(3 downto 0);
   signal trigAdc       : sl;
   signal trigAdcc      : slv(3 downto 0);
   signal sendAdcData   : sl;
   type adcarray is array(3 downto 0) of Slv16Array(11 downto 0);
   signal adcDatachan   : adcarray;
   signal adcData       : Slv16Array(11 downto 0);
   signal phasebusySelc : Slv2Array(3 downto 0);
   signal phasebusySel  : slv(1 downto 0);
   signal phasebusyEnc  : slv(3 downto 0);
   signal phasebusyEn   : sl;


   -- Register delay for simulation
   constant tpd:time := 0.5 ns;

begin

   -- Reset input
   U_ResetIn: IBUF port map ( I => iResetInL, O => resetInL );

   -- PGP Clock Generator
   U_PgpClkGen: entity work.PgpClkGen generic map (
         RefClkEn1  => "ENABLE",
         RefClkEn2  => "DISABLE",
         DcmClkSrc  => "RefClk1",
         UserFxDiv  => 4,
         UserFxMult => 2
      ) port map (
         pgpRefClkInP  => iPgpRefClkP,
         pgpRefClkInN  => iPgpRefClkM,
         ponResetL     => resetInL,
         locReset      => resetOut,
         pgpRefClk1    => refClock,
         pgpRefClk2    => open,
         pgpClk        => pgpClk,
         pgpClk90      => pgpClk90,
         pgpReset      => pgpReset,
         clk320        => clk320,
         pgpClkIn      => pgpClk,
         userClk       => sysClk125,
         userReset     => sysRst125,
         userClkIn     => sysClk125,
         pgpClkUnbuf   => pgpClkUnbuf,
         pgpClk90Unbuf => pgpClk90Unbuf,
         locClkUnbuf   => sysClkUnbuf
         
         
      );

   -- Generate Divided Clock, sample reset
   process ( pgpClk ) begin
      if rising_edge(pgpClk) then
         tmpClk250 <= not tmpClk250 after tpd;
         sysRst250 <= sysRst125     after tpd;
      end if;
   end process;

   -- Global Buffer For 125Mhz Clock
   U_CLK125: BUFGMUX port map (
      O  => sysClk250,
      I0 => tmpClk250,
      I1 => '0',
      S  => '0'
   );

   -- LED Display
   U_DispClk    : OBUF   port map ( I  => dispClk      , O => oDispClk      );
   U_DispDat    : OBUF   port map ( I  => dispDat      , O => oDispDat      );
   U_DispLoadL1 : OBUF   port map ( I  => dispLoadL(1) , O => oDispLoadL(1) );
   U_DispLoadL0 : OBUF   port map ( I  => dispLoadL(0) , O => oDispLoadL(0) );
   U_DispRstL   : OBUF   port map ( I  => dispRstL     , O => oDispRstL     );
   U_reload     : OBUF   port map ( I  => reload       , O => oReload     );
   U_clkout40   : OBUF   port map ( I  => sysClk125i   , O => clkout40     );
   U_refclk     : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => sysClk125i   , O => refclk_p , OB => refclk_n  );
   U_xclk       : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => sysClk125i   , O => xclk_p , OB => xclk_n  );

   TRANSDISGEN:
   for I in 0 to 3 generate
     U_transdis     : OBUF   port map ( I  =>  transdissig(I)     , O => transDis(I)   );
   end generate TRANSDISGEN;

   U_rd2            : OBUF   port map ( I  => RD2b              , O => RD2);
   U_auxclk         : OBUF   port map ( I  => AuxClkb           , O => AuxClk);
   U_RA             : OBUF   port map ( I  => RAb               , O => RA );
   U_rd1            : OBUF   port map ( I  => RD1b              , O => RD1 );
   U_IOMXIN3        : OBUF   port map ( I  => IOMXINb(3)        , O => IOMXIN(3) );
   U_IOMXIN2        : OBUF   port map ( I  => IOMXINb(2)        , O => IOMXIN(2) );
   U_IOMXIN1        : OBUF   port map ( I  => IOMXINb(1)        , O => IOMXIN(1) );
   U_IOMXIN0        : OBUF   port map ( I  => IOMXINb(0)        , O => IOMXIN(0) );
   U_IOMXSEL2       : OBUF   port map ( I  => IOMXSELb(2)       , O => IOMXSEL(2) );
   U_IOMXSEL1       : OBUF   port map ( I  => IOMXSELb(1)       , O => IOMXSEL(1) );
   U_IOMXSEL0       : OBUF   port map ( I  => IOMXSELb(0)       , O => IOMXSEL(0) );
   U_HITOR          : IBUF   port map ( O  => HITORb            , I => HITOR);
   U_IOMXOUT2       : IBUF   port map ( O  => IOMXOUTb(2)       , I => IOMXOUT(2) );
   U_IOMXOUT1       : IBUF   port map ( O  => IOMXOUTb(1)       , I => IOMXOUT(1) );
   U_IOMXOUT0       : IBUF   port map ( O  => IOMXOUTb(0)       , I => IOMXOUT(0) );
   U_SELALTBUS      : OBUF   port map ( I  => SELALTBUSb        , O => SELALTBUS);
   U_REGABDACLD     : OBUF   port map ( I  => REGABDACLDb       , O => REGABDACLD);
   U_REGABSTBLD     : OBUF   port map ( I  => REGABSTBLDb       , O => REGABSTBLD);
   U_SELCMD         : OBUF   port map ( I  => SELCMDb           , O => SELCMD );
   U_CMDEXTTRIGGER  : OBUF   port map ( I  => CMDEXTTRIGGERb    , O => CMDEXTTRIGGER );
   U_CMDALTPLS      : OBUF   port map ( I  => CMDALTPLSb        , O => CMDALTPLS );
   U_exttrgclk  : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => exttrgclkinv   , O => oExttrgclkP , OB => oExttrgclkN  );
   exttrgclkinv<= not exttrgclk;
   U_extbusy    : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => extbusyinv   , O => oExtBusyP , OB => oExtbusyN  );
   extbusyinv<= not extbusy;
   U_exttrigger : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                           port map ( I  => iExttriggerP  , IB=>iExttriggerN   , O => exttriggerinv   );
   exttrigger<= not exttriggerinv;
   U_extrst     : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                           port map ( I  => iExtrstP  , IB=>iExtrstN   , O => extrstinv   );
   extrst<= not extrstinv;
   U_exttriggero  : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => exttriggero   , O => oExttriggerP , OB => oExttriggerN  );

   U_extrsto    : OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                           port map ( I  => extrsto   , O => oExtrstP , OB => oExtrstN  );

   U_HSIObusy   : OBUF   port map ( I  => hsiobusyb              , O => oHSIObusy);
   U_HSIOtriggero  : OBUF   port map ( I  => l1a              , O => oHSIOtrigger);
   U_HSIOtrigger  : IBUF   port map ( O  => hsiotriggerb(0)       , I => iHSIOtrigger(0) );
   U_HSIOtrigger2 : IBUF   port map ( O  => hsiotriggerb(1)       , I => iHSIOtrigger(1) );
   U_hsiobusyin  : IBUF   port map ( O  => hsiobusyinb       , I => iHSIObusy );
   U_extreload  : IBUF   port map ( O  => extreload       , I => iExtreload );

   SERIAL_IO_DATA:
   for I in 0 to 7 generate
--       U_serialin   : IBUF port map ( I => serialin(I)    , O => serialinb(I) );
     U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                               port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );

     U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                              port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );

   end generate SERIAL_IO_DATA;

   U_serialout  : OBUF port map ( I => serialoutb(8)  , O => serialout(8) );
   U_serialin_pn_8 : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                              port map ( I  => serialin_p(8)  , IB=>serialin_n(8)   , O => serialinb(8)   );

--   U_serialout11  : OBUF port map ( I => serialoutb(11)  , O => serialout(11) );
--   U_serialin11   : IBUF port map ( I => serialin(11)    , O => serialinb(11) );
   SERIAL_IO_DATA_FEI3:
   for I in 11 to 14 generate
     U_serialout_pn  :OBUFDS   generic map     ( IOSTANDARD=>"LVDS_25", SLEW=>"FAST")
                               port map ( I  => serialoutb(I)   , O => serialout_p(I) , OB => serialout_n(I)  );

     U_serialin_pn : IBUFDS   generic map ( DIFF_TERM=>TRUE, IOSTANDARD=>"LVDS_25")
                              port map ( I  => serialin_p(I)  , IB=>serialin_n(I)   , O => serialinb(I)   );

   end generate SERIAL_IO_DATA_FEI3;
   --DISC_1: IBUFDS port map ( O => discinb(0), I => discinP(0) , IB => discinN(0) );
   --DISC_2: IBUFDS port map ( O => discinb(1), I => discinP(1) , IB => discinN(1) );
   DISC_1: IBUF port map ( O => discinb(0), I => discinP(0) );
   DISC_2: IBUF port map ( O => discinb(1), I => discinP(1) );
   DISC_3: IBUF port map ( O => discinb(2), I => discinP(2) );
   DISC_4: IBUF port map ( O => discinb(3), I => discinP(3) );
   -- Display Controller A
   U_DispCntrlA: entity work.DisplayControl port map (
      sysClk     => sysClk125,   sysRst     => sysRst125,
      dispStrobe => dispStrobe,  dispUpdate => dispUpdateA,
      dispRotate => "01",        dispDigitA => dispDigitA,
      dispDigitB => dispDigitB,  dispDigitC => dispDigitC,
      dispDigitD => dispDigitD,  dispClk    => dispClk,
      dispDat    => dispDatA,    dispLoadL  => dispLoadL(0),
      dispRstL   => dispRstL
   );

   -- Display Controller B
   U_DispCntrlB: entity work.DisplayControl port map (
      sysClk     => sysClk125,   sysRst     => sysRst125,
      dispStrobe => dispStrobe,  dispUpdate => dispUpdateB,
      dispRotate => "01",        dispDigitA => dispDigitE,
      dispDigitB => dispDigitF,  dispDigitC => dispDigitG,
      dispDigitD => dispDigitH,  dispClk    => open,
      dispDat    => dispDatB,    dispLoadL  => dispLoadL(1),
      dispRstL   => open
   );
   -- Output LED Data
   dispDat    <= dispDatA or dispDatB;
   -- Generate display strobe (200ns) and update control
   process ( sysClk125, sysRst125 ) begin
      if sysRst125 = '1' then
         sysClkCnt   <= (others=>'0') after tpd;
         dispStrobe  <= '0'           after tpd;
         dispUpdateA <= '0'           after tpd;
         dispUpdateB <= '0'           after tpd;
      elsif rising_edge(sysClk125) then

         -- Display strobe, 320ns
         dispStrobe <= sysClkCnt(4) after tpd;

         -- Update Display 0
         if sysClkCnt(15 downto 0) = x"8000" then
            dispUpdateA <= '1' after tpd;
         else
            dispUpdateA <= '0' after tpd;
         end if;

         -- Update Display B
         if sysClkCnt(15 downto 0) = x"0000" then
            dispUpdateB <= '1' after tpd;
         else
            dispUpdateB <= '0' after tpd;
         end if;

         -- Update counter
         sysClkCnt <= sysClkCnt + 1 after tpd;


      end if;
   end process;

  paused <= uOr(pausedc);
  resetdelay <= uOr(resetdelayc);
  incrementdelay <= incrementdelayc(0) or incrementdelayc(1) or incrementdelayc (2) or incrementdelayc(3);
  serbusy <= uOr(serbusyc and present);
  tdcreadoutbusy<= uOr(tdcreadoutbusyc);
  rstFromCore <= uOr(rstFromCorec);

  fifothresh <= uOr(fifothreshc and present);
  trigenabled <= uAnd(trigenabledc or not present) and uOr(present);
  trigAdc <= uOr(trigAdcc and present);

  reload <= uAnd(reloadc) and extreload;       -- active low
  resetOut <= uOr(resetOutc);
  phasebusyEn <= uOr(phasebusyEnc);
  phasebusySel <= phasebusySelc(0) or phasebusySelc(1) or phasebusySelc(2) or phasebusySelc(3);

  process(present, discopc) begin
    if(present(0)='1')then discop<=discopc(0);
    elsif(present(1)='1')then discop<=discopc(1);                     
    elsif(present(2)='1')then discop<=discopc(2);
    else discop<=discopc(3);
    end if;
  end process;

  process(present, calibmodec) begin
    if(present(0)='1')then calibmode<=calibmodec(0);
    elsif(present(1)='1')then calibmode<=calibmodec(1);                     
    elsif(present(2)='1')then calibmode<=calibmodec(2);
    else calibmode<=calibmodec(3);
    end if;
  end process;

  process(present, triggermaskc) begin
    if(present(0)='1')then triggermask<=triggermaskc(0);
    elsif(present(1)='1')then triggermask<=triggermaskc(1);                     
    elsif(present(2)='1')then triggermask<=triggermaskc(2);
    else triggermask<=triggermaskc(3);
    end if;
  end process;

  process(present, periodc) begin
    if(present(0)='1')then period<=periodc(0);
    elsif(present(1)='1')then period<=periodc(1);                     
    elsif(present(2)='1')then period<=periodc(2);
    else period<=periodc(3);
    end if;
  end process;

  process(present, telescopeopc) begin
    if(present(0)='1')then telescopeop<=telescopeopc(0);
    elsif(present(1)='1')then telescopeop<=telescopeopc(1);                     
    elsif(present(2)='1')then telescopeop<=telescopeopc(2);
    else telescopeop<=telescopeopc(3);
    end if;
  end process;

  process(present, phaseclkselc) begin
    if(present(0)='1')then phaseclksel<=phaseclkselc(0);
    elsif(present(1)='1')then phaseclksel<=phaseclkselc(1);                     
    elsif(present(2)='1')then phaseclksel<=phaseclkselc(2);
    else phaseclksel<=phaseclkselc(3);
    end if;
  end process;

  process(present, startmeasc) begin
    if(present(0)='1')then startmeas<=startmeasc(0);
    elsif(present(1)='1')then startmeas<=startmeasc(1);                     
    elsif(present(2)='1')then startmeas<=startmeasc(2);
    else startmeas<=startmeasc(3);
    end if;
  end process;

  U_triggerlogic: entity work.triggerlogic 
    port map(
      clk => sysClk125,
      rst => rstFromCore,
      clk160 => pgpClk,
      rst160 => pgpReset,
      -- hardware inputs
      discin => discinb,
      hitbusin => hitbus(1 downto 0),
      -- HSIO trigger
      HSIObusy => hsiobusyb,
      HSIOtrigger => hsiotriggerb,
      HSIObusyin => hsiobusyinb,
      hitbusout => open,
      
      -- eudet trigger
      exttrigger => exttrigger,
      extrst => extrst,
      extbusy => extbusy,
      exttrgclk => exttrgclk,
      
      calibmode => calibmode,
      startmeas => startmeas,
      eudaqdone => eudaqdone,
      eudaqtrgword => eudaqtrgword,
      tcounter1 => tcounter1,
      tcounter2 => tcounter2,
      
      trigenabled => trigenabled,
      paused => paused,
      triggermask => triggermask,
      resetdelay => resetdelay,
      incrementdelay => incrementdelay,
      discop => discop,
      telescopeop => telescopeop,
      period => period,
      fifothresh => fifothresh,
      serbusy => serbusy,
      tdcreadoutbusy => tdcreadoutbusy,
      phaseclksel => phaseclksel,
      phasebusyEn => phasebusyEn,
      phasebusySel => phasebusySel,
      l1a => l1a,
      triggerword => triggerword,
      busy =>busy,
      coincd =>open
);

   -- FPGA Cores
   -- Telescope 1
   U_HsioCosmicCore0: entity work.HsioPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 2,
                  rawFirstChannel => 11,
                  rawLastChannel => 10,
                  buffersizefe => 4096,
                  buffersizetdc => 16384,
                  fixed160 => '1',
                  encodingDefault => "00",
                  hitbusreadout => '0')
     port map (
      sysClk250  => sysClk250,  sysRst250 => sysRst250,
      sysClk125  => sysClk125,  sysRst125 => sysRst125,
      refClock   => refClock,   pgpClk    => pgpClk,
      pgpClk90 => pgpClk90, pgpReset   => pgpReset,
      clk320 => clk320, reload => reloadc(0),
      mgtRxN    => iMgtRxN(0),
      mgtRxP     => iMgtRxP(0),     mgtTxN    => oMgtTxN(0),
      mgtTxP     => oMgtTxP(0),
      serialin(2 downto 0)  => serialinb(2 downto 0),
      serialin(15 downto 3) => "0000000000000",
      serialout(2 downto 0)  => serialoutb(2 downto 0),
      serialout(15 downto 3) => open,
      clock160 => pgpClk, 
      clock80   => halfclock, clock40 => quarterclock,
      resetOut  => resetOutc(0), l1a => l1ac(0),
      latchtriggerword => triggerword, tcounter1=>tcounter1,
      tcounter2 => tcounter2, busy =>busy, 
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      calibmodeout => calibmodec(0), startmeas => startmeasc(0),
      pausedout => pausedc(0), present => present(0),
      trgenabledout => trigenabledc(0), rstFromCore => rstFromCorec(0),
      fifothresh => fifothreshc(0), triggermask => triggermaskc(0),
      discop => discopc(0), period => periodc(0),
      telescopeop => telescopeopc(0), resetdelay => resetdelayc(0),
      incrementdelay => incrementdelayc(0),
      sbusy => serbusyc(0), tdcbusy => tdcreadoutbusyc(0),
      phaseclksel  => phaseclkselc(0) , hitbus => hitbus(0),
      debug     => open, 
      exttriggero => open, extrsto => open,
      doricreset => open,
      phasebusyEn => phasebusyEnc(0), phasebusySel => phasebusySelc(0),
      dispDigitA => dispDigitAA(0), dispDigitB => dispDigitBB(0),
      dispDigitC => dispDigitCC(0), dispDigitD => dispDigitDD(0),
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      pgpClkUnbuf => pgpClkUnbuf, pgpClk90Unbuf => pgpClk90Unbuf,
      sysClkUnbuf => sysClkUnbuf, trigAdc => trigAdcc(0),
      sendAdcData => sendAdcData, adcData => adcDataChan(0)
   );
   --Remap ADC channels. Unused channel is f.
   adcDataChan(0)(0)<=x"000f";
   adcDataChan(0)(1)<=x"0f0f";
   adcDataChan(0)(2)<=x"0f0f";
   adcDataChan(0)(3)<=x"0f0f";
   adcDataChan(0)(11 downto 4)<=adcData(11 downto 4);
   -- Telescope 2
   U_HsioCosmicCore1: entity work.HsioPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 2,
                  rawFirstChannel => 11,
                  rawLastChannel => 10,
                  buffersizefe => 4096,
                  buffersizetdc => 16384,
                  fixed160 => '1',
                  encodingDefault => "00",
                  hitbusreadout => '0')
     port map (
      sysClk250  => sysClk250,  sysRst250 => sysRst250,
      sysClk125  => sysClk125,  sysRst125 => sysRst125,
      refClock   => refClock,   pgpClk    => pgpClk,
      pgpClk90 => pgpClk90, pgpReset   => pgpReset,
      clk320 => clk320, reload => reloadc(1),
      mgtRxN    => iMgtRxN(1),
      mgtRxP     => iMgtRxP(1),     mgtTxN    => oMgtTxN(1),
      mgtTxP     => oMgtTxP(1),
      serialin(2 downto 0)  => serialinb(5 downto 3),
      serialin(15 downto 3) => "0000000000000",
      serialout(2 downto 0)  => serialoutb(5 downto 3),
      serialout(15 downto 3) => open,
      clock160 => pgpClk, 
      clock80   => halfclock, clock40 => quarterclock,
      resetOut  => resetOutc(1), l1a => l1ac(1),
      latchtriggerword => triggerword, tcounter1=>tcounter1,
      tcounter2 => tcounter2, busy =>busy, 
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      calibmodeout => calibmodec(1), startmeas => startmeasc(1),
      pausedout => pausedc(1), present => present(1),
      trgenabledout => trigenabledc(1), rstFromCore => rstFromCorec(1),
      fifothresh => fifothreshc(1), triggermask => triggermaskc(1),
      discop => discopc(1), period => periodc(1),
      telescopeop => telescopeopc(1), resetdelay => resetdelayc(1),
      incrementdelay => incrementdelayc(1),
      sbusy => serbusyc(1), tdcbusy => tdcreadoutbusyc(1),
      phaseclksel  => phaseclkselc(1) , hitbus => hitbus(1),
      debug     => open, 
      exttriggero => open, extrsto => open,
      doricreset => open,
      phasebusyEn => phasebusyEnc(1), phasebusySel => phasebusySelc(1),
      dispDigitA => dispDigitAA(1), dispDigitB => dispDigitBB(1),
      dispDigitC => dispDigitCC(1), dispDigitD => dispDigitDD(1),
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      pgpClkUnbuf => pgpClkUnbuf, pgpClk90Unbuf => pgpClk90Unbuf,
      sysClkUnbuf => sysClkUnbuf, trigAdc => trigAdcc(1),
      sendAdcData => sendAdcData, adcData => adcDataChan(1)
   );
   adcDataChan(1)(0)<=x"0f00";
   adcDataChan(1)(1)<=x"0f0f";
   adcDataChan(1)(2)<=x"0f0f";
   adcDataChan(1)(3)<=x"0f0f";
   adcDataChan(1)(11 downto 4)<=adcData(11 downto 4);
   -- Telescope 3
   U_HsioCosmicCore2: entity work.HsioPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 0,
                  rawFirstChannel => 11,
                  rawLastChannel => 10,
                  buffersizefe => 8192,
                  buffersizetdc => 16384,
                  fixed160 => '0',
                  encodingDefault => "00",
                  hitbusreadout => '0')
     port map (
      sysClk250  => sysClk250,  sysRst250 => sysRst250,
      sysClk125  => sysClk125,  sysRst125 => sysRst125,
      refClock   => refClock,   pgpClk    => pgpClk,
      pgpClk90 => pgpClk90, pgpReset   => pgpReset,
      clk320 => clk320, reload => reloadc(2),
      mgtRxN    => iMgtRxN(2),
      mgtRxP     => iMgtRxP(2),     mgtTxN    => oMgtTxN(2),
      mgtTxP     => oMgtTxP(2),
      serialin(0)  => serialinb(6),
      serialin(15 downto 1) => "000000000000000",
      serialout(0)  => serialoutb(6),
      serialout(15 downto 1) => open,
      clock160 => pgpClk, 
      clock80   => halfclock, clock40 => quarterclock,
      resetOut  => resetOutc(2), l1a => l1ac(2),
      latchtriggerword => triggerword, tcounter1=>tcounter1,
      tcounter2 => tcounter2, busy =>busy, 
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      calibmodeout => calibmodec(2), startmeas => startmeasc(2),
      pausedout => pausedc(2), present => present(2),
      trgenabledout => trigenabledc(2), rstFromCore => rstFromCorec(2),
      fifothresh => fifothreshc(2), triggermask => triggermaskc(2),
      discop => discopc(2), period => periodc(2),
      telescopeop => telescopeopc(2), resetdelay => resetdelayc(2),
      incrementdelay => incrementdelayc(2),
      sbusy => serbusyc(2), tdcbusy => tdcreadoutbusyc(2),
      phaseclksel  => phaseclkselc(2) , hitbus => hitbus(2),
      debug     => open, 
      exttriggero => open, extrsto => open,
      doricreset => open,
      phasebusyEn => phasebusyEnc(2), phasebusySel => phasebusySelc(2),
      dispDigitA => dispDigitAA(2), dispDigitB => dispDigitBB(2),
      dispDigitC => dispDigitCC(2), dispDigitD => dispDigitDD(2),
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      pgpClkUnbuf => pgpClkUnbuf, pgpClk90Unbuf => pgpClk90Unbuf,
      sysClkUnbuf => sysClkUnbuf, trigAdc=>trigAdcc(2),
      sendAdcData => sendAdcData, adcData => adcDataChan(2)
   );
   adcDataChan(2)(0)<=x"0f0f";
   adcDataChan(2)(1)<=x"0201";
   adcDataChan(2)(2)<=x"0f00";
   adcDataChan(2)(3)<=x"0f0f";
   adcDataChan(2)(11 downto 4)<=adcData(11 downto 4);
   -- DUT
   U_HsioCosmicCore3: entity work.HsioPixelCore
     generic map( framedFirstChannel=> 0,
                  framedLastChannel=> 0,
                  rawFirstChannel => 11,
                  rawLastChannel => 10,
                  buffersizefe => 8192,
                  buffersizetdc => 16384,
                  fixed160 => '0',
                  encodingDefault => "00",
                  hitbusreadout => '0')
     port map (
      sysClk250  => sysClk250,  sysRst250 => sysRst250,
      sysClk125  => sysClk125,  sysRst125 => sysRst125,
      refClock   => refClock,   pgpClk    => pgpClk,
      pgpClk90 => pgpClk90, pgpReset   => pgpReset,
      clk320 => clk320, reload => reloadc(3),
      mgtRxN    => iMgtRxN(3),
      mgtRxP     => iMgtRxP(3),     mgtTxN    => oMgtTxN(3),
      mgtTxP     => oMgtTxP(3),
      serialin(0)  => serialinb(7),
      serialin(15 downto 1) => "000000000000000",
      serialout(0)  => serialoutb(7),
      serialout(15 downto 1) => open,
      clock160 => pgpClk, 
      clock80   => halfclock, clock40 => quarterclock,
      resetOut  => resetOutc(3), l1a => l1ac(3),
      latchtriggerword => triggerword, tcounter1=>tcounter1,
      tcounter2 => tcounter2, busy =>busy, 
      eudaqdone => eudaqdone, eudaqtrgword => eudaqtrgword,
      calibmodeout => calibmodec(3), startmeas => startmeasc(3),
      pausedout => pausedc(3), present => present(3),
      trgenabledout => trigenabledc(3), rstFromCore => rstFromCorec(3),
      fifothresh => fifothreshc(3), triggermask => triggermaskc(3),
      discop => discopc(3), period => periodc(3),
      telescopeop => telescopeopc(3), resetdelay => resetdelayc(3),
      incrementdelay => incrementdelayc(3),
      sbusy => serbusyc(3), tdcbusy => tdcreadoutbusyc(3),
      phaseclksel  => phaseclkselc(3) , hitbus => hitbus(3),
      debug     => open, 
      exttriggero => open, extrsto => open,
      doricreset => open,
      phasebusyEn => phasebusyEnc(3), phasebusySel => phasebusySelc(3),
      dispDigitA => dispDigitAA(3), dispDigitB => dispDigitBB(3),
      dispDigitC => dispDigitCC(3), dispDigitD => dispDigitDD(3),
      dispDigitE => open, dispDigitF => open,
      dispDigitG => open, dispDigitH => open,
      pgpClkUnbuf => pgpClkUnbuf, pgpClk90Unbuf => pgpClk90Unbuf,
      sysClkUnbuf => sysClkUnbuf, trigAdc => trigAdcc(3),
      sendAdcData => sendAdcData, adcData => adcDataChan(3)
   );
   adcDataChan(3)(0)<=x"0f0f";
   adcDataChan(3)(1)<=x"0f0f";
   adcDataChan(3)(2)<=x"020f";
   adcDataChan(3)(3)<=x"0100";
   adcDataChan(3)(11 downto 4)<=adcData(11 downto 4);
   tempadc_inst: entity work.tempadc
     generic map(
       PRESCALE_G => 79,
       MAPPING_G => (0, 1, 2, 3, 5, 4, 6, 7) )  
     port map(
       clk => sysClk125,
       d_out => adcData,
       trig => trigAdc,
       ld => sendAdcData,
       adcAS => adcAs,
       scl => adcI2cScl,
       sda => adcI2cSda
       );
   HSIOCORES:
   for I in 0 to 3 generate
     l1ac(I) <= l1a and present(I);
     transdissig(I)<='0';
   end generate HSIOCORES;

   dispDigitE<=dispDigitAA(0);
   dispDigitF<=dispDigitAA(1);
   dispDigitG<=dispDigitAA(2);
   dispDigitH<=dispDigitAA(3);
   dispDigitA<=dispDigitBB(0);
   dispDigitB<=dispDigitBB(1);
   dispDigitC<="0000"&dispDigitCC(0)(0)&dispDigitCC(1)(0)&dispDigitCC(2)(0)&dispDigitCC(3)(0);
   dispDigitD<=dispDigitDD(3);
   
   sysClk125i <= not sysClk125;
   --sysClk125i<=debug(0);
   U_clock160: entity work.clock160 port map(
     CLKIN_N_IN => iMainClkN,
     CLKIN_P_IN => iMainClkP,
     RST_IN => sysRst125,
     CLKFX_OUT => mainClk,
     CLKIN_IBUFGDS_OUT => clkin,
     CLK0_OUT => clk0,
     LOCKED_OUT => open);
  
   process(mainClk)
   begin
     if(mainClk'event and mainClk='1') then
       halfclock<= not halfclock;
     end if;
   end process;
   process(halfclock)
   begin
     if(halfclock'event and halfclock='1') then
       quarterclock<= not quarterclock;
     end if;
   end process;
      U_idelctrlclk: entity work.clock200 port map(
       CLKIN_IN  => mainClk,
       RST_IN    => holdrst,
       CLKFX_OUT => clockidctrl,
       CLK0_OUT  => open,
       LOCKED_OUT => lockedid);  
     U_idelayctrl: IDELAYCTRL port map(
       RDY     => open,
       REFCLK  => clockidctrl,
       RST     => idctrlrst);

     process(sysRst125, sysClk125) -- clock interface
     begin
       if (sysRst125='1') then
         holdctr<=(others=>'1');
         holdrst<='1';
       elsif(sysClk125'event and sysClk125='1') then
         if (holdctr(24)='0') then
           holdrst<='0';
         end if;
         holdctr<=unsigned(holdctr)-1;
       end if;
     end process;

     process(sysClk125, sysRst125) -- reset logic for IDELAYCTRL
     begin
       if(sysRst125='1') then
         idctrlrst<='0';
         idcounter<="000";
         oldlockedid<='0';
       elsif(sysClk125'event and sysClk125='1') then
         if(lockedid='1' and oldlockedid='0')then
           idcounter<="111";
           idctrlrst<='1';
         elsif(unsigned(idcounter)>0)then
           idcounter<=unsigned(idcounter)-1;
         else
           idctrlrst<='0';
         end if;
         oldlockedid<=lockedid;
       end if;
     end process;

end HsioCosmic;
