-------------------------------------------------------------------------------
-- Title         : Version Constant File
-- Project       : HSIO 
-------------------------------------------------------------------------------
-- File          : Version.vhd
-- Author        : Martin Kocian, kocian@slac.stanford.edu
-- Created       : 01/07/2013
-------------------------------------------------------------------------------
-- Description:
-- Version Constant Module
-------------------------------------------------------------------------------
-- Copyright (c) 2012 by SLAC. All rights reserved.
-------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

package Version is

constant FpgaVersion : std_logic_vector(31 downto 0) := x"00000009"; -- MAKE_VERSION

end Version;

-------------------------------------------------------------------------------
-- Revision History:
-- 01/07/2013 (0x00000001): Initial XST version
-- 03/22/2013 (0x00000002): HSIO trigger/busy added to opto ucf file
-- 04/02/2013 (0x00000003): Memory buffer for triggering.
-- 06/20/2014 (0x00000004): Added hitbus readout for DBM
-- 07/24/2014 (0x00000005): Separated trigger from core.
-- 11/07/2014 (0x00000006): Set multiplicities for multiplexer
-- 12/15/2014 (0x00000007): Added Temperature ADC readout
-- 01/09/2015 (0x00000008): New readout for FEI4
-- 01/09/2015 (0x00000009): New ADC data format, max 31 readout links.
-------------------------------------------------------------------------------
