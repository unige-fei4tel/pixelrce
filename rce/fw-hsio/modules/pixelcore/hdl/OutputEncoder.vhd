
LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity outputencoder is 
   port (
     clock      : in std_logic;
     rst        : in std_logic;
     clockx2    : in std_logic;
     datain     : in std_logic;
     encode     : in std_logic_vector(1 downto 0);
     dataout    : out std_logic
     );
end outputencoder;

architecture OUTPUTENCODER of outputencoder is

  signal bmp: std_logic;
  signal man: std_logic;

begin

  process(clockx2, rst) 
  begin
    if(rst='1') then
      bmp<='0';
    elsif(clockx2'event and clockx2='1') then
      if(clock='1' or datain='1')then
        bmp<=not bmp;
      end if;
    end if;
  end process;

  process(clockx2, rst) 
  begin
    if(rst='1') then
      man<='0';
    elsif(clockx2'event and clockx2='1') then
      if(clock='1')then
        man<= datain;
      else
        man<=not datain;
      end if;
    end if;
  end process;

  with encode select
      dataout<= datain when "00",
                bmp when "01",
                man when "10",
                datain when others;

end OUTPUTENCODER;
