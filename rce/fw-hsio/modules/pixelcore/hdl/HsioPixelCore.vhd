-------------------------------------------------------------------------------
-- Title         : BNL ASIC Test FGPA Core 
-- Project       : LCLS Detector, BNL ASIC
-------------------------------------------------------------------------------
-- File          : BnlAsicCore.vhd
-- Author        : Ryan Herbst, rherbst@slac.stanford.edu
-- Created       : 07/21/2008
-------------------------------------------------------------------------------
-- Description:
-- Core logic for BNL ASIC test FPGA.
-------------------------------------------------------------------------------
-- Copyright (c) 2008 by Ryan Herbst. All rights reserved.
-------------------------------------------------------------------------------
-- Modification history:
-- 07/21/2008: created.
-------------------------------------------------------------------------------

LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.arraytype.all;
use work.Version.all;
use work.StdRtlPkg.all;

entity HsioPixelCore is 
   generic (
     framedFirstChannel   : integer := 0; --First framed channel
     framedLastChannel    : integer := 7; --Last framed channel
     rawFirstChannel      : integer := 11; --First raw channel
     rawLastChannel       : integer := 14; -- Last raw channel
     buffersizefe         : integer := 8192; --FIFO size
     buffersizetdc        : integer := 8192; --FIFO size
     fixed160             : std_logic := '0';-- fixed 160 or choice 40/160
     encodingDefault      : std_logic_vector(1 downto 0) := "00"; --BPM or Manchester or nothing on startup
     hitbusreadout        : std_logic := '0' -- hitbus configuration
   );
   port ( 

      -- Master system clock, 250Mhz, 125Mhz
      sysClk250    : in  std_logic;
      sysClk125    : in  std_logic;
      sysRst125    : in  std_logic;
      sysRst250    : in  std_logic;

      -- PGP Clocks
      refClock     : in  std_logic;
      pgpClk       : in  std_logic;
      pgpClk90     : in  std_logic;
      pgpReset     : in  std_logic;

      pgpClkUnbuf  : in std_logic;
      pgpClk90Unbuf: in std_logic;
      sysClkUnbuf  : in std_logic;

      clk320       : in std_logic;
      -- reload firmware
      reload       : out std_logic;

      -- MGT Serial Pins
      mgtRxN       : in  std_logic;
      mgtRxP       : in  std_logic;
      mgtTxN       : out std_logic;
      mgtTxP       : out std_logic;

      -- ATLAS Pixel module pins
      serialin     : in std_logic_vector(15 downto 0);
      serialout    : out std_logic_vector(15 downto 0);
      clock160     : in std_logic;
      clock80      : in std_logic;
      clock40      : in std_logic;

      -- Reset out to PGP Clock generation
      resetOut     : out std_logic;

      -- Input from trigger logic
      l1a           : in std_logic;
      latchtriggerword: in std_logic_vector(7 downto 0);
      tcounter1     : in std_logic_vector(31 downto 0);
      tcounter2     : in std_logic_vector(31 downto 0);
      busy          : in std_logic;
      eudaqdone     : in std_logic;
      eudaqtrgword  : in std_logic_vector(14 downto 0);
 
      -- Output to trigger logic
      present       : out std_logic;
      calibmodeout  : out std_logic_vector(1 downto 0);
      startmeas     : out std_logic;
      pausedout     : out std_logic;
      trgenabledout : out std_logic;
      rstFromCore   : out std_logic;
      fifothresh    : out std_logic;
      triggermask   : out std_logic_vector(15 downto 0);
      discop        : out std_logic_vector(15 downto 0);
      period        : out std_logic_vector(31 downto 0);
      telescopeop   : out std_logic_vector(2 downto 0);

      resetdelay    : out std_logic;
      incrementdelay: out std_logic_vector(4 downto 0);
      sbusy         : out std_logic; --serbusy
      tdcbusy       : out std_logic;
      phaseclksel   : out std_logic;
      hitbus        : out std_logic;

      -- Debug
      debug         : out std_logic_vector(7 downto 0);
      exttriggero   : out std_logic;
      extrsto       : out std_logic;

      doricreset    : out std_logic;
      phasebusyEn   : out std_logic;
      phasebusySel  : out std_logic_vector(1 downto 0);
      
      dispDigitA    : out std_logic_vector(7 downto 0);
      dispDigitB    : out std_logic_vector(7 downto 0);
      dispDigitC    : out std_logic_vector(7 downto 0);
      dispDigitD    : out std_logic_vector(7 downto 0);
      dispDigitE    : out std_logic_vector(7 downto 0);
      dispDigitF    : out std_logic_vector(7 downto 0);
      dispDigitG    : out std_logic_vector(7 downto 0);
      dispDigitH    : out std_logic_vector(7 downto 0);
      trigAdc       : out sl:='0';
      sendAdcData   : in sl;
      adcData       : in Slv16Array(11 downto 0)

   );
end HsioPixelCore;


-- Define architecture
architecture HsioPixelCore of HsioPixelCore is

   component decode_8b10b_wrapper
     port (
       CLK        : in  STD_LOGIC;
       DIN        : in  STD_LOGIC_VECTOR(9 downto 0);
       DOUT       : out STD_LOGIC_VECTOR(7 downto 0);
       KOUT       : out STD_LOGIC;
       
       CE         : in  STD_LOGIC;
       SINIT      : in  STD_LOGIC;
       CODE_ERR   : out STD_LOGIC;
       ND         : out STD_LOGIC
       );
   end component;
   
   component datafifo
        generic (
          buffersize : integer :=8192 -- FIFO size
        );
	port (
	din: IN std_logic_VECTOR(17 downto 0);
	rd_clk: IN std_logic;
	rd_en: IN std_logic;
	rst: IN std_logic;
	wr_clk: IN std_logic;
	wr_en: IN std_logic;
	dout: OUT std_logic_VECTOR(17 downto 0);
	empty: OUT std_logic;
	full: OUT std_logic;
	overflow: OUT std_logic;
	prog_full: OUT std_logic;
	valid: OUT std_logic;
        underflow: out std_logic);
   end component;
   component datafifo8192
	port (
	din: IN std_logic_VECTOR(17 downto 0);
	rd_clk: IN std_logic;
	rd_en: IN std_logic;
	rst: IN std_logic;
	wr_clk: IN std_logic;
	wr_en: IN std_logic;
	dout: OUT std_logic_VECTOR(17 downto 0);
	empty: OUT std_logic;
	full: OUT std_logic;
	overflow: OUT std_logic;
	prog_full: OUT std_logic;
	valid: OUT std_logic;
        underflow: out std_logic);
   end component;
   COMPONENT datafifo16384
     PORT (
       rst : IN STD_LOGIC;
       wr_clk : IN STD_LOGIC;
       rd_clk : IN STD_LOGIC;
       din : IN STD_LOGIC_VECTOR(17 DOWNTO 0);
       wr_en : IN STD_LOGIC;
       rd_en : IN STD_LOGIC;
       dout : OUT STD_LOGIC_VECTOR(17 DOWNTO 0);
       full : OUT STD_LOGIC;
       overflow : OUT STD_LOGIC;
       empty : OUT STD_LOGIC;
       valid : OUT STD_LOGIC;
       underflow : OUT STD_LOGIC;
       prog_full : OUT STD_LOGIC
       );
   END COMPONENT;

   component datafifo1024
	port (
	din: IN std_logic_VECTOR(17 downto 0);
	rd_clk: IN std_logic;
	rd_en: IN std_logic;
	rst: IN std_logic;
	wr_clk: IN std_logic;
	wr_en: IN std_logic;
	dout: OUT std_logic_VECTOR(17 downto 0);
	empty: OUT std_logic;
	full: OUT std_logic;
	overflow: OUT std_logic;
	prog_full: OUT std_logic;
	valid: OUT std_logic;
        underflow: out std_logic);
   end component;

   component wordswapper 
    port (
     rst        : in std_logic;
     clk        : in std_logic;
     wordin     : in std_logic_vector(15 downto 0);
     eofin      : in std_logic;
     eeofin     : in std_logic;
     sofin      : in std_logic;
     validin    : in std_logic;
     wordout    : out std_logic_vector(15 downto 0);
     eofout     : out std_logic;
     eeofout    : out std_logic;
     sofout     : out std_logic;
     validout   : out std_logic
      );
   end component;

component fifo8b10b
	port (
	din: IN std_logic_VECTOR(9 downto 0);
	rd_clk: IN std_logic;
	rd_en: IN std_logic;
	rst: IN std_logic;
	wr_clk: IN std_logic;
	wr_en: IN std_logic;
	almost_empty: OUT std_logic;
	dout: OUT std_logic_VECTOR(9 downto 0);
	empty: OUT std_logic;
        full: OUT std_logic;
        underflow: OUT std_logic);
end component;
component fifo8b10bnew
	port (
	rst: IN std_logic;
	wr_clk: IN std_logic;
	rd_clk: IN std_logic;
	din: IN std_logic_VECTOR(9 downto 0);
	wr_en: IN std_logic;
	rd_en: IN std_logic;
	dout: OUT std_logic_VECTOR(9 downto 0);
	full: OUT std_logic;
	empty: OUT std_logic;
	almost_empty: OUT std_logic;
	valid: OUT std_logic);
end component;

component noframefifo
	port (
	rst: IN std_logic;
	wr_clk: IN std_logic;
	rd_clk: IN std_logic;
	din: IN std_logic_VECTOR(17 downto 0);
	wr_en: IN std_logic;
	rd_en: IN std_logic;
	dout: OUT std_logic_VECTOR(17 downto 0);
	full: OUT std_logic;
	empty: OUT std_logic;
	almost_empty: OUT std_logic;
	valid: OUT std_logic);
end component;

   component OBUF    port ( O : out std_logic; I  : in  std_logic ); end component;
   -- PGP Front End Wrapper

   component BUFGMUX port (O: out std_logic; I0: in std_logic; I1: in std_logic; S: in std_logic); end component;

 component ila
    PORT (
      CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CLK : IN STD_LOGIC;
      DATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TRIG0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0));

 end component;
  component icon
    PORT (
      CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

  end component;
  component ODDR port ( 
         Q  : out std_logic;
         CE : in std_logic;
         C  : in std_logic;
         D1 : in std_logic;
         D2 : in std_logic;
         R  : in std_logic;
         S  : in std_logic
      );
  end component;

  --attribute syn_noprune : boolean;
  --attribute syn_noprune of chipscope : label is true;
  --attribute syn_noprune of chipscopeicon : label is true;


 component pattern_blk_mem
         port (
         clka: IN std_logic;
         dina: IN std_logic_VECTOR(31 downto 0);
         addra: IN std_logic_VECTOR(9 downto 0);
         ena: IN std_logic;
         wea: IN std_logic_VECTOR(0 downto 0);
         douta: OUT std_logic_VECTOR(31 downto 0);
         clkb: IN std_logic;
         dinb: IN std_logic_VECTOR(31 downto 0);
         addrb: IN std_logic_VECTOR(9 downto 0);
         enb: IN std_logic;
         web: IN std_logic_VECTOR(0 downto 0);
         doutb: OUT std_logic_VECTOR(31 downto 0));
 end component;

   -- Local Signals
   signal cmdEn           : std_logic;
   signal cmdOpCode       : std_logic_vector(7  downto 0);
   signal cmdCtxOut       : std_logic_vector(23 downto 0);
   signal readDataValid   : std_logic;
   signal readDataSOF     : std_logic;
   signal readDataEOF     : std_logic;
   signal readDataEOFE    : std_logic;
   signal readData        : std_logic_vector(15 downto 0);
   signal pgpDispA        : std_logic_vector(7 downto 0);
   signal pgpDispB        : std_logic_vector(7 downto 0);
   signal regReq          : std_logic;
   signal regOp           : std_logic;
   signal regAck          : std_logic;
   signal regFail         : std_logic;
   signal regAddr         : std_logic_vector(23 downto 0);
   signal regDataOut      : std_logic_vector(31 downto 0);
   signal regDataIn       : std_logic_vector(31 downto 0);
   signal ack             : std_logic;
   signal err             : std_logic;
   signal eof             : std_logic;
   signal sof             : std_logic;
   signal vvalid          : std_logic;
   signal req             : std_logic;
   signal frameRxValid    : std_logic;
   signal frameRxReady    : std_logic;
   signal frameRxSOF      : std_logic;
   signal frameRxEOF      : std_logic;
   signal frameRxEOFE     : std_logic;
   signal frameRxData     : std_logic_vector(15 downto 0);
   signal regInp          : std_logic;
   signal blockdata       : std_logic_vector(17 downto 0);
   signal configdataout   : std_logic_vector(17 downto 0);
   signal blockcounter    : std_logic_vector(2 downto 0);
   signal handshake       : std_logic;
   signal replynow        : std_logic;
   signal readword        : std_logic;
   signal swapsof         : std_logic;
   signal swappedeof      : std_logic;
   signal oldswappedeof   : std_logic;
   signal gocounter       : std_logic_vector(3 downto 0);
   signal swapvalid       : std_logic;
   signal readvalid       : std_logic;
   signal counter1        : std_logic_vector(3 downto 0);
   signal counter2        : std_logic_vector(3 downto 0);
   signal idcounter       : std_logic_vector(2 downto 0);
   signal daddr           : std_logic_vector (6 downto 0);
   signal denphase        : std_logic;
   signal phaseclk        : std_logic;
   signal phaseclk90      : std_logic;
   signal phaseclk180     : std_logic;
   signal phaseclk270     : std_logic;
   signal o01             : std_logic;
   signal o23             : std_logic;
   signal clockselect     : std_logic_vector(1 downto 0);
   signal fxclock         : std_logic;
   signal serclk          : std_logic;
   signal drdy            : std_logic;
   signal drdy2           : std_logic;
   signal lockedfx        : std_logic;
   signal lockedid        : std_logic;
   signal oldlockedid     : std_logic;
   signal lockedphase     : std_logic;
   signal lockednophase   : std_logic;
   signal reqclkphase     : std_logic;
   signal oldreqclkphase  : std_logic;
   signal clkenaphase     : std_logic;
   signal holdrst         : std_logic;
   signal phaserst        : std_logic;
   signal clockrst        : std_logic;
   signal clockrst2       : std_logic;
   signal idctrlrst       : std_logic;
   signal clkdata         : std_logic_vector(15 downto 0);
   signal holdctr         : std_logic_vector(24 downto 0);
   signal clockidctrl     : std_logic;
   signal dreset          : std_logic_vector(23 downto 16);
   signal ena             : std_logic;
   signal cout            : std_logic;
   signal enaold          : std_logic;
   signal pgpEnaOld       : std_logic;
   signal countclk        : std_logic;
   signal clockcountv     : std_logic_vector(16 downto 0);
   signal starttdcreadout : std_logic;
   signal resettdc        : std_logic;
   signal counterout1     : std_logic_vector(31 downto 0);
   signal counterout2     : std_logic_vector(31 downto 0);
   signal oldl1a          : std_logic;
   signal l1amod          : std_logic;
   signal stop            : std_logic;
   signal oldstop         : std_logic;
   signal go              : std_logic;
   signal qout            : std_logic_vector(15 downto 0);
   signal tdcdata         : std_logic_vector(17 downto 0);
   signal tdcld           : std_logic;
   signal datawaiting     : std_logic_vector(31 downto 0);
   signal moredatawaiting : std_logic_vector(31 downto 0);
   signal indatavalid     : std_logic_vector(31 downto 0);
   signal chanld          : std_logic_vector(31 downto 0);
   signal datain          : dataarray;
   signal channeldata     : dataarray;
   signal dataout         : std_logic_vector(31 downto 0);
   signal eofout          : std_logic;
   signal sofout          : std_logic;
   signal datavalid       : std_logic;
   signal reqdata         : std_logic_vector(31 downto 0);
   signal dfifothresh     : std_logic_vector(31 downto 0);
   signal andr            : std_logic_vector(31 downto 0);
   signal empty           : std_logic;
   signal full            : std_logic;
   signal configfull      : std_logic;
   signal bufoverflow     : std_logic_vector(31 downto 0);
   signal overflow        : std_logic_vector(31 downto 0);
   signal underflow       : std_logic_vector(31 downto 0);
   signal prog_full       : std_logic;
   signal emptyv          : std_logic_vector(31 downto 0);
   signal fullv           : std_logic_vector(31 downto 0);
   signal overflowv       : std_logic_vector(31 downto 0);
   signal underflowv      : std_logic_vector(31 downto 0);
   signal prog_fullv      : std_logic_vector(31 downto 0);
   signal isrunning       : std_logic_vector(31 downto 0);
   signal startdc         : std_logic;
   signal rst             : std_logic;
   signal softrst         : std_logic;
   signal trigenabled     : std_logic;
   signal exttrgclkeu     : std_logic;
   signal waitfordata     : std_logic;
   signal marker          : std_logic;
   signal mxdata          : std_logic_vector(15 downto 0);
   signal mxvalid         : std_logic;
   signal mxeof           : std_logic;
   signal mxsof           : std_logic;
   signal ldser           : std_logic;
   signal configoverflow  : std_logic;
   signal configunderflow : std_logic;
   signal d_out           : std_logic;
   signal serbusy         : std_logic;
   signal trgbusy         : std_logic;
   signal stdc            : std_logic;
   signal calibmode       : std_logic_vector(1 downto 0);
   signal edge1           : std_logic;
   signal edge2           : std_logic;
   signal startrun        : std_logic;
   signal paused          : std_logic;
   signal nobackpressure  : std_logic;
   signal nobackpress     : std_logic;
   signal reg             : std_logic_vector(31 downto 0);
   signal backprescounter : std_logic_vector(3 downto 0);
   signal counter4        : std_logic_vector(31 downto 0);
   signal counter10       : std_logic_vector(31 downto 0);
   signal counter10e      : std_logic_vector(31 downto 0);
   signal tdccounter      : std_logic_vector(15 downto 0);
   signal counter4m       : std_logic_vector(31 downto 0);
   signal counter10m      : std_logic_vector(31 downto 0);
   signal frameTxAFull    : std_logic;
   signal channelmask     : std_logic_vector(31 downto 0);
   signal channeloutmask  : std_logic_vector(31 downto 0);
   signal enablereadout   : std_logic_vector(31 downto 0);
   signal phases          : std_logic_vector(31 downto 0);
   signal status          : std_logic_vector(31 downto 0);
   signal statusd         : std_logic_vector(31 downto 0);
   signal trgtime         : std_logic_vector(63 downto 0);
   signal trgtimel        : std_logic_vector(63 downto 0);
   signal deadtime        : std_logic_vector(63 downto 0);
   signal deadtimel       : std_logic_vector(63 downto 0);
   signal l1count         : std_logic_vector(3 downto 0);
   signal l1countlong     : std_logic_vector(31 downto 0);
   signal l1countl        : std_logic_vector(3 downto 0);
   signal rstl1count      : std_logic;
   signal starttdccount   : std_logic_vector(3 downto 0);
   signal trgdelay        : std_logic_vector(7 downto 0);
   signal markercounter   : std_logic_vector(9 downto 0);
   signal conftrg         : std_logic;
   signal ld8b10b         : std_logic_vector(15 downto 0);
   signal ldout8b10b      : std_logic_vector(15 downto 0);
   signal data8b10b       : array10b;
   signal dout8b10b       : array10b;
   signal ldfei4          : std_logic_vector(15 downto 0);
   signal ldfifo          : std_logic_vector(15 downto 0);
   signal dnextvalid      : std_logic_vector(15 downto 0);
   signal dvalid          : std_logic_vector(15 downto 0);
   signal frameoverflow   : std_logic_vector(15 downto 0);
   signal fei4data        : fei4array;
   signal fifodata        : fei4array;
   signal dnext           : fei4array;
   signal aligned         : std_logic_vector(15 downto 0);
   signal alignout        : std_logic_vector(15 downto 0);
   signal receiveclock    : std_logic;
   signal receiveclock90  : std_logic;
   signal serdesclock     : std_logic;
   signal recdata         : std_logic_vector(15 downto 0);
   signal read8b10bfifo   : std_logic_vector(15 downto 0);
   signal fifo8b10bempty  : std_logic_vector(15 downto 0);
   signal valid8b10b      : std_logic_vector(15 downto 0);
   signal pgpencdatain    : array10b;
   signal selfei4clk      : std_logic_vector(1 downto 0);
   signal trgcount        : std_logic_vector(15 downto 0);
   signal trgcountdown    : std_logic_vector(15 downto 0);

   signal encoding        : std_logic_vector(1 downto 0);
   signal oldbpm          : std_logic;
   signal doriccounter    : std_logic_vector(15 downto 0);
   signal doricresetb     : std_logic;
   signal trgin           : std_logic;
   signal setdeadtime     : std_logic_vector(15 downto 0);
   signal serialoutb      : std_logic_vector(15 downto 0);
   signal ccontrol        : std_logic_vector(35 downto 0);
   signal cdata           : std_logic_vector(31 downto 0);
   signal ctrig           : std_logic_vector(0 downto 0);
   signal setfifothresh   : std_logic_vector(15 downto 0);

   signal phaseConfig     : std_logic;

   signal hitbusa           : std_logic;
   signal hitbusb           : std_logic;
   signal hitbusop          : std_logic_vector(15 downto 0);
   signal hitbusin          : std_logic_vector(5 downto 0);
   signal hitbusword         : hitbusoutput;
   signal hitbusdepth : std_logic_vector(4 downto 0);
   signal tdcreadoutdelay:   std_logic_vector(4 downto 0);
    
   signal flatch:            std_logic_vector(7 downto 0);
   signal ofprotection      : std_logic;

   signal phaseClkUnbuf : std_logic;
   signal phaseClk90Unbuf : std_logic;
   signal phaseClk180Unbuf : std_logic;
   signal phaseClk270Unbuf : std_logic;

   signal writemem: std_logic;
   signal maxmem: std_logic_vector(9 downto 0);
   signal ld32: std_logic;
   signal oldld32: std_logic;
   signal serdata32: std_logic_vector(31 downto 0);
   signal readpointer: std_logic_vector(9 downto 0);
   signal memout : std_logic;
   signal stop32: std_logic;
   signal go32 : std_logic;
   signal going32: std_logic;
   signal l1route: std_logic;
   signal l1modin: std_logic;
   signal l1memin: std_logic;
   signal readmem: std_logic;
   signal bpl: std_logic;
   signal multiplicity: std_logic_vector(63 downto 0);
   signal tdcfifothresh: std_logic_vector(7 downto 0);
   signal oldtdcbusy: std_logic;
   signal tdcbusys: std_logic;

   signal adccounter: slv(31 downto 0):=x"00000000";
   signal adcperiod : slv(31 downto 0):=x"02625a00";
   signal oldadcperiod : slv(31 downto 0):=x"02625a00";
   signal enableAdcReadout: sl;

   signal oldbackpressure: sl:='1';
   signal olddft: sl:='1';
   signal maxlength: natural range 0 to 16383;
   signal oldindatav: sl;
   signal oldindata: slv(23 downto 0);

   -- Register delay for simulation
   constant tpd:time := 0.5 ns;
   type DELAR is array (0 to 15) of integer;
   constant setting: DELAR := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
   --attribute syn_noprune : boolean;
   constant maxchannel : integer := maximum(framedLastChannel, rawLastChannel);
   constant ackchannel : integer:=31;
   constant tdcchannel: integer:=30;
   constant adcchannel: integer:=29;

   function vectorize(s: std_logic) return std_logic_vector is
     variable v: std_logic_vector(0 downto 0);
   begin
     v(0) := s;
     return v;
   end;

   function vectorize(v: std_logic_vector) return std_logic_vector is
   begin
     return v;
   end;

begin


   -- PGP Front End
   U_PgpFrontEnd: entity work.PgpFrontEnd port map (
      pgpRefClk1    => refClock,       pgpRefClk2    => '0',
      mgtRxRecClk   => open,           pgpClk        => pgpClk,
      pgpReset      => pgpReset,       pgpDispA      => pgpDispA,
      pgpDispB      => pgpDispB,       resetOut      => resetOut,
      locClk        => sysClk125,      locReset      => sysRst125,
      cmdEn         => cmdEn,          cmdOpCode     => cmdOpCode,
      cmdCtxOut     => cmdCtxOut,      regReq        => regReq,
      regInp        => regInp,
      regOp         => regOp,          regAck        => regAck,
      regFail       => regFail,        regAddr       => regAddr,
      regDataOut    => regDataOut,     regDataIn     => regDataIn,
      frameTxEnable => readDataValid,  frameTxSOF    => readDataSOF,
      frameTxEOF    => readDataEOF,    frameTxEOFE   => readDataEOFE,
      frameTxData   => readData,       frameTxAFull  => frameTxAFull,
      frameRxValid  => frameRxValid,
      frameRxReady  => frameRxReady,   frameRxSOF    => frameRxSOF,
      frameRxEOF    => frameRxEOF,     frameRxEOFE   => frameRxEOFE,
      frameRxData   => frameRxData,    valid => vvalid,
      eof => eof, sof => sof,
      mgtRxN        => mgtRxN,
      mgtRxP        => mgtRxP,         mgtTxN        => mgtTxN,
      mgtTxP        => mgtTxP,         mgtCombusIn   => (others=>'0'),
      mgtCombusOut  => open
   );

   pausedout<=paused;
   trgenabledout<=trigenabled;
   calibmodeout<=calibmode;
   andr<= channeloutmask and dfifothresh;
   fifothresh<=andr(0) or andr(1) or andr(2) or andr(3) or andr(4)
               or andr(5) or andr(6) or andr(7) or andr(8)
               or andr(9) or andr(10) or andr(11) or andr(12)
               or andr(13) or andr(14) or andr(15) or andr(16)
               or andr(17) or andr(18) or andr(19) or andr(20)
               or andr(21) or andr(22) or andr(23) or andr(24)
               or andr(25) or andr(26) or andr(27) or andr(28)
               or andr(30);

   dispDigitA<=pgpDispA;
   dispDigitB<=pgpDispB;
--   dispDigitC(7 downto 1)<="000000"&bpl;
--   dispDigitC(0)<=flatch(7) or flatch(6) or flatch(5) or flatch(4) or flatch(3) or flatch(2) or flatch(1) or flatch(0);
   dispDigitC(7 downto 2)<=(others => '0');
   dispDigitD<=x"0"&FpgaVersion(3 downto 0);
   dispDigitE<=x"00";
   dispDigitF<=x"00";
   dispDigitG<=x"00";
   dispDigitH<=x"00";

   dispdigitC(0)<=dfifothresh(0);
   dispdigitC(1)<=frameTxAFull;
   --process(sysRst125, sysClk125) begin
   --  if(sysRst125='1')then
   --    dispDigitC(0)<='0';
   --    dispDigitC(1)<='0';
   --  elsif(rising_edge(sysClk125))then
   --    olddft<=overflowv(0);
   --    oldbackpressure<=frameTxAFull;
   --    if(overflowv(0)='1' and olddft='0')then
   --      dispDigitC(0)<='1';
   --    end if;
   --    if(frameTxAFull='1' and oldbackpressure='0')then
   --      dispDigitC(1)<='1';
   --    end if;
   --  end if;
   --end process;

   nobackpressure<= not frameTxAFull;
   --blockdata(15 downto 0) <= frameRxData;
   blockdata(17 downto 16) <= "00";
   frameRxReady <= not configfull;
   vvalid<='0';
   readDataEOFE<='0';
   --readData<=(others=>'0');
   regAck<= drdy2 or ack; 
   regFail<= '0';
   req<='1' when regReq='1' and regAddr(8 downto 7) = "00" else '0';   
   reqclkphase <='1' when regReq='1' and (regAddr(8 downto 4) = "11000" or regAddr(8 downto 4) = "11001" or regAddr(8 downto 4) = "11010") else '0';
   
     U_phaseshift: entity work.phaseshift port map(
       CLKIN_IN  => sysClk125,
       DADDR_IN  => daddr,
       DCLK_IN   => sysClk125,
       DEN_IN    => denphase,
       DI_IN     => regDataOut(15 downto 0),
       DWE_IN    => regOp,
       RST_IN    => clockrst2,
       CLK0_OUT  => phaseclk,
       CLK90_OUT  => phaseclk90,
       CLK180_OUT  => phaseclk180,
       CLK270_OUT  => phaseclk270,
       CLKFX_OUT  => open,
       CLK2X_OUT  => open,
       DRDY_OUT  => drdy2,
       LOCKED_OUT=> lockedphase,
       pclkUnbuf    => phaseClkUnbuf,
       pclk90Unbuf  => phaseClk90Unbuf,
       pclk180Unbuf => phaseClk180Unbuf,
       pclk270Unbuf => phaseClk270Unbuf
       );

     clockrst2<=phaserst;


     with regAddr(8 downto 4) select
       daddr<="1010101" when "11000",
              "0010001" when "11001",
              "0000000" when others;
     process(sysRst125, sysClk125) -- clock interface
     begin
       if (sysRst125='1') then
         holdctr<=(others=>'1');
       elsif(sysClk125'event and sysClk125='1') then
         if (holdctr=x"000000"&'0') then
           phaserst<='0';
         else
           if (holdctr(24)='0') then
             phaserst<='1';
           end if;
           holdctr<=unsigned(holdctr)-1;
         end if;
         if (reqclkphase='1' and oldreqclkphase='0') then
           clkenaphase<='1';
         else
           clkenaphase<='0';
         end if;
         denphase<=clkenaphase;
         oldreqclkphase<=reqclkphase;
       end if;
     end process;

    process(sysClk125,sysRst125) -- test for tdc
    begin
      if(sysRst125='1')then
        ena<='0';
        enaold<='0';
        pgpEnaOld<='0';
        clockselect<="00";
        trgcount<=x"0000";
        calibmode<="00";
        resetdelay<='0';
        incrementdelay<="00000";
        conftrg<='0';
        trgdelay<=x"02";
        triggermask<=x"0000";
        period<=x"00000000";
        reg<=x"00000000";
        setdeadtime<=x"0007";
        channelmask<=(others => '0');
        channeloutmask<=(ackchannel => '1', others => '0');
        phases <= (others =>'0');
        encoding<=encodingDefault;
        hitbusop<=x"0000";
        discop<=x"0000";
        telescopeop<="000";
        setfifothresh<=x"efff";
        ofprotection<='0';
        writemem<='0';
        maxmem<="0000000000";
        l1route<='0';
        hitbusdepth<="00000";
        tdcreadoutdelay<="00000";
        multiplicity<=x"0000000000000000";
        maxlength<=100;
        phasebusyEn<='0';
        phasebusySel<="00";
      elsif(rising_edge(sysClk125))then
        if(req='1' and pgpEnaOld='0')then
          ena<='1';
          case regOp is
            when '1' => -- Write
              case regAddr(7 downto 0) is
                when x"01" => -- Start TDC measurement when in calib mode
                  startmeas<='1';
                when x"02" => -- Clock select for calib
                  clockselect<=regDataOut(1 downto 0);
                when x"03" => -- Select calib mode
                  calibmode<= regDataOut(1 downto 0);
                  -- 0 is normal
                  -- 1 is tdc calib
                  -- 2 is eudaq
                when x"00" => -- Set channelmask
                 channelmask<=regDataOut;
                when x"04" => -- Switch TDC/Trigger data on and off
                 channeloutmask(tdcchannel)<=regDataOut(0);
                when x"05" => -- Increment disc delays
                  incrementdelay<=regDataOut(4 downto 0);
                when x"06" =>
                  discop<=regDataOut(15 downto 0);
                when x"07" => -- reset input delays
                  resetdelay<='1';
                when x"08" => -- Set trigger delay
                  conftrg<='1';
                  if(unsigned(regDataOut(7 downto 0))<2)then
                    trgdelay<=x"02";
                  else
                    trgdelay<=regDataOut(7 downto 0);
                  end if;
                when x"09" => -- Set cyclic trigger period
                  period<=regDataOut;
                when x"0a" => -- Clock select for receive clock
                  selfei4clk<=regDataOut(1 downto 0);
                when x"0d" => -- enable data output
                  channeloutmask(28 downto 0)<=regDataOut(28 downto 0);
                  channeloutmask(ackchannel)<='1';
                when x"0b" => -- Set trigger mask -1 is scintillator
                               -- 2 is cyclic
                               -- 4 is external
                               -- 8 is external (HSIO)
                               -- 16 is hitbus
                  triggermask<=regDataOut(15 downto 0);
                when x"0e" => 
                  trgcount<=regDataOut(15 downto 0);
                when x"0f" =>
                  setdeadtime<=regDataOut(15 downto 0);
                when x"11" =>
                  phases<=regDataOut;
                when x"12" =>  -- write a word into the command stream buffer
                  writemem<='1';
                when x"13" => -- clear the command stream buffer
                  maxmem<="0000000000";
                when x"14" =>
                  encoding<=regDataOut(1 downto 0);
                when x"15" =>
                  hitbusop<=regDataOut(15 downto 0);
                when x"16" =>
                  multiplicity(31 downto 0)<=regDataOut;
                when x"17" =>
                  multiplicity(63 downto 32)<=regDataOut;
                when x"19" =>
                  setfifothresh<=regDataOut(15 downto 0);
                when x"1a" =>
                  ofprotection<=regDataOut(0);
                when x"1b" =>
                  l1route<=regDataOut(0);
                when x"1c" =>
                  hitbusdepth <= regDataOut(4 downto 0);
                when x"1d" =>
                  tdcreadoutdelay <= regDataOut(4 downto 0);
                when x"1e" =>
                  telescopeop<=regDataOut(2 downto 0);
                when x"1f" =>
                  adcperiod<=regDataOut;
                when x"20" =>
                  channeloutmask(adcchannel)<=regDataOut(0);
                when x"21" =>
                  maxlength<=conv_integer(unsigned(regDataOut(13 downto 0)));
                when x"22" =>
                  phasebusyEn<=regDataOut(0);
                  phasebusySel<=regDataOut(2 downto 1);
                when others =>
                  
              end case;
            when '0' => -- Read
              startmeas<='0';
              case regAddr(3 downto 0) is
                when "0000" =>
                  regDataIn<=tcounter1;
                when "0001" =>
                  regDataIn<=tcounter2;
                when "0011" =>
                  regDataIn<=status;
                when "0100" =>
                  regDataIn<=statusd;
                when "0101" =>
                  regDataIn<=trgtimel(63 downto 32);
                when "0110" =>
                  regDataIn<=trgtimel(31 downto 0);
                when "0111" =>
                  regDataIn<=deadtimel(63 downto 32);
                when "1000" =>
                  regDataIn<=deadtimel(31 downto 0);
                when "1001" =>
                  regDataIn<=counter4;
                when "1010" =>
                  regDataIn<=counter10;
                when "1011" =>
                  regDataIn<=l1countlong;
                when "1100" =>
                  regDataIn<=counter4m;
                when "1101" =>
                  regDataIn<=FpgaVersion;
                when "1110" =>
                  regDataIn<=reg;
                when "1111" =>
                  regDataIn<=channeloutmask;
                when others =>
              end case;
            when others =>
          end case;
        else
          if(writemem='1')then
            maxmem<=unsigned(maxmem)+1;
            writemem<='0';
          end if;
          startmeas<='0';
          resettdc<='0';
          ena<='0';
          incrementdelay<="00000";
          resetdelay<='0';
          conftrg<='0';
          extrsto<='0';
        end if;
        pgpEnaOld<=req;
        enaold<=ena;
        ack<=enaold;
        err<='0';
      end if;
   end process;
  process (sysRst125, sysClk125) begin
   if(sysRst125='1')then
     startrun<='0';
     softrst<='0';
     present<='0';
     trigenabled<='0';
     rstl1count<='0';
     marker<='0';
     reload <='1';
     markercounter<="0000000000";
     phaseConfig <='0';
   elsif rising_edge(sysClk125) then
     if(cmdEn='1')then
      if(cmdOpCode=x"03")then -- start run
        softrst<='1';
        startrun<='1';
        trgcountdown<=trgcount; -- for runs with a finite number of events.
      elsif(cmdOpCode=x"04")then -- pause run
        trigenabled<='0';
        paused<='1';
      elsif(cmdOpCode=x"05")then -- stop run
        trigenabled<='0';
        paused<='0';
        present<='0';
      elsif(cmdOpCode=x"06")then -- resume run
        trigenabled<='1';
        paused<='0';
      elsif(cmdOpCode=x"07")then -- resume run and set marker after delay
        markercounter<="1111111111";
      elsif(cmdOpCode=x"08")then -- Reboot
        reload<='0';          
      elsif(cmdOpCode=x"09")then -- soft reset
        softrst<='1';
      elsif(cmdOpCode=x"10")then -- FS: Write new phase configuration from phase recognition
        phaseConfig <= '1';
      elsif(cmdOpCode=x"11")then -- Tell HSIO that this core is active.
        present <= '1';
      elsif(cmdOpCode=x"12")then -- Tell HSIO that this core is not active.
        present <= '0';
      end if;
     elsif(startrun='1')then
        softrst<='0';
        startrun<='0';
        trigenabled<='1';
        paused<='0';
     elsif(softrst='1')then
        softrst<='0';
     elsif(trgcountdown=x"0001")then -- stop run
       trigenabled<='0';
       paused<='0';
     elsif(markercounter="0000000011")then
       softrst<='1';
     elsif(markercounter="0000000001")then
       marker<='1';
       trigenabled<='1';
       paused<='0';
       rstl1count<='1';
     else
       marker<='0';
       rstl1count<='0';
       phaseConfig<='0';
     end if;
     if(markercounter/="0000000000")then
       markercounter<=unsigned(markercounter)-1;
     end if;
     if(l1a='1' and trgcountdown/=x"0000")then
       trgcountdown<=unsigned(trgcountdown)-1;
     end if;
   end if;
  end process;
  rst<= sysRst125 or softrst; 
  rstFromCore<= rst;

  process begin
    wait until rising_edge(sysClk125);
    oldadcperiod<=adcperiod;
    if(adccounter=adcperiod)then
      adccounter<=(others => '0');
      trigAdc <= '1';
    elsif (adcperiod/=oldadcperiod)then
      adccounter<=(others => '0');
      trigAdc <= '0';
    else
      adccounter<=unsigned(adccounter)+1;
      trigAdc <='0';
    end if;
  end process;

  doricreset<=doricresetb;
  process(sysClk125, sysRst125) -- reset logic for DORIC
  begin
     if(sysRst125='1') then
       doricresetb<='1';
       doriccounter<=x"0000";
       oldbpm<='0';
     elsif(sysClk125'event and sysClk125='1') then
       oldbpm<=encoding(0);
       if(encoding(0)='1' and oldbpm='0')then
         doricresetb<='0';
         doriccounter<=x"ffff";
       elsif(doriccounter/=x"0000")then
         doriccounter<=unsigned(doriccounter)-1;
       end if;
       if(doriccounter=x"0001")then
         doricresetb<='1';
       end if;
     end if;
  end process;

  process(sysClk125,rst) begin
   if(rst='1')then
     counter4<=(others=>'0');
     counter10<=(others=>'0');
     counter4m<=(others=>'0');
     counter10m<=(others=>'0');
   elsif(rising_edge(sysClk125))then
     if(mxsof='1' and mxvalid='1')then
       counter4<=unsigned(counter4)+1;
     end if;
     if(mxeof='1' and mxvalid='1')then
       counter10<=unsigned(counter10)+1;
     end if;
     if(mxsof='1')then
       counter4m<=unsigned(counter4m)+1;
     end if;
     if(mxvalid='1' and mxeof='1')then
       counter10m<=unsigned(counter10m)+1;
     end if;
   end if;
  end process;
  process (rst, sysClk125) begin
   if(rst='1')then
     status<=x"00000000";
     statusd<=x"00000000";
     l1count<=x"1";
     l1countlong<=x"00000000";
     starttdccount<=x"1";
     l1countl<=x"1";
     trgtime<=x"0000000000000000";
     trgtimel<=x"0000000000000000";
     deadtime<=x"0000000000000000";
     deadtimel<=x"0000000000000000";
   elsif (rising_edge(sysClk125)) then
     oldl1a<=l1a;
     if(trigenabled='1' or paused='1')then
       trgtime<=unsigned(trgtime)+1;
     end if;
     if(busy='1' or paused='1')then
       deadtime<=unsigned(deadtime)+1;
     end if;
     if(rstl1count='1')then
       l1count<=x"1";
     elsif(l1a='1')then
       l1countl<=l1count;
       l1count<=unsigned(l1count)+1;
       if(oldl1a='0')then
         l1countlong<=unsigned(l1countlong)+1;
       end if;
       trgtimel<=trgtime;
       deadtimel<=deadtime;
     end if;
     status(10 downto 0)<= dfifothresh(10 downto 0);
     status(11)<= paused;
     status(12)<= frameTxAFull;
     
     --statusd(10 downto 0)<= statusd(10 downto 0) or underflow;
     statusd(3 downto 0) <= l1count;
     statusd(7 downto 4) <= starttdccount;
     statusd(10 downto 9) <=(others=>'0');
     statusd(21 downto 11)<= statusd(21 downto 11) or overflow(10 downto 0);
     statusd(22)<=statusd(22) or configunderflow;
     statusd(23)<=statusd(23) or configoverflow;
     statusd(24)<=busy;
     --statusd(26)<=extbusy;
     statusd(26)<='0';
   end if;
   end process;

   --B01: BUFGMUX
   --  port map(
   --    O=> o01,
   --    I0 => phaseclkUnbuf,
   --    I1 => phaseclk90Unbuf,
   --    S => clockselect(0)
   --    );
   --B02: BUFGMUX
   --  port map(
   --    O=> o23,
   --    I0 => phaseclk180Unbuf,
   --    I1 => phaseclk270Unbuf,
   --    S => clockselect(0)
   --    );
   --B04: BUFGMUX
   --  port map(
   --    O=> phaseclksel,
   --    I0 => o01,
   --    I1 => o23,
   --    S => clockselect(1)
   --    );
   phaseclksel<='0';

    sbusy<=trgbusy or going32;
    l1modin<=l1a and not l1route;
    l1memin<=l1a and l1route;
    trgpipeline: entity work.triggerpipeline
      port map(
        rst=> sysRst125,
        clk=> sysClk125,
        L1Ain=> l1modin, -- was l1a
        L1Aout=> l1amod,
        configure=> conftrg,
        delay => trgdelay,
        busy => trgbusy,
        deadtime => setdeadtime
        );
   
   with hitbusop(3) select 
   hitbusa<= (hitbusin(0) and hitbusop(0)) or (hitbusin(1) and hitbusop(1)) or (hitbusin(2) and hitbusop(2)) when '0',
             (hitbusin(0) or not hitbusop(0)) and (hitbusin(1) or not hitbusop(1)) and (hitbusin(2) or not hitbusop(2)) 
             and (hitbusop(0) or hitbusop(1) or hitbusop(2)) when '1',
             '0' when others;
   with hitbusop(7) select 
   hitbusb<= (hitbusin(3) and hitbusop(4)) or (hitbusin(4) and hitbusop(5)) or (hitbusin(5) and hitbusop(6)) when '0',
             (hitbusin(3) or not hitbusop(4)) and (hitbusin(4) or not hitbusop(5)) and (hitbusin(5) or not hitbusop(6)) 
             and  (hitbusop(4) or hitbusop(5) or hitbusop(6)) when '1',
             '0' when others;
   with hitbusop(8) select
   hitbus <= hitbusa or hitbusb when '0',
             hitbusa and hitbusb when '1',
             '0' when others;

      hr1: if(hitbusreadout='1') generate
        counterout1<=hitbusword(0);
        counterout2<=hitbusword(1);
      end generate hr1;
      hr2: if(hitbusreadout='0') generate
        counterout1<=tcounter1;
        counterout2<=tcounter2;
      end generate hr2;
   -- Filter out the message header. The only interesting thing is the handshake
   process (rst,sysClk125) begin
    if(rst='1')then
      blockcounter<="000";
      readword<='0';
      handshake<='0';
      swapsof<='0';
      gocounter<=x"0";
      oldswappedeof<='0';
    elsif rising_edge(sysClk125) then
      if(frameRxValid='1' and frameRxSOF='1')then
        blockcounter<="111";
      elsif(blockcounter /= "000")then
        blockcounter<=unsigned(blockcounter)-1;
        if(blockcounter="110")then
          handshake<=frameRxData(0);
        elsif(blockcounter="001")then
          readword<='1';
          swapsof<='1';
        end if;
      elsif(frameRxEOF='1')then
        swapsof<='0';
        readword<='0';
      else
        swapsof<='0';
      end if;
      oldswappedeof<=swappedeof;
      if(swappedeof='1' and oldswappedeof='0') then
        if(stop='0')then
          go<='1';
        else
          gocounter<=x"4";
        end if;
      elsif(gocounter/=x"0")then
        gocounter<=unsigned(gocounter)-1;
        if(gocounter=x"1")then
          go<='1';
        end if;
      else
        go<='0';
      end if;
    end if;
   end process;
   readvalid<=readword and frameRxValid;
   process (sysClk125,sysRst125) begin
    if(sysRst125='1')then
      oldstop<='0';
    elsif rising_edge(sysClk125) then
     oldstop<=stop;
     if (stop='1' and oldstop='0')then
       replynow<=handshake;
     else
       replynow<='0';
     end if;
    end if;
   end process;
   swapconfig: wordswapper 
   port map(
     rst=>rst,
     clk=>sysClk125,
     wordin=>frameRxData,
     eofin=>frameRxEOF,
     eeofin=>'0',
     sofin=>swapsof,
     validin=>readvalid,
     wordout=>blockdata(15 downto 0),
     eofout=>swappedeof,
     eeofout=>open,
     sofout=>open,
     validout=>swapvalid
     );
    theconfigfifo: datafifo8192
	port map (
		din => blockdata,
                rd_clk => sysClk125,
                rd_en => ldser,
                rst => sysRst125,
                wr_clk => sysClk125,
                wr_en => swapvalid,
                dout => configdataout,
                empty => stop,
                full => configfull,
                overflow => configoverflow,
                prog_full => open,
                underflow => open);
   theser: entity work.ser
     port map(
       clk=>sysClk125,
       ld=>ldser,
--       l1a=>l1a,
       l1a=>l1amod,
       go=>go,
       busy=>serbusy,
       stop=>stop,
       rst=>rst,
       d_in=>configdataout(15 downto 0),
       d_out=>d_out
       );

   process (sysClk125, sysRst125) begin
     if(sysRst125='1')then
       oldld32<='0';
       stop32<='1';
       readpointer<="0000000000";
     elsif rising_edge(sysClk125)then
       oldld32<=ld32 or l1memin;
       if(l1memin='1' and going32 ='0' and maxmem/="0000000000")then -- serialize trigger sequence
         stop32<='0';
       end if;
       if(going32='1')then
         if(oldld32='1' and ld32='0' )then
           if(readpointer=unsigned(maxmem)-1)then
             stop32<='1';
             readpointer<="0000000000";      
           else
             readpointer<=unsigned(readpointer)+1;
           end if;
         end if;
       end if;
     end if;
   end process;

   readmem<=ld32 or l1memin;
   configmem : pattern_blk_mem
     port map (
       clka => sysClk125,
       dina => regDataOut,
       addra => maxmem,
       ena => writemem,
       wea => vectorize('1'),
       douta => open,
       clkb => sysClk125,
       dinb => (others=>'0'),
       addrb => readpointer,
       enb => readmem,
       web => vectorize('0'),
       doutb => serdata32);
  
   theser32: entity work.ser32
     port map(
       clk => sysClk125,
       ld => ld32,
       go => l1memin,
       busy => going32,
       stop => stop32,
       rst => sysRst125,
       d_in => serdata32,
       d_out => memout);
   
   
   fanout: for I in 0 to maxchannel generate
     bpmenc: entity work.outputencoder
       port map(
         clock=>sysClk125,
         rst=>sysRst125,
         clockx2=>sysClk250,
         datain=>serialoutb(I),
         encode=>encoding,
         dataout=>serialout(I));
     serialoutb(I)<= (d_out or memout) when channelmask(I)='1' and doricresetb='1' else '0';   
   end generate fanout;

  pgpack: entity work.deser
    generic map( CHANNEL=>toSlv(ackchannel, 8) )
    port map (
      clk       => sysClk125,
      rst       => sysRst125,
      d_in      => '0',
      enabled   => channeloutmask(ackchannel),
      replynow  => replynow,
      marker =>'0',
      d_out     => channeldata(ackchannel)(15 downto 0),
      ld        => chanld(ackchannel),
      sof       => channeldata(ackchannel)(16),
      eof       => channeldata(ackchannel)(17)
      );
  pgpackfifo : datafifo1024
    port map (
      din => channeldata(ackchannel),
      rd_clk => sysClk125,
      rd_en => reqdata(ackchannel),
      rst => rst,
      wr_clk => sysClk125,
      wr_en => chanld(ackchannel),
      dout => datain(ackchannel),
      empty => open,
      full => open,
      overflow => overflow(ackchannel),
      prog_full => dfifothresh(ackchannel),
      valid => indatavalid(ackchannel),
      underflow => underflow(ackchannel));
  pgpackdataflag: entity work.dataflag
    port map(
      eofin=>channeldata(ackchannel)(17),
      eofout=>datain(ackchannel)(17),
      datawaiting=> datawaiting(ackchannel),
      clk=>sysClk125,
      rst=>rst,
      counter=>open
      );
      moredatawaiting(ackchannel)<='0';
   
  enableAdcReadout<=not dfifothresh(adcchannel) and channeloutmask(adcchannel) and trigenabled;
  adcreadout_inst: entity work.adcreadout
    generic map( CHANNEL=>toSlv(adcchannel, 8) )
    port map (
      clk       => sysClk125,
      rst       => rst,
      d_in      => AdcData,
      enabled   => enableAdcReadout,
      go        => sendAdcData,
      d_out     => channeldata(adcchannel)(15 downto 0),
      ld        => chanld(adcchannel),
      sof       => channeldata(adcchannel)(16),
      eof       => channeldata(adcchannel)(17)
      );
  adcfifo: datafifo1024
    port map (
      rd_clk => sysClk125,
      wr_clk => sysClk125,
      rst => rst,
      din => channeldata(adcchannel),
      rd_en => reqdata(adcchannel),
      wr_en => chanld(adcchannel),
      dout => datain(adcchannel),
      empty => open,
      full => open,
      overflow => overflow(adcchannel),
      prog_full => dfifothresh(adcchannel),
      valid => indatavalid(adcchannel),
      underflow => underflow(adcchannel));
  adcdataflag: entity work.dataflag
    port map(
      eofin=>channeldata(adcchannel)(17),
      eofout=>datain(adcchannel)(17),
      datawaiting=> datawaiting(adcchannel),
      clk=>sysClk125,
      rst=>rst,
      counter => open
      );
   
   CHANNELREADOUT:
   for I in rawFirstChannel to rawLastChannel generate
     enablereadout(I)<=channeloutmask(I) and ((trigenabled and not ofprotection) or not dfifothresh(I)); 
     channelreadout: entity work.deser
       generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,4)))
       port map (
        clk       => sysClk125,
	rst       => sysRst125,
	d_in      => serialin(I),
        enabled   => enablereadout(I),
        replynow => '0',
        marker    => marker,
	d_out     => channeldata(I)(15 downto 0),
        ld        => chanld(I),
        sof       => channeldata(I)(16),
        eof       => channeldata(I)(17)
        );
      channelfifo : datafifo8192
         port map (
           din => channeldata(I),
           rd_clk => sysClk125,
           rd_en => reqdata(I),
           rst => rst,
           wr_clk => sysClk125,
           wr_en => chanld(I),
           dout => datain(I),
           empty => open,
           full => open,
           overflow => overflow(I),
           prog_full => dfifothresh(I),
           valid => indatavalid(I),
           underflow => underflow(I));
     channeldataflag: entity work.dataflagnew
       port map(
         eofin=>channeldata(I)(17),
         eofout=>datain(I)(17),
         datawaiting=> datawaiting(I),
         moredatawaiting=> moredatawaiting(I),
         clkin=>sysClk125,
         clkout=>sysClk125,
         rst=>rst
       );
   end generate CHANNELREADOUT;

   starttdcreadout<=l1a and channeloutmask(tdcchannel);

   process(rst, pgpClk) begin
      if(rst='1')then
        oldtdcbusy<='0';
      elsif(rising_edge(pgpClk))then
        if(tdcbusys='0' and oldtdcbusy='1') then --tdc readout is done
          tdcfifothresh<=(others => '0');
        elsif(andr(10) ='1')then
          tdcfifothresh(7)<=andr(10);
        elsif(andr(6) ='1')then
          tdcfifothresh(6)<=andr(6);
        elsif(andr(5) ='1')then
          tdcfifothresh(5)<=andr(5);
        elsif(andr(4) ='1')then
          tdcfifothresh(4)<=andr(4);
        elsif(andr(3) ='1')then
          tdcfifothresh(3)<=andr(3);
        elsif(andr(2) ='1')then
          tdcfifothresh(2)<=andr(2);
        elsif(andr(1) ='1')then
          tdcfifothresh(1)<=andr(1);
        elsif(andr(0) ='1')then
          tdcfifothresh(0)<=andr(0);
        end if;
        oldtdcbusy<=tdcbusys;
      end if;
   end process;
   tdcbusy<=tdcbusys;
   thereeadout: entity work.tdcreadout
     generic map(CHANNEL=>toSlv(tdcchannel, 8))
     port map(
       clk=>pgpClk,
       slowclock=>sysClk125,
       rst=>rst,
       go=>starttdcreadout,
       delay=>tdcreadoutdelay,
       counter1=>counterout1,
       counter2=>counterout2,
       trgtime=>trgtimel,
       deadtime=>deadtimel,
       status=>status(14 downto 0),
       marker=>marker,
       l1count=>l1countl,
       bxid=>trgtimel(7 downto 0),
       d_out=>tdcdata(15 downto 0),
       ld=>tdcld,
       busy=>tdcbusys,
       sof=>tdcdata(16),
       eof=>tdcdata(17),
       runmode => calibmode,
       eudaqdone => eudaqdone,
       eudaqtrgword => eudaqtrgword,
       fifothresh => tdcfifothresh,
       triggerword => latchtriggerword
       );
   tdcfifo : datafifo
         generic map( buffersize => buffersizetdc)
         port map (
           din => tdcdata,
           rd_clk => sysClk125,
           rd_en => reqdata(tdcchannel),
           rst => rst,
           wr_clk => pgpClk,
           wr_en => tdcld,
           dout => datain(tdcchannel),
           empty => empty,
           full => full,
           overflow => overflow(tdcchannel),
           prog_full => dfifothresh(tdcchannel),
           valid => indatavalid(tdcchannel),
           underflow => underflow(tdcchannel));
   tdcdataflag: entity work.dataflagnew
     port map(
       eofin=>tdcdata(17),
       eofout=>datain(tdcchannel)(17),
       datawaiting=>datawaiting(tdcchannel),
       moredatawaiting=>moredatawaiting(tdcchannel),
       clkin=>pgpClk,
       clkout=>sysClk125,
       rst=>rst);
              
   multiplexer: entity work.multiplexdata
     generic map (
         maxchannel => maxchannel
         )
     port map(
       clk=>sysClk125,
       rst=>rst,
       enabled=>nobackpressure,
       channelmask=>channeloutmask,
       datawaiting=>datawaiting,
       moredatawaiting=>moredatawaiting,
       indatavalid => indatavalid,
       datain=>datain,
       dataout=>mxdata,
       eofout=>mxeof,
       sofout=>mxsof,
       datavalid=>mxvalid,
       reqdata=>reqdata,
       multiplicity => multiplicity,
       counter4=>open,
       counter10b=>counter10e,
       counter10=>open
       );
   swapdata: wordswapper 
   port map(
     rst=>rst,
     clk=>sysClk125,
     wordin=>mxdata,
     eofin=>mxeof,
     eeofin=>'0',
     sofin=>mxsof,
     validin=>mxvalid,
     wordout=>readData,
     eofout=>readDataEOF,
     eeofout=>open,
     sofout=>readDataSOF,
     validout=>readDataValid
     );

   FULLDISPLAY:
   for I in 0 to 7 generate
    process (rst, dfifothresh(I)) begin
      if(rst='1')then
       flatch(I)<='0';
      elsif(rising_edge(dfifothresh(I)))then
        flatch(I)<='1';
      end if;
    end process;  
   end generate FULLDISPLAY; 
   process (rst, frameTxAFull) begin
     if(rst='1')then
       bpl<='0';
     elsif(rising_edge(frameTxAFull))then
       bpl<='1';
     end if;
   end process;

   bgmf: if(fixed160='1') generate
    receiveclock<=pgpClk;
    receiveclock90<=pgpClk90;
   end generate bgmf;
   bgmv: if(fixed160='0') generate
    CLKFEI4: BUFGMUX
      port map(
        O=> receiveclock,
        I0 => phaseclkUnbuf,
        I1 => pgpClkUnbuf,
        S => selfei4clk(1)
        );
    CLKFEI490: BUFGMUX
      port map(
        O=> receiveclock90,
        I0 => phaseclk90Unbuf,
        I1 => pgpClk90Unbuf,
        S => selfei4clk(1)
        );
    end generate bgmv;
    FRAMEDCHANNELREADOUT:
    for I in framedFirstChannel to framedLastChannel generate
    receivedata: entity work.syncdatac
      port map(
        phaseConfig => phaseConfig,
        clk => receiveclock,
        clk90 => receiveclock90,
        rdatain => serialin(I),
        rst => sysRst125,
        useaout => open,
        usebout => open,
        usecout => open,
        usedout => open,
        sdataout => recdata(I));
    FE_CHANNEL: if ((I/=3 and I/=7) or hitbusreadout='0') generate
    alignframe: entity work.framealign
      port map(
        clk => receiveclock,
        rst => rst,
        d_in => recdata(I),
        d_out => alignout(I),
        aligned => aligned(I)
        );
    deser8b10b: entity work.deser10b
      port map(
        clk => receiveclock,
        rst => rst,
        d_in => alignout(I),
        align => aligned(I),
        d_out => data8b10b(I),
        ld => ld8b10b(I)
        );
   decoder8b10b: decode_8b10b_wrapper
     port map(
       CLK => receiveclock,
       DIN => data8b10b(I),
       DOUT => dout8b10b(I)(7 downto 0),
       KOUT => dout8b10b(I)(8),
       CE => ld8b10b(I),
       SINIT => rst,
       CODE_ERR =>dout8b10b(I)(9),
       ND =>ldout8b10b(I)
       );
   enablereadout(I)<=channeloutmask(I) and ((trigenabled and not ofprotection) or not dfifothresh(I));
   decode: entity work.decodefei4record
    port map(clk => receiveclock,
             rst => rst,
             enabled => enablereadout(I),
             isrunning => isrunning(I),
             d_in => dout8b10b(I)(7 downto 0),
             k_in => dout8b10b(I)(8),
             err_in => dout8b10b(I)(9),
             d_out => fei4data(I),
             ldin => ldout8b10b(I),
             ldout => ldfei4(I),
             overflow => bufoverflow(I)
             );
   fei4fifo : entity work.lookaheadfifo
     generic map( buffersize => buffersizefe)
     port map (
       din => fei4data(I),
       rd_clk => sysClk125,
       rd_en => ldfifo(I), 
       rst => rst,
       wr_clk => receiveclock,
       wr_en => ldfei4(I),
       dout => fifodata(I),
       dnext => dnext(I),
       dnextvalid => dnextvalid(I),
       empty => emptyv(I),
       full => fullv(I),
       overflow => overflowv(I),
       prog_full => dfifothresh(I),
       valid => dvalid(I),
       underflow => underflowv(I));
    fei4dataflag: entity work.dataflagff
      port map(
        eofin=>fei4data(I)(24),
        ldin => ldfei4(I),
        eofout=>dnext(I)(24),
        ldout => ldfifo(I),
        datawaiting=>datawaiting(I),
        moredatawaiting=>moredatawaiting(I),
        clkin=>receiveclock,
        clkinrate=>selfei4clk(1),
        clkout=>sysClk125,
        rst=>rst);
   encode: entity work.encodepgp24bit
    generic map( CHANNEL=> std_logic_vector(conv_unsigned(I,4)))
    port map(clk => sysClk125,
         rst => rst,
         enabled =>channeloutmask(I),
         maxlength => maxlength,
         isrunning => open,
         d_in => fifodata(I),
         d_next => dnext(I),
         marker => marker,
         d_out => datain(I),
         ldin => reqdata(I), 
         ldout => ldfifo(I),
         dnextvalid => dnextvalid(I),
         dvalid => dvalid(I),
         datawaiting=> datawaiting(I),
         moredatawaiting=> moredatawaiting(I),
         overflow => frameoverflow(I),
         valid => indatavalid(I)
);
    end generate FE_CHANNEL;
    HITBUS_CHANNEL: if (hitbusreadout='1' and (I=3 or I=7)) generate
    dfifothresh(I)<='0';
    alignframe: entity work.framealignhitbus
      port map(
        clk => receiveclock,
        rst => rst,
        d_in => recdata(I),
        d_out => alignout(I),
        aligned => aligned(I)
        );
    deserhitbus: entity work.deser4b
      port map(
        clk => receiveclock,
        rst => rst,
        d_in => alignout(I),
        align => aligned(I),
        d_out => data8b10b(I)(3 downto 0),
        ld => ld8b10b(I)
        );
    hitbusin(I/4*3)<=data8b10b(I)(2);
    hitbusin(I/4*3+1)<=data8b10b(I)(1);
    hitbusin(I/4*3+2)<=data8b10b(I)(0);
    thehitbuspipeline: entity work.hitbuspipeline
     port map(
     rst  => rst,
     clk => receiveclock,
     ld => ld8b10b(I),
     depth => hitbusdepth,
     wordin => data8b10b(I)(2 downto 0),
     wordout => hitbusword(I/4)
     );
    end generate HITBUS_CHANNEL;
   end generate FRAMEDCHANNELREADOUT;
   hbin: if framedfirstchannel>7 or framedlastchannel<7 or hitbusreadout='0' generate
      hitbusin(3)<='0';  
      hitbusin(4)<='0';  
      hitbusin(5)<='0';  
   end generate hbin;
   hbin2: if framedfirstchannel>3 or framedlastchannel<3 or hitbusreadout='0' generate
      hitbusin(0)<='0';  
      hitbusin(1)<='0';  
      hitbusin(2)<='0';  
   end generate hbin2;
   dfifothresh(6 downto framedlastchannel+1)<=(others => '0');
   debug(0)<=recdata(7);
   debug(1)<=alignout(7);
   debug(2)<=aligned(7);
   debug(3)<=data8b10b(7)(3);
   debug(4)<=ld8b10b(7);
   debug(5)<=hitbusin(3);
   debug(6)<=hitbusin(5);
   --debug(7)<=hitbus;

       
 --process (rst, sysClk125) begin
 --  if(rst='1')then
 --    ctrig(0)<='0';
 --    oldindatav<='0';
 --    oldindata<=x"000000";
 --  elsif rising_edge(sysClk125) then
 --    oldindatav<=ldfifo(1);
 --    oldindata<=fifodata(1)(23 downto 0);
 --    if(ldfifo(1)='1' and fifodata(1)(23 downto 0)=x"02211f" and 
 --       oldindatav='1' and oldindata=x"02211f")then
 --      ctrig(0)<='1';
 --    else
 --      ctrig(0)<='0';
 --    end if;
 --  end if;
 --end process;
-- cdata(9 downto 0)<=dout8b10b(0);
   
ctrig(0)<=dnextvalid(1);
cdata(23 downto 0)<=dnext(1)(23 downto 0);
cdata(24)<=fei4data(1)(24);
cdata(25)<=dnext(1)(24);
cdata(26)<=ldfei4(1);
cdata(30)<=dnextvalid(1);
cdata(28)<=datawaiting(1);
cdata(29)<=moredatawaiting(1);
cdata(27)<=ldfifo(1);
cdata(31)<=indatavalid(1);
--cdata(8)<=alignout(2);
--cdata(9)<=aligned(2);
--cdata(11 downto 10)<=dout8b10b(2)(9 downto 8);
   
--cdata(12)<=alignout(3);
--cdata(13)<=aligned(3);
--cdata(15 downto 14)<=dout8b10b(3)(9 downto 8);
   
--cdata(16)<=alignout(0);
--cdata(17)<=aligned(0);
--cdata(18)<=datawaiting(0);
--cdata(28 downto 19)<=dout8b10b(0);
   
--cdata(29)<=alignout(5);
--cdata(30)<=aligned(5);
--cdata(31)<=dout8b10b(5)(9);
   
   
--chipscope : ila
  --port map (
    --CONTROL => ccontrol,
    --CLK => sysClk125,
    --DATA => cdata,
    --TRIG0 => ctrig);
--chipscopeicon : icon
  --port map (
    --CONTROL0 => ccontrol);       
            --
end HsioPixelCore;

