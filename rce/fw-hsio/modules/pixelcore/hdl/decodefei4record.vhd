--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;

--------------------------------------------------------------

entity decodefei4record is
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        enabled:    in std_logic;
        isrunning:    out std_logic;
	d_in:	    in std_logic_vector(7 downto 0);
	k_in:	    in std_logic;
        err_in:     in std_logic;
	d_out:	    out std_logic_vector(24 downto 0);
        ldin:       in std_logic;
        ldout:      out std_logic;
        overflow:   out std_logic
--        acounter:    out std_logic_vector(15 downto 0)
);
end decodefei4record;

--------------------------------------------------------------

architecture DECODEFEI4RECORD of decodefei4record is

signal running: std_logic;
signal parity: natural range 0 to 2:=0;
signal wordcount: std_logic_vector(11 downto 0);
signal dint: std_logic_vector(23 downto 0):=(others => '0');
signal nowrite: std_logic := '0';

begin
  isrunning<=running;
    process(rst, clk)
    begin
        if(rst='1') then
          d_out<=(others => '0');
          running<='0';
          parity<=0;
          ldout<='0';
          overflow<='0';
          wordcount<=(others =>'0');
          nowrite<='0';
        elsif (clk'event and clk='1') then
          if(ldin='1')then 
            d_out(23 downto 0)<=dint;
            if(running='0')then
              if(enabled='1' and d_in=x"fc" and k_in='1')then  -- SOF
                d_out(24)<='0';
                running<='1';
                parity<=2;
                ldout<='0';
                wordcount<=(others =>'0');
                nowrite<='1';
              end if;
            else -- running
              if(k_in='1' or err_in='1' or wordcount=x"800")then --EOF or idle or error
                running<='0';
                if(wordcount=x"800")then
                  overflow<='1';
                end if;
                ldout<='1';
                d_out(24)<='1';
              else-- data 
                dint(parity*8+7 downto parity*8)<=d_in;
                if(parity=0)then 
                  parity<=2;
                  wordcount<=unsigned(wordcount)+1;
                elsif(parity=2)then
                  if(nowrite='0')then
                    ldout<='1';
                  end if;
                  nowrite<='0';
                  parity<=1;
                else
                  parity<=0;
                end if;
              end if;
            end if;      
          else
            ldout<='0';
          end if;
 	end if;
    
    end process;		

end DECODEFEI4RECORD;

--------------------------------------------------------------
