ifneq ($(findstring linux,$(tgt_os)),)
libnames := gpib

libsrcs_gpib := GpibController.cc 

libincs_gpib := $(boost_include_path)

tgtnames := powercycleLV LVmonitor

tgtsrcs_powercycleLV := powercycleLV.cc
tgtincs_powercycleLV := rcecalib/gpib 
                    #root/include

tgtlibs_powercycleLV := rcedcs/gpib
#=====================================================================
GUIHEADERSDEP = LVmonitor.hh LVchan.hh
tgtsrcs_LVmonitor := LVmonitor.cc LVchan.cc LVmonitor_rootDict.cc

tgtincs_LVmonitor := rcedcs \
                     rcedcs/gpib \
                     root/include

tgtlibs_LVmonitor := rcedcs/gpib

tgtslib_LVmonitor := $(tdaq_cmdline_lib)

guiheaders_LVmonitor = LVmonitor.hh LVchan.hh

export  PATH:=$(RELEASE_DIR)/build/root/bin:${PATH}
#export  LD_LIBRARY_PATH:=$(RELEASE_DIR)/build/root/lib
LXFLAGS +=-L$(RELEASE_DIR)/build/root/lib -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lGui -pthread -lm -ldl -rdynamic 
endif
