#ifndef CONFIGGUI_HH
#define CONFIGGUI_HH

#include <TGLabel.h>

class GpibController;

class LVchan: public TGVerticalFrame{
public:
  LVchan(const char* name, unsigned chan, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options);
  virtual ~LVchan();
  void update(GpibController* controller);
private:
  void updateText(TGLabel* label, const char* newtext);
  TGLabel *m_voltage, *m_current;
  unsigned m_channel;
  ClassDef(LVchan,0);
};

#endif
