#include "GpibController.hh"
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

void readoutPS(GpibController &controller, int channel, float& voltage, float&current);

int main(int argc, char* argv[]){
  if(argc!=3){
    printf("Usage: powercycleLV hostname stationId\n");
    exit(0);
  }
  const char* hostname=argv[1];
  int id=atoi(argv[2]);
  GpibController controller(hostname,id);
  controller.connect();
  float voltage, current;
  readoutPS(controller, 1, voltage, current);
  printf("Before:\n");
  printf("-------\n");
  printf("Channel 1: %.02f V  %.03f A\n",voltage, current);
  readoutPS(controller, 2, voltage, current);
  printf("Channel 2: %.02f V  %.03f A\n",voltage, current);
  // turn off power supply

  controller.write("OUTPUT OFF");
  readoutPS(controller, 1, voltage, current);
  printf("Off:\n");
  printf("-------\n");
  printf("Channel 1: %.02f V  %.03f A\n",voltage, current);
  readoutPS(controller, 2, voltage, current);
  printf("Channel 2: %.02f V  %.03f A\n",voltage, current);
  sleep(1);
  controller.write("OUTPUT ON");
  readoutPS(controller, 1, voltage, current);
  printf("After:\n");
  printf("-------\n");
  printf("Channel 1: %.02f V  %.03f A\n",voltage, current);
  readoutPS(controller, 2, voltage, current);
  printf("Channel 2: %.02f V  %.03f A\n",voltage, current);
  controller.disconnect();


}

void readoutPS(GpibController &controller, int channel, float& voltage, float&current){
  assert(channel==1 || channel==2);
  const char *v1,*c1;
  char cmd[128];
  sprintf(cmd,"INST:SELECT OUTPUT%d", channel);
  controller.write(cmd);
  v1=controller.read("MEASURE:VOLTAGE?");
  voltage=atof(v1);
  c1=controller.read("MEASURE:CURRENT?");
  current=atof(c1);
}  
