#include "rcedcs/gpib/LVmonitor.hh"
#include "rcedcs/gpib/LVchan.hh"
#include "rcedcs/gpib/GpibController.hh"
#include "rcedcs/gpib/psimage.hh"
#include "TApplication.h"
#include "TGIcon.h"
#include <cmdl/cmdargs.h>
#include <iostream>
#include <time.h>
#include <stdlib.h>


LVmonitor::~LVmonitor(){
  Cleanup();
}

LVmonitor::LVmonitor(GpibController& controller, const TGWindow *p,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h), m_controller(controller), m_noconn(0){
  
  // connect x icon on window manager
  Connect("CloseWindow()","LVmonitor",this,"quit()");
  ChangeBackground(GetWhitePixel());

  TGVerticalFrame* datapanel=new TGVerticalFrame(this,1,1, kSunkenFrame);
  datapanel->ChangeBackground(GetWhitePixel());
  // scan panel
  TGHorizontalFrame *datapanel1 = new TGHorizontalFrame(datapanel, 2,2, kSunkenFrame);
  datapanel1->ChangeBackground(GetWhitePixel());
  datapanel->AddFrame(datapanel1,new TGLayoutHints(kLHintsExpandX ));
  TGHorizontalFrame *datapanel2 = new TGHorizontalFrame(datapanel, 2, 2, kSunkenFrame);
  datapanel2->ChangeBackground(GetWhitePixel());
  datapanel->AddFrame(datapanel2,new TGLayoutHints(kLHintsExpandX ));
  TGVerticalFrame *datapanel3 = new TGVerticalFrame(datapanel2, 2, 2, 0);
  datapanel3->ChangeBackground(GetWhitePixel());
  datapanel2->AddFrame(datapanel3,new TGLayoutHints(kLHintsExpandX|kLHintsCenterX|kLHintsCenterY));
  AddFrame(datapanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  const char* channelname[]={"Channel 1", "Channel 2"};
  for (int i=0;i<2;i++){
    m_lvchan[i]=new LVchan(channelname[i], i+1,datapanel1,1,1, kSunkenFrame);
    datapanel1->AddFrame(m_lvchan[i],new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,0,0));
  } 
  m_timers=new TTimer;
  m_timers->Connect("Timeout()","LVmonitor",this,"timeouts()");
  m_timers->Start(1000,kFALSE);  // update every second

  m_start=new TGTextButton(datapanel3,"ON/OFF");
  m_start->ChangeBackground(GetWhitePixel());
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_start->SetFont(labelfont);
  m_start->SetMargins(10,40,10,10);
  datapanel3->AddFrame(m_start,new TGLayoutHints(kLHintsCenterY | kLHintsCenterX  ,5,5,5,5));
  m_start->Connect("Clicked()", "LVmonitor", this, "toggle()");
  m_ison=atoi(m_controller.read("OUTPUT?"));
  if(m_ison)m_start->SetDown(true);
  else m_start->SetDown(false);

  TGTextButton* m_quit=new TGTextButton(datapanel3,"Quit");
  m_quit->ChangeBackground(GetWhitePixel());
  m_quit->Connect("Clicked()", "LVmonitor", this,"quit()");
  m_quit->SetFont(labelfont);
  m_quit->SetMargins(15,30,10,10);
  datapanel3->AddFrame(m_quit,new TGLayoutHints(kLHintsCenterY | kLHintsCenterX ,2,2,5,5));
  
  TGPicturePool tgpool(0,0);
  const TGPicture* pic=tgpool.GetPicture("psimage",const_cast<char**>(psimage));
  TGIcon* tgi=new TGIcon(datapanel2 ,pic,150,98);
  datapanel2->AddFrame(tgi,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 10, 0));

  SetWindowName("Low Voltage Monitoring");
  Resize(w,h);
  Layout();
  MapSubwindows();
  MapWindow();
}
void LVmonitor::toggle(){
  if(m_ison==false){ //turn on
    m_controller.write("OUTPUT ON");
    m_start->SetDown(true);
    m_ison=true;
  }else{ // turn off
    m_controller.write("OUTPUT OFF");
    m_start->SetDown(false);
    m_ison=false;
  }
}
void LVmonitor::timeouts(){
  // 1 s timeout, update number of events
  for(int i=0;i<2;i++){
    m_lvchan[i]->update(&m_controller);
  }
  const char* outp=m_controller.read("OUTPUT?");
  if(strlen(outp)<2){
    printf("No connection.\n");
    m_noconn++;
  }else {
    m_noconn=0;
  }
  if(m_noconn>4){
    printf("Lost connection. Maybe somebody else took over control.\n");
    quit();
  }
  m_ison=atoi(outp);
  m_start->SetDown(m_ison);
  
}

void LVmonitor::quit(){
  gApplication->Terminate(0);
}

//====================================================================
int main(int argc, char **argv){
  
//
// Declare command object and its argument-iterator
//       
  CmdArgStr host_name ('h', "hostname", "controller-hostname", "hostname of the GPIB ethernet controller",CmdArg::isREQ);
  CmdArgInt stationid ('n', "stationID", "gpib-stationID", "GPIB station ID of the power supply",CmdArg::isREQ);
  CmdLine  cmd(*argv, &host_name, &stationid, NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
//
// Parse arguments
//
  cmd.parse(arg_iter);
  GpibController controller( (const char*)host_name ,(int)stationid);

  TApplication theapp("app",&argc,argv);
  new LVmonitor(controller, gClient->GetRoot(),350,285);
  theapp.Run();
  return 0;
}


