#include "rcecalib/server/EthPrimitive.hh"
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/uio.h>
#include <iostream>
#include <new>


EthPrimitive::EthPrimitive(unsigned short port){ 
   RceNet::IpAddress src(port);
   m_socket.listen(src);
   RceNet::IpAddress name;
   m_socket.getname(name);
   printf("TCP upload socket %d bound to port %d\n", m_socket.socket(), name.port());
}


const char* EthPrimitive::receiveCommand() {
   const unsigned buflen=1024;
   static char buffer[buflen];
   int connfd = m_socket.accept();
   m_connection=new RceNet::Socket(connfd);
   int bytes = m_connection->recv(buffer, buflen);
   assert(bytes<(int)buflen);
   buffer[bytes]=0;
   while(buffer[bytes-1]==' ' && bytes>0)buffer[--bytes]=0;
   std::cout<<"COMMAND: "<<buffer<<std::endl;
   return buffer;
}
void EthPrimitive::reply(const char* msg){
  assert(m_connection!=0);
  m_connection->send((void*)msg, strlen(msg) );
  delete m_connection;
  m_connection=0;
}
  
const char* EthPrimitive::receiveModule(unsigned char *buffer, int size) {
  assert(m_connection==0);
  int connfd = m_socket.accept();
  m_connection=new RceNet::Socket(connfd);
  int remaining = size;
  int offset = 0;
  while( remaining > 0 ) {
    int bytes = m_connection->recv(buffer + offset, remaining);
    if(bytes<0)break;
    offset += bytes;
    remaining -= bytes;
  }
  if(remaining!=0)return "Download failed.";
  return "OK";
}

