ifneq ($(findstring linux,$(tgt_os)),)

tgtnames := measureApp testMeasure

tgtsrcs_measureApp := MeasureApp.cc EthPrimitive.cc
tgtincs_measureApp := rce
tgtlibs_measureApp :=     rce/rceservice \
    rce/rcenet 

tgtslib_measureApp := labjackusb

tgtsrcs_testMeasure := TestMeasure.cc
tgtincs_testMeasure := rce
tgtlibs_testMeasure :=     rce/rceservice \
    rce/rcenet 

tgtslib_testMeasure := rt

endif
