-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-07
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity lookaheadfifotb is end lookaheadfifotb;

architecture testbed of lookaheadfifotb is
   signal clk  : sl;
   signal rst  : sl;
   signal eof, dnextvalid, underflow: sl;
   signal ldout: sl:='0';
   signal valid: sl;
   signal wen: sl := '0';
   signal ldint: sl;
   signal d_in: slv(24 downto 0) := (others => '0');
   signal d_next: slv(24 downto 0);
   signal datain: slv(24 downto 0):=(others => '0');
   signal d_inv: Slv25Array(2 downto 0) :=(0=>'0'& x"010203",
                                          1=>'0'& x"040506",
                                          2=>'1'& x"070809");
   signal counter: integer range 0 to 63:=0;
   signal datawaiting: std_logic;
   signal moredatawaiting: std_logic;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   if(rst='0')then
     counter<=counter+1;
   end if;
   if(counter > 0 and counter<4)then
     datain<=d_inv(counter-1);
     wen<='1';
   elsif(counter=10 )then
     ldout<='1';
   else
     wen<='0';
     ldout<='0';
   end if;
   end process;
     
   eof<=d_next(24);
   fei4fifo : entity work.lookaheadfifo
     port map (
       din => datain,
       clk => clk,
       rd_en => ldout,
       rst => rst,
       wr_en => wen,
       ldint => ldint,
       dout => d_in,
       dnext => d_next,
       valid => valid,
       dnextvalid => dnextvalid);

  pgpackdataflag: entity work.dataflagnew
    port map(
      eofin=>datain(24),
      ldin=>wen,
      eofout=>d_next(24),
      ldout=>ldint,
      datawaiting=> datawaiting,
      moredatawaiting=> moredatawaiting,
      clkin=>clk,
      clkout=>clk,
      rst=>rst
      );


end testbed;
