-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2016-02-03
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.arraytype.all;
use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;

entity efbtb is end efbtb;

architecture testbed of efbtb is
   function l1idcomp(l1id: slv; ttcid: slv) return integer is
   begin
     return (conv_integer(unsigned(l1id))-conv_integer(unsigned(ttcid))) mod 32; 
   end function l1idcomp;
   signal clk  : sl;
   signal rst  : sl;
   signal wen1: sl := '0';
   signal wen2: sl := '0';
   signal fdatain1: slv(32 downto 0):=(others => '0');
   signal fdatain2: slv(32 downto 0):=(others => '0');
   signal d_inv1: Slv33Array(5 downto 0) :=(0=>'0'& x"3000000c",
                                            1=>'1'& x"00003c01",
                                            2=>'1'& x"30000423",
                                            3=>'1'& x"30000c02",
                                            4=>'1'& x"12345678",
                                            5=>'1'& x"12345678");
   signal d_inv2: Slv33Array(5 downto 0) :=(0=>'1'& x"3200000c",
                                            1=>'1'& x"32000424",
                                            2=>'1'& x"3000080c",
                                            3=>'1'& x"12345678",
                                            4=>'1'& x"12345678",
                                            5=>'1'& x"12345678");
   signal d_invt: Slv52Array(5 downto 0) :=(0=>x"0000a00000000",
                                            1=>x"0002200000001",
                                            2=>x"0000000000002",
                                            3=>x"0000000000003",
                                            4=>x"0000000000004",
                                            5=>x"0000000000005");
   signal counter: integer range 0 to 63:=0;
   signal datavalid :  std_logic_vector(15 downto 0):=(others => '0');
   signal nextdatavalid: std_logic_vector(15 downto 0):=(others => '0');
   signal datain: Slv33Array(15 downto 0):=(others => (others => '0'));
   signal nextdatain: Slv33Array(15 downto 0):=(others => (others => '0'));
   signal ldfei4 :   std_logic_vector(15 downto 0);
   signal ttcvalid : sl:='0';
   signal ttcdata: slv(51 downto 0):= (others => '0');
   signal tValid: sl;
   signal tLast: sl;
   signal tData: slv(31 downto 0);
   signal ldttcefb: sl:='0';
   signal ttcld: sl;
   signal ttcefbdata: slv(51 downto 0):= (others => '0');
   signal mAxiMaster: AxiStreamMasterType:=AXI_STREAM_MASTER_INIT_C;
   signal mAxiSlave: AxiStreamSlaveType:=AXI_STREAM_SLAVE_INIT_C;
   signal channelmask: slv(15 downto 0):=x"0005";
   signal masked: slv(15 downto 0);
   signal nExp: slv(4 downto 0):="00001";
   signal resetrun: sl:='0';
   signal counters: CounterType;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 6.25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 6.25 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);

   tValid<=mAxiMaster.tValid;
   tLast<=mAxiMaster.tLast;
   tData<=mAxiMaster.tData(31 downto 0);
   mAxiSlave.tReady<='1';

   process begin
   wait until rising_edge(clk);
   if(rst='0')then
     counter<=counter+1;
   end if;
   if(counter = 1)then
     resetrun<='1';
   elsif(counter=3)then
     ttcefbdata<=d_invt(0);
    ldttcefb<='1';
   elsif(counter=10 or counter=11)then
     fdatain2<=d_inv2(counter-10);
     wen2<='1';
   elsif(counter>=5 and counter<=7)then
     fdatain1<=d_inv1(counter-5);
     wen1<='1';
   elsif(counter=15)then
     ttcefbdata<=d_invt(1);
    ldttcefb<='1';
   else
     resetrun<='0';
     wen1<='0';
     wen2<='0';
     ldttcefb<='0';
   end if;
   end process;
     
   fei4fifo1 : entity work.lookaheadfifo
     generic map(ASYNC_FIFO_G=>false,
                 DATA_WIDTH_G => 33,
                 ADDR_WIDTH_G => 10,
                 FULL_THRES_G => 800)
     port map (
       din => fdatain1,
       wr_clk => '0',
       rd_clk => clk,
       rd_en => ldfei4(0),
       rst => rst,
       wr_en => wen1,
       dout => datain(0),
       dnext => nextdatain(0),
       valid => datavalid(0),
       dnextvalid => nextdatavalid(0));
  fei4fifo2 : entity work.lookaheadfifo
     generic map(ASYNC_FIFO_G=>false,
                 DATA_WIDTH_G => 33,
                 ADDR_WIDTH_G => 10,
                 FULL_THRES_G => 800)
     port map (
       din => fdatain2,
       wr_clk => '0',
       rd_clk => clk,
       rd_en => ldfei4(2),
       rst => rst,
       wr_en => wen2,
       dout => datain(2),
       dnext => nextdatain(2),
       valid => datavalid(2),
       dnextvalid => nextdatavalid(2));

      ttcefbfifo : entity work.FifoSync
        generic map(
          DATA_WIDTH_G => 52,
          ADDR_WIDTH_G => 10,
          FWFT_EN_G => true,
          FULL_THRES_G => 800)  
        port map (
          din => ttcefbdata,
          clk => clk,
          rd_en => ttcld,
          rst => rst,
          wr_en => ldttcefb,
          dout => ttcdata,
          prog_full => open,
          underflow => open,
          valid => ttcvalid);

efb_inst: entity work.eventfragmentbuilder
port map(clk => clk,
         regClk => clk,
	rst => rst, 
        enabled => '1',
        mAxisMaster => mAxiMaster,
        mAxisSlave => mAxiSlave,
        channelmask => channelmask,
        masked => masked,
        counters => counters,
        resetRun => resetRun,
        nExp => nExp,
        timeout => "00"&x"0a",
        timeoutfirst => "00"&x"05",
        missing_header_timeout => x"05",
        runnumber => x"00000007",
        datavalid => datavalid,
        nextdatavalid => nextdatavalid,
        datain => datain,
        nextdatain => nextdatain,
        ldfei4 => ldfei4,
        ttcvalid => ttcvalid,
        ttcdata => ttcdata,
        ttcld => ttcld
);

end testbed;
