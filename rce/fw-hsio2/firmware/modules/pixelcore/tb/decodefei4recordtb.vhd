-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2015-01-06
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity decodefei4recordtb is end decodefei4recordtb;

architecture testbed of decodefei4recordtb is
   signal clk  : sl;
   signal rst  : sl;
   signal isrunning: sl;
   signal eof, ldout, overflow: sl;
   signal ldinv : slv(15 downto 0) :="0001001001001001";
   signal ldin, k_in : sl;
   signal k_inv : slv(15 downto 0) :="0111000000000111";
   signal err_in: sl :='0';
   signal d_in: slv(7 downto 0);
   signal d_inv: Slv8Array(15 downto 0) :=(0=>x"fc",
                                          1=>x"fc",
                                          2=>x"fc",
                                          3=>x"01",
                                          4=>x"01",
                                          5=>x"01",
                                          6=>x"02",
                                          7=>x"02",
                                          8=>x"02",
                                          9=>x"03",
                                          10=>x"03",
                                          11=>x"03",
                                          12=>x"bc",
                                          13=>x"bc",
                                          14=>x"bc",
                                          15=>x"00");
                                          
     
   signal d_out: slv(23 downto 0);
   signal counter: integer range 0 to 15:=0;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 50 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);


   process begin
   wait until rising_edge(clk);
   if(rst='0' and counter/=15)then
     counter<=counter+1;
   end if;
   end process;
   d_in<=d_inv(counter);
   k_in<=k_inv(counter);
   ldin<=ldinv(counter);

   decode: entity work.decodefei4record
    port map(clk => clk,
         rst => rst,
         enabled => '1',
         isrunning => isrunning,
         d_in => d_in,
         k_in => k_in,
         err_in => err_in,
         d_out(23 downto 0) => d_out,
         d_out(24) => eof,
         ldin => ldin,
         ldout => ldout,
         overflow => overflow
);

end testbed;
