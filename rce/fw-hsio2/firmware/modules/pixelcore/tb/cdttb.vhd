-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : HeartbeatTb.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2013-09-26
-- Last update: 2016-04-27
-- Platform   : ISE 14.5
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use work.all;
use work.StdRtlPkg.all;

entity cdttb is end cdttb;

architecture testbed of cdttb is
   signal clk : sl;
   signal rst  : sl:='1';
   signal l1a : sl := '0';
   signal busy: sl := '0';
   signal counter: integer range 0 to 255 := 0;
begin
   CLK_0 : entity work.ClkRst
      generic map (
         CLK_PERIOD_G      => 25 ns,
         RST_START_DELAY_G => 0 ns,  -- Wait this long into simulation before asserting reset
         RST_HOLD_TIME_G   => 25 ns)  -- Hold reset for this long)
      port map (
         clkP => clk,
         clkN => open,
         rst  => rst,
         rstL => open);

   process begin
     wait until rising_edge(clk);
     if(counter<255)then
       counter <= counter+1;
     if(counter=4 or counter=10 or counter=15 or counter=20 or counter=25 or counter=30)then
         l1a<='1';
       else
         l1a<='0';
       end if;
     end if;
   end process;

   cdt_inst: entity work.complexdeadtime
      port map (
        clk => clk,
        rst => rst,
        enabled=>'1',
        l1a => l1a,
        busy => busy,
        numbuffers=> "00011",
        window => x"0014");

end testbed;
