
LIBRARY ieee;
use work.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity outputencoderphase is 
   port (
     clock      : in std_logic;
     rst        : in std_logic;
     datain     : in std_logic;
     encode     : in std_logic_vector(1 downto 0);
     dataout    : out std_logic_vector(7 downto 0) := x"00"
     );
end outputencoderphase;

architecture OUTPUTENCODER of outputencoderphase is

  signal lastbit : std_logic := '0';

begin

  process
  begin
    wait until rising_edge(clock);
    if(encode="01") then
      dataout(7 downto 4) <= (others => not lastbit);
      dataout(3 downto 0) <= (others => datain xor not lastbit);
      lastbit <= datain xor not lastbit;
    elsif(encode="10") then
      dataout(7 downto 4) <= (others => not datain);
      dataout(3 downto 0) <= (others => datain);
    else
      dataout<= (others => datain);
    end if;
  end process;

end OUTPUTENCODER;
