--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;
use work.arraytype.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.StdRtlPkg.all;
--------------------------------------------------------------


entity multiplexdata is
generic(
        AXI_CONFIG_C : AxiStreamConfigType := ssiAxiStreamConfig(2);
        maxchannel: integer:=31);
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        pausedout:  out std_logic;
        badparity:  out std_logic:='0';
        channelmask: in std_logic_vector(31 downto 0);
        datawaiting: in std_logic_vector(31 downto 0);
        moredatawaiting: in std_logic_vector(31 downto 0);
        indatavalid: in std_logic_vector(31 downto 0);
        datain: in dataarray;
        mAxisMaster: out AxiStreamMasterType;
        mAxisSlave: in AxiStreamSlaveType;
        reqdata: out std_logic_vector(31 downto 0);
        multiplicity: in std_logic_vector(63 downto 0)
);
end multiplexdata;

--------------------------------------------------------------

architecture MULTIPLEXDATA of multiplexdata is


  type state_type is (idle, first, sending, paused, switch);
  type RegType is record
    txMaster   : AxiStreamMasterType;
    state      : state_type;
    reqdatas   : slv(31 downto 0);
    pvalid     : slv(31 downto 0);
    pdata      : dataarray;
    index      : integer;
    repcounter: std_logic_vector(3 downto 0);
    fw         :sl;
    parity     :sl;
   end record RegType;

   constant REG_INIT_C : RegType :=(
     txMaster => AXI_STREAM_MASTER_INIT_C,
     state => idle,
     reqdatas => (others => '0'),
     pvalid => (others => '0'),
     pdata => (others => (others => '0')),
     index => 0,
     repcounter => (others => '0'),
     fw=>'0',
     parity => '0'
     );
    

  signal enabled: std_logic:='0';
  
  signal r   : RegType := REG_INIT_C;
  signal rin : RegType;

  function reps(w:std_logic_vector; i:integer) return std_logic_vector is
  begin
    return w(i*4+3 downto i*4);
  end;

begin

  reqdata<=r.reqdatas; 
  pausedout<=not enabled;
  enabled<=mAxisSlave.tReady;
  mAxisMaster<=r.txMaster;
  badparity<=r.parity;

  comb: process (r, enabled, indatavalid, datawaiting, channelmask, datain, rst) is
    variable v: RegType;
  begin
    v := r;
    v.txMaster.tUser := (others => '0');
    case v.state is
      when switch =>
        v.txMaster.tLast:='0';
        v.txMaster.tValid:='0';
        if(indatavalid(r.index)='1')then
          v.pvalid(r.index):='1';
          v.pdata(r.index):=datain(r.index);
        else
          v.pvalid(r.index):='0';
        end if;
        if(moredatawaiting(r.index)='1' and r.repcounter/=reps(multiplicity, r.index))then
          v.repcounter:=unsigned(r.repcounter)+1;
        else
          v.repcounter:=x"0";
          if(r.index=maxchannel)then
            v.index:=29;
          elsif(r.index=31)then
            v.index:=0;
          else
            v.index:=r.index+1;
          end if;
        end if;
        v.state:=idle;
      when idle =>
        if(enabled='1' and channelmask(r.index)='1' and datawaiting(r.index)='1')then
          v.reqdatas(r.index):='1';
          v.fw:='0';
          v.state:=first;
        elsif(enabled='0')then
          v.state:=idle;
        else
          v.state:=idle;
          if(r.index=maxchannel)then
            v.index:=29;
          elsif(r.index=31)then
            v.index:=0;
          else
            v.index:=r.index+1;
          end if;
        end if;
      when first =>
        if(r.pvalid(r.index)='1')then
          ssiSetUserSof(AXI_CONFIG_C, v.txMaster, '1');
          v.txMaster.tData:= datain(r.index)(7 downto 0) & datain(r.index)(15 downto 8); 
          v.txMaster.tValid:='1';
          v.pvalid(r.index):='0';
        end if;
        v.state:= sending;
      when sending =>
        if(r.fw='0')then
          if(datain(r.index)(15 downto 0)/=x"0000")then
            v.parity:='1';
          end if;
          v.fw:='1';
        end if;
        v.txMaster.tData := datain(r.index)(7 downto 0) & datain(r.index)(15 downto 8); 
        v.txMaster.tValid:='1';
        ssiSetUserSof(AXI_CONFIG_C, v.txMaster, datain(r.index)(16));
        if(datain(r.index)(17)='1') then --EOF
          v.txMaster.tLast:='1';
          v.reqdatas(r.index):='0';
          v.state:=switch;
        elsif(enabled='0')then
          v.reqdatas(r.index):='0';
          v.state:=paused;-- pause transmission
        else
          v.state:=sending;
        end if;
      when paused =>
        if(indatavalid(r.index)='1')then
          v.txMaster.tData := datain(r.index)(7 downto 0) & datain(r.index)(15 downto 8); 
          v.txMaster.tValid:='1';
          ssiSetUserSof(AXI_CONFIG_C, v.txMaster, datain(r.index)(16));
          if(datain(r.index)(17)='1') then --EOF
            v.txMaster.tLast:='1';
            v.state:=switch;
           end if;
        else
          v.txMaster.tValid:='0';
          if(enabled='1')then
            v.reqdatas(r.index):='1';
            v.state:=first;
          else
            v.state:=paused;
          end if;
        end if;
      end case;

      if (rst='1')then
        v:= REG_INIT_C;
      end if;
        
      rin <= v;

  end process comb;

   seq : process (clk) is
   begin
      if rising_edge(clk) then
         r <= rin;
      end if;
   end process seq;
  --ila: entity work.ila_0
  --  port map(
  --    clk => clk,
  --    probe0 => toSlv(r.index, 4),
  --    probe1=> (0=>r.txMaster.tValid),
  --    probe2=> (0=>r.txMaster.tLast),
  --    probe3=> (0=>r.parity),
  --    probe4=> (0=>r.txMaster.tUser(0)),
  --    probe5=> r.txMaster.tData(15 downto 0),
  --    probe6=> (0=>enabled),
  --    probe7=> (0=>indatavalid(r.index)),
  --    probe8=> datain(r.index)(15 downto 0),
  --    probe9=> (0=>datain(r.index)(16)),
  --    probe10=> (0=>datain(r.index)(17)));

end MULTIPLEXDATA;
