--------------------------------------------------------------
-- Serializer for High Speed I/O board (ATLAS Pixel teststand)
-- Martin Kocian 01/2009
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use work.all;
use work.arraytype.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.StdRtlPkg.all;
--------------------------------------------------------------


entity multiplexdata is
generic(
        AXI_CONFIG_C : AxiStreamConfigType := ssiAxiStreamConfig(2));
port(	clk: 	    in std_logic;
	rst:	    in std_logic;
        channelmask: in std_logic_vector(15 downto 0);
        datawaiting: in std_logic_vector(15 downto 0);
        indatavalid: in std_logic_vector(15 downto 0);
        datain: in dataarray;
        mAxisMaster: out AxiStreamMasterType;
        mAxisSlave: in AxiStreamSlaveType;
        reqdata: out std_logic_vector(15 downto 0)
);
end multiplexdata;

--------------------------------------------------------------

architecture MULTIPLEXDATA of multiplexdata is


  type state_type is (idle, first, sending, paused, switch, kludge1, kludge2, kludge3);
  type RegType is record
    txMaster   : AxiStreamMasterType;
    state      : state_type;
    reqdatas   : slv(15 downto 0);
    pvalid     : slv(15 downto 0);
    pdata      : dataarray;
    index      : integer;
   end record RegType;

   constant REG_INIT_C : RegType :=(
     txMaster => AXI_STREAM_MASTER_INIT_C,
     state => idle,
     reqdatas => (others => '0'),
     pvalid => (others => '0'),
     pdata => (others => (others => '0')),
     index => 0);
    

  signal enabled: std_logic;
  
  signal r   : RegType := REG_INIT_C;
  signal rin : RegType;
begin

  reqdata<=r.reqdatas; 
  enabled<=mAxisSlave.tReady;
  mAxisMaster<=r.txMaster;

  comb: process (r, enabled, indatavalid, datawaiting, channelmask, datain, rst) is
    variable v: RegType;
  begin
    v := r;
    v.txMaster.tUser := (others => '0');
    case v.state is
      when kludge1 =>
        v.txMaster.tData:=x"0000";
        v.txMaster.tValid:='1';
        if(indatavalid(r.index)='1')then
          v.pvalid(r.index):='1';
          v.pdata(r.index):=datain(r.index);
        else
          v.pvalid(r.index):='0';
        end if;
        v.state:=kludge2;
      when kludge2 =>
        v.txMaster.tValid:='1';
        v.txMaster.tLast:='1';
        v.state:=kludge3;
      when kludge3 =>
        v.txMaster.tLast:='0';
        v.txMaster.tValid:='0';
        if(r.index=15)then
          v.index:=0;
        else
          v.index:=r.index+1;
        end if;
        v.state:=idle;
      when switch =>
        v.txMaster.tLast:='0';
        v.txMaster.tValid:='0';
        if(indatavalid(r.index)='1')then
          v.pvalid(r.index):='1';
          v.pdata(r.index):=datain(r.index);
        else
          v.pvalid(r.index):='0';
        end if;
        if(r.index=15)then
          v.index:=0;
        else
          v.index:=r.index+1;
        end if;
        v.state:=idle;
      when idle =>
        if(enabled='1' and channelmask(r.index)='1' and datawaiting(r.index)='1')then
          v.reqdatas(r.index):='1';
          v.state:=first;
        elsif(enabled='0')then
          v.state:=idle;
        else
          v.state:=idle;
          if(r.index=15)then
            v.index:=0;
          else
            v.index:=r.index+1;
          end if;
        end if;
      when first =>
        if(r.pvalid(r.index)='1')then
          ssiSetUserSof(AXI_CONFIG_C, v.txMaster, '1');
          v.txMaster.tData:=r.pdata(r.index)(15 downto 0);
          v.txMaster.tValid:='1';
          v.pvalid(r.index):='0';
        end if;
        v.state:= sending;
      when sending =>
        v.txMaster.tData := datain(r.index)(15 downto 0); 
        v.txMaster.tValid:='1';
        ssiSetUserSof(AXI_CONFIG_C, v.txMaster, datain(r.index)(16));
        if(datain(r.index)(17)='1') then --EOF
          v.reqdatas(r.index):='0';
          v.state:=kludge1;
        elsif(enabled='0')then
          v.reqdatas(r.index):='0';
          v.state:=paused;-- pause transmission
        else
          v.state:=sending;
        end if;
      when paused =>
        if(indatavalid(r.index)='1')then
          v.txMaster.tData := datain(r.index)(15 downto 0); 
          v.txMaster.tValid:='1';
          if(datain(r.index)(17)='1') then --EOF
              v.state:=kludge1;
           end if;
        else
          v.txMaster.tValid:='0';
          if(enabled='1')then
            v.reqdatas(r.index):='1';
            v.state:=first;
          else
            v.state:=paused;
          end if;
        end if;
      end case;

      if (rst='1')then
        v:= REG_INIT_C;
      end if;

      rin <= v;

  end process comb;

   seq : process (clk) is
   begin
      if rising_edge(clk) then
         r <= rin;
      end if;
   end process seq;
   
end MULTIPLEXDATA;
