# Set up environment
set VIVADO_BUILD_DIR $::env(VIVADO_BUILD_DIR)
source -quiet ${VIVADO_BUILD_DIR}/vivado_env_var_v1.tcl
source -quiet ${VIVADO_BUILD_DIR}/vivado_proc_v1.tcl

set bitFile "$::env(IMPL_DIR)/$::env(PROJECT).bit"

# Connect to cable
connect_hw_server -host localhost -port 60001
current_hw_target [get_hw_targets */xilinx_tcf/Digilent/*]
open_hw_target

# Connect to FPGA
current_hw_device [lindex [get_hw_devices xc7a200t*] 0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 0]

# Select bit file
set_property PROGRAM.FILE $bitFile [lindex [get_hw_devices] 0]
program_hw_devices -verbose [lindex [get_hw_devices] 0]
refresh_hw_device [lindex [get_hw_devices] 0]
