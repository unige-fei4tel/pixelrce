
# IO Types
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsP]
#set_property IOSTANDARD LVDS_25  [get_ports dtmToRtmLsM]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareP]
#set_property IOSTANDARD LVCMOS25 [get_ports plSpareM]

# PGP Clocks
create_clock -name locRefClk -period 4.0 [get_ports locRefClkP]

#create_generated_clock -name pgpClk250 -source [get_ports locRefClkP] \
##    -multiply_by 5 -divide_by 8 [get_pins U_HsioPgpLane/U_PgpClkGen/CLKOUT0]
create_generated_clock -name pgpClk -source [get_ports locRefClkP] \
    -multiply_by 5 -divide_by 8 [get_pins U_HsioPgpLane/ClockManager7_1/MmcmGen.U_Mmcm/CLKOUT0]

set_clock_groups -asynchronous \
      -group [get_clocks -include_generated_clocks fclk0] \
      -group [get_clocks -include_generated_clocks locRefClk]

