-------------------------------------------------------------------------------
-- Title      : 
-------------------------------------------------------------------------------
-- File       : PgpFrontEnd.vhd
-- Author     : Larry Ruckman  <ruckman@slac.stanford.edu>
-- Company    : SLAC National Accelerator Laboratory
-- Created    : 2014-01-29
-- Last update: 2015-01-22
-- Platform   : Vivado 2014.1
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Wrapper for front end logic connection to the PGP card.
-------------------------------------------------------------------------------
-- Copyright (c) 2014 SLAC National Accelerator Laboratory
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

use work.StdRtlPkg.all;
use work.AxiStreamPkg.all;
use work.SsiPkg.all;
use work.SsiCmdMasterPkg.all;
use work.AxiLitePkg.all;
use work.Pgp2bPkg.all;

library unisim;
use unisim.vcomponents.all;

entity PgpFrontEnd is
   generic (
      TPD_G               : time                       := 1 ns;
      CASCADE_SIZE_G      : integer range 1 to (2**24) := 1);
   port (
      clk                 : in sl;
      rst                 : in sl;
      refclk              : in sl;
      pgpclk              : in sl;
      txReady             : out sl;
      rxReady             : out sl;
      -- GT Pins
      gtTxP               : out sl;
      gtTxN               : out sl;
      gtRxP               : in  sl;
      gtRxN               : in  sl);        
end PgpFrontEnd;

-- Define architecture
architecture mapping of PgpFrontEnd is


   -- Non VC Rx Signals
   signal pgpRxIn  : Pgp2bRxInType;
   signal pgpRxOut : Pgp2bRxOutType;

   -- Non VC Tx Signals
   signal pgpTxIn  : Pgp2bTxInType;
   signal pgpTxOut : Pgp2bTxOutType;

   -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
   signal pgpTxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpTxSlaves  : AxiStreamSlaveArray(3 downto 0);

   -- Frame Receive Interface - 1 Lane, Array of 4 VCs
   signal pgpRxMasters : AxiStreamMasterArray(3 downto 0);
   signal pgpRxCtrl    : AxiStreamCtrlArray(3 downto 0);
   signal sAxisCtrl    : AxiStreamCtrlType;

   signal pgpRst: sl;

begin

   SysRst_Inst : entity work.RstSync
    port map(
      clk      => pgpClk,
      asyncRst => rst,
      syncRst  => pgpRst);  

   Pgp2bGtx7MultiLane_Inst : entity work.Pgp2bGtx7MultiLane
      generic map (
         -- CPLL Settings
         CPLL_REFCLK_SEL_G => "010",
         CPLL_FBDIV_G      => 2,
         CPLL_FBDIV_45_G   => 5,
         CPLL_REFCLK_DIV_G => 1,
         RXOUT_DIV_G       => 1,
         TXOUT_DIV_G       => 1,
         RX_CLK25_DIV_G    => 13,
         TX_CLK25_DIV_G    => 13,
         -- Configure PLL sources
         TX_PLL_G          => "CPLL",
         RX_PLL_G          => "CPLL",
         -- Configure Number of Virtual Channels
         NUM_VC_EN_G       => 4,
         -- Configure Number of Lanes
         LANE_CNT_G        => 1)
      port map (
         -- GT Clocking
         stableClk        => clk,
         gtCPllRefClk     => refclk,
         gtCPllLock       => open,
         gtQPllRefClk     => '0',
         gtQPllClk        => '0',
         gtQPllLock       => '1',
         gtQPllRefClkLost => '0',
         gtQPllReset      => open,
         -- Gt Serial IO
         gtTxP(0)         => gtTxP,
         gtTxN(0)         => gtTxN,
         gtRxP(0)         => gtRxP,
         gtRxN(0)         => gtRxN,
         -- Tx Clocking
         pgpTxReset       => pgpRst,
         pgpTxClk         => pgpClk,
         pgpTxMmcmReset   => open,
         pgpTxMmcmLocked  => '1',
         -- Rx clocking
         pgpRxReset       => pgpRst,
         pgpRxRecClk      => open,
         pgpRxClk         => pgpClk,
         pgpRxMmcmReset   => open,
         pgpRxMmcmLocked  => '1',
         -- Non VC Rx Signals
         pgpRxIn          => pgpRxIn,
         pgpRxOut         => pgpRxOut,
         -- Non VC Tx Signals
         pgpTxIn          => pgpTxIn,
         pgpTxOut         => pgpTxOut,
         -- Frame Transmit Interface - 1 Lane, Array of 4 VCs
         pgpTxMasters     => pgpTxMasters,
         pgpTxSlaves      => pgpTxSlaves,
         -- Frame Receive Interface - 1 Lane, Array of 4 VCs
         pgpRxMasters     => pgpRxMasters,
         pgpRxMasterMuxed => open,
         pgpRxCtrl        => pgpRxCtrl);    


   -- Lane 0, VC3 TX/RX Loopback Data Buffer
   VCTX3_VCRX3 : entity work.SsiFifo
      generic map (
         CASCADE_SIZE_G      => 1,
         BRAM_EN_G           => true,
         USE_BUILT_IN_G      => false,
         GEN_SYNC_FIFO_G     => true,
         FIFO_ADDR_WIDTH_G   => 9,
         FIFO_FIXED_THRESH_G => true,
         FIFO_PAUSE_THRESH_G => 128,
         SLAVE_AXI_CONFIG_G  => SSI_PGP2B_CONFIG_C,
         MASTER_AXI_CONFIG_G => SSI_PGP2B_CONFIG_C) 
      port map (
         -- Slave Port
         sAxisClk    => pgpClk,
         sAxisRst    => pgpRst,
         sAxisMaster => pgpRxMasters(0),
         sAxisSlave  => open,
         sAxisCtrl   => pgpRxCtrl(0),
         -- Master Port
         mAxisClk    => pgpClk,
         mAxisRst    => pgpRst,
         mAxisMaster => pgpTxMasters(0),
         mAxisSlave  => pgpTxSlaves(0));          

   -- Misc. PGP signals
   pgpRxIn <= PGP2B_RX_IN_INIT_C;
   pgpTxIn <= PGP2B_TX_IN_INIT_C;

   SyncOut_Inst : entity work.SynchronizerVector
      generic map (
         WIDTH_G => 2)    
      port map (
         clk        => clk,
         dataIn(1)  => pgpTxOut.linkReady,
         dataIn(0)  => pgpRxOut.linkReady,
         dataOut(1) => txReady,
         dataOut(0) => rxReady);   

end mapping;
