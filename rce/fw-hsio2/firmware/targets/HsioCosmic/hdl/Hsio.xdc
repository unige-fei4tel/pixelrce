# StdLib
set_property ASYNC_REG true [get_cells -hierarchical *crossDomainSyncReg_reg*]

# FPGA Port Definition
set_property PACKAGE_PIN W24 [get_ports iResetInL]
set_property IOSTANDARD LVCMOS25 [get_ports iResetInL]
set_property PACKAGE_PIN H31 [get_ports iPor]
set_property IOSTANDARD LVCMOS33 [get_ports iPor]

# FPGA Hardware Configuration
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]

### HSIO User I/O ###
# Green HSIO LEDs
set_property PACKAGE_PIN AM9  [get_ports {led[0]}]
set_property PACKAGE_PIN AM11 [get_ports {led[1]}]
set_property PACKAGE_PIN AN11 [get_ports {led[2]}]
set_property PACKAGE_PIN AJ11 [get_ports {led[3]}]
set_property PACKAGE_PIN AK11 [get_ports {led[4]}]
set_property PACKAGE_PIN AL10 [get_ports {led[5]}]
set_property PACKAGE_PIN AM10 [get_ports {led[6]}]
set_property PACKAGE_PIN AL8  [get_ports {led[7]}]

set_property IOSTANDARD LVCMOS25 [get_ports {led[*]}]

set_property PACKAGE_PIN AA32  [get_ports fpga_status_led]
set_property IOSTANDARD LVCMOS25  [get_ports fpga_status_led]
set_property PACKAGE_PIN AB26  [get_ports usr_led]
set_property IOSTANDARD LVCMOS25  [get_ports usr_led]

# Display I2C
set_property PACKAGE_PIN M26 [get_ports {displayI2cScl}]
set_property PACKAGE_PIN L34 [get_ports {displayI2cSda}]

set_property IOSTANDARD LVCMOS33 [get_ports {displayI2cScl}]
set_property IOSTANDARD LVCMOS33 [get_ports {displayI2cSda}]

# DTM
set_property PACKAGE_PIN R31 [get_ports {dtm_ps0_l}]
set_property IOSTANDARD LVCMOS25 [get_ports {dtm_ps0_l}]
set_property PULLUP TRUE [get_ports {dtm_ps0_l}]  
set_property PACKAGE_PIN P31 [get_ports {dtm_en_l}]
set_property IOSTANDARD LVCMOS25 [get_ports {dtm_en_l}]
set_property PACKAGE_PIN L29 [get_ports {dtmI2cScl}]
set_property PACKAGE_PIN L30 [get_ports {dtmI2cSda}]
set_property IOSTANDARD LVCMOS33 [get_ports {dtmI2cScl}]
set_property IOSTANDARD LVCMOS33 [get_ports {dtmI2cSda}]

set_property PACKAGE_PIN U26    [get_ports ttc_busy_p]
set_property PACKAGE_PIN U27    [get_ports ttc_busy_n]
set_property IOSTANDARD LVDS_25 [get_ports ttc_busy_p]
set_property IOSTANDARD LVDS_25 [get_ports ttc_busy_n]
set_property PACKAGE_PIN R26 [get_ports ttc_sd_p]
set_property PACKAGE_PIN P26 [get_ports ttc_sd_n]
set_property IOSTANDARD LVDS_25 [get_ports ttc_sd_p]
set_property IOSTANDARD LVDS_25 [get_ports ttc_sd_n]
set_property PACKAGE_PIN N26  [get_ports ttc_locked_p]
set_property PACKAGE_PIN M27 [get_ports ttc_locked_n]
set_property IOSTANDARD LVDS_25 [get_ports ttc_locked_p]
set_property IOSTANDARD LVDS_25 [get_ports ttc_locked_n]

### MGTs ###
#312.5 MHz clk 1 on Bank 113 (West)
#set_property PACKAGE_PIN AG16 [get_ports iMgtRefClkP]
#set_property PACKAGE_PIN AH16 [get_ports iMgtRefClkM]
##250 MHz clk 0 on Bank 113 (West)
set_property PACKAGE_PIN AG14 [get_ports iMgtRefClk2P]
set_property PACKAGE_PIN AH14 [get_ports iMgtRefClk2M]
#312.5 MHz clk 1 on Bank 116 (West)
set_property PACKAGE_PIN H14 [get_ports iMgtRefClkP]
set_property PACKAGE_PIN G14 [get_ports iMgtRefClkM]
#250 MHz clk 0 on Bank 116 (West)
#set_property PACKAGE_PIN H16 [get_ports iMgtRefClk2P]
#set_property PACKAGE_PIN G16 [get_ports iMgtRefClk2M]
#TTC CLK 
set_property PACKAGE_PIN H20 [get_ports iMgtRefClk3P]
set_property PACKAGE_PIN G20 [get_ports iMgtRefClk3M]
# MGT SFP0
set_property PACKAGE_PIN AJ15 [get_ports iSLinkRxP]
set_property PACKAGE_PIN AK15 [get_ports iSLinkRxN]
set_property PACKAGE_PIN AL14 [get_ports oSLinkTxP]
set_property PACKAGE_PIN AM14 [get_ports oSLinkTxN]
# MGT SFP1
#set_property PACKAGE_PIN AJ13 [get_ports iMgtRxP]
#set_property PACKAGE_PIN AK13 [get_ports iMgtRxN]
#set_property PACKAGE_PIN AN13 [get_ports oMgtTxP]
#set_property PACKAGE_PIN AP13 [get_ports oMgtTxN]
# MGT to DTM 0
set_property PACKAGE_PIN F13 [get_ports iMgtRxP]
set_property PACKAGE_PIN E13 [get_ports iMgtRxN]
set_property PACKAGE_PIN B13 [get_ports oMgtTxP]
set_property PACKAGE_PIN A13 [get_ports oMgtTxN]
# MGT to DTM 1
#set_property PACKAGE_PIN F15 [get_ports {iMgtB116RxP[1]}]
#set_property PACKAGE_PIN E15 [get_ports {iMgtB116RxM[1]}]
#set_property PACKAGE_PIN D14 [get_ports {oMgtB116TxP[1]}]
#set_property PACKAGE_PIN C14 [get_ports {oMgtB116TxM[1]}]
# MGT QSFP0
#set_property PACKAGE_PIN AL18 [get_ports iMgtRxP]
#set_property PACKAGE_PIN AM18 [get_ports iMgtRxN]
#set_property PACKAGE_PIN AN19 [get_ports oMgtTxP]
#set_property PACKAGE_PIN AP19 [get_ports oMgtTxN]
# MGT QSFP1
#set_property PACKAGE_PIN AJ19 [get_ports iMgtRxP]
#set_property PACKAGE_PIN AK19 [get_ports iMgtRxN]
#set_property PACKAGE_PIN AN21 [get_ports oMgtTxP]
#set_property PACKAGE_PIN AP21 [get_ports oMgtTxN]
# MGT QSFP2
#set_property PACKAGE_PIN AL20 [get_ports iMgtRxP]
#set_property PACKAGE_PIN AM20 [get_ports iMgtRxN]
#set_property PACKAGE_PIN AL22 [get_ports oMgtTxP]
#set_property PACKAGE_PIN AM22 [get_ports oMgtTxN]
# MGT QSFP3
#set_property PACKAGE_PIN AJ21 [get_ports iMgtRxP]
#set_property PACKAGE_PIN AK21 [get_ports iMgtRxN]
#set_property PACKAGE_PIN AN23 [get_ports oMgtTxP]
#set_property PACKAGE_PIN AP23 [get_ports oMgtTxN]
#MGT zone-3 0
#set_property PACKAGE_PIN F21 [get_ports iMgtRxP]
#set_property PACKAGE_PIN E21 [get_ports iMgtRxN]
#set_property PACKAGE_PIN B23 [get_ports oMgtTxP]
#set_property PACKAGE_PIN A23 [get_ports oMgtTxN]
#MGT zone-3 1
#set_property PACKAGE_PIN D20 [get_ports iMgtRxP]
#set_property PACKAGE_PIN C20 [get_ports iMgtRxN]
#set_property PACKAGE_PIN D22 [get_ports oMgtTxP]
#set_property PACKAGE_PIN C22 [get_ports oMgtTxN]
#MGT SMA
#set_property PACKAGE_PIN AJ17 [get_ports iMgtRxP]
#set_property PACKAGE_PIN AK17 [get_ports iMgtRxN]
#set_property PACKAGE_PIN AN17 [get_ports oMgtTxP]
#set_property PACKAGE_PIN AP17 [get_ports oMgtTxN]


#LEMO connectors
set_property PACKAGE_PIN AJ29 [get_ports {iHSIOtrigger[0]}]
set_property PACKAGE_PIN AK30 [get_ports {iHSIOtrigger[1]}]
set_property PACKAGE_PIN AL30 [get_ports oHSIOtrigger]
set_property PACKAGE_PIN AM30 [get_ports oHSIObusy]
--set_property PACKAGE_PIN AM30 [get_ports iHSIObusy]
set_property PACKAGE_PIN Y31 [get_ports iExtreload]
set_property PACKAGE_PIN AA30 [get_ports clockcheck]
set_property PACKAGE_PIN AB30 [get_ports hbcheck]

set_property IOSTANDARD LVTTL [get_ports {iHSIOtrigger[0]}]
set_property IOSTANDARD LVTTL [get_ports {iHSIOtrigger[1]}]
set_property IOSTANDARD LVTTL [get_ports oHSIOtrigger]
set_property IOSTANDARD LVTTL [get_ports oHSIObusy]
--set_property IOSTANDARD LVTTL [get_ports iHSIObusy]
set_property IOSTANDARD LVCMOS25 [get_ports iExtreload]
set_property IOSTANDARD LVCMOS25 [get_ports clockcheck]
set_property IOSTANDARD LVCMOS25 [get_ports hbcheck]

set_property SLEW FAST [get_ports oHSIOtrigger]
--set_property PULLDOWN true [get_ports iHSIObusy]
set_property PULLDOWN true [get_ports iHSIOtrigger]
set_property PULLUP true [get_ports iExtreload]

set_property PACKAGE_PIN G34  [get_ports {sw_dtmboot}]
set_property IOSTANDARD LVCMOS33 [get_ports {sw_dtmboot}]

# I2C to TTC board
set_property PACKAGE_PIN AM31   [get_ports ttc_scl]
set_property PACKAGE_PIN AN32   [get_ports ttc_sda]
set_property IOSTANDARD LVCMOS33   [get_ports ttc_scl]
set_property IOSTANDARD LVCMOS33   [get_ports ttc_sda]

# data from TTC board
set_property PACKAGE_PIN T28   [get_ports ttc_data_p]
set_property PACKAGE_PIN R28   [get_ports ttc_data_n]
set_property IOSTANDARD LVDS_25   [get_ports ttc_data_p]
set_property IOSTANDARD LVDS_25   [get_ports ttc_data_n]

### Timing Constraints ###
# MGT Clocks
create_clock -period 3.200 -name mgtClkP [get_ports iMgtRefClkP]
create_clock -period 4.0   -name mgtClk2P [get_ports iMgtRefClk2P]
create_clock -period 6.25   -name mgtClk3P [get_ports iMgtRefClk3P]
create_clock -period 10.0 -name clk100 [get_pins U_ClkGen/SysClk100_inst/U0/mmcm_adv_inst/CLKOUT0]
create_clock -period 25 -name clk40 [get_pins U_ClkGen/SysClk100_inst/U0/mmcm_adv_inst/CLKOUT2]
create_clock -period 25 -name clk40fe [get_pins U_ClkGen/SysClkGen_Inst/U0/mmcm_adv_inst/CLKIN1]
create_clock -name sysClk40Unbuf_cc -period 25 [get_pins U_HsioCosmicCore1/CLKFEI4/I0]
create_clock -name sysClk40Unbuf90_cc -period 25 [get_pins U_HsioCosmicCore1/CLKFEI490/I0]
create_clock -name sysClk160Unbuf_cc -period 6.25 [get_pins U_HsioCosmicCore1/CLKFEI4/I1]
create_clock -name sysClk160Unbuf90_cc -period 6.25 [get_pins U_HsioCosmicCore1/CLKFEI490/I1]
create_clock -name rxclock160  -period 6.25 [get_pins AtlasTtcRx_Inst/AtlasTtcRxCdrInputs_Inst/BUFG_160MHz/O]

# Reset
#set_false_path -from [get_ports iResetInL]
set_false_path -from [get_pins U_HsioCosmicCore1/registers_inst/datapath_reg/C]

# CE signals for FE-I4 Clock Muxes
set_false_path -to [get_pins -hierarchical -filter {NAME =~"*CLKFEI4*/CE1*" }]
# async reset to receiver flip flops
set_false_path -to [get_pins -hierarchical -filter {NAME =~"*receivedata/ff_az*/CLR" }]
set_false_path -to [get_pins -hierarchical -filter {NAME =~"*receivedata/ff_bz*/CLR" }]
set_false_path -to [get_pins -hierarchical -filter {NAME =~"*receivedata/ff_cz*/CLR" }]
set_false_path -to [get_pins -hierarchical -filter {NAME =~"*receivedata/ff_dz*/CLR" }]
set_property IOB false [get_cells U_HsioCosmicCore*/*receivedata/ff_*]
# async trigger input 
set_false_path -to [get_cells {"U_triggerlogic/coin/coinct_reg" }]

set_clock_groups -asynchronous -group [get_clocks -include_generated_clocks mgtClkP] -group [get_clocks -include_generated_clocks mgtClk2P] -group [get_clocks -include_generated_clocks mgtClk3P] -group [get_clocks -include_generated_clocks clk100] -group [get_clocks -include_generated_clocks clk40fe] -group [get_clocks -include_generated_clocks clk40] -group [get_clocks -include_generated_clocks rxclock160] 
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks sysClk40Unbuf_cc] -group [get_clocks -include_generated_clocks sysClk160Unbuf_cc]
set_clock_groups -logically_exclusive -group [get_clocks -include_generated_clocks sysClk40Unbuf90_cc] -group [get_clocks -include_generated_clocks sysClk160Unbuf90_cc]

set_false_path -from [get_cells {U_HsioCosmicCore1/discop_reg[*]}]
set_false_path -from [get_cells {U_HsioCosmicCore1/triggermask_reg[*]}]
set_false_path -from [get_cells {U_triggerlogic/triggerword_reg[*]}]
#set_false_path -to [get_cells {U_triggerlogic/triggerword_reg[*]}]
set_false_path -from [get_cells {U_triggerlogic/cyclic_reg}]


