#ifndef MASKSTAGEFACTORY_HH
#define MASKSTAGEFACTORY_HH

#include "rcecalib/config/Masks.hh"
#include "rcecalib/config/MaskStaging.hh"

class MaskStageFactory{
public:
  MaskStageFactory(){}
  ~MaskStageFactory(){}
  template <typename T>
  MaskStaging<T>* createMaskStaging(T* mod, const char* maskStagingType, const char* maskStagingMode);
  template <typename T>
  Masks createIOmasks(const char* maskStagingMode);
};

#endif
