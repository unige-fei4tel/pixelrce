#ifndef HITBUS__MODULE_HH
#define HITBUS__MODULE_HH

#include "rcecalib/config/AbsModule.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <map>
#include <string>

class AbsFormatter;
namespace FEI4{
  class FECommands;
};

namespace Hitbus{

  struct RegParams{
    int reg;
    int width;
    std::string name;
  };

  class Module: public AbsModule{
  public:
    Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt);
    virtual ~Module();
    enum {N_FRONTENDS=0, N_ROWS=0, N_COLS=0};
    void configureHW();
    void resetFE();
    void setupMaskStageHW(int stage){};
    void enableDataTakingHW(){};
    void setRegister(const char* name, unsigned short val);
    int setupParameterHW(const char* name, int val); //HW setup
    int configureScan(boost::property_tree::ptree* scanOptions);
    ModuleInfo getModuleInfo();
    const float dacToElectrons(int fe, int dac){return 0;}
    void printRegisters(std::ostream &os);
    unsigned writeRegisterHW(int i, unsigned short val);
    void writeRegisterRawHW(int i, unsigned short val);
    void writeHW();
    virtual void destroy();
  private:
    FEI4::FECommands* m_commands;
    unsigned short *m_regw;// write registers
    static void addRegister(const char* name, int reg, unsigned width);
    static void addParameter(const char* name, const char* field);
    static int findRegister(const char* name);
    static void initialize();
    static std::vector<RegParams> m_registers;
    static std::map<std::string, int> m_parameters;
    static bool m_initialized;

};

};
#endif
