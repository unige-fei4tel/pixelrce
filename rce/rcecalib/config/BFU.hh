#ifndef BFU_H
#define BFU_H


/* ---------------------------------------------------------------------- *//*!

   \file  BFU.h
   \brief Bit Field Unpack Routines
   \author JJRussell - russell@slac.stanford.edu

    Inline functions to (left justified) unpack bit fields.
                                                                          */
/* ---------------------------------------------------------------------- */


/* ---------------------------------------------------------------------- *\
 * 
 * HISTORY
 * -------
 *
 * DATE     WHO WHAT
 * -------- --- ---------------------------------------------------------
 * 08.25.09 jjr Pilfered from the LAT code, then made two modifications
 *               1. Changed BFU from a structure to unsigned long long
 *                  Turns out that any structure that is returned from
 *                  a routine is returned via stack memory. This defeats
 *                  the whole purpose of the bit-extraction routines
 *                  which is to avoid memory like the plague.
 *               2. Then, after looking at the generated code, made
 *                  macro versions of BFU__wordL, BFU__wordR, BFU__boolean.
 *                  While changing BFU as indicated in (1) did result
 *                  in it being returned in registers, the generated
 *                  code showed a lot of register thrashing. Once the
 *                  move to a macro was made, the interface was no longer
 *                  constrained to being able to modify just two 32-bit
 *                  values (typically the cached bit field and the
 *                  extracted value). To take advantage of this, the
 *                  buffer address is now incremented when a new cache
 *                  value is extracted.
 *
 *              This made a factor of 2 difference in the MCC decoder.
 *
 *              Future improvements
 *                1. Started on a better implementation of the left
 *                   justified extraction routines. Currently they
 *                   just call the right justified extraction routines
 *                   and appropriately shift the result. While this 
 *                   produces the correct answer, it has a performance
 *                   cost.
 *                2. Add probe routine for the booleans. Since the
 *                   cache value always contains at least one valid
 *                   bit, it can be probed without advancing the stream.
 *                   Doing arbitrary field probes is also possible,
 *                   but is much more difficult.
 *                3. The most ambitious thing would be to build custom
 *                   routines for the different platforms. In particular,
 *                   a PPC-specific routine could be substantially faster,
 *                   taking advantage of the rlinm instructions.
 *
 * 09.30.06 jjr Corrected _bfu_construct (it didn't used the newly 
 *              calculated bit offset to extract the buffered word into
 *              cur). Added _bfu_constructW for constructing a BFU from
 *              an aligned source buffer.
 * 09.30.06 jjr Added History log
\* ---------------------------------------------------------------------- */
#ifdef __cplusplus
extern "C" {
#endif




#define _bfu_wordR(_val, _cur, _wrds, _position, _width)                      \
{                                                                             \
   /* Compute where in the current word this field is to be extracted from */ \
   int    rposition = (_position) & 0x1f;                                     \
   int        shift = 32 - (rposition + (_width));                            \
   unsigned int msk = ((((unsigned int)0x1) << ((_width)-1)) << 1) - 1;       \
                                                                              \
   if      (shift  > 0) _val = ((_cur) >> shift) & msk;                       \
   else if (shift == 0)                                                       \
   {                                                                          \
      _val = (_cur)  & msk;                                                   \
      _cur = *(++_wrds);                                                      \
   }                                                                          \
   else                                                                       \
   {                                                                          \
       /*                                                                     \
        | Crosses over to the next word                                       \
        | In this case -shift is the number of bits in the next word          \
       */                                                                     \
       shift     = -shift;                                                    \
      _val       = ((_cur) & (msk >> shift)) << shift;                        \
      _cur       = *(++_wrds);                                                \
      _val      |= (_cur) >> (32 - shift);                                    \
   }                                                                          \
}


#if 1

/*
 | This is the original version of the extrating a left justified field.
 | It calls the right justified field extrractor, then shifts the value
 | into place. This is algorithmically correct, but costs in performance
*/
#define _bfu_wordL(_val, _cur, _wrds, _position, _width)       \
{                                                              \
   _bfu_wordR (_val, _cur, _wrds, _position, _width);          \
   _val     <<= 32 - (_width);                                  \
}

#else


/* 
 | This is an experimental version of extracting a left justified field
 | Basically it works, but because its interface differs slightly from
 | the original version above (the difference is that the unused bits
 | are not cleared, but actually carry the next 'n' bits of the data
 | stream, which, in certain applications can be useful as a look-ahead
 | operation) cannot be a drop-in replacement. 
 |
 | It should be more efficient than the original version.
*/
#define _bfu_wordL(_val, _cur, _wrds, _position, _width)                     \
{                                                                             \
   /* Compute where in the current word this field is to be extracted from */ \
   int    rposition = (_position) & 0x1f;                                     \
   int          end = (rposition + (_width));                                 \
   int         left = 32 - end;                                               \
                                                                              \
   _val = _cur << _position;                                                  \
   if (left <= 0)                                                             \
   {                                                                          \
       _cur = *(++_wrds);                                                     \
       if (left < 0)                                                          \
       {                                                                      \
           unsigned int s = 32 - (_position);                                 \
           unsigned int x = _cur >> s;                                        \
            _val &= (signed)0x80000000 >> (s-1);                              \
           _val  |= x;                                                        \
       }                                                                      \
   }                                                                          \
}
   
#endif

/* ---------------------------------------------------------------------- */
#define _bfu_testBit(_cur, _position)  (_cur << ((_position) & 0x1f))
/* ---------------------------------------------------------------------- */

#ifdef __cplusplus
}
#endif

#endif
