#ifndef MULTITRIGGER_HH
#define MULTITRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/AbsTrigger.hh"
#include "rcecalib/HW/BitStream.hh"
#include "namespace_aliases.hh"




  class MultiTrigger: public AbsTrigger{
  public:
    MultiTrigger();
    ~MultiTrigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
  private:
    void createByteStream(std::vector<char>& stream, unsigned int command, unsigned int len, unsigned int offset);
    BitStream m_triggerStream;
    int m_calL1ADelay;
    int m_repetitions;
    int m_interval;
    int m_nIntervals;
    std::vector<int> m_intervals;
    int m_injectForTrigger;
    RcePic::Tds* m_tds;
    RcePic::Pool *m_pool;
  };


#endif
