//Dummy interface for debugging

#include "ConfigIF.hh"
#include <boost/property_tree/ptree.hpp>
#include <stdio.h>
#include "rcecalib/config/ModuleFactory.hh"
#include "rcecalib/config/AbsModule.hh"
#include "rcecalib/config/AbsModuleGroup.hh"
#include "rcecalib/config/AbsTrigger.hh"
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/util/VerifyErrors.hh"
#include "ers/ers.h"
#include "ipc/object.h"
#include "rcecalib/HW/SerialIF.hh"

ConfigIF::ConfigIF(ModuleFactory *mf):m_moduleFactory(mf),m_trigger(0), m_linkInMask(0), m_linkOutMask(0){
  m_modulegroups=mf->getModuleGroups();
}

int ConfigIF::setupParameter(const char* name, int val, bool enable){
  int retval=0;
  assert(m_trigger!=0);
  bool istrgpar=m_trigger->lookupParameter(name);
  if(istrgpar==true){
    retval=m_trigger->setupParameter(name,val);
  }else{
    disableAllChannels();
    for (unsigned int i=0;i<m_modulegroups.size();i++){
      if(m_modulegroups[i]->getNmodules()!=0)retval+=m_modulegroups[i]->setupParameterHW(name,val, enable);
    }
    if(enable){
      enableDataTakingHW();
      setChannelMask();
    }
    //  m_trigger->resetCounters();
  }
  return retval;
}

int ConfigIF::setupMaskStage(int stage){
  disableAllChannels();
  for (unsigned int i=0;i<m_modulegroups.size();i++){
    if(m_modulegroups[i]->getNmodules()!=0)m_modulegroups[i]->setupMaskStageHW(stage);
  }
  enableDataTakingHW();
  setChannelMask();
  //  m_trigger->resetCounters();
  return 0;
}

void ConfigIF::configureModulesHW(bool enable){
  //std::cout<<"Configure Modules HW"<<std::endl;
  disableAllChannels();
  sendHWcommand(9); // reset Firmware
  for (unsigned int i=0;i<m_modulegroups.size();i++){
    if(m_modulegroups[i]->getNmodules()!=0)m_modulegroups[i]->configureModulesHW();
  }
  if(enable==true){
    enableDataTakingHW();
    setChannelMask();
    if(m_trigger){
      //std::cout<<"Resetting counters"<<std::endl;
      m_trigger->resetCounters();
    }
  }
}

int ConfigIF::verifyModuleConfigHW(int id){
  int retval=0;
  int mod=-1;
  for (size_t i=0; i<m_modules.size();i++){
    if(m_modules[i]->getModuleInfo().getId()==id){
      mod=i;
      break;
    }
  }
  if(mod<0)return ModuleVerify::NO_MODULE;
  SerialIF::setChannelInMask(m_modules[mod]->getInLink());
  SerialIF::setChannelOutMask(m_modules[mod]->getOutLink());
  retval=m_modules[mod]->verifyModuleConfigHW();
  disableAllChannels();
  return retval;
}

unsigned ConfigIF::getNmodules(){
  return m_modules.size();
}

int ConfigIF::sendTrigger(){
  m_trigger->sendTrigger();
  return 0;
}

int ConfigIF::setChannelMask(){
  SerialIF::setChannelInMask(m_linkInMask);
  SerialIF::setChannelOutMask(m_linkOutMask);
  //std::cout<<"Link In  Mask "<<std::hex<<m_linkInMask<<std::dec<<std::endl;
  //std::cout<<"Link Out Mask "<<std::hex<<m_linkOutMask<<std::dec<<std::endl;
  return 0;
}

void ConfigIF::disableAllChannels(){
  SerialIF::setChannelInMask(0);
  SerialIF::setChannelOutMask(0);
}
unsigned ConfigIF::sendHWcommand(unsigned char opcode){
  return SerialIF::sendCommand(opcode);
}
unsigned ConfigIF::writeHWregister(unsigned addr, unsigned val){
  return SerialIF::writeRegister(addr,val);
}
unsigned ConfigIF::readHWregister(unsigned addr, unsigned &val){
  return SerialIF::readRegister(addr, val);
}
unsigned ConfigIF::writeHWblockData(std::vector<unsigned>& data){
  return SerialIF::writeBlockData(data);
}
unsigned ConfigIF::readHWblockData(std::vector<unsigned>& data, std::vector<unsigned>& retvec){
  return SerialIF::readBlockData(data, retvec);
}
unsigned ConfigIF::readHWbuffers(std::vector<unsigned char>& retvec){
  return SerialIF::readBuffers(retvec);
}
void ConfigIF::resetCountersHW(){
  m_trigger->resetCounters();
}

int ConfigIF::enableTrigger(){
  return m_trigger->enableTrigger(1);
}
int ConfigIF::disableTrigger(){
  return m_trigger->enableTrigger(0);
}
void ConfigIF::resetFE(){
  disableAllChannels();
  for (unsigned int i=0;i<m_modulegroups.size();i++){
    if(m_modulegroups[i]->getNmodules()!=0)m_modulegroups[i]->resetFE();
  }
  if(m_trigger)disableTrigger();
}
void ConfigIF::resetErrorCountersHW(){
  for (unsigned int i=0;i<m_modulegroups.size();i++){
    if(m_modulegroups[i]->getNmodules()!=0)m_modulegroups[i]->resetErrorCountersHW();
  }
}
void ConfigIF::enableDataTakingHW(){
  //std::cout<<"ConfigIF enabled data taking"<<std::endl;
  for (unsigned int i=0;i<m_modulegroups.size();i++){
    if(m_modulegroups[i]->getNmodules()!=0)m_modulegroups[i]->enableDataTakingHW();
  }
}

int ConfigIF::configureScan(boost::property_tree::ptree *scanOptions){
  int retval=0;
  if(m_trigger!=0)retval=m_trigger->configureScan(scanOptions);
  for (unsigned int i=0;i<m_modulegroups.size();i++){
    if(m_modulegroups[i]->getNmodules()!=0)retval+=m_modulegroups[i]->configureScan(scanOptions);
  }
  return retval;
}

int ConfigIF::setupModule(const char* name, const char* type, unsigned id, unsigned inLink, unsigned outLink, const char* formatter){
  int retval=0;
  char err_msg[128];
  try{
    //name, id, link are unique. Check that they don't exist already.
    for (unsigned int i=0;i<m_modules.size();i++){
      if(m_modules[i]->getId()==id ){
	std::cout<<"setupModule error: Duplicate id "<<id<<std::endl;
	sprintf(err_msg,"id = %d",id);
	rcecalib::Param_exists issue(ERS_HERE,err_msg);
	throw issue;
      }
      if(m_modules[i]->getName()==name ){
	std::cout<<"setupModule error: Duplicate name "<<name<<std::endl;
	sprintf(err_msg,"name = %s",name);
	rcecalib::Param_exists issue(ERS_HERE,err_msg);
	throw issue;
      }
      if( m_modules[i]->getOutLink()==outLink){
	std::cout<<"setupModule error: Duplicate outlink "<<outLink<<std::endl;
	sprintf(err_msg,"link = %d",outLink);
	rcecalib::Param_exists issue(ERS_HERE,err_msg);
	throw issue;
      }
    }      
    //std::cout<<name<<" "<<type<<" "<<id<<" "<<link<<std::endl;
    AbsModule *newmod=m_moduleFactory->createModule(name, type,id, inLink, outLink, formatter);
    //std::cout<<"setupModule after create "<<newmod<<std::endl;
    //success
    if(std::string(type)!="IPC_Hitbus_multiThread"){
      m_modules.push_back(newmod);
      m_linkInMask|=1<<newmod->getInLink();
    }
      m_linkOutMask|=1<<newmod->getOutLink();
  }
  catch(rcecalib::Param_exists ex){
    ers::error(ex);
    retval=1;
  }
  catch(rcecalib::Unknown_Module_Type ex){
    ers::error(ex);
    retval=1;
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  catch(std::bad_alloc &ex){
    std::cerr<<"Bad alloc "<<ex.what()<<std::endl;
  }
  catch(...){
    std::cerr<<"Unknown error "<<std::endl;
  }
  return retval;
}

int ConfigIF::setupTriggerIF(const char* type){
  delete m_trigger;
  m_trigger=m_moduleFactory->createTriggerIF(type, this);
  return 0;
}

void ConfigIF::deleteModules(){
  for (unsigned i=0; i<m_modulegroups.size();i++){
    //cannot call delete directly because IPC modules need to call _destroy() instead.
    m_modulegroups[i]->deleteModules();
  }
  m_modules.clear();
  m_linkInMask=0;
  m_linkOutMask=0;
}

int ConfigIF::nTrigger(){
  return m_trigger!=0? m_trigger->getNTriggers(): -1;
}

ModuleInfo ConfigIF::getModuleInfo(int mod){
  ERS_ASSERT(mod<(int)m_modules.size());
  return m_modules[mod]->getModuleInfo();
}
const float ConfigIF::dacToElectrons(int mod, int fe, int dac){
  if(mod>=(int)m_modules.size())return -1;
  return m_modules[mod]->dacToElectrons(fe,dac);
}
