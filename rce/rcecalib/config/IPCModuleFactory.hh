#ifndef IPCMODULEFACTORY_HH
#define IPCMODULEFACTORY_HH

#include "ipc/partition.h"

#include "rcecalib/config/ModuleFactory.hh"
#include "rcecalib/config/FEI3/ModuleGroup.hh"
#include "rcecalib/config/FEI4/ModuleGroup.hh"
#include "rcecalib/config/hitbus/ModuleGroup.hh"
#include "rcecalib/config/afp-hptdc/ModuleGroup.hh"

class ConfigIF;
class AbsFormatter;

class IPCModuleFactory: public ModuleFactory {
public:
  IPCModuleFactory(IPCPartition &p);
  AbsModule* createModule(const char* name, const char* type, unsigned id, unsigned inpos, unsigned outPos, const char* formatter);
  AbsFormatter* createFormatter(const char* formatter, int id);
  AbsTrigger* createTriggerIF(const char* type, ConfigIF* cif);
private:
  IPCPartition &m_partition;
  FEI3::ModuleGroup m_modgroupi3;
  FEI4::ModuleGroup m_modgroupi4a;
  FEI4::ModuleGroup m_modgroupi4b;
  Hitbus::ModuleGroup m_modgrouphitbus;
  afphptdc::ModuleGroup m_modgroupafphptdc;
  
};


#endif
