
#include "rcecalib/config/FEI4/TemperatureTrigger.hh"
#include "rcecalib/config/FEI4/FECommands.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/HW/SerialIF.hh"
#include "rcecalib/profiler/Profiler.hh"

namespace FEI4{

  TemperatureTrigger::TemperatureTrigger():AbsTrigger(){
  }
  TemperatureTrigger::~TemperatureTrigger(){
  }
  int TemperatureTrigger::configureScan(boost::property_tree::ptree* scanOptions){
    int retval=0;
    m_i=0; //reset the number of triggers
    try{
      //     int calL1ADelay = scanOptions->get<int>("trigOpt.CalL1ADelay");
      setupTriggerStream();
    }
     catch(boost::property_tree::ptree_bad_path ex){
       rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
       ers::error(issue);
       retval=1;
     }
    return retval;
  }
  int TemperatureTrigger::setupParameter(const char* name, int val){
    return 0;
  }

  void TemperatureTrigger::setupTriggerStream(){
    m_triggerStream.clear();
    FECommands commands;
    commands.setBroadcast(true);
    
    commands.switchMode(&m_triggerStream, FECommands::CONF);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    commands.writeGlobalRegister(&m_triggerStream, 31, 0xf000);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    commands.writeGlobalRegister(&m_triggerStream, 27, 0x8400);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    m_triggerStream.push_back(0);
    commands.globalPulse(&m_triggerStream, 63);
    for(int i=0;i<128;i++)m_triggerStream.push_back(0);
    commands.readGlobalRegister(&m_triggerStream, 40);
  }
  
  int TemperatureTrigger::sendTrigger(){
    SerialIF::send(&m_triggerStream,SerialIF::DONT_CLEAR|SerialIF::WAITFORDATA);
    m_i++;
    return 0;
  }

  int TemperatureTrigger::enableTrigger(bool on){
    return SerialIF::enableTrigger(on);
    return 0;
  }
  int TemperatureTrigger::resetCounters(){
    return 0;
  }
};
