#include "rcecalib/config/FEI4/FEI4HitorFormatter.hh"
#include "rcecalib/config/FEI4/FEI4ARecord.hh"
#include "rcecalib/config/FormattedRecord.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "ers/ers.h"
#include <iostream>
#include <stdio.h>

FEI4HitorFormatter::FEI4HitorFormatter(int id):
    AbsFormatter(id){
}
FEI4HitorFormatter::~FEI4HitorFormatter(){
  
}
void FEI4HitorFormatter::configure(boost::property_tree::ptree* config){
}

int FEI4HitorFormatter::decode (const unsigned int *buffer, int buflen, unsigned* parsedData, int &parsedsize, int &nL1A){
  if(buflen==0)return FEI4::FEI4ARecord::Empty;
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  FEI4::FEI4ARecord rec;
  bool header=false;
  nL1A=0;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if(rec.isDataHeader()){
      header=true;
      //printf("Formatter word %x\n",rec.getValue());
      //printf("FEI %d Header.\n",getId());
    }else if (rec.isEmptyRecord()){
      header=false;
    }else if(rec.isData()){
      //printf("FEI %d Data.\n",getId());
      if(header==false)return FEI4::FEI4ARecord::NoHeader;
      // std::cout<<std::hex<<rec.getHeader()<<std::endl;
    }else if(rec.isServiceRecord()){
      //printf("Service record FEI %d. Error code: %d. Count: %d \n", getId(), rec.getErrorCode(), rec.getErrorCount());
      unsigned code=rec.getErrorCode();
      unsigned count=rec.getErrorCount();
      if(code==10){ //HitOr
	//printf("Service record FEI %d. Error code: %d. Count: %d \n", getId(), rec.getErrorCode(), rec.getErrorCount());
	nL1A=1; //pretend all triggers came in
	if(count>0)parsedData[parsedsize++]=1; //pixel is stuck
	return FEI4::FEI4ARecord::OK;
      }
      header=false;
    }else if(rec.isValueRecord()){ //val rec without addr rec
      //printf("FEI %d: Value record: %04x.\n",getId(), rec.getValue());
      header=false;
    }else if(rec.isAddressRecord()){ // address record
      //printf("FEI %d: Address record\n",getId());
      header=false;
    }else{
      std::cout<<"Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      return FEI4::FEI4ARecord::BadRecord;
    }
    bytepointer+=3;
  }
  return FEI4::FEI4ARecord::OK;
}

