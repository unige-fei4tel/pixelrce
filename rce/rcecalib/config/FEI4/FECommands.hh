#ifndef FECOMMANDS_FEI4_HH
#define FECOMMANDS_FEI4_HH

#include "rcecalib/HW/BitStream.hh"

namespace FEI4{
  
  class FECommands{
  public:
    enum MODE {CONF=0x7, RUN=0x38};
    enum {PULSE_WIDTH=1};
    FECommands():m_chipAddr(0), m_broadcasting(0){}
    void writeGlobalRegister(BitStream* bs, int reg, unsigned short value);
    void readGlobalRegister(BitStream* bs, int reg);
    void globalReset(BitStream* bs);
    void globalPulse(BitStream* bs, unsigned width);
    void switchMode(BitStream *bs, MODE m);
    void L1A(BitStream *bs);
    void sendECR(BitStream *bs);
    void sendBCR(BitStream *bs);
    void feWriteCommand(BitStream* bs);
    void setAddr(unsigned addr){m_chipAddr=addr;}
    void setBroadcast(bool bc){
      if(bc)m_broadcasting=0x8;
      else m_broadcasting=0;
    }
    bool broadcasting(){return (m_broadcasting!=0);}
  private:
    unsigned m_chipAddr;
    unsigned m_broadcasting;
  };

};
#endif

