#include "rcecalib/config/FEI4/FEI4AModule.hh"
#include "rcecalib/config/FEI4/FEI4AGlobalRegister.hh"
#include "rcecalib/HW/BitStream.hh"
#include "rcecalib/config/FEI4/FECommands.hh"
#include "rcecalib/config/MaskStageFactory.hh"
#include "rcecalib/HW/SerialIF.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/util/VerifyErrors.hh"
#include <iostream>
#include <fstream>
#include "ers/ers.h"
#include <stdio.h>

namespace FEI4{


  FEI4AModule::FEI4AModule(const char* name, unsigned id, unsigned inlink, unsigned outlink, AbsFormatter* fmt):
    Module(name, id, inlink, outlink, fmt){
    //    std::cout<<"Module"<<std::endl;
    m_commands=new FECommands;
    m_global=new FEI4AGlobalRegister(m_commands);
    m_pixel=new PixelRegister(m_commands);
    m_pixel_readback=new PixelRegister(m_commands);
  }
  FEI4AModule::~FEI4AModule(){
    delete m_global;
    delete m_pixel;
    delete m_pixel_readback;
    delete m_commands;
    //m_timer.Print("Module");
  }
  void FEI4AModule::setupGlobalPulseHW(int pulsereg){
    m_global->setField("Efuse_sense",(pulsereg&GlobalRegister::Efuse_sense)!=0, GlobalRegister::SW);
    m_global->setField("Stop_Clk",(pulsereg&GlobalRegister::Stop_Clk)!=0, GlobalRegister::SW);
    m_global->setField("ReadErrorReq",(pulsereg&GlobalRegister::ReadErrorReq)!=0, GlobalRegister::SW);
    m_global->setField("ReadSkipped",(pulsereg&GlobalRegister::ReadSkipped)!=0, GlobalRegister::SW);
    m_global->setField("CalEn",(pulsereg&GlobalRegister::CalEn)!=0, GlobalRegister::SW);
    m_global->setField("SR_clr",(pulsereg&GlobalRegister::SR_clr)!=0, GlobalRegister::SW);
    m_global->setField("Latch_en",(pulsereg&GlobalRegister::Latch_en)!=0, GlobalRegister::SW);
    m_global->setField("SR_Clock",(pulsereg&GlobalRegister::SR_Clock)!=0, GlobalRegister::HW); //do one HW write for all fields
  }
    
  //  void Frontend::pixelUsrToRaw(BitStream *bs, unsigned bit){
   // }

  void FEI4AModule::resetHW(){
    BitStream *bs=new BitStream;
    BitStreamUtils::prependZeros(bs);
    m_commands->globalReset(bs);
    SerialIF::send(bs);
    delete bs;
  }


  int FEI4AModule::verifyModuleConfigHW(){
    int retval=0;
    SerialIF::setChannelInMask(1<<m_inLink);
    SerialIF::setChannelOutMask(1<<m_outLink);
    std::vector <unsigned> readback;
    if(m_formatter){
      // global register
      m_formatter->setReadbackPointer(&readback);
      std::cout<<"***FE "<<m_id<<"***"<<std::endl;
      retval=m_global->verifyConfigHW(readback);
      /*
      // pixel registers
      m_global->setField("Colpr_Mode", 0, GlobalRegister::SW); // only write to 1 column pair
      BitStream *bs=new BitStream;
      bs->push_back(0);
      m_commands->globalPulse(bs, FECommands::PULSE_WIDTH);
      int ngoodcol=13;
      const int goodcol[]={0,1,2,30,31,32,33,34,35,36,37,38,39};
      for (int dcoli=0;dcoli<ngoodcol;dcoli++){
	int dcol=goodcol[dcoli];
	for (int bit=PixelRegister::N_PIXEL_REGISTER_BITS-1;bit>=0;bit--){
	  m_global->setField("Colpr_Addr", dcol, GlobalRegister::HW);
	  if(bit!=PixelRegister::N_PIXEL_REGISTER_BITS-1){//shift register is last latch bit
	    setupS0S1HitldHW(1, 1, 0);
	    setupGlobalPulseHW(GlobalRegister::SR_Clock); //SR clock
	    //set up strobe bit
	    setupPixelStrobesHW(1<<bit);
	    SerialIF::send(bs, SerialIF::DONT_CLEAR); //global pulse
	  }
	  setupS0S1HitldHW(0, 0, 0);
	  retval|=m_pixel->verifyDoubleColumnHW(bit, dcol, readback); //check double column
	}
      }
      setupGlobalPulseHW(0); //clear
      setupPixelStrobesHW(0); //clear
      delete bs;
      */
      m_formatter->setReadbackPointer(0);
    }else{
      retval=ModuleVerify::NO_FORMATTER;
    }
    return retval;
  }


  void FEI4AModule::destroy(){
    delete this;
  }


};

