#include "rcecalib/config/FEI4/ModuleCrosstalkMaskStaging.hh"
#include "rcecalib/config/FEI4/Module.hh"

namespace FEI4{
  
  ModuleCrosstalkMaskStaging::ModuleCrosstalkMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    m_nStages=3;
  }
  
  void ModuleCrosstalkMaskStaging::setupMaskStageHW(int maskStage){
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    //    std::cout<<"Setting up mask stage "<<maskStage<<std::endl;
    if(global->getField("GateHitOr")==1){
      //std::cout<<"Disabling injection "<<std::endl;
      global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
      global->setField("Colpr_Addr", 40, GlobalRegister::SW); // switch off injection into module
      return;
    }
    //std::cout<<"Injecting "<<std::endl;
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    unsigned dcolStage=maskStage/m_nStages;
    unsigned stage=maskStage%m_nStages;
    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){
      if(m_masks.iMask&(1<<i)){
	pixel->setupMaskStageCol(dcolStage*2+1, i, stage, m_nStages);
	if(dcolStage==39)pixel->setupMaskStageCol(80, i, stage, m_nStages);
	m_module->writeDoubleColumnHW(i, i, dcolStage, dcolStage); 
	//m_pixel->setBitCol(maskStage*2+1, i, 1); //enable bit in double column
	if(dcolStage!=0){
	  if(stage==0)pixel->setBitCol(dcolStage*2-1, i, 0); //disable previous column
	  //m_pixel->setBitCol(maskStage*2, i, 1); //enable bit in double column
	  pixel->setupMaskStageCol(dcolStage*2, i, stage, m_nStages);
	  //if(maskStage==39)m_pixel->setBitCol(80, i, 1); //column 80 is special
	  m_module->writeDoubleColumnHW(i, i, dcolStage-1, dcolStage-1); 
	  if(dcolStage>1){
	    if(stage==0)pixel->setBitCol(dcolStage*2-2, i, 0);
	    m_module->writeDoubleColumnHW(i, i, dcolStage-2, dcolStage-2); 
	  }
	  global->setField("Colpr_Addr", dcolStage, GlobalRegister::HW); 
	}
      }
    }
  }
}
