#include "rcecalib/config/FEI4/CrosstalkFastMaskStaging.hh"
#include "rcecalib/config/FEI4/Module.hh"

namespace FEI4{
  
  CrosstalkFastMaskStaging::CrosstalkFastMaskStaging(FEI4::Module* mod, Masks masks, std::string type)
    :MaskStaging<FEI4::Module>(mod, masks, type){
    m_nStages=8;
  }
  
  void CrosstalkFastMaskStaging::setupMaskStageHW(int maskStage){
    if(m_initialized==false || maskStage==0){
      clearBitsHW();
      m_initialized=true;
    }
    PixelRegister* pixel=m_module->pixel();
    GlobalRegister* global=m_module->global();
    unsigned rowstage=maskStage%m_nStages;
    unsigned dcolstage=maskStage/m_nStages;

    global->setField("Colpr_Mode", 0, GlobalRegister::SW); 
    
    // std::cout<<"stage = "<<maskStage<<" : dcol = "<<dcolstage<<" ; rowgroup = "<<rowstage<<std::endl;

    for(int i=0;i<PixelRegister::N_PIXEL_REGISTER_BITS;i++){

      if(m_masks.xtalk_on&(1<<i)){
	//this is for the pixel we are reading out, this should set the "enable" bit

	pixel->setupMaskStageCol(2*dcolstage+1, i, rowstage, m_nStages);  //this sets bits starting in rowstage+1
	if(dcolstage==39)pixel->setupMaskStageCol(80, i, rowstage, m_nStages);
	m_module->writeDoubleColumnHW(i, i, dcolstage, dcolstage); 

        if(dcolstage!=0){
          if(rowstage==0)pixel->setBitCol(dcolstage*2-1, i, 0); //disable previous column
          pixel->setupMaskStageCol(dcolstage*2, i, rowstage, m_nStages);
	  m_module->writeDoubleColumnHW(i, i, dcolstage-1, dcolstage-1);
          if(dcolstage>1){
            if(rowstage==0)pixel->setBitCol(dcolstage*2-2, i, 0); //disable previous-previous column
            m_module->writeDoubleColumnHW(i, i, dcolstage-2, dcolstage-2);
          }
        }
	

      }
      if(m_masks.xtalk_off&(1<<i)){
	//this is for the pixel we are strobing

	if(rowstage>0){
	  pixel->setupMaskStageCol(2*dcolstage+1, i, rowstage-1, m_nStages);  //this sets bits starting in rowstage
	}
	else{
	  pixel->setupMaskStageCol(2*dcolstage+1, i, rowstage+m_nStages-1, m_nStages);  //this sets bits starting in rowstage+m_nStages
	}
	//we have to set the bits for rowstage+2 by hand, since setupMaskStageCol clears the bits each time it's called
	for(int krow=rowstage+2;krow<=PixelRegister::N_ROWS;krow+=m_nStages){
	  pixel->setBit(i, krow, 2*dcolstage+1, 1);
	}
	
        if(dcolstage==39){

	  if(rowstage>0){
	    pixel->setupMaskStageCol(80, i, rowstage-1, m_nStages);  //this sets bits starting in rowstage
	  }
	  else{
	    pixel->setupMaskStageCol(80, i, rowstage+m_nStages-1, m_nStages);  //this sets bits starting in rowstage+m_nStages
	  }
	  //we have to set the bits for rowstage+2 by hand, since setupMaskStageCol clears the bits each time it's called
	  for(int krow=rowstage+2;krow<=PixelRegister::N_ROWS;krow+=m_nStages){
	    pixel->setBit(i, krow, 80, 1);
	  }

	}
        m_module->writeDoubleColumnHW(i, i, dcolstage, dcolstage);
	
        if(dcolstage!=0){
          if(rowstage==0)pixel->setBitCol(dcolstage*2-1, i, 0); //disable previous column

	  if(rowstage>0){
	    pixel->setupMaskStageCol(2*dcolstage, i, rowstage-1, m_nStages);  //this sets bits starting in rowstage
	  }
	  else{
	    pixel->setupMaskStageCol(2*dcolstage, i, rowstage+m_nStages-1, m_nStages);  //this sets bits starting in rowstage+m_nStages
	  }
	  
	  //we have to set the bits for rowstage+2 by hand, since setupMaskStageCol clears the bits each time it's called
	  for(int krow=rowstage+2;krow<=PixelRegister::N_ROWS;krow+=m_nStages){
	    pixel->setBit(i, krow, 2*dcolstage, 1);
	  }
          m_module->writeDoubleColumnHW(i, i, dcolstage-1, dcolstage-1);

          if(dcolstage>1){
            if(rowstage==0)pixel->setBitCol(dcolstage*2-2, i, 0); //disable previous-previous column
            m_module->writeDoubleColumnHW(i, i, dcolstage-2, dcolstage-2);
	  }
	  
	}
      
      } //end if(m_masks.xtalk_off&(1<<i))

    } //end for-loop over pixel register bits

    global->setField("Colpr_Addr", dcolstage, GlobalRegister::HW); 
            
  }
}
