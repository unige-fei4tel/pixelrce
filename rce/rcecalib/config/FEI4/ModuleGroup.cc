
#include <boost/property_tree/ptree.hpp>
#include <stdio.h>
#include "rcecalib/config/FEI4/Module.hh"
#include "rcecalib/config/FEI4/ModuleGroup.hh"
#include "rcecalib/util/exceptions.hh"
#include "ers/ers.h"
#include "rcecalib/HW/SerialIF.hh"

namespace FEI4{

void ModuleGroup::addModule(Module* module){
  m_modules.push_back(module);
  m_channelInMask|=1<<module->getInLink();
  m_channelOutMask|=1<<module->getOutLink();
}

void ModuleGroup::deleteModules(){
  for (unsigned i=0; i<m_modules.size();i++){
    //cannot call delete directly because IPC modules need to call _destroy() instead.
    m_modules[i]->destroy();
  }
  m_modules.clear();
  m_channelInMask=0;
  m_channelOutMask=0;
}

int ModuleGroup::setupParameterHW(const char* name, int val, bool bcOK){
  int retval=0;
  int oneByOne=m_oneByOne;
  if(bcOK==false)m_oneByOne=true; //always go one by one in certain cases
  bool pixelReg=(PixelRegister::lookupParameter(name)!=PixelRegister::not_found);
  if(m_oneByOne==false){
    m_modules[0]->setBroadcast(true);
    setChannelInMask();
  }
  switchToConfigModeHW();
  if(pixelReg==true){
    enableSrHW(false); //disable SR in case it's a pixel reg parameter
  }else if(m_oneByOne==false){
    m_modules[0]->setBroadcast(false);
  } 
  if(pixelReg==false || m_oneByOne==true){ // one by one
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      retval+=m_modules[i]->setupParameterHW(name,val);
    }
  }else{ //it's a pixel param and oneByOne is false => broadcast
    m_modules[0]->setupParameterHW(name,val);
  } 
  if(pixelReg==true){
    enableSrHW(true); //enable
    if(m_oneByOne==false)m_modules[0]->setBroadcast(false);
  }
  disableAllInChannels();
  if(bcOK==false)m_oneByOne=oneByOne; //restore value
  return retval;
}

int ModuleGroup::setupMaskStageHW(int stage){
  if(m_oneByOne==false){
    m_modules[0]->setBroadcast(true);
    setChannelInMask();
  }
  switchToConfigModeHW();
  if(m_oneByOne==true){
    enableSrHW(false); //disable
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      m_modules[i]->setupMaskStageHW(stage);
      m_modules[i]->enableSrHW(false);
    }
    enableSrHW(true);//enable
  }else{ //broadcast
      m_modules[0]->setupMaskStageHW(stage);
      m_modules[0]->setBroadcast(false);
  }
  if(stage==0)SerialIF::sendCommand(0x10); //phase calibration
  disableAllInChannels();
  return 0;
}

void ModuleGroup::configureModulesHW(){
  //std::cout<<"Configure Modules HW"<<std::endl;
  if(m_oneByOne==false){
    m_modules[0]->setBroadcast(true);
    setChannelInMask();
  }
  switchToConfigModeHW();
  enableSrHW(false); //disable
  if(m_oneByOne==false)m_modules[0]->setBroadcast(false);
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    m_modules[i]->configureHW();
    m_modules[i]->enableSrHW(false); //disable SR
  }
  SerialIF::sendCommand(0x10); //phase calibration
  disableAllInChannels();
}

int ModuleGroup::verifyModuleConfigHW(){
  int retval=0;
  bool oneByOne=m_oneByOne;
  m_oneByOne=true; // always go one by one
  switchToConfigModeHW();
  enableSrHW(false); //disable
  for (unsigned int i=0;i<m_modules.size();i++){
    std::cout<<"Frontend with outlink "<<m_modules[i]->getOutLink()<<std::endl;
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    retval|=m_modules[i]->verifyModuleConfigHW();
    m_modules[i]->enableSrHW(false); //disable SR
  }
  m_oneByOne=oneByOne; //restore value
  return retval;
}

void ModuleGroup::resetFE(){
  if(m_oneByOne==false){
    m_modules[0]->setBroadcast(true);
    setChannelInMask();
  }
  switchToConfigModeHW();
  if(m_oneByOne==true){
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      m_modules[i]->resetFE();
    }
  }else{ //broadcast
    m_modules[0]->resetFE();
    m_modules[0]->setBroadcast(false);
  }
  disableAllInChannels();
}

void ModuleGroup::resetErrorCountersHW(){
  if(m_oneByOne==false){
    m_modules[0]->setBroadcast(true);
    setChannelInMask();
  }
  switchToConfigModeHW();
  if(m_oneByOne==true){
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      m_modules[i]->resetErrorCountersHW();
    }
  }else{ //broadcast
    m_modules[0]->resetErrorCountersHW();
    m_modules[0]->setBroadcast(false);
  }
  //disableAllInChannels();
}

void ModuleGroup::enableDataTakingHW(){
  //std::cout<<"ConfigIF enabled data taking"<<std::endl;
  if(m_oneByOne==false){
    m_modules[0]->setBroadcast(true);
    setChannelInMask();
  }
  if(m_oneByOne==true){
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      m_modules[i]->enableDataTakingHW();
    }
  }else{ //broadcast
    m_modules[0]->enableDataTakingHW();
    m_modules[0]->setBroadcast(false);
  }
  disableAllInChannels();
}

void ModuleGroup::switchToConfigModeHW(){
  //std::cout<<"ConfigIF switch config mode"<<std::endl;
  if(m_oneByOne == true){
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      m_modules[i]->switchToConfigModeHW();
    }
  }else{ //broadcast
    m_modules[0]->switchToConfigModeHW();
  } 
}

void ModuleGroup::enableSrHW(bool on){ //not used in broadcast mode
  //std::cout<<"ConfigIF switch config mode"<<std::endl;
  if(m_oneByOne == true){
    for (unsigned int i=0;i<m_modules.size();i++){
      SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
      m_modules[i]->enableSrHW(on);
    }
  }else{ //broadcast
    m_modules[0]->enableSrHW(on);
  } 
}

int ModuleGroup::configureScan(boost::property_tree::ptree *scanOptions){
  int retval=0;
  try{
    m_oneByOne=scanOptions->get("trigOpt.optionsMask.ONE_BY_ONE", 0);
    for (unsigned int i=0;i<m_modules.size();i++){
      retval+=m_modules[i]->configureScan(scanOptions);
    }
  } catch(boost::property_tree::ptree_bad_path ex){
    retval=1;
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
  }
  return retval;
}

}
