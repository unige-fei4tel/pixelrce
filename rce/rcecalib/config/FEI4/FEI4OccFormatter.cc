#include "rcecalib/config/FEI4/FEI4OccFormatter.hh"
#include "rcecalib/config/FEI4/FEI4ARecord.hh"
#include "rcecalib/config/FormattedRecord.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "ers/ers.h"
#include <iostream>
#include <stdio.h>
#include "rcecalib/profiler/Profiler.hh"

FEI4OccFormatter::FEI4OccFormatter(int id):
    AbsFormatter(id){
  char name[128], title[128];
  sprintf(name, "Mod_%d_FEI4_Errors", id);
  sprintf(title, "Module %d FEI4 Errors", id);
  m_errhist=new RceHisto1d<int, int>(name, title, 32, -.5, 31.5);
}
FEI4OccFormatter::~FEI4OccFormatter(){
  delete m_errhist;
  //m_timer.Print("FEI4OccupancyFormatter");
  
}
void FEI4OccFormatter::configure(boost::property_tree::ptree* config){
  m_errhist->clear();
  //m_timer.Reset();
}

int FEI4OccFormatter::decode (const unsigned int *buffer, int buflen, unsigned* parsedData, int &parsedsize, int &nL1A){
  if(buflen==0)return FEI4::FEI4ARecord::Empty;
  //m_timer.Start();
  unsigned char* bytepointer=(unsigned char*)buffer;
  unsigned char* last=bytepointer+buflen*sizeof(unsigned)-3;
  FEI4::FEI4ARecord rec;
  bool header=false;
  while(bytepointer<=last){
    rec.setRecord(bytepointer); 
    if (rec.isEmptyRecord()){
      header=false;
    }else if(rec.isData()){
      if(header==false)return FEI4::FEI4ARecord::NoHeader;
      //there are potentially 2 hits in one data record
      if(((rec.getTotBottom())>>1)!=0x7){ //above threshold
	FormattedRecord fr(FormattedRecord::DATA);
	fr.setCol(rec.getColumn()-1);
	fr.setRow(rec.getRow()-1);
	parsedData[parsedsize++]=fr.getWord();

      }
	if(((rec.getTotTop())>>1)!=0x7){ //above threshold
	FormattedRecord fr(FormattedRecord::DATA);
	fr.setCol(rec.getColumn()-1);
	fr.setRow(rec.getRow());
	parsedData[parsedsize++]=fr.getWord();
      }
    } else if(rec.isDataHeader()){
      header=true;
      nL1A++;
    }else if(rec.isServiceRecord()){
      printf("Service record FE %d. Error code: %d. Count: %d \n",
	     getId(), rec.getErrorCode(), rec.getErrorCount());
      if( rec.getErrorCode()<32)m_errhist->fill(rec.getErrorCode(), rec.getErrorCount());
      header=false;
    }else if(rec.isValueRecord()){ //val rec without addr rec
      //      printf("Value record: %04x.\n",rec.getValue());
      header=false;
    }else if(rec.isAddressRecord()){ // address record
      std::cout<<"Address record for ";
      if(rec.isGlobal())std::cout<<" global register ";
      else std::cout<<" shift register ";
      std::cout<<rec.getAddress()<<std::endl;
      std::cout<<"This should not happen."<<std::endl;
      header=false;
    }else{
      std::cout<<"Unexpected record type: "<<std::hex<<rec.getUnsigned()<<std::dec<<std::endl;
      return FEI4::FEI4ARecord::BadRecord;
    }
    bytepointer+=3;
  }
  //  m_timer.Stop();
  return FEI4::FEI4ARecord::OK;
}

