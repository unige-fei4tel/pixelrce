#include "rcecalib/config/EventFromDspTrigger.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/config/TriggerReceiverIF.hh"
#include "rcecalib/legacy/dsp_types.h"
#include "rcecalib/legacy/sysParams_specific.h"
#include "rcecalib/legacy/histogram.h"
#include "rcecalib/legacy/histogramCtrl.h"

#include <iostream>




EventFromDspTrigger::EventFromDspTrigger():AbsTrigger(){
  m_inpf.open("/reg/lab1/home/wittgen/data/___slave_dump___",std::ios::binary);
  unsigned int file_header[3];
  if(m_inpf.is_open()) {
    m_inpf.read((char*)&file_header,sizeof(file_header));
    unsigned int nFrames=file_header[1];
    unsigned int nWords=file_header[2];
    std::cout << nFrames << " " << nWords <<std::endl;
    HistogramOptions histogramOptions;
    m_inpf.read((char*)&histogramOptions,sizeof(HistogramOptions));
    std::cout<<"nTriggers=           "<<histogramOptions.nTriggers<<std::endl;
    std::cout<<"nFrames=             "<<nFrames<<std::endl;
    std::cout<<"nTriggers=           "<<histogramOptions.nTriggers<<std::endl;
    std::cout<<"nBins=               "<<histogramOptions.nBins[0]<<std::endl;
    std::cout<<"nModules=            "<<histogramOptions.nModules<<std::endl;
    histogramOptions.nMaskStages--;
    std::cout<<"nMaskStages=         "<<histogramOptions.nMaskStages<<std::endl;
    
    std::cout<<"maskStageBegin=      "<<histogramOptions.maskStageBegin<<std::endl;
    std::cout<<"type=                "<<histogramOptions.type<<std::endl;
    std::cout<<"maskStageTotalSteps= "<<histogramOptions.maskStageTotalSteps<<std::endl;

    

    m_nBins=histogramOptions.nBins[0];
    m_nStages=histogramOptions.nMaskStages;
  }  
  m_inpf.read((char *) &m_frame_header,sizeof(m_frame_header));
  m_bin=m_frame_header[2];
  m_stage=m_frame_header[3];  
  m_frameSize=m_frame_header[1];    
  m_inpf.read((char*) &m_frame,m_frameSize*sizeof(unsigned int));
  m_inpf.read((char*) &m_frame_trailer,sizeof(m_frame_trailer));
  
}
    
  
int EventFromDspTrigger::sendTrigger(){
  //  static int count=0;
  static int eof=0;
  static int bof=0;
  static int hits=0;
  static int skipframe=0;
  static int newEvent=0;

  
  
  newEvent=0;hits=0;
  do  {    
    if(!skipframe) {
      if(m_frame[255]==0xe0f00000) skipframe=1;
      for(int i=0;i<m_frameSize;i++) {
	if((m_frame[i]&0xffff0000)==0xb0f00000) {bof++;}
	if((m_frame[i]>>28)==0x8) { 
	  m_event[hits]=m_frame[i];
	  hits++;
	}
	if((m_frame[i]&0xffff0000)==0xe0f00000) {eof++;newEvent=1;break;}    
      }
    } else skipframe=0;
      
    m_inpf.read((char *) &m_frame_header,sizeof(m_frame_header));
    m_bin=m_frame_header[2];
    m_stage=m_frame_header[3];
    m_frameSize=m_frame_header[1];
    m_inpf.read((char*) &m_frame,m_frameSize*sizeof(unsigned int));
    m_inpf.read((char*) &m_frame_trailer,sizeof(m_frame_trailer));
    
  }  while(!m_inpf.eof() && m_bin<m_nBins && m_stage<m_nStages && !newEvent);
  //  std::cout << "send Trigger "<< count++ << std::endl;
  TriggerReceiverIF::receive(&m_event[0],hits);
  m_i++;
  // if(m_i%100==0) std::cout << "trigger " << m_i<<std::endl;

  return 0;
}



