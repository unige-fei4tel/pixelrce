
#include "rcecalib/config/EventFromFileTrigger.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/config/TriggerReceiverIF.hh"
#include <iostream>
#include <fstream>



EventFromFileTrigger::EventFromFileTrigger():AbsTrigger(){
    //std::cout<<"eventfromfiletrigger"<<std::endl;
    std::ifstream flog("/nfs/event1.txt");
    while(!flog.eof()){
      unsigned word;
      flog>>std::hex>>word;
      m_event.push_back((word<<16)|(word>>16));
    }
    m_event.push_back(0);
  }
  
  
  int EventFromFileTrigger::sendTrigger(){
    TriggerReceiverIF::receive(&m_event[0],m_event.size());
    m_i++;
    return 0;
  }


