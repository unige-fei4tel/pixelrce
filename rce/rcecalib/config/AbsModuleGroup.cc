#include "rcecalib/config/AbsModuleGroup.hh"
#include "rcecalib/HW/SerialIF.hh"

void AbsModuleGroup::setChannelInMask(){
  SerialIF::setChannelInMask(m_channelInMask);
}
void AbsModuleGroup::setChannelOutMask(){
  SerialIF::setChannelOutMask(m_channelOutMask);
}
void AbsModuleGroup::disableAllOutChannels(){
  SerialIF::setChannelOutMask(0);
}
void AbsModuleGroup::disableAllInChannels(){
  SerialIF::setChannelInMask(0);
}

unsigned AbsModuleGroup::getChannelInMask(){
  return m_channelInMask;
}
unsigned AbsModuleGroup::getChannelOutMask(){
  return m_channelOutMask;
}
