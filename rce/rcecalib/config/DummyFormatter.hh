#ifndef DUMMYFORMATTER_HH
#define DUMMYFORMATTER_HH

#include "rcecalib/config/AbsFormatter.hh"

class DummyFormatter:public AbsFormatter{
public:
  DummyFormatter(int id);
  virtual ~DummyFormatter();
  int decode(const unsigned* data, int size, unsigned* parsedData, int &parsedsize, int &nL1A);
  void configure(boost::property_tree::ptree* scanOptions);
};
#endif
