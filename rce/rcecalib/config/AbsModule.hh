#ifndef ABSMODULE_HH
#define ABSMODULE_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/ModuleInfo.hh"
#include "rcecalib/config/AbsFormatter.hh"


class AbsModule{
public:
  AbsModule(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt)
    :m_name(name), m_id(id), m_inLink(inLink), m_outLink(outLink), m_formatter(fmt){}
  virtual ~AbsModule(){delete m_formatter;}
  virtual void configureHW()=0;
  virtual void setupMaskStageHW(int stage)=0;
  virtual int setupParameterHW(const char* name, int val)=0; //HW setup
  virtual int configureScan(boost::property_tree::ptree* scanOptions)=0;
  virtual void resetFE()=0;
  virtual void enableDataTakingHW()=0;
  virtual int verifyModuleConfigHW(){return 0;}
  unsigned getId(){return m_id;}
  AbsFormatter* getFormatter(){return m_formatter;}
  virtual ModuleInfo getModuleInfo()=0;
  virtual const float dacToElectrons(int fe, int dac)=0;
  unsigned getInLink(){return m_inLink;}
  unsigned getOutLink(){return m_outLink;}
  void setInLink(unsigned pos){m_inLink=pos;}
  void setOutLink(unsigned pos){m_outLink=pos;}
  std::string getName(){return m_name;}
  virtual void destroy()=0;
    
protected:
  std::string m_name;
  unsigned m_id;
  unsigned m_inLink;
  unsigned m_outLink;
  AbsFormatter *m_formatter;
};

#endif
