#ifndef AFPHPTDC__MODULE_HH
#define AFPHPTDC__MODULE_HH

#include "rcecalib/config/AbsModule.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include <map>
#include <string>

class AbsFormatter;
namespace FEI4{
  class FECommands;
};

namespace afphptdc{

  struct FieldParams{
    int reg;
    int width;
    int pos;
  };

  class Module: public AbsModule{
  public:
    Module(const char* name, unsigned id, unsigned inLink, unsigned outLink, AbsFormatter* fmt);
    virtual ~Module();
    enum {N_FPGA_REGS=40, N_TDC_REGS=21};
    void configureHW();
    void resetFE();
    void setupMaskStageHW(int stage){};
    void enableDataTakingHW();
    void switchToConfigModeHW();
    int setupParameterHW(const char* name, int val); //HW setup
    int configureScan(boost::property_tree::ptree* scanOptions);
    ModuleInfo getModuleInfo();
    const float dacToElectrons(int fe, int dac){return 0;}
    void printRegisters(std::ostream &os);
    unsigned writeRegisterHW(int i, unsigned short val);
    void writeRegisterRawHW(int i, unsigned short val);
    void writeHW();
    void writeTdcBitstreamHW(int i);
    void setFieldFun(unsigned *reg, int bitpos, int width, unsigned val);
    void setFpgaField(const char* name, unsigned val);
    void setTdcField(const char* name, int index, unsigned val);
    virtual void destroy();
  private:
    FEI4::FECommands* m_commands;
    static void addFpgaField(const char* name, int reg, int width, int pos);
    static void addTdcField(const char* name, int width);
    //static void addParameter(const char* name, const char* field);
    static void initialize();
    unsigned m_fpga_registers[N_FPGA_REGS];
    unsigned m_tdc_registers[3][N_TDC_REGS];
    static std::map<std::string, FieldParams> m_fpga_fields;
    static std::map<int, int> m_used_regs;
    static std::map<std::string, FieldParams> m_tdc_fields;
    //static std::map<std::string, int> m_parameters;
    static bool m_initialized;
    static int m_regpos;
    static int m_bitpos;

};

};
#endif
