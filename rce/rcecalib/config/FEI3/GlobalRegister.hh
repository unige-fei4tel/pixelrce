#ifndef GLOBALREGISTER_FEI3_HH
#define GLOBALREGISTER_FEI3_HH

#include <map>
#include <string>
#include "rcecalib/HW/BitStream.hh"

namespace FEI3{
  
  struct FieldParams{
    unsigned position;
    unsigned width;
    bool parity;
  };

  class GlobalRegister{
  public:
    GlobalRegister(unsigned fe, unsigned chip);
    ~GlobalRegister();
    void setField(const char* name, unsigned val);
    unsigned getField(const char* name);
    void writeHW();
    void enableAll();
    static std::string lookupParameter(const char* name);
    enum {FE_I3_N_GLOBAL_BITS=231};
    void dump();
  private:
    unsigned char* m_data;
    BitStream m_bitStream;
		  
    // static functions
    static void addField(const char* name, unsigned width, bool parity=1);
    static void addParameter(const char* name, const char* field);
    static void initialize();
    static std::map<std::string, FieldParams> m_fields;
    static std::map<std::string, std::string> m_parameters;
    static std::string m_cachedName, m_cachedField; 
    static unsigned m_pos;
    static bool m_initialized;
  };

};

#endif
