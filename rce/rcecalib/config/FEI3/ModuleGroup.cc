
#include <boost/property_tree/ptree.hpp>
#include <stdio.h>
#include "rcecalib/config/FEI3/Module.hh"
#include "rcecalib/config/FEI3/ModuleGroup.hh"
#include "rcecalib/util/exceptions.hh"
#include "ers/ers.h"
#include "rcecalib/HW/SerialIF.hh"

namespace FEI3{

void ModuleGroup::addModule(Module* module){
  m_modules.push_back(module);
  m_channelInMask|=1<<module->getInLink();
  m_channelOutMask|=1<<module->getOutLink();
}

void ModuleGroup::deleteModules(){
  for (unsigned i=0; i<m_modules.size();i++){
    //cannot call delete directly because IPC modules need to call _destroy() instead.
    m_modules[i]->destroy();
  }
  m_modules.clear();
  m_channelInMask=0;
  m_channelOutMask=0;
}

int ModuleGroup::setupParameterHW(const char* name, int val, bool bcOK){
  int retval=0;
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    retval+=m_modules[i]->setupParameterHW(name,val);
  }
  disableAllInChannels();
  return retval;
}

int ModuleGroup::setupMaskStageHW(int stage){
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    m_modules[i]->setupMaskStageHW(stage);
  }
  disableAllInChannels();
  return 0;
}

void ModuleGroup::configureModulesHW(){
  //std::cout<<"Configure Modules HW"<<std::endl;
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    m_modules[i]->configureHW();
  }
  disableAllInChannels();
}

void ModuleGroup::resetFE(){
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    m_modules[i]->resetFE();
  }
  disableAllInChannels();
}

void ModuleGroup::enableDataTakingHW(){
  //std::cout<<"ConfigIF enabled data taking"<<std::endl;
  for (unsigned int i=0;i<m_modules.size();i++){
    SerialIF::setChannelInMask(1<<m_modules[i]->getInLink());
    m_modules[i]->enableDataTakingHW();
  }
  disableAllInChannels();
}

int ModuleGroup::verifyModuleConfigHW(){
  return 0; // does not exist for FEI3
}
void ModuleGroup::resetErrorCountersHW(){
  //does not exist for FEI3
}

int ModuleGroup::configureScan(boost::property_tree::ptree *scanOptions){
  int retval=0;
  for (unsigned int i=0;i<m_modules.size();i++){
    retval+=m_modules[i]->configureScan(scanOptions);
  }
  return retval;
}

}
