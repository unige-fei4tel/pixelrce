
#include "rcecalib/config/FEI3/GlobalRegister.hh"
#include "rcecalib/config/FEI3/FECommands.hh"
#include "rcecalib/HW/SerialIF.hh"
#include <iostream>
#include "ers/ers.h"
#include <stdio.h>

namespace FEI3{

  GlobalRegister::GlobalRegister(unsigned fe, unsigned chip){
    // initialize static field dictionay
    if(!m_initialized)initialize();

    //now build the write global register stream which will be cached.
    // enable the front end 
    BitStreamUtils::prependZeros(&m_bitStream);
    FECommands::mccWriteRegister(&m_bitStream, Mcc::MCC_FEEN, (1 << fe));

    // cnt register with dcnt = # of global register bits = 231 
    unsigned dcnt = FE_I3_N_GLOBAL_BITS;
    unsigned cnt = (dcnt << 3) | 4;
    FECommands::mccWriteRegister(&m_bitStream, Mcc::MCC_CNT, cnt);
	
    // clock in the global register.
    FECommands::feWriteCommand(&m_bitStream, chip, FECommands::FE_CMD_CLOCK_GLOBAL);
    // remember the index at which the global register itself will go
    unsigned globalReg=m_bitStream.size();
    // initialize the register with zeroes.
    unsigned gsize=(m_pos+sizeof(unsigned)-1)/sizeof(unsigned); 
    for(unsigned int i=0;i<gsize;i++)m_bitStream.push_back(0);
    
    // cnt register with dcnt = 0 
    cnt = 4;
    FECommands::mccWriteRegister(&m_bitStream, Mcc::MCC_CNT, cnt);
	
    // issue a write global 
    FECommands::feWriteCommand(&m_bitStream, chip, FECommands::FE_CMD_WRITE_GLOBAL);
    m_bitStream.push_back(0);
    FECommands::feWriteCommand(&m_bitStream, chip, FECommands::FE_CMD_NULL);
    m_bitStream.push_back(0);

    // byte swap because the global register is big endian
    //BitStreamUtils::byteSwap(&m_bitStream);
    //set the pointer to the location of the global register
    m_data=(unsigned char*)&m_bitStream[globalReg];
  }
  GlobalRegister::~GlobalRegister(){
  }
  void GlobalRegister::writeHW(){
    //SerialIF::send(&m_bitStream,SerialIF::DONT_CLEAR, SerialIF::BYTESWAP);
    SerialIF::send(&m_bitStream,SerialIF::DONT_CLEAR);
  }

  void GlobalRegister::setField(const char* name, unsigned val){
    char msg[128];
    sprintf(msg,"parameter %s is invalid",name);
    ERS_ASSERT_MSG(m_fields.find(name)!=m_fields.end(),msg);
    FieldParams params=m_fields[name];
    unsigned parity=0;
    if(params.parity==true)
      parity=getField("parity");
    unsigned char par;
    for (unsigned int i=0;i<params.width;i++){
      unsigned char bit=(val>>(params.width-1-i))&0x1;
      unsigned char bit5mhz= bit ? 0xff : 0;
      if(params.parity){
	par=bit5mhz ^ m_data[params.position+i];
	parity ^= par;
      }
      m_data[params.position+i]=bit5mhz;
    }
    if(params.parity){
      setField("parity",parity);
    }
  }

  unsigned GlobalRegister::getField(const char* name){
    ERS_ASSERT_MSG(m_fields.find(name)!=m_fields.end(),"parameter name is invalid");
    FieldParams params=m_fields[name];
    unsigned retval=0;
    for (unsigned int i=0;i<params.width;i++){
      retval<<=1;
      if(m_data[params.position+i]) retval|=1;
    }
    return retval;
  }
  void GlobalRegister::enableAll(){
  setField("enableCP0",1);
  setField("enableCP1",1);
  setField("enableCP2",1);
  setField("enableCP3",1);
  setField("enableCP4",1);
  setField("enableCP5",1);
  setField("enableCP6",1);
  setField("enableCP7",1);
  setField("enableCP8",1);
  setField("doMux",8);
}
  
     
  std::string GlobalRegister::lookupParameter(const char* name){
    // check if this is the last name used, return cached value
    if(name==m_cachedName)return m_cachedField;
    // Now check if we can translate the name to a field name
    if(m_parameters.find(name)!=m_parameters.end()){
      //cache result
      m_cachedName=name;
      m_cachedField=m_parameters[name]; 
      return m_cachedField;
    // maybe it's a proper field name?
    } else if (m_fields.find(name)!=m_fields.end()){
      m_cachedName=name;
      m_cachedField=name;
      return m_cachedField;
    }
    // not found.
    else return "";
  }
  void GlobalRegister::addParameter(const char* name, const char* field){
    m_parameters[name]=field;
  }
  void GlobalRegister::addField(const char* name,unsigned width, bool parity){
    FieldParams temp;
    temp.position=m_pos;
    temp.width=width;
    temp.parity=parity;
    m_fields[name]=temp;
    m_pos+=width;
  }

  std::map<std::string, FieldParams > GlobalRegister::m_fields;
  std::map<std::string, std::string> GlobalRegister::m_parameters;
  unsigned GlobalRegister::m_pos = 0;
  bool GlobalRegister::m_initialized = false;
  std::string GlobalRegister::m_cachedName;
  std::string GlobalRegister::m_cachedField;

  void GlobalRegister::initialize(){
    addField("enableIpMonitor",1);
    addField("enableBiasComp",1);
    addField("enableTune",1);
    addField("gTDac",5);
    addField("enableHitbus",1);
    addField("enableCP0",1);
    addField("modeTOTThresh",2);
    addField("threshTOTDouble",8);
    addField("threshTOTMinimum",8);
    addField("enableCP1",1);
    addField("testDacIL2Dac",1);
    addField("dacIL2",8);
    addField("dacIL",8);
    addField("testDacILDac",1);
    addField("enableCP2",1);
    addField("testDacForITH2Dac",1);
    addField("dacITH2",8);
    addField("dacITH1",8);
    addField("testDacForITH1Dac",1);
    addField("enableCP3",1);
    addField("enableDigitalInject",1);
    addField("CEUClockControl",2);
    addField("eocMux",2);
    addField("enableTestAnalogRef",1);
    addField("enableExternalInj",1);
    addField("enableCinjHigh",1);
    addField("enableCP4",1);
    addField("testDacForVCalDac",1);
    addField("dacVCAL",10);
    addField("dacITRIMIF",8);
    addField("testDacForITrimIfDac",1);
    addField("enableCP5",1);
    addField("testDacForIFDac",1);
    addField("dacIF",8);
    addField("dacITRIMTH",8);
    addField("testDacForITrimThDac",1);
    addField("enableCP6",1);
    addField("testDacForIPDac",1);
    addField("dacIP",8);
    addField("dacIP2",8);
    addField("testDacForIP2Dac",1);
    addField("enableCP7",1);
    addField("testDacForIDDac",1);
    addField("dacID",8);
    addField("dacIVDD2",8);
    addField("testDacForIVDD2Dac",1);
    addField("enableCP8",1);
    addField("enableBufferBoost",1);
    addField("enableLeakMeas",1);
    addField("enableVCalMeas",1);
    addField("enableTestPixelMux",2);
    addField("enableAnalogOut",1);
    addField("enableCapTest",1);
    addField("capMeasCircuitry",6);
    addField("dRegMeas",2);
    addField("enableDRegMeas",1);
    addField("dRegTrim",2);
    addField("enableLVDSRefMeas",1);
    addField("enableAReg",1);
    addField("aRegMeas",2);
    addField("enableARegMeas",1);
    addField("aRegTrim",2);
    addField("monLeakADCMonComp",1, 0); // does not contribute to parity
    addField("monLeakADCEnableComparator",1);
    addField("monLeakDACTest",1);
    addField("monLeakADCDAC",10);
    addField("monLeakADCRefTest",1);
    addField("hitBusScaler",8, 0); // does not contribute to parity
    addField("enableEOEParity",1);
    addField("selectDataPhase",1);
    addField("tsiTscEnable",1);
    addField("selectMonHit",4);
    addField("doMux",4);
    addField("enableHitParity",1);
    addField("enableSelfTrigger",1);
    addField("selfTriggerWidth",4);
    addField("selfTriggerDelay",4);
    addField("latency",8);
    addField("parity",1, 0);
    addField("rsvd",1, 0);/*to ensure word alignment*/
    
    // Translation from scan parameter to Global register field
    addParameter("LATENCY", "latency");
    addParameter("GDAC", "gTDac");
    addParameter("VCAL", "dacVCAL");
    addParameter("IF", "dacIF");
    m_initialized=true;
  }
  void GlobalRegister::dump(){
    std::cout<<"Global Register"<<std::endl;
    for(std::map<std::string, FieldParams>::iterator it=m_fields.begin(); it!=m_fields.end();it++){
      std::cout<<it->first<<" "<<getField(it->first.c_str())<<std::endl;
    }
  }
      
};
