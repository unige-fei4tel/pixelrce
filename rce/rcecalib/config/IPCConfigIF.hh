#ifndef IPCCONFIGIF_HH
#define IPCCONFIGIF_HH

#include "IPCConfigIFAdapter.hh"
#include "ipc/object.h"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/config/ModuleFactory.hh"
#include <omnithread.h>

class IPCPartition;

template <class TP = ipc::single_thread>
class IPCConfigIF: public IPCNamedObject<POA_ipc::IPCConfigIFAdapter,TP>, public ConfigIF {
public:
  IPCConfigIF(IPCPartition & p, const char * name, ModuleFactory* mf);
  ~IPCConfigIF();
  void IPCsendTrigger();
  //void IPCsetupParameter(const char* name, CORBA::Long val);
  CORBA::ULong IPCsetupParameter(const char* name, CORBA::Long val);
  void IPCsetupMaskStage(CORBA::Long stage);
  void IPCenableTrigger();
  void IPCdisableTrigger();
  void IPCresetFE();
  void IPCconfigureModulesHW();
  CORBA::Long IPCverifyModuleConfigHW(CORBA::Long id);
  CORBA::ULong IPCwriteHWregister(CORBA::ULong addr, CORBA::ULong val);
  CORBA::ULong IPCsendHWcommand(CORBA::Octet opcode);
  CORBA::ULong IPCwriteHWblockData(const ipc::blockdata& data);
  CORBA::ULong IPCreadHWblockData(const ipc::blockdata & data, ipc::blockdata_out retv);
  CORBA::ULong IPCreadHWbuffers(ipc::chardata_out retv);
  CORBA::ULong IPCreadHWregister(CORBA::ULong addr, CORBA::ULong &val);
  CORBA::Long IPCdeleteModules();
  CORBA::Long IPCnTrigger();
  CORBA::Long IPCsetupTriggerIF(const char* type);
  CORBA::Long IPCsetupModule(const char* name, const char* type, const ipc::ModSetup& par, const char* formatter);
  void shutdown();

};
    

#endif
