#ifndef FEI3__TRIGGER_HH
#define FEI3__TRIGGER_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/config/AbsTrigger.hh"
#include "rcecalib/HW/BitStream.hh"
#include "namespace_aliases.hh"


namespace FEI3{

  class Trigger: public AbsTrigger{
  public:
    Trigger();
    ~Trigger();
    int configureScan(boost::property_tree::ptree* scanOptions);
    int setupParameter(const char* name, int val);
    int sendTrigger();
    int enableTrigger(bool on);
    int resetCounters();
    void setupTriggerStream();
  private:
    BitStream m_triggerStream;
    int m_calL1ADelay;
    RcePic::Tds* m_tds;
    RcePic::Pool *m_pool;
  };

};

#endif
