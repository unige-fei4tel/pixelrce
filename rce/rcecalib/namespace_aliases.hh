#ifndef __NAMESPACE_ALIASES_H__
#define __NAMESPACE_ALIASES_H__
#ifdef RCE_V2
namespace service {
  namespace fci {
    class Exception;
  }
  namespace debug {

  }
  namespace dynalink {
    class RunnableModule;
    class Linker;
    class LinkingError;
  }
}

namespace oldPpi{
  namespace pic {
    class Pool;
    class Tds;
    class Params;
  }


  namespace net {
   class ipAddress;
  }
  namespace pgp {
    class Driver;
  }
}

/* define aliases to map new core 2.2 namespaces to core 1.4 */
namespace RcePic=oldPpi::pic;
namespace RcePgp=oldPpi::pgp;
namespace RceSvc=service::fci;
namespace RceNet=oldPpi::net;
namespace RceDebug=service::debug;
namespace RCE  {
  namespace ELF=service::dynalink;
  namespace Dynalink=service::dynalink;
}
#else
/* forward declaration for old core 1.4 */
namespace RcePic {
  class Pool;
  class Tds;
  class Params;
}
namespace RceNet {
    class ipAddress;
}
namespace RcePgp {
  class Driver;
}
namespace RceSvc {
    class Exception;
}
#endif
#endif
