#include "rcecalib/scanctrl/RceCallback.hh"
#include <iostream>

RceCallback* RceCallback::m_instance=0;
omni_mutex RceCallback::m_guard;

RceCallback* RceCallback::instance(){
  if( ! m_instance){
    omni_mutex_lock ml(m_guard);
    if( ! m_instance){
      m_instance=new RceCallback;
    }
  }
  return m_instance;
}
    
RceCallback::RceCallback(): m_configured(false){}

void RceCallback::configure(ipc::Callback_ptr cb, ipc::Priority pr){
  m_cb = ipc::Callback::_duplicate( cb );
  m_pr=pr;
  m_configured=true;
}

void RceCallback::sendMsg(ipc::Priority pr, const ipc::CallbackParams *msg){
  if(m_configured==true){
    if(pr>=m_pr){
      try {
	m_cb->notify( *msg );
      }
      catch(...) {
	std::cout << "callback : notification fails" << std::endl;
      }
    }
  }
}
void RceCallback::shutdown(){
  if(m_configured==true){
    try {
      m_cb->stopServer();
    }
    catch(...) {
      std::cout << "callback : notification fails" << std::endl;
    }
    m_configured=false;
  }
}
