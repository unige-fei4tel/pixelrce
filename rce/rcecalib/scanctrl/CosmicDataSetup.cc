#include "rcecalib/scanctrl/CosmicDataSetup.hh"
#include "rcecalib/scanctrl/ScanLoop.hh"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/scanctrl/ActionFactory.hh"
#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/scanctrl/EndOfLoopAction.hh"
#include "rcecalib/util/exceptions.hh"

int CosmicDataSetup::setupLoops( NestedLoop& loop, boost::property_tree::ptree *scanOptions, ActionFactory* af){
  int retval=0; 
  loop.clear();
  try{ //catch bad scan option parameters
    // Data Taking has a single loop with one entry
    ScanLoop *scanloop=new ScanLoop("data",1);
    LoopAction* conf=af->createLoopAction("CONFIGURE_MODULES");
    scanloop->addLoopAction(conf);
    LoopAction* linkmask=af->createLoopAction("SETUP_TRIGGER", scanOptions);
    scanloop->addLoopAction(linkmask);
    LoopAction* enabletdc=0;
    if(scanOptions->get<int>("trigOpt.triggerDataOn")){
      enabletdc=af->createLoopAction("COSMIC_ENABLE_TDC");
      scanloop->addLoopAction(enabletdc);
    }
    LoopAction* enable=af->createLoopAction("ENABLE_TRIGGER");
    scanloop->addLoopAction(enable);
    // End of loop actions
    EndOfLoopAction* disabletrigger=af->createEndOfLoopAction("DISABLE_TRIGGER","");
    scanloop->addEndOfLoopAction(disabletrigger);
    EndOfLoopAction* closefile=af->createEndOfLoopAction("CLOSE_FILE","");
    scanloop->addEndOfLoopAction(closefile);
    EndOfLoopAction* resetfe=af->createEndOfLoopAction("RESET_FE","");
    scanloop->addEndOfLoopAction(resetfe);
    EndOfLoopAction* disablechannels=af->createEndOfLoopAction("DISABLE_ALL_CHANNELS","");
    scanloop->addEndOfLoopAction(disablechannels);
    // loop is the nested loop object
    loop.addNewInnermostLoop(scanloop);
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    retval=1;
  }
  //  loop.print();
  return retval;
}
