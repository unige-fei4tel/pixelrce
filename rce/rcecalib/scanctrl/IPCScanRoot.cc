#ifndef IPCSCANROOT_CC
#define IPCSCANROOT_CC

#include "rcecalib/scanctrl/IPCScanRoot.hh"
#include "ers/ers.h"
#include "ipc/partition.h"
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/dataproc/AbsDataProc.hh"
#include "rcecalib/dataproc/AbsReceiver.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/scanctrl/Scan.hh"
#include "rcecalib/util/IPCHistoManager.hh"
#include "rcecalib/scanctrl/RceCallback.hh"

#include <string>
#include <iostream>


template <class TP>
IPCScanRoot<TP>::IPCScanRoot(IPCPartition & p, const char * name, ConfigIF* cif, Scan* scan):
  IPCNamedObject<POA_ipc::IPCScanRootAdapter, TP>( p, name ) , ScanRoot(cif,scan){
  try {
    IPCNamedObject<POA_ipc::IPCScanRootAdapter,TP>::publish();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
}

template <class TP>
IPCScanRoot<TP>::~IPCScanRoot(){
  try {
    IPCNamedObject<POA_ipc::IPCScanRootAdapter,TP>::withdraw();
  }
  catch( daq::ipc::InvalidPartition & ex ) {
    ers::warning( ex );
  }
  catch( daq::ipc::InvalidObjectName & ex ) {
    ers::error( ex );
  }
  catch( daq::ipc::ObjectNotFound & ex ) {
    ers::error( ex );
  }
}
  
template <class TP>
CORBA::Long IPCScanRoot<TP>::IPCconfigureScan(const ipc::ScanOptions &options){
  //PixScan::dump(options);
  //std::cout<<"Start IPCScanRoot "<<std::endl;
  //convert struct into property tree
  boost::property_tree::ptree *pt=new boost::property_tree::ptree;
  //Top level 
  pt->put("Name",options.name); // scan name. Can be used as a file name, for example.
  pt->put("scanType",options.scanType); // scan type. Determines what loop setup to use
  pt->put("Receiver",options.receiver); 
  pt->put("DataHandler",options.dataHandler);
  pt->put("DataProc",options.dataProc);
  pt->put("Timeout.Seconds",options.timeout.seconds);
  pt->put("Timeout.Nanoseconds",options.timeout.nanoseconds);
  pt->put("Timeout.AllowedTimeouts",options.timeout.allowedTimeouts);
  pt->put("runNumber", options.runNumber);
  pt->put("stagingMode",options.stagingMode);
  //maskStages is not used in NewDsp, only nMaskStages
  pt->put("maskStages", options.maskStages);
  pt->put("nMaskStages",options.nMaskStages);
  pt->put("firstStage",options.firstStage);
  pt->put("stepStage",options.stepStage);
  pt->put("nLoops",options.nLoops);
  //Loops
  for(int i=0;i<options.nLoops;i++){
    char loopname[128];
    sprintf (loopname,"scanLoop_%d.",i);
    pt->put(std::string(loopname)+"scanParameter", options.scanLoop[i].scanParameter);
    pt->put(std::string(loopname)+"nPoints",options.scanLoop[i].nPoints);
    pt->put(std::string(loopname)+"endofLoopAction.Action", options.scanLoop[i].endofLoopAction.Action);
    pt->put(std::string(loopname)+"endofLoopAction.fitFunction", options.scanLoop[i].endofLoopAction.fitFunction);
    pt->put(std::string(loopname)+"endofLoopAction.targetThreshold", options.scanLoop[i].endofLoopAction.targetThreshold);
    if(options.scanLoop[i].dataPoints.length()>0){
      char pointname[10];
      for(unsigned int j=0;j<options.scanLoop[i].dataPoints.length();j++){
	sprintf(pointname,"P_%d",j);
	pt->put(std::string(loopname)+"dataPoints."+pointname,options.scanLoop[i].dataPoints[j]);
      }
    }
  }
  //Trigger options
  pt->put("trigOpt.nEvents",options.trigOpt.nEvents);
  pt->put("trigOpt.triggerMask",options.trigOpt.triggerMask);
  pt->put("trigOpt.moduleTrgMask",options.trigOpt.moduleTrgMask);
  pt->put("trigOpt.triggerDataOn",options.trigOpt.triggerDataOn);
  pt->put("trigOpt.deadtime",options.trigOpt.deadtime);
  pt->put("trigOpt.nL1AperEvent",options.trigOpt.nL1AperEvent);
  pt->put("trigOpt.nTriggersPerGroup",options.trigOpt.nTriggersPerGroup);
  pt->put("trigOpt.nTriggers",options.trigOpt.nTriggers);
  pt->put("trigOpt.Lvl1_Latency",options.trigOpt.Lvl1_Latency);
  pt->put("trigOpt.Lvl1_Latency_Secondary",options.trigOpt.Lvl1_Latency_Secondary);
  pt->put("trigOpt.strobeDuration",options.trigOpt.strobeDuration);
  pt->put("trigOpt.strobeMCCDelay",options.trigOpt.strobeMCCDelay);
  pt->put("trigOpt.strobeMCCDelayRange",options.trigOpt.strobeMCCDelayRange);
  pt->put("trigOpt.CalL1ADelay",options.trigOpt.CalL1ADelay);
  pt->put("trigOpt.eventInterval",options.trigOpt.eventInterval);
  pt->put("trigOpt.injectForTrigger",options.trigOpt.injectForTrigger);
  pt->put("trigOpt.vcal_charge",options.trigOpt.vcal_charge);
  pt->put("trigOpt.threshold",options.trigOpt.threshold);
  pt->put("trigOpt.hitbusConfig",options.trigOpt.hitbusConfig);

  // each option gets an entry in the tree below optionsMask with value 1
  for (unsigned int i=0;i<options.trigOpt.optionsMask.length();i++){
    pt->put(std::string("trigOpt.optionsMask.")+(const char*)options.trigOpt.optionsMask[i],1);
  }
  pt->put("trigOpt.triggerMode", options.trigOpt.triggerMode);

  // front-end options
  pt->put("feOpt.phi",options.feOpt.phi);
  pt->put("feOpt.totThresholdMode",options.feOpt.totThresholdMode);
  pt->put("feOpt.totMinimum",options.feOpt.totMinimum);
  pt->put("feOpt.totTwalk",options.feOpt.totTwalk);
  pt->put("feOpt.totLeMode",options.feOpt.totLeMode);
  pt->put("feOpt.hitbus",options.feOpt.hitbus);

  pt->put("nHistoNames",options.histos.length()); 
  if(options.histos.length()>0){
    char pointname[10];
    for(unsigned int j=0;j<options.histos.length();j++){
      sprintf(pointname,"Name_%d",j);
      pt->put(std::string("HistoNames.")+pointname,options.histos[j]);
    }
  }
  //Trigger options
  //call the real initScan function
  //std::cout<<"Calling ScanRoot "<<std::endl;
  configureScan(pt);
  delete pt;
  return 0;
}

template <class TP>
ipc::StringVect* IPCScanRoot<TP>::IPCgetHistoNames(const char* reg){
  std::vector<std::string> strvec=IPCHistoManager::instance()->getHistoNames(reg);
  ipc::StringVect &ipcstringvect=*new ipc::StringVect;
  ipcstringvect.length(strvec.size());
  for(size_t i=0;i<strvec.size();i++){
    ipcstringvect[i]=CORBA::string_dup(strvec[i].c_str());
  }
  return &ipcstringvect;
}
template <class TP>
ipc::StringVect* IPCScanRoot<TP>::IPCgetPublishedHistoNames(){
  std::vector<std::string> strvec=IPCHistoManager::instance()->getPublishedHistoNames();
  ipc::StringVect &ipcstringvect=*new ipc::StringVect;
  ipcstringvect.length(strvec.size());
  for(size_t i=0;i<strvec.size();i++){
    ipcstringvect[i]=CORBA::string_dup(strvec[i].c_str());
  }
  return &ipcstringvect;
}

template <class TP>
CORBA::ULong IPCScanRoot<TP>::IPCnEvents(){
  if(m_dataProc)
    return m_dataProc->nEvents();
  else
    return 0;
}
template <class TP>
void IPCScanRoot<TP>::IPCresynch(){
  if(m_receiver)
    m_receiver->resynch();
}

template <class TP>
void IPCScanRoot<TP>::IPCconfigureCallback(ipc::Callback_ptr cb, ipc::Priority pr){
  RceCallback::instance()->configure(cb,pr);
}

template <class TP>
void IPCScanRoot<TP>::shutdown(){
  std::cout<<"Shutdown"<<std::endl;
}

#endif
