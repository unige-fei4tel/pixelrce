#ifndef DISABLEALLCHANNELSACTION_HH 
#define DISABLEALLCHANNELSACTION_HH 

#include "rcecalib/scanctrl/LoopAction.hh"
#include "rcecalib/config/ConfigIF.hh"

class DisableAllChannelsAction: public LoopAction{
public:
  DisableAllChannelsAction(std::string name, ConfigIF* cif):
    LoopAction(name),m_configIF(cif){}
  int execute(int i){
    m_configIF->disableAllChannels();
    return 0;
  }
private:
  ConfigIF* m_configIF;
};

#endif
