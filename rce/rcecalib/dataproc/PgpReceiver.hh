#ifndef PGPRECEIVER_HH
#define PGPRECEIVER_HH

#include "rcecalib/dataproc/AbsReceiver.hh"
#include "rcecalib/HW/Receiver.hh"
#include "rcecalib/profiler/Profiler.hh"



class PgpReceiver: public AbsReceiver, public PgpTrans::Receiver{
public:
  PgpReceiver(AbsDataHandler* handler);
  virtual ~PgpReceiver();
  void receive(PgpTrans::PgpData* pgpdata);
private:
  unsigned* m_buffer;
  unsigned m_counter;
  Profiler::Timer m_timer;
};
#endif
