#ifndef MODULECROSSTALKRAWDATAHANDLER_HH
#define MODULECROSSTALKRAWDATAHANDLER_HH

#include <omnithread.h>
#include <assert.h>

#include "rcecalib/dataproc/AbsDataHandler.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/profiler/Profiler.hh"

class AbsFormatter;
class DataCond;

class ModuleCrosstalkRawDataHandler: public AbsDataHandler{
public:
  ModuleCrosstalkRawDataHandler(AbsDataProc* dataproc, DataCond& datacond, ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  virtual ~ModuleCrosstalkRawDataHandler();
  inline void handle(unsigned link, unsigned* data, int size);
  void timeoutOccurred();
  inline void resetL1counters();
protected:
  int m_nL1AperEv;
  int* m_L1Acounters;
  int m_nModules;
  int *m_linkToIndex;
  int m_moduleTrgMask;
  int* m_outlinkMask;
  Profiler::Timer m_timer;
  int m_counter;
};
#endif
