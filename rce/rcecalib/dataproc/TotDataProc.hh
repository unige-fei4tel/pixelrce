#ifndef TOTDATAPROC_HH
#define TOTDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/dataproc/AbsDataProc.hh"
#include <vector>
#include "rcecalib/util/RceHisto2d.cc"

class TotDataProc: public AbsDataProc{
public:
  TotDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );

  virtual ~TotDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);
protected:

  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_tot;
  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_tot2;
  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_occ;
  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_tot_sigma;
  std::vector<std::vector<RceHisto2d<short, short>*> > m_histo_tot_mean;

  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
};

#endif
