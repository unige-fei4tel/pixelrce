#ifndef MULTITRIGOCCUPANCYDATAPROC_HH
#define MULTITRIGOCCUPANCYDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/dataproc/AbsDataProc.hh"
#include "rcecalib/dataproc/fit/AbsFit.hh"
#include <vector>
#include "rcecalib/util/RceHisto2d.cc"
#include "rcecalib/profiler/Profiler.hh"

class MultiTrigOccupancyDataProc: public AbsDataProc{
public:
  MultiTrigOccupancyDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~MultiTrigOccupancyDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:

  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_occ0;
  std::vector<std::vector<RceHisto2d<char, char>*> > m_histo_occ1;

  AbsFit *m_fit0;
  AbsFit *m_fit1;

  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;

  int m_l1id_prev;
  int m_bcid_prev;
  int m_nbcid;
  int m_nbcid_total;
  int m_l1id_which;

  Profiler::Timer m_timer;
};

#endif
