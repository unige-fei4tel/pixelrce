#include <stdio.h>
#include <boost/property_tree/ptree.hpp>
#include "rcecalib/dataproc/SelftriggerDataProc.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/config/FormattedRecord.hh"
#include "rcecalib/util/exceptions.hh"
#include "rcecalib/util/RceName.hh"

int SelftriggerDataProc::fit(std::string fitfun){
  if(m_file){
    std::cout<<"Closing file."<<std::endl;
    m_file->close();
  }
  return 0;
}

int SelftriggerDataProc::processData(unsigned link, unsigned *data, int size){
  if(m_counter++==0)usleep(1);
  if(m_link10DataOn && link==10){
    //std::cout<<"Found link=10 for Trigger data, moving on"<<std::endl;
    unsigned trgtime1=data[3];
    unsigned trgtime2=data[4];
    m_trgtime=trgtime1;
    m_trgtime=(m_trgtime<<32) | trgtime2;

    unsigned int link10_l1id = ((data[0]>>24)&0xf);

    if(!m_trgtime_initial_set){
      m_trgtime_initial_set=true;
      m_trgtime_initial=m_trgtime;
    }
    
    int trigtimeDiff = (m_trgtime&0xff) - (m_trgtime_initial&0xff);
    if(trigtimeDiff<0) trigtimeDiff+=256;

    m_trigtimeDiff_index_l1id[link10_l1id]= trigtimeDiff;
    m_trigtime_index_l1id[link10_l1id]= (m_trgtime);
    return 0;
  }

  //    std::cout<<"Process data"<<std::endl;
  for (int i=0;i<size;i++)
    {
      int module=m_linkToIndex[link];
      FormattedRecord current(data[i]);
      
      if (current.isHeader())
	{
	  m_l1id[module] = current.getL1id();
	  m_bcid[module] = current.getBxid();
	  //printf("bcid : %x \n", bcid);
	  //printf("module %d l1id : %x \n", module, m_l1id[module]);

	  if(m_link10DataOn && !m_bcid_initial_set[module]){
	    m_bcid_initial_set[module]=true;
	    m_bcid_initial[module]=m_bcid[module];
	  }
	  
	  if (m_l1id[module] != m_l1id_last[module])
	    {
	      if(m_l1id_last[module]==0xffffffff)
		//std::cout<<"Initial L1ID: m_l1id="<<m_l1id[module]<<" "<< (m_l1id[module]&0xf) <<" m_l1id_last="<<m_l1id_last[module]<<" "<<(m_l1id_last[module]&0xf)<<std::endl;

	      if(m_l1id_last[module]!=0xffffffff){
		///l1id skipping ////
		int l1idDiff = (m_l1id[module]&0xf) - (m_l1id_last[module]&0xf);
		if(l1idDiff<0) l1idDiff+=16;
		if(l1idDiff!=1){
		  //std::cout<<"L1ID Skip: m_l1id="<<m_l1id[module]<<" "<< (m_l1id[module]&0xf) <<" m_l1id_last="<<m_l1id_last[module]<<" "<<(m_l1id_last[module]&0xf)<<" l1idDiff="<<l1idDiff<<std::endl;
		  m_histo_l1idDiff[module]->increment(l1idDiff);
		}
		/////////////////////


		if(m_totev[module]>31)
		  m_totev[module]=31; //last bin is overflow
		if(m_nhit[module]>15)
		  m_nhit[module]=15; //last bin is overflow
		m_histo_totev[module]->increment(m_totev[module]);
		m_histo_nhit[module]->increment(m_nhit[module]);
		m_totev[module]=0;
		m_nhit[module]=0;

                // Cluster Magic
                m_cluster_proc[module].clusterize();
	      }
	      m_l1id_last[module] = m_l1id[module];
	      m_bcid_ref[module]  = m_bcid[module];
	      m_l1idchanged[module]=1;


	      //BCID skipping//////////
	      if(false&&m_link10DataOn)
		{
		  int module_BCID_diff = (m_bcid_ref[module]&0xff) - (m_bcid_initial[module]&0xff);
		  if(module_BCID_diff<0) module_BCID_diff+=256;
		  
		  m_bcidDiff_index_l1id_vs_module[ m_l1id[module]&0xf ][module] = module_BCID_diff;
		  m_bcidDiff_index_l1id_vs_module_isSet[ m_l1id[module]&0xf ][module] = true;
		  
		  //check if all datat for event has arrived
		  bool all_arrived = true;
		  for(unsigned int imod=0; imod<nmod; imod++){
		    all_arrived = all_arrived && m_bcidDiff_index_l1id_vs_module_isSet[ m_l1id[module]&0xf ][imod];
		  }
		  if(all_arrived){
		    for(unsigned int imod=0; imod<nmod; imod++){
		      int event_diff = (m_bcidDiff_index_l1id_vs_module[ m_l1id[module]&0xf ][imod] & 0xff) - (m_trigtimeDiff_index_l1id[ m_l1id[module]&0xf ] & 0xff);	       
		      if(event_diff < 0) event_diff += 256;
		      
		      //if diff found, fill histo, and reset initial diff for module
		      if(event_diff>0){
			//std::cout<<"event "<< n_evt_skip<<" L1ID="<<(m_l1id[module]&0xf)<<" EVENT DIFF="<<event_diff<<" Mod="<<imod<<" BCID diff="<< (m_bcidDiff_index_l1id_vs_module[ m_l1id[module]&0xf ][imod] & 0xff )<<" BCID="<<(m_bcid_ref[module]&0xff)<<" BCID init="<<(m_bcid_initial[module]& 0xff)<<" Trig time="<< (m_trigtime_index_l1id[ m_l1id[module]&0xf ] & 0xff)<<" Trig diff="<< (m_trigtimeDiff_index_l1id[ m_l1id[module]&0xf ] & 0xff )<<" Trig init="<<(m_trgtime_initial & 0xff)<<std::endl;
			
			m_histo_bcidDiff[imod]->increment(event_diff);
			
			m_bcid_initial[imod] = ( (m_bcid_initial[imod] + (event_diff & 0xff)) & 0xff );
			
		      }
		      
		      m_bcidDiff_index_l1id_vs_module_isSet[ m_l1id[module]&0xf ][imod] = false;
		    }
		  }
		}
	      //end BCID skipping/////////

	    }//end L1ID != last L1ID

	  if(m_bcid[module]>=m_bcid_ref[module])
	    m_bcid[module] = m_bcid[module]-m_bcid_ref[module];
	  else
	    m_bcid[module] = m_bcid[module] + (0xff - (0xff&m_bcid_ref[module]) ) ;  //BCID counter rollover 
	  
	  // printf("bcidafter : %x \n", bcid);
	  
	}//end header check
      else if (current.isData())
	{
	  unsigned int chip=current.getFE();
	  unsigned int tot=current.getToT();
	  unsigned int col=current.getCol();
	  unsigned int row=current.getRow();
	  if(m_file){
	    if(m_l1idchanged[module]==1){
	      m_l1idchanged[module]=0;
	      m_hiteventid[module]++;
	    }
	    unsigned word=((link&0xf)<<28)|((m_l1id[module]&0xf)<<24)|(tot<<16)|(row<<7)|col;
	    m_file->write((char*)&word, 4);
	  }
	  //printf("Hit col=%d row=%d tot=%d\n",col, row, tot);

	  m_histo_occ[module]->increment(chip*m_info[module].getNColumns()+col,row);
	  m_histo_tot2d[module]->fill(chip*m_info[module].getNColumns()+col,row, tot);
	  m_histo_tot[module]->increment(tot);
	  m_histo_timing[module]->increment(m_bcid[module]);
	  m_totev[module]+=tot;
	  m_nhit[module]++;
          
          // Clustering (exclude ToT 14 hits)
          m_cluster_proc[module].addHit(col, row, tot, m_bcid[module]);

	  //printf("bcidafter : %x \n", bcid);
	}//end data check
    }//end for loop
  return 0;
}

SelftriggerDataProc::SelftriggerDataProc(ConfigIF* cif, boost::property_tree::ptree* scanOptions)
  :AbsDataProc(cif), m_file(0), m_counter(0){

  nmod=m_configIF->getNmodules();
  m_l1id_last = new unsigned int[nmod];
  m_l1id = new unsigned int[nmod];
  m_bcid_ref  = new unsigned int[nmod];
  m_bcid = new unsigned int[nmod];
  m_totev=new int[nmod];
  m_nhit=new int[nmod];

 
  m_hiteventid   = new unsigned int[nmod];
  m_l1idchanged  = new unsigned int[nmod];
  
  ///BCID skipping
  m_link10DataOn = (bool) scanOptions->get<int>("trigOpt.triggerDataOn");
  
  if(m_link10DataOn)
    {
      m_bcid_initial_set = new bool[nmod];
      m_bcid_initial     = new unsigned int[nmod];
      
      m_trgtime_initial_set=false;
      m_trgtime_initial=(unsigned long long)(-1);
      
      
      m_bcidDiff_index_l1id_vs_module = new unsigned int*[16];
      m_bcidDiff_index_l1id_vs_module_isSet = new bool*[16];
      for (int i=0;i<16;i++){
	m_bcidDiff_index_l1id_vs_module[i] = new unsigned int[nmod];
	m_bcidDiff_index_l1id_vs_module_isSet[i] = new bool[nmod];
	
	for(unsigned int j=0; j<nmod; j++)
	  m_bcidDiff_index_l1id_vs_module_isSet[i][j] = false;
      }
      m_trigtimeDiff_index_l1id = new unsigned long long[16];
      m_trigtime_index_l1id = new unsigned long long[16];
    }
  ////
  

  m_cluster_proc = new ClusterProc[nmod];
  
  try{ 
    std::string name=scanOptions->get<std::string>("Name");
    if(name!="Scan"){
      if(name.find("RUNNUM")!=std::string::npos){ //replace RUNNUM macro with run number
	char runns[12];
	sprintf(runns, "%06d", scanOptions->get<int>("runNumber"));
	name.replace(name.find("RUNNUM"), 6, runns);
      }
      std::string path="/nfs/";
      char rcename[32];
      sprintf(rcename, "_RCE_%d.", RceName::getRceNumber());
      if(name.find(".")!=std::string::npos){
	name.replace(name.find("."), 1, rcename);
      }else{
	name+=rcename;
      }
      path+=name;
      std::cout<<"Opening file "<<path<<std::endl;
      m_file=new std::ofstream(path.c_str(), std::ios::binary);
    }
    for (unsigned int module=0;module<nmod;module++){
      m_l1id_last[module] = 0xffffffff;
      m_l1id[module] = 0;
      m_bcid_ref[module] = 0;
      m_bcid[module] = 0;
      m_totev[module] = 0;
      m_nhit[module] = 0;
  
      m_hiteventid[module]=0;
      m_l1idchanged[module]=0;
      
      if(m_link10DataOn){
	m_bcid_initial_set[module]=false;
	m_bcid_initial[module]=0xffffffff;
      }

      char name[128];
      char title[128];
      RceHisto2d<int, int> *histo;
      RceHisto1d<int, int> *histoTot;
      unsigned int cols=m_info[module].getNColumns()*m_info[module].getNFrontends();
      unsigned int rows=m_info[module].getNRows();
      unsigned int moduleId=m_info[module].getId();
      std::string moduleName=m_info[module].getName();

      /* retrieve scan points - Vcal steps in this case */
      std::cout<<"Creating Occupancy histograms."<<std::endl;
      std::cout<<"Creating ToT spectrum histograms."<<std::endl;

      sprintf(title,"OCCUPANCY Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_Occupancy",moduleId);
      histo=new RceHisto2d<int, int>(name,title,cols,0,cols,rows,0,rows);
      if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
      else histo->setAxisTitle(0,"FE*N_COL+Column");
      histo->setAxisTitle(1, "Row");
      m_histo_occ.push_back(histo);

      sprintf(title,"ToT 2d Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_Occupancy_2d",moduleId);
      histo=new RceHisto2d<int, int>(name,title,cols,0,cols,rows,0,rows);
      if(m_info[module].getNFrontends()==1)histo->setAxisTitle(0,"Column");
      else histo->setAxisTitle(0,"FE*N_COL+Column");
      histo->setAxisTitle(1, "Row");
      m_histo_tot2d.push_back(histo);

      sprintf(title,"ToT Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_ToT",moduleId);
      histoTot=new RceHisto1d<int, int>(name,title,16,-.5,15.5);
      histoTot->setAxisTitle(0,"ToT");
      m_histo_tot.push_back(histoTot);

      sprintf(title,"ToT per event Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_ToT_event",moduleId);
      histoTot=new RceHisto1d<int, int>(name,title,32,-.5,31.5);
      histoTot->setAxisTitle(0,"ToT");
      m_histo_totev.push_back(histoTot);

      sprintf(title,"Number of hits per event Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_nhits",moduleId);
      histoTot=new RceHisto1d<int, int>(name,title,16,-.5,15.5);
      histoTot->setAxisTitle(0,"Number of hits");
      m_histo_nhit.push_back(histoTot);

      sprintf(title,"Timing of bunch per event Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_timing",moduleId);
      histoTot=new RceHisto1d<int, int>(name,title,16,-.5,15.5);
      histoTot->setAxisTitle(0,"Hit timing");
      m_histo_timing.push_back(histoTot);

      sprintf(title,"Hits per cluster Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_nhits_cluster",moduleId);
      histoTot=new RceHisto1d<int, int>(name,title,20,-.5,19.5);
      histoTot->setAxisTitle(0,"Hits per cluster");
      m_histo_nhits_cluster.push_back(histoTot) ; 
      m_cluster_proc[module].assignHitHisto(histoTot);

      sprintf(title,"Clustered ToT Mod %d at %s", moduleId, moduleName.c_str()); 
      sprintf(name,"Mod_%d_Selftrigger_tot_cluster",moduleId); 
      histoTot=new RceHisto1d<int, int>(name,title,32,-.5,31.5); 
      histoTot->setAxisTitle(0,"Clustered ToT"); 
      m_histo_tot_cluster.push_back(histoTot) ;
      m_cluster_proc[module].assignToTHisto(histoTot);

      sprintf(title,"Clustered ToT per Cluster Size Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_tot_per_clustersize",moduleId);
      histo=new RceHisto2d<int, int>(name,title,32,-.5,31.5,20,-.5,19.5);
      histo->setAxisTitle(0,"Clustered ToT");
      histo->setAxisTitle(1,"Cluster Size");
      m_histo_tot_size.push_back(histo); 
      m_cluster_proc[module].assignToTClusterSizeHisto(histo);

      sprintf(title,"L1ID difference per event Mod %d at %s", moduleId, moduleName.c_str());
      sprintf(name,"Mod_%d_Selftrigger_L1IDdiff",moduleId);
      histoTot=new RceHisto1d<int, int>(name,title,16,-0.5,15.5);
      histoTot->setAxisTitle(0,"L1ID difference");
      m_histo_l1idDiff.push_back(histoTot);
      
      if(m_link10DataOn)
	{
	  sprintf(title,"BCID difference per event Mod %d at %s", moduleId, moduleName.c_str());
	  sprintf(name,"Mod_%d_Selftrigger_BCIDdiff",moduleId);
	  histoTot=new RceHisto1d<int, int>(name,title,256,-0.5,255.5);
	  histoTot->setAxisTitle(0,"BCID difference");
	  m_histo_bcidDiff.push_back(histoTot); 
	}
      
    }
  }
  catch(boost::property_tree::ptree_bad_path ex){
    rcecalib::Bad_ptree_param issue( ERS_HERE, ex.what());
    ers::error(issue);
    ERS_ASSERT(0);
  }
}

SelftriggerDataProc::~SelftriggerDataProc(){
  delete m_file;
  for (size_t module=0;module<m_histo_tot.size();module++){
    delete m_histo_tot[module];
    delete m_histo_totev[module];
    delete m_histo_nhit[module];
    delete m_histo_occ[module];
    delete m_histo_tot2d[module];
    delete m_histo_timing[module];
    delete m_histo_tot_cluster[module];
    delete m_histo_nhits_cluster[module];
    delete m_histo_tot_size[module];
    delete m_histo_l1idDiff[module];
    if(m_link10DataOn)
      delete m_histo_bcidDiff[module];
  }
  delete [] m_totev;
  delete [] m_nhit;
  delete [] m_bcid_ref;
  delete [] m_bcid;
  delete [] m_l1id_last;
  delete [] m_l1id;
  delete [] m_cluster_proc;

  
  delete [] m_l1idchanged;
  delete [] m_hiteventid;

  if(m_link10DataOn)
    {  
      delete [] m_bcid_initial_set;
      delete [] m_bcid_initial;
      
      for (int i=0;i<16;i++){
	delete [] m_bcidDiff_index_l1id_vs_module[i];
	delete [] m_bcidDiff_index_l1id_vs_module_isSet[i];
      }
      delete [] m_bcidDiff_index_l1id_vs_module;
      delete [] m_bcidDiff_index_l1id_vs_module_isSet;
      
      delete [] m_trigtimeDiff_index_l1id;
      delete [] m_trigtime_index_l1id;
    }
}
  

