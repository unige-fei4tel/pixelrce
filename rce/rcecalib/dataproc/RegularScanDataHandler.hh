#ifndef REGULARSCANDATAHANDLER_HH
#define REGULARSCANDATAHANDLER_HH

#include <vector>
#include <assert.h>

#include "rcecalib/dataproc/AbsDataHandler.hh"
#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/profiler/Profiler.hh"

class AbsFormatter;
class DataCond;

class RegularScanDataHandler: public AbsDataHandler{
public:
  RegularScanDataHandler(AbsDataProc* dataproc, DataCond& datacond, ConfigIF* cif, boost::property_tree::ptree* scanOptions);
  virtual ~RegularScanDataHandler();
  void handle(unsigned link, unsigned* data, int size);
  void timeoutOccurred();
  void resetL1counters();
protected:
  int m_nL1AperEv;
  omni_mutex m_nL1Alock;
  std::vector<int> m_L1Acounters;
  int m_nModules;
  int *m_linkToIndex;
  unsigned *m_parsedData;
  std::vector<AbsFormatter*> m_formatter;
  Profiler::Timer m_timer;
};
#endif
