#ifndef ABSFIT_HH
#define ABSFIT_HH

#include <string>
#include <vector>
#include "rcecalib/config/ModuleInfo.hh"

class ConfigIF;


class AbsFit {
public:
  enum FitOpt {NOCONV=0x1,XTALK=0x2};
  AbsFit(ConfigIF *cif,std::vector<int> &vcal,int nTrigger):m_config(cif),m_vcal(vcal),m_nTrigger(nTrigger){};
  virtual ~AbsFit(){};
  virtual int doFit(int fitopt=0) = 0;
  
protected:
  ConfigIF *m_config;
  std::vector<int> m_vcal;
  int m_nTrigger;
};

#endif
