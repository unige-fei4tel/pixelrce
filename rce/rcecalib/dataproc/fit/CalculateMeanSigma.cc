
namespace{

  int SquareRootFunc(int variance)
  {
    int op, res, one;
    op = variance;
    res = 0;
    
    // "one" starts at the highest power of four <= than the argument.
    // if this value is decreased, a smaller range of values is allowed. needs to be an even number
    one = 1 << 30;	
    while (one > op)  {
      one >>= 2;
    }
    while (one != 0) {
      if (op >= res + one) {
	op = op - (res + one);
	res = res +  (one<<1);
      }
      res >>= 1;
      one >>= 2;
    }
    return res;  
  }
}
template<typename H, typename HE>
int calculateMeanSigma( std::vector<std::vector<RceHisto2d<H, HE>*> > &histo_occ, std::vector<std::vector<RceHisto2d<short, short>*> > &hvalue,   std::vector<std::vector<RceHisto2d<short, short>*> > &hvalue2,   std::vector<std::vector<RceHisto2d<short, short>*> > &hmean,  std::vector<std::vector<RceHisto2d<short, short>*> > &hsigma )
{
  
  unsigned int numberofmodules =histo_occ.size();

  for(unsigned module=0;module<numberofmodules;module++) {
    for(unsigned int bin=0;bin<histo_occ[module].size();bin++) {
      for(int col=0;col<histo_occ[module][bin]->nBin(0);col++) {
	for(int row=0;row<histo_occ[module][bin]->nBin(1);row++) {
	  int counting =(int) (*histo_occ[module][bin])(col,row);
	  int toting = (int)(*hvalue[module][bin])(col,row);
	  int tot2ing = (int)(*hvalue2[module][bin])(col,row);
	  int mean=0;
	  int variance=0;
	  if(counting>0)mean=(toting<<7)/counting; 
	  if(mean>0) {
	    hmean[module][bin]->set(col,row,mean);
	  }
	  else {
	    if (counting>0) {
	      hmean[module][bin]->set(col,row,1);
	    }
	    else {
	      hmean[module][bin]->set(col,row,0);
	    }
	  }
	  
	  if(counting>1) variance=  (((tot2ing<<14)/(counting))-mean*mean);
	  if(variance >0)  {
	    int sigma =  SquareRootFunc(variance);
	    hsigma[module][bin]->set(col,row,sigma);
	  } else{
	    
	    if(counting>1) {
	      hsigma[module][bin]->set(col,row,1);
	    }
	    else {
	      hsigma[module][bin]->set(col,row,0);
	    }
	  }
	}  //rows
      } //columns
    } // bins
  }//modules
  return 0;
}
