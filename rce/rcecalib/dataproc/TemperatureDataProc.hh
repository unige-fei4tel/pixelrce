#ifndef TEMPERATUREDATAPROC_HH
#define TEMPERATUREDATAPROC_HH

#include <boost/property_tree/ptree_fwd.hpp>
#include "rcecalib/dataproc/AbsDataProc.hh"
#include "rcecalib/dataproc/fit/AbsFit.hh"
#include <vector>
#include "rcecalib/util/RceHisto1d.cc"

class TemperatureDataProc: public AbsDataProc{
public:
  TemperatureDataProc(ConfigIF* cif,boost::property_tree::ptree* scanOptions );
  virtual ~TemperatureDataProc();
  int processData(unsigned link, unsigned *data, int size);
  int fit(std::string fitfun);

protected:
  std::vector<RceHisto1d<int, int>*> m_histo_occ;
  std::vector<RceHisto1d<int, int>*> m_histo_adc;
  std::vector<RceHisto1d<int, int>*> m_histo_adc2;
  std::vector<RceHisto1d<float, float>*> m_histo_norm;
  AbsFit *m_fit;
  std::vector<int> m_vcal;
  int m_nTrigger;
  int m_nLoops;
  int m_nPoints;
};

#endif
