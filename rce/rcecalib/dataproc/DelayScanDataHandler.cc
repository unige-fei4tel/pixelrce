#include "rcecalib/dataproc/DelayScanDataHandler.hh"
#include "rcecalib/dataproc/AbsDataProc.hh"
#include "rcecalib/config/ConfigIF.hh"
#include "rcecalib/util/DataCond.hh"
#include <boost/property_tree/ptree.hpp>
#include <iostream>

DelayScanDataHandler::DelayScanDataHandler(AbsDataProc* dataproc, DataCond& datacond) 
  :AbsDataHandler(dataproc, datacond, 0 ){}


void DelayScanDataHandler::handle(unsigned link, unsigned *data, int size){
  assert(link==10);
  m_dataProc->processData(link, data, size);
}
