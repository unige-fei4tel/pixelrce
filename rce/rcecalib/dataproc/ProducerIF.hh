#ifndef PRODUCERIF_HH
#define PRODUCERIF_HH

namespace eudaq{
  class Event;
  class Producer;
}

class ProducerIF{
public:
  static void SendEvent(eudaq::Event *ev);
  static void setProducer(eudaq::Producer* producer);
private:
  eudaq::Producer *m_producer;
  
};

#endif
