#ifdef RCE_V2
#include "datCode.hh"
#include DAT_PUBLIC( oldPpi, net,       Error.hh)
#include DAT_PUBLIC( oldPpi, net,       SocketTcp.hh)
#ifndef __rtems__
#include DAT_PUBLIC( oldPpi, net,       GetAddr.hh)
#endif
#else
#include "rce/net/Error.hh"
#include "rce/net/Getaddr.hh"
#include "rce/net/SocketTcp.hh"
#endif
#include "namespace_aliases.hh"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include "rcecalib/HW/RCDImaster.hh"
#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>

#include <arpa/inet.h> // for htonl

#include "rcecalib/dataproc/MeasurementReceiver.hh"


MeasurementReceiver::MeasurementReceiver(AbsDataHandler* handler, boost::property_tree::ptree* scanOptions):
  PgpReceiver(handler), TriggerReceiverIF(){
  PgpTrans::RCDImaster::instance()->setReceiver(this);

  //the name is "ipAddr:port"
  std::string name=scanOptions->get<std::string>("Name");
  boost::cmatch matches;
  boost::regex re("(^.*):(\\d+)");
  std::string ipaddr, port;
  if(boost::regex_search(name.c_str(), matches, re)){
    assert(matches.size()>2);
    ipaddr=std::string(matches[1].first, matches[1].second);
    port=std::string(matches[2].first, matches[2].second);
  }else{
    std::cout<<"No ip addr:port given"<<std::endl;
    assert(0);
  }
#ifdef __rtems__
  // construct address from string with dots
  boost::regex re2("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");
  unsigned address;
  if(boost::regex_search(ipaddr.c_str(), matches, re2)){
    assert(matches.size()>4);
    address=atoi(std::string(matches[1].first, matches[1].second).c_str())<<24;
    address|=atoi(std::string(matches[2].first, matches[2].second).c_str())<<16;
    address|=atoi(std::string(matches[3].first, matches[3].second).c_str())<<8;
    address|=atoi(std::string(matches[4].first, matches[4].second).c_str());
  }else{
    std::cout<<"bad ip address given"<<std::endl;
    assert(0);
  }
#else
  unsigned address = RceNet::getaddr(ipaddr.c_str());
#endif
  std::cout<<"Ip address is "<<std::hex<<address<<std::dec<<std::endl;
  int porti=atoi(port.c_str());
  m_addr=RceNet::IpAddress(address, porti);
  m_ipname=ipaddr;

  std::cout<<"Created Measurement receiver"<<std::endl;
  
}


void MeasurementReceiver::Receive(unsigned *data, int size){
  std::string vals=sendCommand("measure", m_addr, m_ipname.c_str());
  float val;
  char* end;
  val=strtof(vals.c_str(), &end);
  if(end-vals.c_str()!=(int)vals.size()){
    std::cout<<"Bad value "<<vals<<std::endl;
    assert(0);
  }
  m_handler->handle(0, (unsigned*)&val, 1);
  return;
}
const char* MeasurementReceiver::sendCommand(std::string inpline, RceNet::IpAddress dst, const char* rce){
  RceNet::SocketTcp socket;
  socket.connect(dst);
  socket.send(inpline.c_str(), inpline.size());
  int timeout= 10000;
  static char line[128];
  try{
    socket.setrcvtmo(timeout);
    int bytes=socket.recv(line,128);
    line[bytes]=0;
  }
  catch (...){
    std::cout<<"Network error. Exiting."<<std::endl;
    assert(0);
  }
  socket.close();
  return line;
}

