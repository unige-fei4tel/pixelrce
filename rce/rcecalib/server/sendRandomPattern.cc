#include <ipc/partition.h>
#include <ipc/core.h>
#include <ipc/server.h>
#include <ipc/object.h>

#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/PixScan.hh"
#include "ScanOptions.hh"
#include "rcecalib/config/FEI4/FECommands.hh"
#include "rcecalib/config/FEI4/FEI4ARecord.hh"
#include "rcecalib/config/FEI4/FEI4AFormatter.hh"
#include "rcecalib/config/FormattedRecord.hh"
#include "rcecalib/server/FEI4AConfigFile.hh"

#include <cmdl/cmdargs.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char ** argv ){

    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");
    CmdArgInt	rce_name ('r', "rce", "rce-name", "rce to talk to.");

//
// Initialize command line parameters with default values
//
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

    CmdLine  cmd(*argv, &partition_name, &rce_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   p( (const char*)partition_name );
    IPCController controller(p);
    int rce=(int)rce_name;
    controller.removeAllRces();
    controller.addRce(rce);
    assert(controller.writeHWregister(rce, 0,0xff)==0); //channel mask
    std::vector<unsigned int> rep;
    srand(0);
    std::string a;
    while(1){
      for(int i=0;i<2048;i++)rep.push_back((unsigned int)rand());
      controller.writeHWblockData(rce, rep);      
      rep.clear();
      //std::cin>>a;
    }
}
