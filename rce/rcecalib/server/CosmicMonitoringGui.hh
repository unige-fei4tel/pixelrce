#ifndef COSMICMONITORINGGUI_HH
#define COSMICMONITORINGGUI_HH

#include "TGMdiMainFrame.h"
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include "TFile.h"
#include "TH2F.h"
#include <string>


class CosmicMonitoringGuiEmbedded;

class CosmicMonitoringGui: public TGMainFrame {
public:
  CosmicMonitoringGui(const char* filename, int nevt, const TGWindow *p,UInt_t w,UInt_t h);
  virtual ~CosmicMonitoringGui();
  void handleFileMenu(Int_t);
  void quit();
private:

  CosmicMonitoringGuiEmbedded* embededGui;
  enum Filemenu {LOAD, SAVE, QUIT};

ClassDef (CosmicMonitoringGui,0)
};
#endif
