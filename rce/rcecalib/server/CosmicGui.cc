#include <ipc/partition.h>
#include <ipc/core.h>
#include "rcecalib/server/CosmicDataReceiver.hh"
#include "rcecalib/server/CosmicGui.hh"
#include "rcecalib/server/GlobalConfigGui.hh"
#include "rcecalib/util/VerifyErrors.hh"
#include "rcecalib/server/CosmicMonitoringGuiEmbedded.hh"
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/PixScan.hh"
#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/ScanLog.hh"
#include "rcecalib/server/atlasimage.hh"
#include "rcecalib/config/FEI3/JJFormatter.hh"
#include "rcecalib/config/FEI4/FEI4AFormatter.hh"
#include "rcecalib/config/FEI4/FEI4BFormatter.hh"
#include "rcecalib/config/afp-hptdc/AFPHPTDCFormatter.hh"
#include "rcecalib/config/ModuleInfo.hh"
#include "ScanOptions.hh"
#include "TApplication.h"
#include "TGMsgBox.h"
#include "TGIcon.h"
#include "TGMenu.h"
#include "TGTab.h"
#include <TGFileDialog.h>
#include <TROOT.h>
#include <TStyle.h>
#include <cmdl/cmdargs.h>
#include <iostream>
#include <time.h>
#include <sys/stat.h> 
#include <fstream>
#include <list>
#include <netdb.h>


#include <boost/property_tree/ptree.hpp>

#include <is/infoT.h>
#include <is/infodictionary.h>

using namespace RCE;

CosmicGui::~CosmicGui(){
  Cleanup();
  if (m_dataReceiver) delete m_dataReceiver;
  m_dataReceiver = 0;
}

CosmicGui::CosmicGui(IPCController& controller, bool autostart, const TGWindow *p,UInt_t w,UInt_t h, int exitAfterEvents)
  : TGMainFrame(p,w,h), m_controller(controller), m_realTime(true), m_dataReceiver(0)
{

  // connect x icon on window manager
  Connect("CloseWindow()","CosmicGui",this,"quit()");

  TGMenuBar *menubar=new TGMenuBar(this,1,1,kHorizontalFrame | kRaisedFrame);
  TGLayoutHints *menubarlayout=new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
  // menu "File"
  TGPopupMenu* filepopup=new TGPopupMenu(gClient->GetRoot());
  filepopup->AddEntry("&Load Config",LOAD);
  filepopup->AddEntry("&Save Config",SAVE);
  filepopup->AddSeparator();
  filepopup->AddEntry("Save &Histograms",HISTOS);
  filepopup->AddEntry("Save &Screenshot",SCREENSHOT);
  filepopup->AddSeparator();
  filepopup->AddEntry("&Quit",QUIT);
  menubar->AddPopup("&File",filepopup,menubarlayout);

  AddFrame(menubar, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0,0,0,2));
  
  filepopup->Connect("Activated(Int_t)","CosmicGui",this,"handleFileMenu(Int_t)");

  TGTab* fTab = new TGTab(this);
  AddFrame(fTab,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY )) ;
  TGCompositeFrame *tab0 = fTab->AddTab("Main");
  TGCompositeFrame *tab1 = fTab->AddTab("Config DUT 1-16");
  TGCompositeFrame *tab2 = fTab->AddTab("Config Telescope 1-16");
  TGCompositeFrame *datapanelt2x = fTab->AddTab("Config DUT 17-32");
  TGCompositeFrame *datapanelt2bx = fTab->AddTab("Config Telescope 17-32");
  TGCompositeFrame *tab3 = fTab->AddTab("Monitoring");
  
  TGVerticalFrame* mainframe=new TGVerticalFrame(tab0,2,2,kSunkenFrame);
  TGHorizontalFrame *datapanel1 = new TGHorizontalFrame(mainframe, 2, 2, kSunkenFrame);
  mainframe->AddFrame(datapanel1,new TGLayoutHints(kLHintsExpandX ,0,0,0,20));
  TGHorizontalFrame *datapanel44 = new TGHorizontalFrame(mainframe, 2, 2, kSunkenFrame);
  mainframe->AddFrame(datapanel44,new TGLayoutHints(kLHintsExpandX ,0,0,0,20));
  TGHorizontalFrame *datapanel2 = new TGHorizontalFrame(mainframe, 2, 2, kSunkenFrame);
  mainframe->AddFrame(datapanel2,new TGLayoutHints(kLHintsExpandX ));
  TGHorizontalFrame *datapanel3 = new TGHorizontalFrame(mainframe, 2, 2, kSunkenFrame);
  mainframe->AddFrame(datapanel3,new TGLayoutHints(kLHintsExpandX));
  TGHorizontalFrame *datapanel5 = new TGHorizontalFrame(mainframe, 2, 2, kSunkenFrame);
  mainframe->AddFrame(datapanel5,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  tab0->AddFrame(mainframe,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGVerticalFrame *datapanel4[4];
  datapanel4[0] = new TGVerticalFrame(tab1, 2, 2, kSunkenFrame);
  tab1->AddFrame(datapanel4[0],new TGLayoutHints(kLHintsExpandX));
  datapanel4[1] = new TGVerticalFrame(tab2, 2, 2, kSunkenFrame);
  tab2->AddFrame(datapanel4[1],new TGLayoutHints(kLHintsExpandX));
  datapanel4[2] = new TGVerticalFrame(datapanelt2x, 2, 2, kSunkenFrame);
  datapanelt2x->AddFrame(datapanel4[2],new TGLayoutHints(kLHintsExpandX));
  datapanel4[3] = new TGVerticalFrame(datapanelt2bx, 2, 2, kSunkenFrame);
  datapanelt2bx->AddFrame(datapanel4[3],new TGLayoutHints(kLHintsExpandX));



  int modperpanel=ConfigGui::MAX_MODULES/4;
  for(int j=0;j<4;j++){
    TGHorizontalFrame *datapanelb=new TGHorizontalFrame(datapanel4[j]);
    datapanel4[j]->AddFrame(datapanelb,new TGLayoutHints(kLHintsExpandX ));
    TGTextButton *alloff=new TGTextButton(datapanelb,"All Off");
    alloff->SetMargins(15,25,0,0);
    datapanelb->AddFrame(alloff,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,2,2,2,2));
    TGTextButton *allon=new TGTextButton(datapanelb,"All On");
    allon->SetMargins(15,25,0,0);
    datapanelb->AddFrame(allon,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,2,2,2,2));
    if(j==0){
      alloff->Connect("Clicked()", "CosmicGui", this,"allOffA()");
      allon->Connect("Clicked()", "CosmicGui", this,"allOnA()");
    }else if(j==1){
      alloff->Connect("Clicked()", "CosmicGui", this,"allOffC()");
      allon->Connect("Clicked()", "CosmicGui", this,"allOnC()");
    }else if(j==2){
      alloff->Connect("Clicked()", "CosmicGui", this,"allOffA2()");
      allon->Connect("Clicked()", "CosmicGui", this,"allOnA2()");
    }else if(j==3){
      alloff->Connect("Clicked()", "CosmicGui", this,"allOffC2()");
      allon->Connect("Clicked()", "CosmicGui", this,"allOnC2()");
    }
    TGHorizontalFrame *datapanelc[4];
    for(int i=0;i<4;i++){
      datapanelc[i] = new TGHorizontalFrame(datapanel4[j]);
      datapanel4[j]->AddFrame(datapanelc[i],new TGLayoutHints(kLHintsExpandX |kLHintsExpandY));
    }
    char modname[128];
    for (int i=0;i<modperpanel;i++){
      if(j%2==0) sprintf(modname, "DUT FE %d", 1+i+(j/2)*16);
      else sprintf(modname, "Telescope FE %d", 1+i+(j/2)*16);
      m_config[i+modperpanel*j]=new ConfigGui(modname, datapanelc[i/(modperpanel/4)],1,1, kSunkenFrame, "");
      datapanelc[i/(modperpanel/4)]->AddFrame(m_config[i+modperpanel*j],new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,1,1));
    } 
  }

  std::vector<ConfigGui*> vec_config(m_config,m_config+ConfigGui::MAX_MODULES);
  monitoringGui = new CosmicMonitoringGuiEmbedded(tab3,1,1,kSunkenFrame,vec_config);
  tab3->AddFrame(monitoringGui,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY));

  m_globalconf=new GlobalConfigGui(m_config, ".cosmicDaq.rc", datapanel1, 1, 1, kSunkenFrame);
  datapanel1->AddFrame(m_globalconf,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,2,2));
  
  m_isrunning=false;
  m_nevtMin=0;
  m_nevt=0;
  m_timers=new TTimer;
  m_timers->Connect("Timeout()","CosmicGui",this,"timeouts()");
  m_timerm=new TTimer;
  m_timerm->Connect("Timeout()","CosmicGui",this,"timeoutm()");
  m_timerl=new TTimer;
  m_timerl->Connect("Timeout()","CosmicGui",this,"timeoutl()");

  FontStruct_t maskfont= gClient->GetFontByName("-adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-iso8859-1");
  TGVerticalFrame *startmenu = new TGVerticalFrame(datapanel2, 0, 0, kSunkenFrame);
  datapanel2->AddFrame(startmenu, new TGLayoutHints(kLHintsTop|kLHintsLeft, 2,2,0,2));
  TGLabel *startmenulabel=new TGLabel(startmenu,"Run Options:");
  startmenulabel->SetTextFont(maskfont);
  startmenu->AddFrame(startmenulabel,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 0, 50));

  m_start=new TGTextButton(startmenu,"Start Run");
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_start->SetFont(labelfont);
  m_start->SetMargins(10,40,10,10);
  startmenu->AddFrame(m_start,new TGLayoutHints(kLHintsCenterY | kLHintsCenterX  ,2,2,5,5));
  m_start->Connect("Clicked()", "CosmicGui", this, "toggle()");

  TGHorizontalFrame *maxevents = new TGHorizontalFrame(startmenu);
  startmenu->AddFrame(maxevents, new TGLayoutHints(kLHintsCenterY|kLHintsLeft,0,0,5,5));

  m_maxtrue = new TGCheckButton(maxevents);
  maxevents->AddFrame(m_maxtrue, new TGLayoutHints(kLHintsLeft|kLHintsCenterY));
  TGLabel *maximum=new TGLabel(maxevents,"Max Evts");
  maxevents->AddFrame(maximum, new TGLayoutHints(kLHintsLeft|kLHintsCenterY,0,0,0,0));
  m_maxevents=new TGNumberEntry(maxevents, 0, 10, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMin, 0);
  maxevents->AddFrame(m_maxevents, new TGLayoutHints(kLHintsCenterY|kLHintsLeft,5,0,5,5));
  TGVerticalFrame *fileframe= new TGVerticalFrame(startmenu, 0, 0, kSunkenFrame);
  startmenu->AddFrame(fileframe,new TGLayoutHints(kLHintsCenterY|kLHintsCenterX ,10,0,20,10));
  TGLabel *filelabel=new TGLabel(fileframe,"File Format:");
  fileframe->AddFrame(filelabel,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 2, 0));
  m_eudetfile=new TGCheckButton(fileframe,"Eudet");
  fileframe->AddFrame(m_eudetfile,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,15,0,3,3));
  m_rawfile=new TGCheckButton(fileframe,"Raw");
  fileframe->AddFrame(m_rawfile,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,15,0,3,3));
  m_writeRootFile=new TGCheckButton(fileframe,"ROOT");
  fileframe->AddFrame(m_writeRootFile,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,15,0,3,3));
  

  TGVerticalFrame *trigmenu1 = new TGVerticalFrame(datapanel2, 0, 0, kSunkenFrame);
  datapanel2->AddFrame(trigmenu1,new TGLayoutHints(kLHintsCenterY ,2,0,0,0));

  TGLabel *tscl=new TGLabel(trigmenu1,"Trigger Sources:");
  tscl->SetTextFont(maskfont);
  trigmenu1->AddFrame(tscl,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 0, 0, 0, 0));
  m_scint=new TGCheckButton(trigmenu1,"Scintillators");
  trigmenu1->AddFrame(m_scint,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,0,0,0,0));
  m_eudet=new TGCheckButton(trigmenu1,"Eudet Trg");
  trigmenu1->AddFrame(m_eudet,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,0,0,0,0));
  TGVerticalFrame *trigmenu7 = new TGVerticalFrame(trigmenu1, 0, 0, kSunkenFrame);
  trigmenu1->AddFrame(trigmenu7,new TGLayoutHints(kLHintsCenterX ,5,0,0,0));
  TGHorizontalFrame *trigmenu7a = new TGHorizontalFrame(trigmenu7);
  trigmenu7->AddFrame(trigmenu7a,new TGLayoutHints(kLHintsCenterX ,0,0,0,0));
  TGVerticalFrame *trigmenu7b = new TGVerticalFrame(trigmenu7a);
  trigmenu7a->AddFrame(trigmenu7b,new TGLayoutHints(kLHintsCenterX ,0,0,0,0));
  TGVerticalFrame *trigmenu7c = new TGVerticalFrame(trigmenu7a);
  trigmenu7a->AddFrame(trigmenu7c,new TGLayoutHints(kLHintsCenterX ,0,0,0,0));
  m_HSIO1=new TGCheckButton(trigmenu7b,"HSIO Ext 1");
  trigmenu7b->AddFrame(m_HSIO1,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,0,0,15,0));
  m_HSIO2=new TGCheckButton(trigmenu7b,"HSIO Ext 2");
  trigmenu7b->AddFrame(m_HSIO2,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,0,0,0,0));
  m_hsiologic=new TGButtonGroup(trigmenu7c, 2, 1, 2, 0, "Logic");
  trigmenu7c->AddFrame(m_hsiologic, new TGLayoutHints(kLHintsTop | kLHintsLeft, 2, 0, 0, 0));
  new TGRadioButton(m_hsiologic, "OR", 0);
  new TGRadioButton(m_hsiologic, "AND", 1);
  m_hsiologic->SetButton(1);
  m_hsiostate=1;
  m_hsiologic->Connect("Pressed(Int_t)", "CosmicGui", this, "setHsioLogic(Int_t)");

  TGVerticalFrame *trigmenu4 = new TGVerticalFrame(trigmenu1, 0, 0, kSunkenFrame);
  trigmenu1->AddFrame(trigmenu4,new TGLayoutHints(kLHintsCenterX ,15,0,0,0));
  m_cyclic=new TGCheckButton(trigmenu4,"Cyclic Trg");
  trigmenu4->AddFrame(m_cyclic,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,0,0,0,0));
  TGLabel *cperiodl=new TGLabel(trigmenu4,"Cyclic Period (ticks)");
  trigmenu4->AddFrame(cperiodl,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 3, 3, 0, 0));
  m_cperiod=new TGNumberEntry(trigmenu4,0,10,-1,TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative);
  trigmenu4->AddFrame(m_cperiod,new TGLayoutHints(kLHintsCenterY |kLHintsCenterX, 0, 0, 3, 3)); 
  TGVerticalFrame *trigmenuhb = new TGVerticalFrame(trigmenu1, 0, 0, kSunkenFrame);
  trigmenu1->AddFrame(trigmenuhb,new TGLayoutHints(kLHintsCenterX ,15,0,0,0));
  m_hitbus=new TGCheckButton(trigmenuhb,"Hitbus");
  trigmenuhb->AddFrame(m_hitbus,new TGLayoutHints(kLHintsTop | kLHintsLeft ,0,0,0,0));
  m_hbinput=new TGButtonGroup(trigmenuhb, 4, 1, 4, 0, "Logic");
  trigmenuhb->AddFrame(m_hbinput, new TGLayoutHints(kLHintsTop | kLHintsLeft, 2, 0, 0, 0));
  new TGRadioButton(m_hbinput, "Chip 1", 1);
  new TGRadioButton(m_hbinput, "Chip 2", 2);
  new TGRadioButton(m_hbinput, "1 OR 2", 3);
  new TGRadioButton(m_hbinput, "1 AND 2", 4);
  m_hbstate=1;
  m_hbinput->Connect("Pressed(Int_t)", "CosmicGui", this, "setHbLogic(Int_t)");


  TGVerticalFrame *hbframe= new TGVerticalFrame(datapanel2, 0, 0, kSunkenFrame);
  datapanel2->AddFrame(hbframe,new TGLayoutHints(kLHintsTop ,5,0,0,0));
  TGLabel *hbchiplabel=new TGLabel(hbframe,"Hitbus Chips:");
  hbchiplabel->SetTextFont(maskfont);
  hbframe->AddFrame(hbchiplabel,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 0, 0));
  TGTab* hTab = new TGTab(hbframe);
  hbframe->AddFrame(hTab,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY )) ;
  TGCompositeFrame *tabh[2];
  tabh[0] = hTab->AddTab("Hitbus Chip 1");
  tabh[1] = hTab->AddTab("Hitbus Chip 2");
  for (int k=0;k<2;k++){
    TGHorizontalFrame *trigmenu5 = new TGHorizontalFrame(tabh[k]);
    tabh[k]->AddFrame(trigmenu5,new TGLayoutHints(kLHintsCenterY ,15,2,2,2));
    TGVerticalFrame *planeframe= new TGVerticalFrame(trigmenu5);
    trigmenu5->AddFrame(planeframe, new TGLayoutHints(kLHintsCenterY));
    m_tp[k][0]=new TGCheckButton(planeframe,"TA1");
    m_tp[k][1]=new TGCheckButton(planeframe,"TA2");
    m_tp[k][2]=new TGCheckButton(planeframe,"TA3");
    m_tp[k][3]=new TGCheckButton(planeframe,"TB1");
    m_tp[k][4]=new TGCheckButton(planeframe,"TB2");
    m_tp[k][5]=new TGCheckButton(planeframe,"TB3");
    for (int i=0;i<3;i++)
      planeframe->AddFrame(m_tp[k][i],new TGLayoutHints(kLHintsTop | kLHintsLeft ,2,0,0,0));
    TGLabel *hcl=new TGLabel(planeframe,"RCE");
    planeframe->AddFrame(hcl,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 0, 10, 0));
    m_hbrce[k]=new TGNumberEntry(planeframe,0,2,-1,TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative);
    planeframe->AddFrame(m_hbrce[k],new TGLayoutHints(kLHintsCenterY |kLHintsLeft, 0, 0, 3, 10));
    for (int i=3;i<6;i++)
      planeframe->AddFrame(m_tp[k][i],new TGLayoutHints(kLHintsTop | kLHintsLeft ,2,0,0,0));
    TGVerticalFrame *logframe= new TGVerticalFrame(trigmenu5);
    trigmenu5->AddFrame(logframe, new TGLayoutHints(kLHintsTop));
    m_talogic[k]=new TGButtonGroup(logframe, "TA Logic");
    logframe->AddFrame(m_talogic[k], new TGLayoutHints(kLHintsTop | kLHintsLeft, 2, 0, 0, 0));
    new TGRadioButton(m_talogic[k], "OR", 0);
    new TGRadioButton(m_talogic[k], "AND", 1);
    m_talogic[k]->SetButton(0);
    m_tastate[k]=0;
    m_tablogic[k]=new TGButtonGroup(logframe, 1, 2, 5, 0, "TA to TB");
    logframe->AddFrame(m_tablogic[k], new TGLayoutHints(kLHintsTop | kLHintsLeft, 2, 0, 0, 0));
    new TGRadioButton(m_tablogic[k], "OR", 0);
    new TGRadioButton(m_tablogic[k], "AND", 1);
    m_tablogic[k]->SetButton(0);
    m_tabstate[k]=0;
    m_tblogic[k]=new TGButtonGroup(logframe, "TB Logic");
    logframe->AddFrame(m_tblogic[k], new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 0, 0, 0));
    new TGRadioButton(m_tblogic[k], "OR", 0);
    new TGRadioButton(m_tblogic[k], "AND", 1);
    m_tblogic[k]->SetButton(0);
  } 
  m_talogic[0]->Connect("Pressed(Int_t)", "CosmicGui", this, "setTaLogic0(Int_t)");
  m_tablogic[0]->Connect("Pressed(Int_t)", "CosmicGui", this, "setTabLogic0(Int_t)");
  m_tblogic[0]->Connect("Pressed(Int_t)", "CosmicGui", this, "setTbLogic0(Int_t)");
  m_talogic[1]->Connect("Pressed(Int_t)", "CosmicGui", this, "setTaLogic1(Int_t)");
  m_tablogic[1]->Connect("Pressed(Int_t)", "CosmicGui", this, "setTabLogic1(Int_t)");
  m_tblogic[1]->Connect("Pressed(Int_t)", "CosmicGui", this, "setTbLogic1(Int_t)");
  TGHorizontalFrame *hbdel = new TGHorizontalFrame(hbframe);
  hbframe->AddFrame(hbdel,new TGLayoutHints(kLHintsBottom ,15,2,2,2));
  TGLabel *hmdellabel=new TGLabel(hbdel,"D-:");
  hbdel->AddFrame(hmdellabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_hbdelaym=new TGNumberEntry(hbdel, 0, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 31);
  hbdel->AddFrame(m_hbdelaym,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  m_hbdelay=new TGNumberEntry(hbdel, 0, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 31);
  hbdel->AddFrame(m_hbdelay,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 0, 2, 2, 0));
  TGLabel *hdellabel=new TGLabel(hbdel,"D+:");
  hbdel->AddFrame(hdellabel,new TGLayoutHints(kLHintsRight|kLHintsCenterY, 2, 2, 2, 0));
  

  TGVerticalFrame *discframe= new TGVerticalFrame(datapanel2, 0, 0, kSunkenFrame);
  datapanel2->AddFrame(discframe,new TGLayoutHints(kLHintsTop ,5,0,0,0));

  TGLabel *scint=new TGLabel(discframe,"Scintillators");
  scint->SetTextFont(maskfont);
  discframe->AddFrame(scint,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY,0,0,0,0));
  TGHorizontalFrame *trigmenu6 = new TGHorizontalFrame(discframe);
  discframe->AddFrame(trigmenu6,new TGLayoutHints(kLHintsCenterY ,15,2,2,2));
  TGVerticalFrame *aframe= new TGVerticalFrame(trigmenu6);
  trigmenu6->AddFrame(aframe, new TGLayoutHints(kLHintsTop,0,0,0,0));
  TGLabel *ahcl=new TGLabel(aframe,"Scint:");
  aframe->AddFrame(ahcl,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 0, 0, 0));
  m_NAND[0]=new TGCheckButton(aframe,"0");
  m_NAND[1]=new TGCheckButton(aframe,"1");
  m_NAND[2]=new TGCheckButton(aframe,"2");
  m_NAND[3]=new TGCheckButton(aframe,"3");
  for (int i=0;i<4;i++){
    aframe->AddFrame(m_NAND[i],new TGLayoutHints(kLHintsTop|kLHintsLeft ,2,0,0,0));
  }
  TGVerticalFrame *alogframe= new TGVerticalFrame(trigmenu6);
  trigmenu6->AddFrame(alogframe, new TGLayoutHints(kLHintsTop));
  TGLabel *bhcl = new TGLabel(alogframe,"Logic:");
  alogframe->AddFrame(bhcl, new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 0, 0, 0));
  m_NANDlogic[0]=new TGCheckButton(alogframe,"Anti");
  m_NANDlogic[1]=new TGCheckButton(alogframe,"Anti");
  m_NANDlogic[2]=new TGCheckButton(alogframe,"Anti");
  m_NANDlogic[3]=new TGCheckButton(alogframe,"Anti");
  for (int i=0;i<4;i++){
    alogframe->AddFrame(m_NANDlogic[i], new TGLayoutHints(kLHintsTop | kLHintsLeft, 2, 0, 0,0));
  }

  TGHorizontalFrame *delayframe= new TGHorizontalFrame(discframe);
  discframe->AddFrame(delayframe, new TGLayoutHints(kLHintsCenterY|kLHintsCenterX, 2, 2, 2, 2));
  TGVerticalFrame *delayframe1= new TGVerticalFrame(delayframe);
  delayframe->AddFrame(delayframe1, new TGLayoutHints(kLHintsCenterY|kLHintsCenterX));
  TGVerticalFrame *delayframe2= new TGVerticalFrame(delayframe);
  delayframe->AddFrame(delayframe2, new TGLayoutHints(kLHintsCenterY|kLHintsCenterX));

  
  TGLabel *delay0label=new TGLabel(delayframe1,"Sc0 Del:");
  delayframe1->AddFrame(delay0label,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 2, 2, 2, 0));
  m_delay0=new TGNumberEntry(delayframe1, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 63);
  delayframe1->AddFrame(m_delay0,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 0, 0, 0, 0));
  
  TGLabel *delay1label=new TGLabel(delayframe1,"Sc1 Del:");
  delayframe1->AddFrame(delay1label,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 2, 2, 2, 0));
  m_delay1=new TGNumberEntry(delayframe1, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 63);
  delayframe1->AddFrame(m_delay1,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 0, 0, 0, 0));


  TGLabel *delay2label=new TGLabel(delayframe2,"Sc2 Del:");
  delayframe2->AddFrame(delay2label,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 2, 2, 2, 0));
  m_delay2=new TGNumberEntry(delayframe2, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 63);
  delayframe2->AddFrame(m_delay2,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 0, 0, 0, 0));

  TGLabel *delay3label=new TGLabel(delayframe2,"Sc3 Del:");
  delayframe2->AddFrame(delay3label,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 2, 2, 2, 0));
  m_delay3=new TGNumberEntry(delayframe2, 0, 2, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 63);
  delayframe2->AddFrame(m_delay3,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsCenterX, 0, 0, 0, 0));


  TGVerticalFrame *quitmenu = new TGVerticalFrame(datapanel2, 0, 0);
  datapanel2->AddFrame(quitmenu, new TGLayoutHints(kLHintsTop|kLHintsCenterX, 0, 0, 0, 0));

  TGVerticalFrame *phasebusyframe=new TGVerticalFrame(quitmenu, 0, 0, kSunkenFrame);
  quitmenu->AddFrame(phasebusyframe, new TGLayoutHints(kLHintsTop|kLHintsExpandX|kLHintsCenterX, 2, 2, 2, 2));
  TGLabel *phasebusylabel=new TGLabel(phasebusyframe,"Phase busy");
  phasebusylabel->SetTextFont(maskfont);
  phasebusyframe->AddFrame(phasebusylabel, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 0, 0, 0, 0));

  m_phasebusyen=new TGCheckButton(phasebusyframe, "Enable"); 
  phasebusyframe->AddFrame(m_phasebusyen, new TGLayoutHints(kLHintsTop|kLHintsLeft, 2, 2, 2, 2));
  m_phasebusysel=new TGButtonGroup(phasebusyframe, 2, 2, 4, 0, "Active phase");
  phasebusyframe->AddFrame(m_phasebusysel, new TGLayoutHints(kLHintsTop|kLHintsLeft, 2, 0, 0, 0));
  new TGRadioButton(m_phasebusysel, "0->0", 0);
  new TGRadioButton(m_phasebusysel, "0->1", 1);
  new TGRadioButton(m_phasebusysel, "1->1", 3);
  new TGRadioButton(m_phasebusysel, "1->0", 2);
  m_phasebusysel->Connect("Pressed(Int_t)", "CosmicGui", this, "setTriggerActivePhase(Int_t)");
  m_phasebusysel->SetButton(0);
  m_phasebusystate=0;

  TGHorizontalFrame *tempframe= new TGHorizontalFrame(quitmenu);
  quitmenu->AddFrame(tempframe, new TGLayoutHints(kLHintsTop|kLHintsCenterX|kLHintsExpandX, 2, 2, 2, 2));
  TGVerticalFrame *tempframe1= new TGVerticalFrame(tempframe,0,0,kSunkenFrame);
  tempframe->AddFrame(tempframe1, new TGLayoutHints(kLHintsTop|kLHintsCenterX|kLHintsExpandX));
  TGLabel *tempmenulabel=new TGLabel(tempframe1,"FE Temperatures:");
  tempmenulabel->SetTextFont(maskfont);
  tempframe1->AddFrame(tempmenulabel,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 0, 0));
  m_tempenable=new TGCheckButton(tempframe1,"Read Temps");
  m_tempenable->Connect("Clicked()", "CosmicGui", this, "synchSave2()");
  tempframe1->AddFrame(m_tempenable,new TGLayoutHints(kLHintsTop | kLHintsLeft ,2,2,2,2));
  m_templog=new TGCheckButton(tempframe1,"Log Temps");
  m_templog->Connect("Clicked()", "CosmicGui", this, "synchSave1()");
  tempframe1->AddFrame(m_templog,new TGLayoutHints(kLHintsTop | kLHintsLeft ,2,2,2,5));
  TGLabel *tempfreqlabel=new TGLabel(tempframe1,"Frequency:");
  tempframe1->AddFrame(tempfreqlabel,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 0, 0));
  m_tempfreq=new TGComboBox(tempframe1,100);
  tempframe1->AddFrame(m_tempfreq,new TGLayoutHints(kLHintsTop | kLHintsCenterX,2,2,2,2));
  m_tempfreq->AddEntry("0.1 Hz", 400000000);
  m_tempfreq->AddEntry("0.2 Hz", 200000000);
  m_tempfreq->AddEntry("0.5 Hz", 80000000);
  m_tempfreq->AddEntry("1 Hz", 40000000);
  m_tempfreq->Resize(100,20);

  m_quit=new TGTextButton(quitmenu,"Quit");
  m_quit->Connect("Clicked()", "CosmicGui", this,"quit()");
  m_quit->SetFont(labelfont);
  m_quit->SetMargins(15,30,10,10);
  quitmenu->AddFrame(m_quit,new TGLayoutHints(kLHintsCenterY | kLHintsCenterX ,2,2,15,5));
  
  TGLabel *timelabel=new TGLabel(datapanel3,"Time:");
  datapanel3->AddFrame(timelabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_time=new TGLabel(datapanel3,"0 s       ");
  datapanel3->AddFrame(m_time,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 30, 0, 0));
  m_time->SetTextFont(labelfont);
  
  TGLabel *nevlabel=new TGLabel(datapanel3,"Events:");
  datapanel3->AddFrame(nevlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_nEvents=new TGLabel(datapanel3,"0                 ");
  datapanel3->AddFrame(m_nEvents,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_nEvents->SetTextFont(labelfont);
  
  TGLabel *ratelabel=new TGLabel(datapanel3,"Total Rate:");
  datapanel3->AddFrame(ratelabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_rate=new TGLabel(datapanel3,"0.00 Hz           ");
  datapanel3->AddFrame(m_rate,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_rate->SetTextFont(labelfont);
  
  TGLabel *iratelabel=new TGLabel(datapanel3,"Current Rate:");
  datapanel3->AddFrame(iratelabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_irate=new TGLabel(datapanel3,"0.00 Hz           ");
  datapanel3->AddFrame(m_irate,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_irate->SetTextFont(labelfont);

  TGLabel *ihitlabel=new TGLabel(datapanel3,"Hits per Event:");
  datapanel3->AddFrame(ihitlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_hpe=new TGLabel(datapanel3,"0.00    ");
  datapanel3->AddFrame(m_hpe,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_hpe->SetTextFont(labelfont);



  TGLabel *latencylabel=new TGLabel(datapanel44,"Latency:");
  datapanel44->AddFrame(latencylabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  latencylabel->SetTextFont(maskfont);
  latencylabel=new TGLabel(datapanel44,"DUT");
  datapanel44->AddFrame(latencylabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_l1alatency=new TGNumberEntry(datapanel44, 0, 3, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 255);
  datapanel44->AddFrame(m_l1alatency,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  latencylabel=new TGLabel(datapanel44,"Telescope");
  datapanel44->AddFrame(latencylabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_l1alatencytel=new TGNumberEntry(datapanel44, 0, 3, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 255);
  datapanel44->AddFrame(m_l1alatencytel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  
  TGLabel *ntrglabel=new TGLabel(datapanel44,"Consec. Trgs:");
  ntrglabel->SetTextFont(maskfont);
  datapanel44->AddFrame(ntrglabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 20, 2, 2, 0));
  ntrglabel=new TGLabel(datapanel44,"DUT");
  datapanel44->AddFrame(ntrglabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_ntrg=new TGNumberEntry(datapanel44, 0, 3, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 1, 16);
  datapanel44->AddFrame(m_ntrg,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  ntrglabel=new TGLabel(datapanel44,"Telescope");
  datapanel44->AddFrame(ntrglabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_ntrgtel=new TGNumberEntry(datapanel44, 0, 3, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 1, 16);
  datapanel44->AddFrame(m_ntrgtel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  
  
  TGLabel *delaylabel=new TGLabel(datapanel44,"Trg Delay:");
  delaylabel->SetTextFont(maskfont);
  datapanel44->AddFrame(delaylabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 20, 2, 2, 0));
  m_trgdelay=new TGNumberEntry(datapanel44, 0, 3, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 255);
  datapanel44->AddFrame(m_trgdelay,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  
  TGLabel *deadtimelabel=new TGLabel(datapanel44,"Deadtime:");
  deadtimelabel->SetTextFont(maskfont);
  datapanel44->AddFrame(deadtimelabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_deadtime=new TGNumberEntry(datapanel44, 0, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 65535);
  datapanel44->AddFrame(m_deadtime,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 2, 0));
  
  
  TGPicturePool tgpool(0,0);
  const TGPicture* pic=tgpool.GetPicture("atlas",const_cast<char**>(atlas_detector_small));
  TGIcon* tgi=new TGIcon(datapanel5 ,pic,300,196);
  datapanel5->AddFrame(tgi,new TGLayoutHints(kLHintsCenterX|kLHintsCenterY, 0, 2, 10, 0));
  //    m_controller.writeHWregister(4,1);
    //  sleep(1);
    //  unsigned counter4=m_controller.readHWregister(9);
    //  unsigned counter10=m_controller.readHWregister(10);
    //  unsigned l1counter=m_controller.readHWregister(11);
    //  unsigned counter4m=m_controller.readHWregister(12);
    //  unsigned counter10m=m_controller.readHWregister(13);
    //  unsigned counter10e=m_controller.readHWregister(14);
    //  unsigned tdccounter=m_controller.readHWregister(15);
    //  std::cout<<"Counter 4: "<<counter4<<" Counter 10: "<<counter10<<std::endl;
    //  std::cout<<"Counter 4m: "<<counter4m<<" Counter 10m: "<<counter10m<<std::endl;
    //  std::cout<<"Counter 10e: "<<counter10e<<" TDC counter: "<<(tdccounter&0xffff)<<std::endl;
    //  std::cout<<"l1counter: "<<l1counter<<std::endl;
  
  std::string rcfile=getenv("HOME");
  rcfile+="/.cosmicDaq.rc";

  readConfig(rcfile.c_str());

  m_scanLog=new ScanLog;

  SetWindowName("Cosmic Telescope DAQ");
  Resize(w,h);
  Layout();
  MapSubwindows();
  MapWindow();
  if(autostart==true)m_start->Clicked();
  if(exitAfterEvents>-1){
	m_oneRunAndExit=1;
	m_maxevents->SetIntNumber(exitAfterEvents);
	m_maxtrue->SetOn();
	m_start->Clicked();
   }
 }
void CosmicGui::toggle(){
  setRun(!running());
}
void CosmicGui::synchSave1(){
  if(m_templog->IsOn()==true && m_tempenable->IsOn()==false){
    m_tempenable->SetOn(true);
  }
}
void CosmicGui::synchSave2(){
  if(m_templog->IsOn()==true && m_tempenable->IsOn()==false){
    m_templog->SetOn(false);
  }
}

void CosmicGui::setRun(Bool_t on){
  if (on){
    //unsigned statusd=m_controller.readHWregister(4); 
    //std::cout<<"Statusd before run "<<std::hex<<statusd<<std::dec<<std::endl;
    /*
    if(m_oldRunName==m_name->GetText()){
      time_t t=time(0);
      struct tm* thetime;
      thetime=localtime(&t);
      char runnr[128];
      sprintf(runnr,"cosmic%04d%02d%02d%02d%02d%02d",thetime->tm_year+1900,thetime->tm_mon+1,thetime->tm_mday,
	      thetime->tm_hour, thetime->tm_min, thetime->tm_sec);
      m_name->SetText(runnr);
      m_oldRunName=runnr;
    }
    */
    int status=startRun();
    if(status==0){
      m_timers->Start(1000,kFALSE);  // update every second
      m_timerm->Start(5000,kFALSE);  // update every 5 second
      m_timerl->Start(60000,kFALSE);  // update every minut}
      m_starttime.Set();
      m_oneminago=m_starttime.AsDouble();
      m_nevtMin=0;
      m_nevt=0;
      enableControls(false);
      m_start->SetDown(true);
      m_isrunning=true;
      m_start->SetText("Running");
      m_rate->SetText("Starting...");
      m_irate->SetText("Starting...");
    }
  }else{
    stopRun();
    enableControls(true);
    m_timers->Stop();
    m_timerm->Stop();
    m_timerl->Stop();
    m_start->SetText("Start Run");
    m_isrunning=false;
    m_start->SetDown(false);
    m_irate->SetText("0.00 Hz");
    m_nEvents->SetText(Form("%u", m_nevt));
    /*
    m_nevt = m_controller.getNEventsProcessed();
    char labelstring[128];
    sprintf(labelstring,"%u",m_nevt);
    m_nEvents->SetText(labelstring);
    */
  }
}
//int counter=0;
void CosmicGui::timeouts(){
  // 1 s timeout, update number of events
  m_nEvents->SetText(Form("%u", m_nevt));
  //std::cout<<"nevents = "<<m_nevt<<std::endl;
  //counter++;
  //std::cout<<counter<<" "<<m_nevt<<std::endl;
  //    unsigned status=m_controller.readHWregister(3); 
    //  std::cout<<"Status "<<std::hex<<status<<std::dec<<std::endl;
    //  unsigned statusd=m_controller.readHWregister(4); 
    //  std::cout<<"Statusd "<<std::hex<<statusd<<std::dec<<std::endl;
  TTimeStamp currenttime;
  currenttime.Set();
  int timediff=(int)(currenttime.AsDouble()-m_starttime.AsDouble()+.5);
  m_time->SetText(Form("%d s", timediff));
  m_hpe->SetText(Form("%.3f",monitoringGui->getHitsPerEvent()) );
  if(m_maxtrue->IsDisabledAndSelected()){
    unsigned maxevents;
    maxevents = m_maxevents->GetIntNumber();
    if(m_nevt>=maxevents){
        setRun(false);
	if (m_oneRunAndExit){
	    quit();
	}
        setRun(true);
      }       
    }

}
void CosmicGui::timeoutm(){
  // 5 s timeout, update monitoring display
  monitoringGui->updateDisplay();
}

void CosmicGui::timeoutl(){
  // 60 s timeout, update rates
  TTimeStamp currenttime;
  currenttime.Set();
  double timediff=currenttime.AsDouble()-m_starttime.AsDouble();
  assert(timediff>0);
  double nevtmin=float(m_nevt-m_nevtMin);
  double rate=double(m_nevt)/timediff;
  double tmin=currenttime.AsDouble()-m_oneminago;
  double ratemin=nevtmin/tmin;
  m_oneminago=currenttime.AsDouble();
  m_nevtMin=m_nevt;
  char labelstring[128];
  sprintf(labelstring,"%.2f Hz",rate);
  m_rate->SetText(labelstring);
  sprintf(labelstring,"%.2f Hz",ratemin);
  m_irate->SetText(labelstring);
}

int CosmicGui::openFile(PixScan *scan){
  struct stat stFileInfo;
  int intStat;
  intStat = stat(m_globalconf->getDataDir(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Data directory "<<m_globalconf->getDataDir()<<" does not exist"<<std::endl;
    return 1;
  }
  char runnum[128];
  sprintf(runnum,"%06d",m_globalconf->getRunNumber());
  TString runname;
  runname = (std::string("cosmic_")+runnum);
  TString rcdir(TString("/")+runname);
  rcdir=m_globalconf->getDataDir()+rcdir;
  m_fullpath=std::string(rcdir.Data())+"/"+runname+".raw";
  m_rootfile=std::string(rcdir.Data())+"/"+runname+".root";
  m_tempfile=std::string(rcdir.Data())+"/"+"temperatures.txt";
  TString rcconfig(rcdir+"/globalconfig.txt");
  std::string topconfigname=m_globalconf->getConfigName()+".cfg";
  TString topconfig(rcdir+"/"+topconfigname);
  TString scanconfig(rcdir+"/scanconfig_"+runname+".txt");
  // Attempt to get the file attributes
  intStat = stat(rcdir.Data(),&stFileInfo);
  if(intStat == 0) { //File exists
    int retcode;
    new TGMsgBox(gClient->GetRoot(), 0, "Attention", "Data file exists. Overwrite?",
		 kMBIconExclamation, kMBYes | kMBCancel,&retcode); 
    if(retcode==kMBYes){
      char command[512];
      sprintf(command, "rm -rf %s",rcdir.Data());
      system(command);
    } else {
      return 1;
    }
  }
   if(not m_eudetfile->IsOn() && not m_rawfile->IsOn() && not m_writeRootFile->IsOn()){
      int retcode;
      new TGMsgBox(gClient->GetRoot(), 0, "Attention", "No file format has been selected. Is this OK?",
		 kMBIconExclamation, kMBYes | kMBCancel,&retcode); 
      if(retcode==kMBYes){
        char command[512];
        sprintf(command, "rm -rf %s",rcdir.Data());
        system(command);
      } 
      else {
	return 1;
      }
    }  

  // make new run directory
  mkdir (rcdir.Data(),0777);

  // copy configuration
  writeConfig(rcconfig.Data());

  m_globalconf->copyConfig(topconfig.Data());
  std::ofstream sc(scanconfig.Data());
  ipc::ScanOptions options;
  scan->convertScanConfig(options);
  scan->setRunNumber(m_globalconf->getRunNumber());
  scan->dump(sc, options);
  std::string oldcfg=std::string(rcdir.Data())+"/config";
  mkdir (oldcfg.c_str(),0777);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      m_config[i]->copyConfig(oldcfg.c_str());
    }
  }
  return 0;
}

int CosmicGui::startRun(){
  char name[512];
  int newrunno=m_globalconf->incrementRunNumber();
  std::cerr<<"Starting RCE run " << newrunno << std::endl;
  writeStdConfig(); // remember in case we crash  
  const std::string port = "33000";
  if (m_realTime == true)
  {
    std::stringstream ss("tcp://");
    ss<<getenv("ORBHOST");
    ss << ":" << port;
    std::string address = ss.str();
    std::cout << "<CosmicGui::startRun> : Server address is: " << address << std::endl;
    sprintf(name,"CosmicGui|%05d|%s",newrunno,address.c_str());
  }
  //configure all valid modules
  std::map<int,unsigned int> linkmap;
  std::map<int,int> rcemap;
  std::map<int, unsigned long long> multiplexer;
  //bool valid=false;
  //set up RCEs first
  m_controller.removeAllRces();
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      m_controller.addRce(m_config[i]->getRce());
      rcemap[m_config[i]->getRce()]=1;
      multiplexer[m_config[i]->getRce()]=0;
    }
  }
  //now set up modules

  for(size_t i=0;i<m_moduleinfo.size();i++){
    delete m_moduleinfo[i]->getFormatter();
    delete m_moduleinfo[i];
  }
  m_moduleinfo.clear();
  m_rce.clear();
  if(rcemap.size()==0){
    std::cout<<"No modules included. Adding RCE 0 in case you want to run without modules"<<std::endl;
    m_controller.addRce(0);
    m_rce.push_back(0);
    rcemap[0]=1;
  }
  //Default Trigger IF
  int retval=m_controller.setupTrigger();
  if(retval!=0){//Object not found, i.e. bad RCE specified.
    new TGMsgBox(gClient->GetRoot(), 0, "Attention", 
		 Form("RCE %d specified in config does not exist.", retval-1000), 
		 kMBIconExclamation, kMBOk); 
    return 1;
  }
  int dutmult=m_ntrg->GetIntNumber();
  dutmult==0 ? dutmult=15 : dutmult=dutmult-1;
  int telmult=m_ntrgtel->GetIntNumber();
  telmult==0 ? telmult=15 : telmult=telmult-1;
  m_controller.removeAllModules();
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      //valid=true;
      unsigned rce=m_config[i]->getRce();
      unsigned outlink=m_config[i]->getOutLink();
      assert(linkmap.find(outlink+1000*rce)==linkmap.end());
      linkmap[outlink+1000*rce]=1;
      unsigned inlink=m_config[i]->getInLink();
      int id=m_config[i]->getId();
      const char* modname=m_config[i]->getName();
      //Multiplexer optimization parameters
      if(std::string(modname).substr(0,9)=="Telescope"){
	multiplexer[rce]|=telmult<<(outlink*4);
      }else{
	multiplexer[rce]|=dutmult<<(outlink*4);
      }
      if(m_config[i]->getType()=="FEI4A"){
	printf("FEI4A: addModule (%s, %d, %d, %d, %d)\n",modname, 0, inlink, outlink, rce);
	m_controller.addModule(modname, "FEI4A",id, inlink, outlink, rce, "FEI4A");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 1, 336, 80, new FEI4AFormatter(id)));
	m_rce.push_back(rce);
	ipc::PixelFEI4AConfig *cfg=m_config[i]->getFEI4AConfig();
	m_controller.downloadModuleConfig(rce, id,*cfg);
	    //	    assert(m_controller.writeHWregister(15,0x80)==0); //setup mux
      } else if(m_config[i]->getType()=="FEI4B"){
	ipc::PixelFEI4BConfig *cfg=m_config[i]->getFEI4BConfig();
	AbsFormatter* formatter=new FEI4BFormatter(id);
	boost::property_tree::ptree *pt=new boost::property_tree::ptree;
	pt->put("HitDiscCnfg",cfg->FEGlobal.HitDiscCnfg);
	formatter->configure(pt);
	delete pt;
	printf("FEI4B: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, rce);
	m_controller.addModule(modname, "FEI4B",id, inlink, outlink, rce, "FEI4B");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 1, 336, 80, formatter));
	m_rce.push_back(rce);
	m_controller.downloadModuleConfig(rce, id,*cfg);
      } else if(m_config[i]->getType()=="FEI3"){
	printf("FEI3: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, rce);
	m_controller.addModule(modname, "FEI3",id, inlink, outlink, rce, "JJ");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 16, 160, 18, new JJFormatter(id)));
	m_rce.push_back(rce);
	ipc::PixelModuleConfig *cfg=m_config[i]->getModuleConfig();
	m_controller.downloadModuleConfig(rce, id,*cfg);
      } else if(m_config[i]->getType()=="Hitbus"){
        printf("Hitbus: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, rce);
	m_controller.addModule(modname, "Hitbus",id, inlink, outlink, rce, "");
	ipc::HitbusModuleConfig *cfg=m_config[i]->getHitbusConfig();
	m_controller.downloadModuleConfig(rce, id ,*cfg);
      } else if(m_config[i]->getType()=="HPTDC"){
	ipc::AFPHPTDCModuleConfig *cfg=m_config[i]->getHPTDCConfig();
        printf("AFP-HPTDC: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, rce);
	AbsFormatter* formatter=new AFPHPTDCFormatter(id);
	//this formatter needs to be configured.
	boost::property_tree::ptree *pt=new boost::property_tree::ptree;
	for(int l=0;l<2;l++){
	  for(int j=0;j<12;j++){
	    for(int k=0;k<1024;k++){
	      char name[32];
	      sprintf(name, "c_%d_%d_%d", l, j, k);
	      pt->put(name, cfg->calib[l][j][k]);
	    }
	  }
	}
	formatter->configure(pt);
	delete pt;
	m_controller.addModule(modname, "HPTDC",id, inlink, outlink, rce, "");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 1, 12, 1, formatter));
	m_rce.push_back(rce);
	m_controller.downloadModuleConfig(rce, id ,*cfg);
      } else {
	std::cout<<"Unknown config type"<<std::endl;
	assert(0);
      }
    }
  }

  unsigned goodHSIOconnection;
  for (std::map <int, int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    goodHSIOconnection=m_controller.writeHWregister(rce, 3,0); //Mode 0=normal 1=tdccalib 2=eudaq
    assert(goodHSIOconnection==0);
    // Reset delays
    goodHSIOconnection=m_controller.writeHWregister(rce, 7,0);
    assert(goodHSIOconnection==0);
    //Delay 0
    for(int i=0;i<m_delay0->GetIntNumber();i++){
      goodHSIOconnection=m_controller.writeHWregister(rce, 5,1);
      assert(goodHSIOconnection==0);
    }
    //Delay 1
    for(int i=0;i<m_delay1->GetIntNumber();i++){
      goodHSIOconnection=m_controller.writeHWregister(rce, 5,2);
      assert(goodHSIOconnection==0);
    }
    //Delay 2
    for(int i=0;i<m_delay2->GetIntNumber();i++){
      goodHSIOconnection=m_controller.writeHWregister(rce, 5,4);
      assert(goodHSIOconnection==0);
    }
    //Delay 3
    for(int i=0;i<m_delay3->GetIntNumber();i++){
      goodHSIOconnection=m_controller.writeHWregister(rce, 5,8);
      assert(goodHSIOconnection==0);
    }
    //Hitbus delay
    goodHSIOconnection=m_controller.writeHWregister(rce, 28,m_hbdelay->GetIntNumber());
    assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 29,m_hbdelaym->GetIntNumber());
    assert(goodHSIOconnection==0);
    //Multiplexer optimization
    //goodHSIOconnection=m_controller.writeHWregister(rce, 22,multiplexer[rce]&0xffffffff);
    //assert(goodHSIOconnection==0);
    //goodHSIOconnection=m_controller.writeHWregister(rce, 23,multiplexer[rce]>>32);
    //assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 23,0xf0000000); //TDC multiplicity 16
    assert(goodHSIOconnection==0);

    //Configure phase busy trigger
    int phaseBusySel = 0;
    phaseBusySel |= int(m_phasebusyen->IsOn());
    phaseBusySel |= m_phasebusystate<<1;
    if (it == rcemap.begin()) {
        std::cout << "PhaseBusy trigger " << ((phaseBusySel&1) ? "ON" : "OFF") << " (Mask: " <<
                      bool(phaseBusySel&4) << bool(phaseBusySel&2) << ")" << std::endl;
    }
    goodHSIOconnection=m_controller.writeHWregister(rce, 34,phaseBusySel); // set busy 3 out of 4 phases
    assert(goodHSIOconnection==0);
    //goodHSIOconnection=m_controller.writeHWregister(rce, 33, 100); // buffer length records
    //assert(goodHSIOconnection==0);
    //Configure Temperature ADC readout
    goodHSIOconnection=m_controller.writeHWregister(rce, 31,m_tempfreq->GetSelected());
    assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 32,m_tempenable->IsOn());
    assert(goodHSIOconnection==0);

    //Disc Buttons
      
    unsigned N_ANDbtn=0;
    for (int i=0;i<4;i++){
      if(m_NAND[i]->IsOn()){
	N_ANDbtn|=(1<<i);
      }
      if(m_NANDlogic[i]->IsOn()){
	N_ANDbtn|=(1<<(i+8));
      }
    }
    unsigned extlogic=0;
    if(m_HSIO1->IsOn()&&!m_HSIO2->IsOn())extlogic=0; //for completeness
    else if (!m_HSIO1->IsOn() && m_HSIO2->IsOn())extlogic=1;
    else if (m_HSIO1->IsOn() && m_HSIO2->IsOn() && m_hsiostate==0)extlogic=2;
    else if (m_HSIO1->IsOn() && m_HSIO2->IsOn() && m_hsiostate==1)extlogic=3;
    N_ANDbtn|=extlogic<<14;
    goodHSIOconnection=m_controller.writeHWregister(rce, 6, N_ANDbtn);
    // Hitbus setup
    goodHSIOconnection=m_controller.writeHWregister(rce, 30, m_hbstate);
    m_controller.sendHWcommand(rce, 17); // Tell the HSIO that the RCE is present.
  }
  
  for (int k=0;k<2;k++){
    if(rcemap.find(m_hbrce[k]->GetIntNumber())==rcemap.end())continue;
    unsigned hitbusconfig=0;
    for (int i=0;i<2;i++){
      for (int j=0;j<3;j++)
	if(m_tp[k][i*3+j]->IsOn())hitbusconfig|=(1<<(i*4+j));
    }
    if(m_tastate[k]==1)hitbusconfig|=(1<<3);
    if(m_tbstate[k]==1)hitbusconfig|=(1<<7);
    if(m_tabstate[k]==1)hitbusconfig|=(1<<8);
    m_controller.writeHWregister(m_hbrce[k]->GetIntNumber(), 21,hitbusconfig); 
  }


  PixScan scn(PixScan::COSMIC_DATA, PixLib::EnumFEflavour::PM_FE_I2);
  ipc::ScanOptions options;
  // Set calibration (i.e. output file) name
  // Or the ip address of the linux host: tcp://xxx.xxx.xxx.xxx

  scn.setName(name);
  // Override l1a latency
  scn.setLVL1Latency(m_l1alatency->GetIntNumber());
  scn.setSecondaryLatency(m_l1alatencytel->GetIntNumber());
  // Set number of triggers per L1A
  scn.setConsecutiveLvl1TrigA(0,m_ntrg->GetIntNumber());
  scn.setConsecutiveLvl1TrigA(1,m_ntrgtel->GetIntNumber());
  scn.setTriggerMask(m_scint->IsOn()|(m_cyclic->IsOn()<<1)|(m_eudet->IsOn()<<2)|
		     ((m_HSIO1->IsOn() || m_HSIO2->IsOn())<<3)|(m_hitbus->IsOn()<<4));
  //scn.setHitbusConfig(hitbusconfig);
  scn.setEventInterval(m_cperiod->GetIntNumber());
  scn.setStrobeLVL1Delay(m_trgdelay->GetIntNumber());
  scn.setDeadtime(m_deadtime->GetIntNumber());
  scn.convertScanConfig(options);

  int of=openFile(&scn);
  if(of!=0)return 1;

  monitoringGui->setup(true, m_writeRootFile->IsOn(), m_rootfile.c_str(),
		       m_templog->IsOn(), m_tempfile.c_str());

  if (m_realTime == true)
  {
    // Set-up to receive the data properly
    std::cout << "CosmicGui: starting CosmicDataReceiver at tcp://" << port << std::endl;
    std::vector<ModuleInfo*> modinfo=getModuleInfo();
    std::vector<int> rcesAll=getRces();
    unsigned runNo=m_globalconf->getRunNumber();
    IPCController* controller=getController();
    m_dataReceiver = new CosmicDataReceiver(this, controller, modinfo, rcesAll, 
					    runNo, "tcp://" + port, m_fullpath, m_eudetfile->IsOn(), m_rawfile->IsOn());
  }


  m_controller.downloadScanConfig(options);
  logScan();
  m_controller.startScan();

  return 0;
}
  
void CosmicGui::stopRun(){
  m_controller.stopWaitingForData();
  std::cout<<"called stopwaitingfordata"<<std::endl;
  monitoringGui->saveHistos(m_globalconf->getRunNumber(), m_globalconf->getDataDir());
  finishLogFile();
  bool idle=false;
  for(int i=0;i<50;i++){
    if(m_controller.getScanStatus()==0){
      std::cout<<"called getstatus"<<std::endl;
      idle=true;
      break;
    }
    usleep(100000);
  }
  assert(idle);
  /*
  std::cout<<"Verifying HW configuration"<<std::endl;
  int retval=m_controller.verifyModuleConfigHW();
  if(retval&0x100)std::cout<<"No formatter defined. Skipping register readback."<<std::endl;
  if(retval&0x1)std::cout<<"Global register readback failed for unknown reasons."<<std::endl;
  if(retval&0x2)std::cout<<"Global register readback differs from original configuration."<<std::endl;
  if(retval&0x4)std::cout<<"Pixel register read back wrong number of words."<<std::endl;
  if(retval&0x8)std::cout<<"Pixel register readback differs from original configuration."<<std::endl;
  if(retval==0)std::cout<<"Register verification successful."<<std::endl;
  */
  m_controller.resetFE(); //also sweeps events stuck in the buffer
  m_controller.removeAllModules();
  //unsigned goodHSIOconnection=m_controller.writeHWregister(3,0); //Normal operating mode
  //assert(goodHSIOconnection==0);
  //goodHSIOconnection=m_controller.writeHWregister(0,0); //Inhibit all channels
  //assert(goodHSIOconnection==0);
  //  unsigned l1counter=m_controller.readHWregister(11);
  //  unsigned counter4=m_controller.readHWregister(9);
  //  unsigned counter4m=m_controller.readHWregister(12);
  //  unsigned counter10m=m_controller.readHWregister(13);
  //  unsigned counter10=m_controller.readHWregister(10);
  //  unsigned counter10e=m_controller.readHWregister(14);
  //  unsigned tdccounter=m_controller.readHWregister(15);
  //  std::cout<<"Counter 4: "<<counter4<<" Counter 10: "<<counter10<<std::endl;
  //  std::cout<<"Counter 4m: "<<counter4m<<" Counter 10m: "<<counter10m<<std::endl;
  //  std::cout<<"Counter 10e: "<<counter10e<<" TDC counter: "<<(tdccounter&0xffff)<<std::endl;
  //  std::cout<<"l1counter: "<<l1counter<<std::endl;

  if (m_dataReceiver) delete m_dataReceiver;
  m_dataReceiver = 0;
  if(m_writeRootFile->IsDisabledAndSelected())monitoringGui->closeRootFile();
  if(m_templog->IsDisabledAndSelected())monitoringGui->closeTempFile();

  return;
}

void CosmicGui::verifyConfiguration(){
  std::cout<<"Verifying HW configuration"<<std::endl;
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      unsigned rce=m_config[i]->getRce();
      unsigned id=m_config[i]->getId();
      std::cout<<"Frontend "<<id<<" on RCE "<<rce<<" Outlink "<<m_config[i]->getOutLink()<<":"<<std::endl;
      int retval=m_controller.verifyModuleConfigHW(rce, id);
      if(retval&ModuleVerify::NO_FORMATTER)std::cout<<"No formatter defined. Skipping register readback."<<std::endl;
      if(retval&ModuleVerify::GLOBAL_READBACK_FAILED)std::cout<<"Global register readback failed for unknown reasons."<<std::endl;
      if(retval&ModuleVerify::GLOBAL_READBACK_DIFFERENT)std::cout<<"Global register readback differs from original configuration."<<std::endl;
      if(retval&ModuleVerify::PIXEL_WRONG_N_WORDS)std::cout<<"Pixel register read back wrong number of words."<<std::endl;
      if(retval&ModuleVerify::PIXEL_READBACK_DIFFERENT)std::cout<<"Pixel register readback differs from original configuration."<<std::endl;
      if(retval==ModuleVerify::OK)std::cout<<"Register verification successful."<<std::endl;
    }
  }
}
void CosmicGui::enableControls(bool on){
  m_tempenable->SetEnabled(on);
  m_templog->SetEnabled(on);
  m_tempfreq->SetEnabled(on);
  m_quit->SetEnabled(on);
  m_delay0->SetState(on);
  m_delay1->SetState(on);
  m_delay2->SetState(on);
  m_delay3->SetState(on);
  m_maxevents->SetState(on);
  m_l1alatency->SetState(on);
  m_l1alatencytel->SetState(on);
  m_ntrg->SetState(on);
  m_ntrgtel->SetState(on);
  m_trgdelay->SetState(on);
  m_deadtime->SetState(on);
  m_hbdelay->SetState(on);
  m_hbdelaym->SetState(on);
  m_cperiod->SetState(on);
  m_scint->SetEnabled(on);
  m_eudet->SetEnabled(on);
  m_cyclic->SetEnabled(on);
  m_HSIO1->SetEnabled(on);
  m_HSIO2->SetEnabled(on);
  m_hsiologic->SetState(on);
  m_hitbus->SetEnabled(on);
  m_hbinput->SetState(on);
  m_eudetfile->SetEnabled(on);
  m_rawfile->SetEnabled(on);
  m_writeRootFile->SetEnabled(on);
  m_maxtrue->SetEnabled(on);
  for (int k=0;k<2;k++){
    for (int i=0;i<6;i++)
      m_tp[k][i]->SetEnabled(on);
    m_talogic[k]->SetState(on);
    m_tblogic[k]->SetState(on);
    m_tablogic[k]->SetState(on);
    m_hbrce[k]->SetState(on);
  }
  for (int i=0;i<4;i++){
    m_NAND[i]->SetEnabled(on);
    m_NANDlogic[i]->SetEnabled(on);
  }
  for(int i=0;i<ConfigGui::MAX_MODULES;i++)
    m_config[i]->enableControls(on);
  m_globalconf->enableControls(on);
  m_phasebusysel->SetState(on);
  m_phasebusyen->SetEnabled(on);
}

void CosmicGui::writeConfig(const char* filename){
  std::cout<<"Writing config to "<<filename<<std::endl;
  std::ofstream ofile(filename);

  ofile<<m_trgdelay->GetIntNumber()<<std::endl;
  ofile<<m_deadtime->GetIntNumber()<<std::endl;
  ofile<<m_hbdelay->GetIntNumber()<<std::endl;
  ofile<<m_hbdelaym->GetIntNumber()<<std::endl;
  ofile<<m_l1alatency->GetIntNumber()<<std::endl;
  ofile<<m_l1alatencytel->GetIntNumber()<<std::endl;
  ofile<<m_ntrg->GetIntNumber()<<std::endl;
  ofile<<m_ntrgtel->GetIntNumber()<<std::endl;
  ofile<<m_delay0->GetIntNumber()<<std::endl;
  ofile<<m_delay1->GetIntNumber()<<std::endl;;
  ofile<<m_delay2->GetIntNumber()<<std::endl;
  ofile<<m_delay3->GetIntNumber()<<std::endl;
  ofile<<m_maxevents->GetIntNumber()<<std::endl;
  ofile<<m_scint->IsOn()<<std::endl;
  ofile<<m_cyclic->IsOn()<<std::endl;
  ofile<<m_eudet->IsOn()<<std::endl;
  ofile<<m_HSIO1->IsOn()<<std::endl;
  ofile<<m_HSIO2->IsOn()<<std::endl;
  ofile<<m_hsiostate<<std::endl;
  ofile<<m_hitbus->IsOn()<<std::endl;
  ofile<<m_hbstate<<std::endl;
  ofile<<m_cperiod->GetIntNumber()<<std::endl;
  ofile<<m_maxtrue->IsOn()<<std::endl;
  for (int k=0;k<2;k++){
    for (int i=0;i<6;i++)
      ofile<<m_tp[k][i]->IsOn()<<std::endl;
    ofile<<m_tastate[k]<<std::endl;
    ofile<<m_tbstate[k]<<std::endl;
    ofile<<m_tabstate[k]<<std::endl;
    ofile<<m_hbrce[k]->GetIntNumber()<<std::endl;
  }
  for (int i=0;i<4;i++){
    ofile<<m_NAND[i]->IsOn()<<std::endl;
  }
  for (int i=0;i<4;i++){
    ofile<<m_NANDlogic[i]->IsOn()<<std::endl;
  }
  ofile<<m_eudetfile->IsOn()<<std::endl;
  ofile<<m_rawfile->IsOn()<<std::endl;
  ofile<<m_writeRootFile->IsOn()<<std::endl;
  ofile<<m_tempenable->IsOn()<<std::endl;
  ofile<<m_templog->IsOn()<<std::endl;
  ofile<<m_tempfreq->GetSelected()<<std::endl;
  ofile<<m_phasebusyen->IsOn()<<std::endl;
  ofile<<m_phasebusystate<<std::endl;
  m_globalconf->writeGuiConfig(ofile);
}

void CosmicGui::readConfig(const char* filename){
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes
  intStat = stat(filename,&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<std::endl<<"No config file "<<filename<<" found."<<std::endl;
    return;
  }
  std::ifstream ifile(filename);
  int td, dt, hb, lat, nt, del0, del1, del2, del3, maxevnt, maxt, trg, trgper, fi, pbusy;
  ifile>>td;
  m_trgdelay->SetIntNumber(td);
  ifile>>dt;
  m_deadtime->SetIntNumber(dt);
  ifile>>hb;
  m_hbdelay->SetIntNumber(hb);
  ifile>>hb;
  m_hbdelaym->SetIntNumber(hb);
  ifile>>lat;
  m_l1alatency->SetIntNumber(lat);
  ifile>>lat;
  m_l1alatencytel->SetIntNumber(lat);
  ifile>>nt;
  m_ntrg->SetIntNumber(nt);
  ifile>>nt;
  m_ntrgtel->SetIntNumber(nt);
  ifile>>del0;
  m_delay0->SetIntNumber(del0);
  ifile>>del1;
  m_delay1->SetIntNumber(del1);
  ifile>>del2;
  m_delay2->SetIntNumber(del2);
  ifile>>del3;
  m_delay3->SetIntNumber(del3);
  ifile>>maxevnt;
  m_maxevents->SetIntNumber(maxevnt);
  ifile>>trg;
  m_scint->SetOn(trg);
  ifile>>trg;
  m_cyclic->SetOn(trg);
  ifile>>trg;
  m_eudet->SetOn(trg);
  ifile>>trg;
  m_HSIO1->SetOn(trg);
  ifile>>trg;
  m_HSIO2->SetOn(trg);
  ifile>>trg;
  m_hsiostate=trg;
  m_hsiologic->SetButton(trg);
  ifile>>trg;
  m_hitbus->SetOn(trg);
  ifile>>trg;
  m_hbstate=trg;
  m_hbinput->SetButton(trg);
  ifile>>trg;
  m_cperiod->SetIntNumber(trg);
  ifile>>maxt;
  m_maxtrue->SetOn(maxt);
  for (int k=0;k<2;k++){
    for (int i=0;i<6;i++){
      ifile>>trgper;
      m_tp[k][i]->SetOn(trgper);
    }
    ifile>>trgper;
    m_tastate[k]=trgper;
    m_talogic[k]->SetButton(trgper);
    ifile>>trgper;
    m_tbstate[k]=trgper;
    m_tblogic[k]->SetButton(trgper);
    ifile>>trgper;
    m_tabstate[k]=trgper;
    m_tablogic[k]->SetButton(trgper);
    ifile>>trgper;
    m_hbrce[k]->SetIntNumber(trgper);
  }
  for (int i=0;i<4;i++){
    ifile>>trgper;
    m_NAND[i]->SetOn(trgper);
  }
  for (int i=0;i<4;i++){
    ifile>>trgper;
    m_NANDlogic[i]->SetOn(trgper);
  }
  ifile>>fi;
  m_eudetfile->SetOn(fi);
  ifile>>fi;
  m_rawfile->SetOn(fi);
  ifile>>fi;
  m_writeRootFile->SetOn(fi);
  ifile>>fi;
  m_tempenable->SetOn(fi);
  ifile>>fi;
  m_templog->SetOn(fi);
  ifile>>fi;
  m_tempfreq->Select(fi,0);
  ifile>>pbusy;
  m_phasebusyen->SetOn(pbusy);
  ifile>>pbusy;
  m_phasebusystate=pbusy;
  m_phasebusysel->SetButton(pbusy);
  m_globalconf->readGuiConfig(ifile);
}

void CosmicGui::quit(){
  writeStdConfig();
  gApplication->Terminate(0);
}
  
void CosmicGui::writeStdConfig(){
  std::string rcfile=getenv("HOME");
  rcfile+="/.cosmicDaq.rc";
  writeConfig(rcfile.c_str());
}

void CosmicGui::handleFileMenu(int item){
  TGFileInfo fileinfo;
  const char* types[]={"Teststand config", "*.tcf", "All files", "*", 0,0};
  fileinfo.fFileTypes=types;
  if(item==QUIT)quit();
  else if(item==LOAD){
    new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
    if(!fileinfo.fFilename){
      printf("Scheisse\n");
      return;
    }
    readConfig(fileinfo.fFilename);
  }
  else if(item==SAVE){
    new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fileinfo);
    if(!fileinfo.fFilename){
      printf("Scheisse\n");
      return;
    }
    writeConfig(fileinfo.fFilename);
  }
}

void CosmicGui::logScan(){
  LogData logdata;
  m_scanLog->setAuthor("cosmicGui");
  m_scanLog->setScanName("CosmicData");
  m_scanLog->setRunNumber(m_globalconf->getRunNumber());
  m_scanLog->setDebugging(false);
  //logdata.comment=m_comment->GetText();
  logdata.exported=false;
  logdata.saved=true;
  logdata.datapath=m_globalconf->getDataDir();
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    std::ostringstream ss;
    if(m_config[i]->isIncluded()){
      ss<<"Module: "<<m_config[i]->getModuleId()<<" FE: "<< m_config[i]->getId()<<" Cfg: "<< m_config[i]->getFilename();
      logdata.configs.push_back(ss.str());
    }
  }
  m_scanLog->logScan(logdata);
}
void CosmicGui::finishLogFile(){
  std::string runstatus="OK";
  std::string lstatus="Done";
  m_scanLog->finishLogFile(runstatus, lstatus);
}

void CosmicGui::switchOn(int panel, bool on){
  int modperpanel=ConfigGui::MAX_MODULES/4;
  for (int i=panel*modperpanel;i<(panel+1)*modperpanel;i++){
    m_config[i]->setIncluded(on);
  }
}
  


//====================================================================
int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  try {
    IPCCore::init( argc, argv );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }
  
//
// Declare command object and its argument-iterator
//       
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in.");
  CmdArgBool start('s', "start", "Start run when GUI comes up.");
  CmdArgInt one_run('o', "onerun", "onerun-limit", "Do one run and exit.");
  CmdLine  cmd(*argv, &partition_name, &start, &one_run,  NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
//
// Parse arguments
//
  int exitAfterEvents=-1;
  cmd.parse(arg_iter);
  const char *p_name=getenv("TDAQ_PARTITION");
  if(one_run>0) exitAfterEvents=(int)one_run; 
  if(p_name==NULL) p_name="rcetest";
  if(! partition_name.isNULL()) p_name= (const char*)partition_name;
  try{
    IPCPartition   p(p_name );
    IPCController controller(p);
    new IPCHistoManager(p,"RceIsServer", "dummy");
    
    gROOT->SetStyle("Plain");
    TApplication theapp("app",&argc,argv);
    new CosmicGui(controller, start, gClient->GetRoot(),800, 735, exitAfterEvents);
    theapp.Run();
    return 0;
  }catch(...){
    std::cout<<"Partition "<<p_name<<" does not exist."<<std::endl;
    exit(0);
  }
}


