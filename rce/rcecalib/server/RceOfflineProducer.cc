#include "rcecalib/server/RceOfflineProducer.hh"
#include "rcecalib/server/CosmicDataReceiver.hh"
#include "rcecalib/server/CosmicDataReceiver.hh"
#include "rcecalib/config/ModuleInfo.hh"
#include "rcecalib/eudaq/ProducerIF.hh"
#include "rcecalib/eudaq/RawDataEvent.hh"
#include "rcecalib/eudaq/Utils.hh"
#include "rcecalib/eudaq/Exception.hh"
#include "rcecalib/server/PixScan.hh"
#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/FEI4AConfigFile.hh"
#include "rcecalib/server/FEI4BConfigFile.hh"
#include "rcecalib/server/HitbusConfigFile.hh"
#include "rcecalib/config/FEI3/JJFormatter.hh"
#include "rcecalib/config/FEI4/FEI4AFormatter.hh"
#include "rcecalib/config/FEI4/FEI4BFormatter.hh"
#include <cmdl/cmdargs.h>
#include "rcecalib/util/RceName.hh"
#include <iostream>
#include <ostream>
#include <vector>
#include <stdio.h>
#include <sys/stat.h> 
#include <ipc/core.h>

#include <netdb.h>

namespace{
  static const std::string EVENT_TYPE = "APIX-CT";

  void startScan(void* arg){
    IPCController* controller=(IPCController*)arg;
    controller->startScan();
  }
}
using namespace RCE;

RceOfflineProducer::RceOfflineProducer(const std::string & name, const std::string & runcontrol, IPCPartition& p, int rce) 
  : eudaq::Producer(name, runcontrol), m_run(0), m_PlaneMask(0), m_conseq(1), m_rce(rce), m_controller(new IPCController(p)),
    m_options(0), m_datareceiver(0){
  std::cout<<"Rce producer on RCE "<<m_rce<<std::endl;
}

RceOfflineProducer::~RceOfflineProducer(){
  delete m_controller;
  if (m_datareceiver) delete m_datareceiver;
  m_datareceiver = 0;
}
// This gets called whenever the DAQ is configured
void RceOfflineProducer::OnConfigure(const eudaq::Configuration & config) {
  std::cout << "Configuring: " << config.Name() << std::endl;
    std::cout<<config<<std::endl;
  char msg[64];
  try{
    //Default Trigger IF
    for(size_t i=0;i<m_moduleinfo.size();i++){
      delete m_moduleinfo[i]->getFormatter();
      delete m_moduleinfo[i];
    }
    m_moduleinfo.clear();
    m_controller->removeAllRces();
    m_controller->addRce(m_rce);
    m_controller->setupTrigger();
    m_controller->removeAllModules();
    m_numboards = config.Get("nFrontends", -1);
    if(m_numboards==-1)throw eudaq::Exception("nFrontends not defined");
    m_link.clear();
    m_sensor.clear();
    m_pos.clear();
    m_stype.clear();
    for(int i=0;i<m_numboards;i++){
      int outlink = config.Get("Module" + eudaq::to_string(i) + ".OutLink", "OutLink", -1);
      if(outlink==-1){
	sprintf(msg,"Module %d: Outlink is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_link.push_back(outlink);
      int inlink = config.Get("Module" + eudaq::to_string(i) + ".InLink", "InLink", -1);
      if(inlink==-1){
	sprintf(msg,"Module %d: Inlink is not defined.",i);
	throw eudaq::Exception(msg);
      }
      int id = config.Get("Module" + eudaq::to_string(i) + ".FEID", "FEID", -1);
      if(id==-1){
	sprintf(msg,"Frontend %d: FEID is not defined.",i);
	throw eudaq::Exception(msg);
      }
      int sid = config.Get("Module" + eudaq::to_string(i) + ".SensorId", "SensorId", -1);
      if(sid==-1){
	sprintf(msg,"Frontend %d: sensor id is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_sensor.push_back(sid);
      int lr = config.Get("Module" + eudaq::to_string(i) + ".Position", "Position", -1);
      if(lr==-1){
	sprintf(msg,"Frontend %d: Position is not defined.",i);
	throw eudaq::Exception(msg);
      }
      m_pos.push_back(lr);
      int stype = config.Get("Module" + eudaq::to_string(i) + ".ModuleType", "ModuleType", -1);
      if(stype==-1){
	sprintf(msg,"Frontend %d: Module sensor type is not defined.",i);
	throw eudaq::Exception(msg);
      }
      if(stype<1 || stype>4){ //only 1, 2, and 4-chip modules and FEI3 chips are defined at this point
	sprintf(msg,"Frontend %d: Module sensor type %d does not exist.", i, stype);
	throw eudaq::Exception(msg);
      }
      if(stype!=3 && lr>=stype){
	sprintf(msg,"Frontend %d: Position %d does not exist in %d-chip sensors.", i, lr, stype);
	throw eudaq::Exception(msg);
      }
       if(m_stype.find(sid)==m_stype.end())m_stype[sid]=stype;
      else if(m_stype[sid]!=stype){
	sprintf(msg, "Module %d: Definition of module type clashes with the definition for a different FE of the same sensor", i);
	throw eudaq::Exception(msg);
      }
      std::string modtype = config.Get("Module" + eudaq::to_string(i) + ".Type", "Type", "");
      if(modtype==""){
	sprintf(msg,"Module %d: Module frontend type is not defined.",i);
	throw eudaq::Exception(msg);
      }
      std::string filename = config.Get("Module" + eudaq::to_string(i) + ".File", "File", "");
      if(filename==""){
	sprintf(msg,"Module %d: Configuration filename is not defined.",i);
	throw eudaq::Exception(msg);
      }

      struct stat stFileInfo;
      int intStat;
      // Attempt to get the file attributes
      intStat = stat(filename.c_str(),&stFileInfo);
      if(intStat!=0) { //file does not exist
	sprintf(msg, "Configuration: File %s does not exist.",filename.c_str());
	throw eudaq::Exception(msg);
      }
      // create module via IPC
      char modname[32];
      sprintf(modname,"RCE%d_module_%d",m_rce, outlink);
      if(modtype=="FEI4A"){
	printf("FEI4A: addModule (%s, %d, %d, %d, %d)\n",modname, 0, inlink, outlink, m_rce);
	m_controller->addModule(modname, "FEI4A",id, inlink, outlink, m_rce, "FEI4A");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 1, 336, 80, new FEI4AFormatter(id)));
	FEI4AConfigFile fei4af;
	ipc::PixelFEI4AConfig *cfg=new ipc::PixelFEI4AConfig;
	fei4af.readModuleConfig(cfg, filename);
	m_controller->downloadModuleConfig(m_rce, id,*cfg);
	delete cfg;
	    //	    assert(m_controller.writeHWregister(15,0x80)==0); //setup mux
      } else if(modtype=="FEI4B"){
	printf("FEI4B: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, m_rce);
	m_controller->addModule(modname, "FEI4B",id, inlink, outlink, m_rce, "FEI4B");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 1, 336, 80, new FEI4BFormatter(id)));
	FEI4BConfigFile fei4bf;
	ipc::PixelFEI4BConfig *cfg=new ipc::PixelFEI4BConfig;
	fei4bf.readModuleConfig(cfg, filename);
	m_controller->downloadModuleConfig(m_rce, id,*cfg);
	delete cfg;
      }else if(modtype=="FEI3"){
	printf("FEI3: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, m_rce);
	m_controller->addModule(modname, "FEI3",id, inlink, outlink, m_rce, "JJ");
	m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 16, 160, 18, new JJFormatter(id)));
	TurboDaqFile turbo;
	ipc::PixelModuleConfig *cfg=new ipc::PixelModuleConfig;
	turbo.readModuleConfig(cfg, filename);
	m_controller->downloadModuleConfig(m_rce, id,*cfg);
	delete cfg;
      } else if(modtype=="Hitbus"){
        printf("Hitbus: addModule (%s, %d, %d, %d, %d)\n",modname, id, inlink, outlink, m_rce);
        m_controller->addModule(modname, "Hitbus",id, inlink, outlink, m_rce, "");
	//m_moduleinfo.push_back(new ModuleInfo(modname, id, inlink, outlink, 0, 0, 0, 0));
	HitbusConfigFile hitbusf;
	ipc::HitbusModuleConfig *cfg=new ipc::HitbusModuleConfig;
	hitbusf.readModuleConfig(cfg, filename);
        m_controller->downloadModuleConfig(m_rce, id,*cfg);
      }else{
        std::cout<<"Unknown config"<<std::endl;
      }
    } 
    // set up hardware trigger
    unsigned goodHSIOconnection=0;
    goodHSIOconnection=m_controller->writeHWregister(m_rce, 3,2); //runmode 0=normal 1=tdccalib 2=eudaq assert(serstat==0);
    assert(goodHSIOconnection==0);
    m_controller->sendHWcommand(m_rce, 17); // Tell the HSIO that the RCE is present.
    //serstat=m_controller->writeHWregister(0, 22,1); // coincidence l1
    //serstat=m_controller->writeHWregister(0, 23,1); // coincidence hitbus
    //serstat=m_controller->writeHWregister(0, 24,3); // coincidence hitbus
    //m_controller.writeHWregister(3,0); //Normal mode
    
    // Scan configuration
    std::cout<<"Deleting scan options "<<m_options<<std::endl;
    delete m_options;
    PixScan scn(PixScan::COSMIC_DATA, PixLib::EnumFEflavour::PM_FE_I2);
    m_options=new ipc::ScanOptions;
    scn.setName("TCP"); // send data over the network rather than to file
    // Override l1a latency
    int latency = config.Get("Latency", -1);
    if(latency==-1){
      throw eudaq::Exception("Latency is not defined.");
    }
    scn.setLVL1Latency(latency);
    m_conseq = config.Get("ConseqTriggers", -1);
    if(m_conseq==(unsigned)-1){
      throw eudaq::Exception("Number of consecutive triggers is not defined.");
    }
    scn.setConsecutiveLvl1TrigA(0,m_conseq);
    int trgdelay = config.Get("Trgdelay", -1);
    if(trgdelay==-1){
      throw eudaq::Exception("Trigger delay is not defined.");
    }
    scn.setStrobeLVL1Delay(trgdelay);
    int deadtime = config.Get("Deadtime", 0);
    scn.setDeadtime(deadtime);
    int cyclic = config.Get("Cyclic_Period", 40000000);
    scn.setEventInterval(cyclic);
    scn.setTriggerMask(4); // eudet=4

    scn.convertScanConfig(*m_options);
    //m_controller->downloadScanConfig(*m_options);
    // At the end, set the status that will be displayed in the Run Control.
    SetStatus(eudaq::Status::LVL_OK, "Configured (" + config.Name() + ")");
  } catch (const std::exception & e) {
    printf("Caught exception: %s\n", e.what());
    SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
  } catch (...) {
    printf("Unknown exception\n");
    SetStatus(eudaq::Status::LVL_ERROR, "Configuration Error");
  }
}

  // This gets called whenever a new run is started
  // It receives the new run number as a parameter
void RceOfflineProducer::OnStartRun(unsigned param) {
    m_run = param;
    std::string port="33000";
    std::cout << "Start Run: " << m_run << std::endl;
    std::stringstream ss("tcp://");
    ss<<getenv("ORBHOST");
    ss << ":" << port;
    std::string address = ss.str();
    std::cout << "<CosmicGui::startRun> : Server address is: " << address << std::endl;
    char name[128];
    sprintf(name,"CosmicGui|%05d|%s",m_run,address.c_str());
    m_options->name=CORBA::string_dup(name);

    std::cout << "CosmicGui: starting CosmicDataReceiver at tcp://" << port << std::endl;
    std::vector<int> rcesAll;
    if(m_numboards>0) for(int i=0;i<m_numboards;i++)rcesAll.push_back(m_rce); //only one RCE
    else rcesAll.push_back(m_rce); // no FEs
    m_datareceiver = new CosmicDataReceiver(0, m_controller, m_moduleinfo, rcesAll, 
					    m_run, "tcp://" + port, "", false, false);

    m_controller->downloadScanConfig(*m_options);
    // It must send a BORE to the Data Collector
    eudaq::RawDataEvent bore(eudaq::RawDataEvent::BORE(EVENT_TYPE, m_run));
    // You can set tags on the BORE that will be saved in the data file
    // and can be used later to help decoding
    bore.SetTag("nFrontends", eudaq::to_string(m_numboards));
    bore.SetTag("consecutive_lvl1", eudaq::to_string(m_conseq));
    char tagname[128];
    for(int i=0;i<m_numboards;i++){
      sprintf(tagname, "OutLink_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_link[i]));
      sprintf(tagname, "SensorId_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_sensor[i]));
      sprintf(tagname, "Position_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_pos[i]));
      sprintf(tagname, "ModuleType_%d", i);
      bore.SetTag(tagname, eudaq::to_string(m_stype[m_sensor[i]]));
    }
    // Send the event to the Data Collector
    SendEvent(bore);
    // Enable writing events via tcp/ip
    ProducerIF::setProducer(this);
    //start run
    omni_thread::create(startScan,(void*)m_controller);
    //m_controller->startScan();

    std::cout<<"Started scan"<<std::endl;
    // At the end, set the status that will be displayed in the Run Control.
    SetStatus(eudaq::Status::LVL_OK, "Running");
  }

  // This gets called whenever a run is stopped
void RceOfflineProducer::OnStopRun() {
    std::cout << "Stopping Run" << std::endl;

    m_controller->stopWaitingForData();
    //wait for late events to trickle in
    sleep(1);
    //wait for scan to be complete
    bool idle=false;
    for(int i=0;i<50;i++){
      if(m_controller->getScanStatus()==0){
	std::cout<<"called getstatus"<<std::endl;
	idle=true;
	break;
      }
      usleep(100000);
    }
    if(idle==false)std::cout<<"Scan did not go into idle state"<<std::endl;
    //disable sending events via TCP/IP
    if (m_datareceiver) delete m_datareceiver;
    m_datareceiver = 0;

    ProducerIF::setProducer(0);
    // Send an EORE after all the real events have been sent
    // You can also set tags on it (as with the BORE) if necessary
    unsigned evnum=m_controller->getNEventsProcessed();
    SendEvent(eudaq::RawDataEvent::EORE(EVENT_TYPE, m_run, evnum));
    SetStatus(eudaq::Status::LVL_OK, "Stopped");
  }

  // This gets called when the Run Control is terminating,
  // we should also exit.
void RceOfflineProducer::OnTerminate() {
    std::cout << "Terminating..." << std::endl;
    if (m_datareceiver) delete m_datareceiver;
    m_datareceiver = 0;
    exit(0);
  }

int main ( int argc, char ** argv )
{
   CmdArgStr	partition_name	('p', "partition", "partition-name", "partition to work in.");
   CmdArgInt	rce_number	('r', "rce", "rce-number", "RCE to connect to", CmdArg::isREQ | CmdArg::isVALREQ);
   CmdArgStr	hostname	('d', "runcontrol", "runcontrol-host", "Run control hostname.", CmdArg::isREQ | CmdArg::isVALREQ);
 
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
        return 1;
    }
   
    // Declare command object and its argument-iterator
    CmdLine  cmd(*argv, &partition_name, &rce_number, &hostname, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);
  
       // Parse arguments

   cmd.parse(arg_iter);
   
   std::string rchost;
   hostent * ipAddrContainer = gethostbyname((const char*)hostname);
   if (ipAddrContainer != 0) {
     int nBytes;
     if (ipAddrContainer->h_addrtype == AF_INET) nBytes = 4;
     else if (ipAddrContainer->h_addrtype == AF_INET6) nBytes = 6;
     else {
       std::cout << "Unrecognized IP address type. Run not started." 
		 << std::endl;
       exit(0);
     }
     std::stringstream ss("tcp://");
     ss << "tcp://"; 
     for (int i = 0, curVal; i < nBytes; i++)
       {
	 curVal = static_cast<int>(ipAddrContainer->h_addr[i]);
	 if (curVal < 0) curVal += 256;
	 ss << curVal;
	 if (i != nBytes - 1) ss << ".";
       }
     ss<<":44000";
     rchost=ss.str();
   }else{
     std::cout<<"Bad IP address. Exiting."<<std::endl;
     exit(0);
   }
   const char *p_name=getenv("TDAQ_PARTITION");
   if(p_name==NULL) p_name="rcetest";
   if(! partition_name.isNULL()) p_name= (const char*)partition_name;
   int rce=(int)rce_number;
   IPCPartition   p(p_name );
   IPCController controller(p);
   new IPCHistoManager(p,"RceIsServer", "dummy");
   RceOfflineProducer producer("RceOfflineProducer", rchost.c_str(), p, rce);
   sleep(100000000);
}
