#include "rcecalib/server/ScanGui.hh"
#include "rcecalib/server/PixScan.hh"
#include "ScanOptions.hh"
#include <iostream>
using namespace RCE;

ScanGui::ScanGui(const char* name, const TGWindow *p, UInt_t w, UInt_t h, UInt_t options):
  TGVerticalFrame(p,w,h,options), m_scan(0), m_scanopt(0), m_scn(0), 
  m_thresholdTargetC(-1), m_targetChargeC(-1), m_targetValueC(-1){
  
  m_name=new TGLabel(this,name);
  AddFrame(m_name,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 10, 0));
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_name->SetTextFont(labelfont);
  
  TGLabel *cfg=new TGLabel(this,"Scan Type:");
  AddFrame(cfg,new TGLayoutHints(kLHintsCenterX|kLHintsTop, 2, 2, 10, 0));

  m_scan=new TGComboBox(this,100);
  AddFrame(m_scan,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,5,0));
  if(!m_initialized)initScanTypes();
  m_scan->Resize(150,20);
  //m_scan->Connect("Selected(Int_t)", "ScanGui", this, "selected(Int_t)");

  m_typestring="";
  //Injection
  TGHorizontalFrame *conf0 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf0,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *masklabel=new TGLabel(conf0,"Injection:");
  FontStruct_t maskfont= gClient->GetFontByName("-adobe-helvetica-bold-r-*-*-12-*-*-*-*-*-iso8859-1");
  masklabel->SetTextFont(maskfont);
  conf0->AddFrame(masklabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_dig=new TGLabel(conf0,"");
  conf0->AddFrame(m_dig,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  masklabel=new TGLabel(conf0,"Repetitions:");
  conf0->AddFrame(masklabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 2, 0));
  m_rep=new TGLabel(conf0,"");
  conf0->AddFrame(m_rep,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));

  // mask
  TGHorizontalFrame *conf1 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf1,new TGLayoutHints(kLHintsExpandX ));
  masklabel=new TGLabel(conf1,"Mask:");
  masklabel->SetTextFont(maskfont);
  conf1->AddFrame(masklabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  m_maskmode=new TGLabel(conf1,"");
  conf1->AddFrame(m_maskmode,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  TGLabel *mslabel=new TGLabel(conf1,"Number of Steps:");
  conf1->AddFrame(mslabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 2, 0));
  m_maskstages=new TGLabel(conf1,"");
  conf1->AddFrame(m_maskstages,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
  //Loops
  TGHorizontalFrame *loopframe[3];
  char loopstr[128];
  for(int i=0;i<3;i++){
    loopframe[i] = new TGHorizontalFrame(this, 2,2 );
    AddFrame(loopframe[i],new TGLayoutHints(kLHintsExpandX ));
    sprintf(loopstr, "Loop %d:",i);
    m_active[i]=new TGCheckButton(loopframe[i],loopstr);
    m_active[i]->SetFont(maskfont);
    m_active[i]->SetOn(0);
    m_active[i]->Connect("Toggled(Bool_t)", "ScanGui", this, "dummyAct(Bool_t)");
    loopframe[i]->AddFrame(m_active[i],new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
    m_par[i]=new TGLabel(loopframe[i],"");
    loopframe[i]->AddFrame(m_par[i],new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
    TGLabel *looplabel=new TGLabel(loopframe[i],"N:");
    loopframe[i]->AddFrame(looplabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 2, 0));
    m_steps[i]=new TGLabel(loopframe[i],"");
    loopframe[i]->AddFrame(m_steps[i],new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
    looplabel=new TGLabel(loopframe[i],"Lo:");
    loopframe[i]->AddFrame(looplabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 2, 0));
    m_low[i]=new TGLabel(loopframe[i],"");
    loopframe[i]->AddFrame(m_low[i],new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
    looplabel=new TGLabel(loopframe[i],"Hi:");
    loopframe[i]->AddFrame(looplabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 10, 2, 2, 0));
    m_high[i]=new TGLabel(loopframe[i],"");
    loopframe[i]->AddFrame(m_high[i],new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 2, 0));
    m_rce[i]=new TGCheckButton(loopframe[i],"RCE");
    m_rce[i]->SetOn(false);
    m_rceb[i]=false;
    loopframe[i]->AddFrame(m_rce[i],new TGLayoutHints(kLHintsRight|kLHintsCenterY, 10, 2, 2, 0));
    m_rce[i]->Connect("Toggled(Bool_t)", "ScanGui", this, "dummy(Bool_t)");
  }
  TGHorizontalFrame *conf2 = new TGHorizontalFrame(this, 2,2 );
  AddFrame(conf2,new TGLayoutHints(kLHintsExpandX ));
  TGLabel *linklabeltune=new TGLabel(conf2,"Tuning Parameters:");
  linklabeltune->SetTextFont(maskfont);
  conf2->AddFrame(linklabeltune,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  TGLabel *linklabel2=new TGLabel(conf2,"Target threshold:");
  conf2->AddFrame(linklabel2,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_thresholdTarget=new TGNumberEntry(conf2, 0, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 20000);
  conf2->AddFrame(m_thresholdTarget,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 0, 5));
  TGLabel *linklabel3=new TGLabel(conf2,"     Target charge:");
  conf2->AddFrame(linklabel3,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_targetCharge=new TGNumberEntry(conf2, 0, 5, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 50000);
  conf2->AddFrame(m_targetCharge,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 0, 5));
  TGLabel *linklabel4=new TGLabel(conf2,"     ToT target value:");
  conf2->AddFrame(linklabel4,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 0, 0));
  m_targetValue=new TGNumberEntry(conf2, 0, 3, -1, TGNumberFormat::kNESInteger, TGNumberFormat::kNEANonNegative, TGNumberFormat::kNELLimitMinMax, 0, 127);
  conf2->AddFrame(m_targetValue,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 0, 2, 0, 5));

}
  
void ScanGui::dummy(bool on){
  // disable clicking
  for(int i=0;i<3;i++)m_rce[i]->SetOn(m_rceb[i]);
}

void ScanGui::dummyAct(bool on){
  // disable clicking
  if(m_scn)
    for(int i=0;i<3;i++)m_active[i]->SetOn(m_scn->getLoopActive(i));
  else 
    for(int i=0;i<3;i++)m_active[i]->SetOn(false);
}

ScanGui::~ScanGui(){
  Cleanup();
}

void ScanGui::updateText(TGLabel* label, const char* newtext){
  //unsigned len=strlen(label->GetText()->GetString());
  label->SetText(newtext);
  //if (strlen(newtext)>len)Layout();
  Layout();
}

void ScanGui::enableControls(bool on){
  m_scan->SetEnabled(on);
  m_targetCharge->SetState(on);
  m_targetValue->SetState(on);
  m_thresholdTarget->SetState(on);
}
  

void ScanGui::print(std::ostream &os){
  getScanConfig();
  if(m_scanopt!=0){
    m_scn->dump(os, *m_scanopt);
  }
}

RCE::PixScan* ScanGui::getPixScan(){
  return m_scn;
}
  

ipc::ScanOptions *ScanGui::getScanConfig(){
  return m_scanopt;
}

void ScanGui::initScanTypes(){
  PixLib::EnumScanType scantypes;
  m_scantypes[PixScan::DIGITAL_TEST]=scantypes.lookup(PixScan::DIGITAL_TEST);
  m_scantypes[PixScan::ANALOG_TEST]=scantypes.lookup(PixScan::ANALOG_TEST);
  m_scantypes[PixScan::THRESHOLD_SCAN]=scantypes.lookup(PixScan::THRESHOLD_SCAN);
  m_scantypes[PixScan::CROSSTALK_SCAN]=scantypes.lookup(PixScan::CROSSTALK_SCAN);
  m_scantypes[PixScan::T0_SCAN]=scantypes.lookup(PixScan::T0_SCAN);
  m_scantypes[PixScan::TIMEWALK_MEASURE]=scantypes.lookup(PixScan::TIMEWALK_MEASURE);
  m_scantypes[PixScan::INTIME_THRESH_SCAN]=scantypes.lookup(PixScan::INTIME_THRESH_SCAN);
  m_scantypes[PixScan::DELAY_SCAN]=scantypes.lookup(PixScan::DELAY_SCAN);
  m_scantypes[PixScan::MEASUREMENT_SCAN]=scantypes.lookup(PixScan::MEASUREMENT_SCAN);
  m_scantypes[PixScan::VTHIN_SCAN]=scantypes.lookup(PixScan::VTHIN_SCAN);
  m_scantypes[PixScan::SELFTRIGGER]=scantypes.lookup(PixScan::SELFTRIGGER);
  m_scantypes[PixScan::EXTTRIGGER]=scantypes.lookup(PixScan::EXTTRIGGER);
  m_scantypes[PixScan::MULTISHOT]=scantypes.lookup(PixScan::MULTISHOT);
  m_scantypes[PixScan::STUCKPIXELS]=scantypes.lookup(PixScan::STUCKPIXELS);
  m_scantypes[PixScan::IF_TUNE]=scantypes.lookup(PixScan::IF_TUNE);
  m_scantypes[PixScan::FDAC_TUNE]=scantypes.lookup(PixScan::FDAC_TUNE);
  m_scantypes[PixScan::NOISESCAN]=scantypes.lookup(PixScan::NOISESCAN);
  m_scantypes[PixScan::GDAC_TUNE]=scantypes.lookup(PixScan::GDAC_TUNE);
  m_scantypes[PixScan::TDAC_TUNE]=scantypes.lookup(PixScan::TDAC_TUNE);
  m_scantypes[PixScan::TOT_TEST]=scantypes.lookup(PixScan::TOT_TEST);
  m_scantypes[PixScan::TOT_CALIB]=scantypes.lookup(PixScan::TOT_CALIB);
  m_scantypes[PixScan::CROSSTALK_SCAN]=scantypes.lookup(PixScan::CROSSTALK_SCAN);
  m_scantypes[PixScan::TWOTRIGGER_THRESHOLD]=scantypes.lookup(PixScan::TWOTRIGGER_THRESHOLD);
  m_scantypes[PixScan::TWOTRIGGER_NOISE]=scantypes.lookup(PixScan::TWOTRIGGER_NOISE);
  m_scantypes[PixScan::DIFFUSION]=scantypes.lookup(PixScan::DIFFUSION);
  m_scantypes[PixScan::OFFSET_SCAN]=scantypes.lookup(PixScan::OFFSET_SCAN);
  m_scantypes[PixScan::TDAC_TUNE_ITERATED]=scantypes.lookup(PixScan::TDAC_TUNE_ITERATED);
  m_scantypes[PixScan::MODULE_CROSSTALK]=scantypes.lookup(PixScan::MODULE_CROSSTALK);
  m_scantypes[PixScan::REGISTER_TEST]=scantypes.lookup(PixScan::REGISTER_TEST);
  m_scantypes[PixScan::GDAC_SCAN]=scantypes.lookup(PixScan::GDAC_SCAN);
  m_scantypes[PixScan::GDAC_RETUNE]=scantypes.lookup(PixScan::GDAC_RETUNE);
  m_scantypes[PixScan::TEMPERATURE_SCAN]=scantypes.lookup(PixScan::TEMPERATURE_SCAN);
  m_scantypes[PixScan::MONLEAK_SCAN]=scantypes.lookup(PixScan::MONLEAK_SCAN);
  m_scantypes[PixScan::SERIAL_NUMBER_SCAN]=scantypes.lookup(PixScan::SERIAL_NUMBER_SCAN);
  m_scantypes[PixScan::TDAC_FAST_TUNE]=scantypes.lookup(PixScan::TDAC_FAST_TUNE);
  m_scantypes[PixScan::TDAC_FAST_RETUNE]=scantypes.lookup(PixScan::TDAC_FAST_RETUNE);
  m_scantypes[PixScan::GDAC_FAST_TUNE]=scantypes.lookup(PixScan::GDAC_FAST_TUNE);
  m_scantypes[PixScan::GDAC_FAST_RETUNE]=scantypes.lookup(PixScan::GDAC_FAST_RETUNE);
  m_scantypes[PixScan::EXT_REGISTER_VERIFICATION]=scantypes.lookup(PixScan::EXT_REGISTER_VERIFICATION);
  m_scantypes[PixScan::GDAC_COARSE_FAST_TUNE]=scantypes.lookup(PixScan::GDAC_COARSE_FAST_TUNE);
  m_scantypes[PixScan::NOISESCAN_SELFTRIGGER]=scantypes.lookup(PixScan::NOISESCAN_SELFTRIGGER);
  m_scantypes[PixScan::DIGITALTEST_SELFTRIGGER]=scantypes.lookup(PixScan::DIGITALTEST_SELFTRIGGER);

  for(std::map<int, std::string>::iterator it=m_scantypes.begin();it!=m_scantypes.end();it++){
    m_scan->AddEntry((*it).second.c_str(), (*it).first);
  }
  m_initialized=true;
}


void ScanGui::setupScan(std::string flavor, int runnum){
  PixLib::EnumFEflavour::FEflavour fl;
  if(flavor=="FEI3")fl=PixLib::EnumFEflavour::PM_FE_I2;
  else if (flavor=="FEI4A")fl=PixLib::EnumFEflavour::PM_FE_I4A;
  else if (flavor=="FEI4B")fl=PixLib::EnumFEflavour::PM_FE_I4B;
  else return;

  delete m_scn;
 
  m_scn=new PixScan((PixScan::ScanType)m_scan->GetSelected(), fl);
  // Set targets from GUI unless they are 0 in the GUI
  if(m_targetCharge->GetIntNumber()!=0)
    m_scn->setTotTargetCharge(m_targetCharge->GetIntNumber());
  else
    m_targetCharge->SetIntNumber(m_scn->getTotTargetCharge());

  if(m_thresholdTarget->GetIntNumber()!=0)
    m_scn->setThresholdTargetValue(m_thresholdTarget->GetIntNumber());
  else
    m_thresholdTarget->SetIntNumber(m_scn->getThresholdTargetValue());

  if(m_targetValue->GetIntNumber()!=0)
    m_scn->setTotTargetValue(m_targetValue->GetIntNumber());
  else
    m_targetValue->SetIntNumber(m_scn->getTotTargetValue());

  m_typestring=m_scantypes[m_scan->GetSelected()];
  m_scn->setRunNumber(runnum);
  delete m_scanopt;
  m_scanopt=new ipc::ScanOptions;
  m_scn->convertScanConfig(*m_scanopt);

  char nms[128];
  if(m_scn->getDigitalInjection())updateText(m_dig, "Digital");
  else updateText(m_dig, "Analog");
  sprintf(nms, "%d", m_scn->getRepetitions());
  updateText(m_rep, nms);
  updateText(m_maskmode,  m_scanopt->maskStages);
  sprintf(nms,"%d",m_scanopt->nMaskStages);
  updateText(m_maskstages, nms);
  m_thresholdTargetC=m_scn->getThresholdTargetValue();
  m_thresholdTarget->SetIntNumber(m_thresholdTargetC);
  m_targetChargeC=m_scn->getTotTargetCharge();
  m_targetCharge->SetIntNumber(m_targetChargeC);
  m_targetValueC=m_scn->getTotTargetValue();
  m_targetValue->SetIntNumber(m_targetValueC);
  PixLib::EnumScanParam paramTypes;
  for (int j=0;j<3;j++){
    updateText(m_par[j], paramTypes.lookup(m_scn->getLoopParam(j)).c_str());
    std::vector<float> vals=m_scn->getLoopVarValues(j);
    sprintf(nms,"%d", vals.size());
    updateText(m_steps[j], nms);
    float low=0;
    float high=0;
    if(vals.size()>0){
      low=vals.front();
      high=vals.back();
    }
    sprintf(nms,"%d", (int)low);
    updateText(m_low[j], nms);
    sprintf(nms,"%d", (int)high);
    updateText(m_high[j], nms);
    if(m_scn->getDspProcessing(j))m_rceb[j]=true;
    else m_rceb[j]=false;
    m_rce[j]->SetOn(m_rceb[j]);
    m_active[j]->SetOn(m_scn->getLoopActive(j));
  }
    
}

bool ScanGui::m_initialized=false;
std::map<int, std::string> ScanGui::m_scantypes;
