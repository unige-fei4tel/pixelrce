#ifndef CMDDECODER_HH
#define CMDDECODER_HH

#include <string>
#include <vector>
#include "rcecalib/server/EthPrimitive.hh"

// shell commands need a declaration to be used in code.
#ifdef RCE_V2
namespace service{
  namespace shell{
    int Reboot_main(int argc, char **argv);
    int RunTask_main(int argc, char **argv); 
  }
}   
#else
int main_reboot(int argc, char **argv);
int main_runTask(int argc, char **argv); 
#endif
// use global IP address of bootp server in RTEMS
extern struct in_addr rtems_bsdnet_bootp_server_address;

class CmdDecoder{
public:
  CmdDecoder(EthPrimitive* eth): m_eth(eth),m_running(false){};
  void run();
private:
  std::string decode(const char* msg);
  std::string cmdReboot(std::vector<std::string>& cmd);
  std::string cmdSetenv(std::vector<std::string>& cmd);
  std::string cmdMount(std::vector<std::string>& cmd);
  std::string cmdLd(std::vector<std::string>& cmd);
  std::string cmdShutdown(std::vector<std::string>& cmd);
  std::string cmdDownload(std::vector<std::string>& cmd);

  EthPrimitive* m_eth;
  bool m_running;
  
};

#endif
