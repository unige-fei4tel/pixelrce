#ifndef CONFIGURATION_RTEMS_RCE_DEVEL_HH
#define CONFIGURATION_RTEMS_RCE_DEVEL_HH

// See the RTEMS C USer's Guide Chanper 23, "Configuring a System"
// Modified for RTEMS 4.10.0.

// The file configure_template.cc lists all the available options and
// gives a brief explanation of each.

#include <rtems.h>

#define CONFIGURE_INIT

/////////////////////////////
// 23.2.1: Library support //
/////////////////////////////
//combine RTEMS workspace and C heap 
#define CONFIGURE_UNIFIED_WORK_AREAS

#define CONFIGURE_MALLOC_STATISTICS
#define CONFIGURE_LIBIO_MAXIMUM_FILE_DESCRIPTORS (20)
#define CONFIGURE_MAXIMUM_PTYS (4)
#define CONFIGURE_STACK_CHECKER_ENABLED
#define CONFIGURE_APPLICATION_NEEDS_LIBBLOCK
#define CONFIGURE_IMFS_MEMFILE_BYTES_PER_BLOCK (256)
#define CONFIGURE_FILESYSTEM_FTPFS
#define CONFIGURE_FILESYSTEM_IMFS
#define CONFIGURE_FILESYSTEM_NFS

//////////////////////////////////////
// 23.2.2: Basic system information //
//////////////////////////////////////

// These RCE-specific definitions will be used both here and in the
// section setting limits on the numbers of classic RTEMS objects. We
// expect that most applications will use the queue types defined in
// the RCE core so our limits here are rather small.
namespace {
  enum {RCE_MAX_RTEMS_MSG_QUEUES = 10,
	RCE_MAX_BYTES_PER_MSG = 8,   // Enough for a regular pointer or a std::tr1::smart_ptr.
	RCE_MAX_MSGS_PER_QUEUE = 100
  };
}

#define CONFIGURE_MESSAGE_BUFFER_MEMORY \
  (RCE_MAX_RTEMS_MSG_QUEUES * \
   CONFIGURE_MESSAGE_BUFFERS_FOR_QUEUE(RCE_MAX_MSGS_PER_QUEUE,RCE_MAX_BYTES_PER_MSG)\
  )

// Some applications are known to require extra stack space, e.g.,
// Martin Kocian's PGP test.
#define CONFIGURE_MICROSECONDS_PER_TICK 10000
//#define CONFIGURE_EXECUTIVE_RAM_SIZE   (4096*1024)
#define CONFIGURE_EXTRA_TASK_STACKS (4096*1024*2)

/////////////////////////////////
// 23.2.4: Device driver table //
/////////////////////////////////
#define CONFIGURE_APPLICATION_NEEDS_CONSOLE_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_CLOCK_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_TIMER_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_NULL_DRIVER
#define CONFIGURE_APPLICATION_NEEDS_STUB_DRIVER
#define CONFIGURE_MAXIMUM_DRIVERS (11)

/////////////////////////
// 23.2.6: Classic API //
/////////////////////////
#define CONFIGURE_MAXIMUM_TASKS (50)
#define CONFIGURE_MAXIMUM_TIMERS (20)
#define CONFIGURE_MAXIMUM_SEMAPHORES (100)
#define CONFIGURE_MAXIMUM_MESSAGE_QUEUES (RCE_MAX_RTEMS_MSG_QUEUES)
#define CONFIGURE_MAXIMUM_PARTITIONS (10)
#define CONFIGURE_MAXIMUM_REGIONS (10)
#define CONFIGURE_MAXIMUM_PORTS (10)
#define CONFIGURE_MAXIMUM_PERIODS (10)
#define CONFIGURE_MAXIMUM_USER_EXTENSIONS (5)
// One of the user extensions is defined by the BSP to delete tasks
// that exit from their main functions (entry points). Normally RTEMS
// treats that as an error.

//////////////////////////////////////////
// 23.2.7: Classic API Init-tasks table //
//////////////////////////////////////////
#define CONFIGURE_RTEMS_INIT_TASKS_TABLE

//#define CONFIGURE_POSIX_INIT_THREAD_STACK_SIZE        (128*1024)
//#define CONFIGURE_POSIX_INIT_THREAD_TABLE
//extern "C" void *POSIX_Init( void *argument );
//////////////////////////////
// 23.3 Configuration table //
//////////////////////////////
#define CONFIGURE_CONFDEFS_DEBUG

/////////////////////////////////////////////////////////////
// Enable to print information about the work area and heap//
/////////////////////////////////////////////////////////////
//#define BSP_GET_WORK_AREA_DEBUG

/* POSIX API resources */
#define CONFIGURE_MAXIMUM_POSIX_THREADS                   100 
#define CONFIGURE_MAXIMUM_POSIX_MUTEXES                         100 
#define CONFIGURE_MAXIMUM_POSIX_CONDITION_VARIABLES    100 
#define CONFIGURE_MAXIMUM_POSIX_KEYS                       1024
#define CONFIGURE_MAXIMUM_POSIX_TIMERS                100
#define CONFIGURE_MAXIMUM_POSIX_QUEUED_SIGNALS            100 
#define CONFIGURE_MAXIMUM_POSIX_MESSAGE_QUEUES        100
#define CONFIGURE_MAXIMUM_POSIX_MESSAGE_QUEUE_DESCRIPTORS        100
#define CONFIGURE_MAXIMUM_POSIX_SEMAPHORES            100
#define CONFIGURE_MAXIMUM_POSIX_BARRIERS              100
#define CONFIGURE_MAXIMUM_POSIX_SPINLOCKS              100
#define CONFIGURE_MAXIMUM_POSIX_RWLOCKS              100
#define CONFIGURE_MINIMUM_TASK_STACK_SIZE                      16384
#define CONFIGURE_POSIX_INIT_THREAD_STACK_SIZE        (256*1024)
#include <rtems/confdefs.h>

/////////////////
// RTEMS shell //
/////////////////
#define CONFIGURE_SHELL_COMMANDS_INIT
#define CONFIGURE_SHELL_COMMANDS_ALL
#define CONFIGURE_SHELL_COMMANDS_ALL_NETWORKING
#define CONFIGURE_SHELL_MOUNT_NFS

#include <rtems/shellconfig.h>

#endif
