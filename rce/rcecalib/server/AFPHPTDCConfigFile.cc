#include "rcecalib/server/AFPHPTDCConfigFile.hh"
#include "rcecalib/util/exceptions.hh"
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <sys/stat.h> 

std::string AFPHPTDCConfigFile::getFullPath(std::string relPath){
  std::string newPath = relPath, basePath=m_moduleCfgFilePath, testName;
  unsigned int pos;
  // skip config file-name part of base path
  pos = basePath.find_last_of('/');
  if(pos!=std::string::npos) basePath.erase(pos,basePath.length()-pos);
  // skip "config" part of base path
  pos = basePath.find_last_of('/');
  if(pos!=std::string::npos) basePath.erase(pos+1,basePath.length()-pos);
  else basePath="";
  // then add relative path of DAC or mask file
  newPath = basePath + "calib/"+newPath;
  return newPath;
}

AFPHPTDCConfigFile::~AFPHPTDCConfigFile(){}
  
unsigned AFPHPTDCConfigFile::lookupToUnsigned(std::string par, int index, int size){
  if( m_params.find(par)==m_params.end()){
    std::cout<<"Parameter "<<par<<" does not exist."<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  std::string vals=m_params[par][index];
  unsigned val;
  int success=convertToUnsigned(vals, val, size);
  if(success==false){
    std::cout<<"Bad value "<<vals<< " for parameter "<<par<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  return val;
}
/*
float AFPHPTDCConfigFile::lookupToFloat(std::string par){
  if( m_params.find(par)==m_params.end()){
    std::cout<<"Parameter "<<par<<" does not exist."<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  std::string vals=m_params[par];
  float val;
  char* end;
  val=strtof(vals.c_str(), &end);
  if(end-vals.c_str()!=(int)vals.size()){
    std::cout<<"Bad value "<<vals<< " for parameter "<<par<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  return val;
}
*/

int AFPHPTDCConfigFile::convertToUnsigned(std::string par, unsigned &val, int size){
  char* end;
  val=strtoul(par.c_str(), &end, 0);
  if(end-par.c_str()!=(int)par.size()){
    return 0;
  }
  if(size==0 && (val&0xffffff00)!=0){
    std::cout<<"Value "<<val<<" too large."<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  else if(size==1 && (val&0xffff0000)!=0){
    std::cout<<"Value "<<val<<" too large."<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  return 1;
}


void AFPHPTDCConfigFile::setupCalib(float cal[2][12][ipc::IPC_N_CALIBVALS] , std::string par, int i1, int i2){
  char pa[20];
  sprintf(pa, "%s_%d", par.c_str(), i2);
  std::string pastr(pa);
  if( m_params.find(pastr)==m_params.end()){
    std::cout<<"Parameter "<<pastr<<" does not exist."<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  std::string fullpath=getFullPath(m_params[pastr][0]);
  std::ifstream dacfile(fullpath.c_str());
  if(!dacfile.good()){
    std::cout<<"Cannot open file with name "<<fullpath<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  for(int i=0;i<ipc::IPC_N_CALIBVALS;i++){
    dacfile>>cal[i1][i2][i];
    if(dacfile.eof()){
      std::cout<<"Not enough values in file "<<fullpath<<std::endl;
      throw rcecalib::Config_File_Error(ERS_HERE);
    }
  }
}
  
void AFPHPTDCConfigFile::writeModuleConfig(ipc::AFPHPTDCModuleConfig* config, const std::string &base, const std::string &confdir, 
				       const std::string &configname, const std::string &key){
/*
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes
  intStat = stat(base.c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Directory "<<base<<" does not exist. Not writing config file"<<std::endl;
    return;
  }
  intStat = stat((base+"/"+confdir).c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    //std::cout<<"Directory "<<base<<"/"<<confdir<<" does not exist. Creating."<<std::endl;
    mkdir ((base+"/"+confdir).c_str(),0777);
    mkdir ((base+"/"+confdir+"/configs").c_str(),0777);
    mkdir ((base+"/"+confdir+"/masks").c_str(),0777);
    mkdir ((base+"/"+confdir+"/tdacs").c_str(),0777);
    mkdir ((base+"/"+confdir+"/fdacs").c_str(),0777);
  }
  std::string cfgname=configname;
  if(key.size()!=0)cfgname+="__"+key;
  std::string fullpath=base+"/"+confdir+"/configs/"+cfgname+".cfg";
  std::ofstream cfgfile(fullpath.c_str());
  cfgfile<<"# FEI4B Configuration"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Module name"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"ModuleID\t\t"<<config->idStr<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Geographical address"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"Address\t\t\t"<<(unsigned)config->FECommand.address<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Global register"<<std::endl;
  cfgfile<<std::endl;
  //Global register
  ipc::AFPHPTDCModuleGlobal* cfg=&config->FEGlobal;
  cfgfile<<"TrigCnt\t\t\t"<<cfg->TrigCnt<<std::endl;
  cfgfile<<"Conf_AddrEnable\t\t"<<cfg->Conf_AddrEnable <<std::endl;
  cfgfile<<"Reg2Spare\t\t"<<cfg->Reg2Spare <<std::endl;
  cfgfile<<"ErrMask0\t\t"<<"0x"<<std::hex<<cfg->ErrMask0 <<std::dec<<std::endl;
  cfgfile<<"ErrMask1\t\t"<<"0x"<<std::hex<<cfg->ErrMask1 <<std::dec<<std::endl;
  cfgfile<<"PrmpVbpRight\t\t"<<        cfg->PrmpVbpRight <<std::endl;
  cfgfile<<"BufVgOpAmp\t\t"<<cfg->BufVgOpAmp <<std::endl;
  cfgfile<<"Reg6Spare\t\t"<<cfg->Reg6Spare <<std::endl;
  cfgfile<<"PrmpVbp\t\t\t"<<cfg->PrmpVbp <<std::endl;
  cfgfile<<"TdacVbp\t\t\t"<<        cfg->TdacVbp <<std::endl;
  cfgfile<<"DisVbn\t\t\t"<<cfg->DisVbn <<std::endl;
  cfgfile<<"Amp2Vbn\t\t\t"<<cfg->Amp2Vbn <<std::endl;
  cfgfile<<"Amp2VbpFol\t\t"<<cfg->Amp2VbpFol <<std::endl;
  cfgfile<<"Reg9Spare\t\t"<<cfg->Reg9Spare <<std::endl;
  cfgfile<<"Amp2Vbp\t\t\t"<<cfg->Amp2Vbp <<std::endl;
  cfgfile<<"FdacVbn\t\t\t"<<cfg->FdacVbn <<std::endl;
  cfgfile<<"Amp2Vbpf\t\t"<<cfg->Amp2Vbpf <<std::endl;
  cfgfile<<"PrmpVbnFol\t\t"<<cfg->PrmpVbnFol <<std::endl;
  cfgfile<<"PrmpVbpLeft\t\t"<<cfg->PrmpVbpLeft <<std::endl;
  cfgfile<<"PrmpVbpf\t\t"<<cfg->PrmpVbpf <<std::endl;
  cfgfile<<"PrmpVbnLcc\t\t"<<cfg->PrmpVbnLcc <<std::endl;
  cfgfile<<"Reg13Spare\t\t"<<cfg->Reg13Spare <<std::endl;
  cfgfile<<"PxStrobes\t\t"<<cfg->PxStrobes <<std::endl;
  cfgfile<<"S0\t\t\t"<<cfg->S0 <<std::endl;
  cfgfile<<"S1\t\t\t"<<cfg->S1 <<std::endl;
  cfgfile<<"LVDSDrvIref\t\t"<<cfg->LVDSDrvIref <<std::endl;
  cfgfile<<"GADCOpAmp\t\t"<<cfg->GADCOpAmp <<std::endl;
  cfgfile<<"PllIbias\t\t"<<cfg->PllIbias <<std::endl;
  cfgfile<<"LVDSDrvVos\t\t"<<cfg->LVDSDrvVos <<std::endl;
  cfgfile<<"TempSensBias\t\t"<<cfg->TempSensBias <<std::endl;
  cfgfile<<"PllIcp\t\t\t"<<cfg->PllIcp <<std::endl;
  cfgfile<<"Reg17Spare\t\t"<<cfg->Reg17Spare <<std::endl;
  cfgfile<<"PlsrIdacRamp\t\t"<<cfg->PlsrIdacRamp <<std::endl;
  cfgfile<<"VrefDigTune\t\t"<<cfg->VrefDigTune <<std::endl;
  cfgfile<<"PlsrVgOPamp\t\t"<<cfg->PlsrVgOPamp <<std::endl;
  cfgfile<<"PlsrDacBias\t\t"<<cfg->PlsrDacBias <<std::endl;
  cfgfile<<"VrefAnTune\t\t"<<cfg->VrefAnTune <<std::endl;
  cfgfile<<"Vthin_AltCoarse\t\t"<<cfg->Vthin_AltCoarse <<std::endl;
  cfgfile<<"Vthin_AltFine\t\t"<<cfg->Vthin_AltFine <<std::endl;
  cfgfile<<"PlsrDAC\t\t\t"<<cfg->PlsrDAC <<std::endl;
  cfgfile<<"DIGHITIN_Sel\t\t"<<cfg->DIGHITIN_Sel <<std::endl;
  cfgfile<<"DINJ_Override\t\t"<<cfg->DINJ_Override <<std::endl;
  cfgfile<<"HITLD_In\t\t"<<cfg->HITLD_In <<std::endl;
  cfgfile<<"Reg21Spare\t\t"<<cfg->Reg21Spare <<std::endl;
  cfgfile<<"Reg22Spare2\t\t"<<cfg->Reg22Spare2 <<std::endl;
  cfgfile<<"Colpr_Addr\t\t"<<cfg->Colpr_Addr <<std::endl;
  cfgfile<<"Colpr_Mode\t\t"<<cfg->Colpr_Mode <<std::endl;
  cfgfile<<"Reg22Spare1\t\t"<<cfg->Reg22Spare1 <<std::endl;
  cfgfile<<"DisableColumnCnfg0\t"<<"0x"<<std::hex<<cfg->DisableColumnCnfg0 <<std::dec<<std::endl;
  cfgfile<<"DisableColumnCnfg1\t"<<"0x"<<std::hex<<cfg->DisableColumnCnfg1 <<std::dec<<std::endl;
  cfgfile<<"DisableColumnCnfg2\t"<<"0x"<<std::hex<<cfg->DisableColumnCnfg2 <<std::dec<<std::endl;
  cfgfile<<"TrigLat\t\t\t"<<	        cfg->TrigLat <<std::endl;
  cfgfile<<"CMDcnt\t\t\t"<<cfg->CMDcnt <<std::endl;
  cfgfile<<"StopModeCnfg\t\t"<<cfg->StopModeCnfg <<std::endl;
  cfgfile<<"HitDiscCnfg\t\t"<<cfg->HitDiscCnfg <<std::endl;
  cfgfile<<"EN_PLL\t\t\t"<<cfg->EN_PLL <<std::endl;
  cfgfile<<"Efuse_sense\t\t"<<cfg->Efuse_sense <<std::endl;
  cfgfile<<"Stop_Clk\t\t"<<cfg->Stop_Clk <<std::endl;
  cfgfile<<"ReadErrorReq\t\t"<<cfg->ReadErrorReq <<std::endl;
  cfgfile<<"Reg27Spare1\t\t"<<cfg->Reg27Spare1 <<std::endl;
  cfgfile<<"GADC_Enable\t\t"<<cfg->GADC_Enable <<std::endl;
  cfgfile<<"ShiftReadBack\t\t"<<cfg->ShiftReadBack <<std::endl;
  cfgfile<<"Reg27Spare2\t\t"<<cfg->Reg27Spare2 <<std::endl;
  cfgfile<<"GateHitOr\t\t"<<cfg->GateHitOr <<std::endl;
  cfgfile<<"CalEn\t\t\t"<<cfg->CalEn <<std::endl;
  cfgfile<<"SR_clr\t\t\t"<<cfg->SR_clr <<std::endl;
  cfgfile<<"Latch_en\t\t"<<cfg->Latch_en <<std::endl;
  cfgfile<<"SR_Clock\t\t"<<cfg->SR_Clock <<std::endl;
  cfgfile<<"LVDSDrvSet06\t\t"<<cfg->LVDSDrvSet06 <<std::endl;
  cfgfile<<"Reg28Spare\t\t"<<cfg->Reg28Spare <<std::endl;
  cfgfile<<"EN40M\t\t\t"<<cfg->EN40M <<std::endl;
  cfgfile<<"EN80M\t\t\t"<<cfg->EN80M <<std::endl;
  cfgfile<<"CLK0_S2\t\t\t"<<(cfg->CLK0 &0x1)<<std::endl;
  cfgfile<<"CLK0_S1\t\t\t"<<((cfg->CLK0>>1)&0x1 )<<std::endl;
  cfgfile<<"CLK0_S0\t\t\t"<<((cfg->CLK0>>2)&0x1 )<<std::endl;
  cfgfile<<"CLK1_S2\t\t\t"<<(cfg->CLK1&0x1 )<<std::endl;
  cfgfile<<"CLK1_S1\t\t\t"<<((cfg->CLK1>>1)&0x1 )<<std::endl;
  cfgfile<<"CLK1_S0\t\t\t"<<((cfg->CLK1>>2)&0x1 )<<std::endl;
  cfgfile<<"EN160M\t\t\t"<<cfg->EN160M <<std::endl;
  cfgfile<<"EN320M\t\t\t"<<cfg->EN320M <<std::endl;
  cfgfile<<"Reg29Spare1\t\t"<<cfg->Reg29Spare1 <<std::endl;
  cfgfile<<"no8b10b\t\t\t"<<cfg->no8b10b <<std::endl;
  cfgfile<<"Clk2OutCnfg\t\t"<<cfg->Clk2OutCnfg <<std::endl;
  cfgfile<<"EmptyRecord\t\t"<<cfg->EmptyRecord <<std::endl;
  cfgfile<<"Reg29Spare2\t\t"<<cfg->Reg29Spare2 <<std::endl;
  cfgfile<<"LVDSDrvEn\t\t"<<cfg->LVDSDrvEn <<std::endl;
  cfgfile<<"LVDSDrvSet30\t\t"<<cfg->LVDSDrvSet30 <<std::endl;
  cfgfile<<"LVDSDrvSet12\t\t"<<cfg->LVDSDrvSet12 <<std::endl;
  cfgfile<<"TempSensDiodeSel\t"<<cfg->TempSensDiodeSel <<std::endl;
  cfgfile<<"TempSensDisable\t\t"<<cfg->TempSensDisable <<std::endl;
  cfgfile<<"IleakRange\t\t"<<cfg->IleakRange <<std::endl;
  cfgfile<<"Reg30Spare\t\t"<<cfg->Reg30Spare <<std::endl;
  cfgfile<<"PlsrRiseUpTau\t\t"<<cfg->PlsrRiseUpTau <<std::endl;
  cfgfile<<"PlsrPwr\t\t\t"<<cfg->PlsrPwr <<std::endl;
  cfgfile<<"PlsrDelay\t\t"<<cfg->PlsrDelay <<std::endl;
  cfgfile<<"ExtDigCalSW\t\t"<<cfg->ExtDigCalSW <<std::endl;
  cfgfile<<"ExtAnaCalSW\t\t"<<cfg->ExtAnaCalSW <<std::endl;
  cfgfile<<"Reg31Spare\t\t"<<cfg->Reg31Spare <<std::endl;
  cfgfile<<"GADCSel\t\t\t"<<cfg->GADCSel <<std::endl;
  cfgfile<<"SELB0\t\t\t"<<cfg->SELB0 <<std::endl;
  cfgfile<<"SELB1\t\t\t"<<cfg->SELB1 <<std::endl;
  cfgfile<<"SELB2\t\t\t"<<cfg->SELB2 <<std::endl;
  cfgfile<<"Reg34Spare1\t\t"<<cfg->Reg34Spare1 <<std::endl;
  cfgfile<<"PrmpVbpMsnEn\t\t"<<cfg->PrmpVbpMsnEn <<std::endl;
  cfgfile<<"Reg34Spare2\t\t"<<cfg->Reg34Spare2 <<std::endl;
  cfgfile<<"Chip_SN\t\t\t"<<cfg->Chip_SN <<std::endl;
  cfgfile<<"Reg1Spare\t\t"<<cfg->Reg1Spare <<std::endl;
  cfgfile<<"SmallHitErase\t\t"<<cfg->SmallHitErase <<std::endl;
  cfgfile<<"Eventlimit\t\t"<<cfg->Eventlimit <<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Pixel register"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"enable\t\t\t"<<confdir<<"/masks/enable_"<<cfgname<<".dat"<<std::endl;
  cfgfile<<"largeCap\t\t"<<confdir<<"/masks/largeCap_"<<cfgname<<".dat"<<std::endl;
  cfgfile<<"smallCap\t\t"<<confdir<<"/masks/smallCap_"<<cfgname<<".dat"<<std::endl;
  cfgfile<<"hitbus\t\t\t"<<confdir<<"/masks/hitbus_"<<cfgname<<".dat"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"tdac\t\t\t"<<confdir<<"/tdacs/tdac_"<<cfgname<<".dat"<<std::endl;
  cfgfile<<"fdac\t\t\t"<<confdir<<"/fdacs/fdac_"<<cfgname<<".dat"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"# Charge injection parameters"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"cinjLo\t\t\t"<<config->FECalib.cinjLo <<std::endl;
  cfgfile<<"cinjHi\t\t\t"<<config->FECalib.cinjHi <<std::endl;
  cfgfile<<"vcalCoeff[0]\t\t"<<config->FECalib.vcalCoeff[0] <<std::endl;
  cfgfile<<"vcalCoeff[1]\t\t"<<config->FECalib.vcalCoeff[1] <<std::endl;
  cfgfile<<"vcalCoeff[2]\t\t"<<config->FECalib.vcalCoeff[2] <<std::endl;
  cfgfile<<"vcalCoeff[3]\t\t"<<config->FECalib.vcalCoeff[3] <<std::endl;
  cfgfile<<"chargeCoeffClo\t\t"<<config->FECalib.chargeCoeffClo<<std::endl;
  cfgfile<<"chargeCoeffChi\t\t"<<config->FECalib.chargeCoeffChi<<std::endl;
  cfgfile<<"chargeOffsetClo\t\t"<<config->FECalib.chargeOffsetClo<<std::endl;
  cfgfile<<"chargeOffsetChi\t\t"<<config->FECalib.chargeOffsetChi<<std::endl;
  cfgfile<<"monleakCoeff\t\t"<<config->FECalib.monleakCoeff<<std::endl;
  writeMaskFile(ipc::enable, config, base+"/"+confdir+"/masks/enable_"+cfgname+".dat");
  writeMaskFile(ipc::largeCap, config, base+"/"+confdir+"/masks/largeCap_"+cfgname+".dat");
  writeMaskFile(ipc::smallCap, config, base+"/"+confdir+"/masks/smallCap_"+cfgname+".dat");
  writeMaskFile(ipc::hitbus, config, base+"/"+confdir+"/masks/hitbus_"+cfgname+".dat");
    
  writeDacFile(config->FETrims.dacThresholdTrim, base+"/"+confdir+"/tdacs/tdac_"+cfgname+".dat");
  writeDacFile(config->FETrims.dacFeedbackTrim,  base+"/"+confdir+"/fdacs/fdac_"+cfgname+".dat");
  
 */ 
  
}

void AFPHPTDCConfigFile::writeCalibFile(float cal[2][12][ipc::IPC_N_CALIBVALS] , const std::string filename, int i1, int i2){
  std::ofstream maskfile(filename.c_str());
  for (int i=0;i<ipc::IPC_N_CALIBVALS;i++)
    maskfile<<cal[i1][i2][i];
  maskfile<<std::endl;
}

void AFPHPTDCConfigFile::readModuleConfig(ipc::AFPHPTDCModuleConfig* cfg, std::string filename){
  //clear structure
  char* ccfg=(char*)cfg;
  for (unsigned int i=0;i<sizeof(ipc::AFPHPTDCModuleConfig);i++)ccfg[i]=0;
  //open file
  m_moduleCfgFile=new std::ifstream(filename.c_str());
  if(!m_moduleCfgFile->good()){
    std::cout<<"Cannot open file with name "<<filename<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  m_moduleCfgFilePath = filename;
  // parse config file
  std::string inpline;
  m_params.clear();
  while(true){
    getline(*m_moduleCfgFile, inpline);
    if(m_moduleCfgFile->eof())break;
    boost::trim(inpline);   
    if(inpline.size()!=0 && inpline[0]!='#'){ //remove comment lines and empty lines
      std::vector<std::string> splitVec; 
      split( splitVec, inpline, boost::is_any_of(" \t"), boost::token_compress_on ); 
      if(splitVec.size()<2){
	std::cout<<"Bad input line "<<inpline<<std::endl;
	continue;
      }
      for (size_t i=1;i<splitVec.size();i++)
	m_params[splitVec[0]].push_back(splitVec[i]);
    }
  }
  // Module name
  std::string modname;
  if( m_params.find("ModuleID")==m_params.end()){
    std::cout<<"No Module ID defined."<<std::endl;
  }else{
    modname=m_params["ModuleID"][0];
  }
  sprintf((char*)cfg->idStr, "%s", modname.c_str());
  cfg->test = lookupToUnsigned("test", 0, 1);
  cfg->tdcControl = lookupToUnsigned("tdcControl", 0);
  cfg->run = lookupToUnsigned("run", 0);
  cfg->bypassLut = lookupToUnsigned("bypassLut", 0);
  cfg->localClockEn = lookupToUnsigned("localClockEn", 0);
  cfg->calClockEn = lookupToUnsigned("calClockEn", 0);
  cfg->refEn = lookupToUnsigned("refEn", 0);
  cfg->hitTestEn = lookupToUnsigned("hitTestEn", 0);
  cfg->inputSel = lookupToUnsigned("inputSel", 0);
  cfg->address = lookupToUnsigned("address", 0);
  cfg->fanspeed = lookupToUnsigned("fanspeed", 0);
  cfg->channelEn = lookupToUnsigned("channelEn", 0, 1);

  for (int i=0;i<3;i++){
    cfg->test_select[i] = lookupToUnsigned("test_select", i);
    cfg->enable_error_mark[i] = lookupToUnsigned("enable_error_mark", i);
    cfg->enable_error_bypass[i] = lookupToUnsigned("enable_error_bypass", i);
    cfg->enable_error[i] = lookupToUnsigned("enable_error", i, 1);
    cfg->readout_single_cycle_speed[i] = lookupToUnsigned("readout_single_cycle_speed", i);
    cfg->serial_delay[i] = lookupToUnsigned("serial_delay", i);
    cfg->strobe_select[i] = lookupToUnsigned("strobe_select", i);
    cfg->readout_speed_select[i] = lookupToUnsigned("readout_speed_select", i);
    cfg->token_delay[i] = lookupToUnsigned("token_delay", i);
    cfg->enable_local_trailer[i] = lookupToUnsigned("enable_local_trailer", i);
    cfg->enable_local_header[i] = lookupToUnsigned("enable_local_header", i);
    cfg->enable_global_trailer[i] = lookupToUnsigned("enable_global_trailer", i);
    cfg->enable_global_header[i] = lookupToUnsigned("enable_global_header", i);
    cfg->keep_token[i] = lookupToUnsigned("keep_token", i);
    cfg->master[i] = lookupToUnsigned("master", i);
    cfg->enable_bytewise[i] = lookupToUnsigned("enable_bytewise", i);
    cfg->enable_serial[i] = lookupToUnsigned("enable_serial", i);
    cfg->enable_jtag_readout[i] = lookupToUnsigned("enable_jtag_readout", i);
    cfg->tdc_id[i] = lookupToUnsigned("tdc_id", i);
    cfg->select_bypass_inputs[i] = lookupToUnsigned("select_bypass_inputs", i);
    cfg->readout_fifo_size[i] = lookupToUnsigned("readout_fifo_size", i);
    cfg->reject_count_offset[i] = lookupToUnsigned("reject_count_offset", i, 1);
    cfg->search_window[i] = lookupToUnsigned("search_window", i, 1);
    cfg->match_window[i] = lookupToUnsigned("match_window", i, 1);
    cfg->leading_resolution[i] = lookupToUnsigned("leading_resolution", i);
    cfg->fixed_pattern[i] = lookupToUnsigned("fixed_pattern", i, 2);
    cfg->enable_fixed_pattern[i] = lookupToUnsigned("enable_fixed_pattern", i);
    cfg->max_event_size[i] = lookupToUnsigned("max_event_size", i);
    cfg->reject_readout_fifo_full[i] = lookupToUnsigned("reject_readout_fifo_full", i);
    cfg->enable_readout_occupancy[i] = lookupToUnsigned("enable_readout_occupancy", i);
    cfg->enable_readout_separator[i] = lookupToUnsigned("enable_readout_separator", i);
    cfg->enable_overflow_detect[i] = lookupToUnsigned("enable_overflow_detect", i);
    cfg->enable_relative[i] = lookupToUnsigned("enable_relative", i);
    cfg->enable_automatic_reject[i] = lookupToUnsigned("enable_automatic_reject", i);
    cfg->event_count_offset[i] = lookupToUnsigned("event_count_offset", i, 1);
    cfg->trigger_count_offset[i] = lookupToUnsigned("trigger_count_offset", i, 1);
    cfg->enable_set_counters_on_bunch_reset[i] = lookupToUnsigned("enable_set_counters_on_bunch_reset", i);
    cfg->enable_master_reset_code[i] = lookupToUnsigned("enable_master_reset_code", i);
    cfg->enable_master_reset_code_on_event_reset[i] = lookupToUnsigned("enable_master_reset_code_on_event_reset", i);
    cfg->enable_reset_channel_buffer_when_separator[i] = lookupToUnsigned("enable_reset_channel_buffer_when_separator", i);
    cfg->enable_separator_on_event_reset[i] = lookupToUnsigned("enable_separator_on_event_reset", i);
    cfg->enable_separator_on_bunch_reset[i] = lookupToUnsigned("enable_separator_on_bunch_reset", i);
    cfg->enable_direct_event_reset[i] = lookupToUnsigned("enable_direct_event_reset", i);
    cfg->enable_direct_bunch_reset[i] = lookupToUnsigned("enable_direct_bunch_reset", i);
    cfg->enable_direct_trigger[i] = lookupToUnsigned("enable_direct_trigger", i);
    for (int j=0;j<32;j++){
      char offs[32];
      sprintf(offs, "offset%d", j);
      cfg->offset[j][i] = lookupToUnsigned(offs, i, 1);
    }
    cfg->coarse_count_offset[i] = lookupToUnsigned("coarse_count_offset", i, 1);
    cfg->dll_tap_adjust3_0[i] = lookupToUnsigned("dll_tap_adjust3_0", i, 1);
    cfg->dll_tap_adjust7_4[i] = lookupToUnsigned("dll_tap_adjust7_4", i, 1);
    cfg->dll_tap_adjust11_8[i] = lookupToUnsigned("dll_tap_adjust11_8", i, 1);
    cfg->dll_tap_adjust15_12[i] = lookupToUnsigned("dll_tap_adjust15_12", i, 1);
    cfg->dll_tap_adjust19_16[i] = lookupToUnsigned("dll_tap_adjust19_16", i, 1);
    cfg->dll_tap_adjust23_20[i] = lookupToUnsigned("dll_tap_adjust23_20", i, 1);
    cfg->dll_tap_adjust27_24[i] = lookupToUnsigned("dll_tap_adjust27_24", i, 1);
    cfg->dll_tap_adjust31_28[i] = lookupToUnsigned("dll_tap_adjust31_28", i, 1);
    cfg->rc_adjust[i] = lookupToUnsigned("rc_adjust", i, 1);
    cfg->not_used[i] = lookupToUnsigned("not_used", i);
    cfg->low_power_mode[i] = lookupToUnsigned("low_power_mode", i);
    cfg->width_select[i] = lookupToUnsigned("width_select", i);
    cfg->vernier_offset[i] = lookupToUnsigned("vernier_offset", i);
    cfg->dll_control[i] = lookupToUnsigned("dll_control", i);
    cfg->dead_time[i] = lookupToUnsigned("dead_time", i);
    cfg->test_invert[i] = lookupToUnsigned("test_invert", i);
    cfg->test_mode[i] = lookupToUnsigned("test_mode", i);
    cfg->enable_trailing[i] = lookupToUnsigned("enable_trailing", i);
    cfg->enable_leading[i] = lookupToUnsigned("enable_leading", i);
    cfg->mode_rc_compression[i] = lookupToUnsigned("mode_rc_compression", i);
    cfg->mode_rc[i] = lookupToUnsigned("mode_rc", i);
    cfg->dll_mode[i] = lookupToUnsigned("dll_mode", i);
    cfg->pll_control[i] = lookupToUnsigned("pll_control", i);
    cfg->serial_clock_delay[i] = lookupToUnsigned("serial_clock_delay", i);
    cfg->io_clock_delay[i] = lookupToUnsigned("io_clock_delay", i);
    cfg->core_clock_delay[i] = lookupToUnsigned("core_clock_delay", i);
    cfg->dll_clock_delay[i] = lookupToUnsigned("dll_clock_delay", i);
    cfg->serial_clock_source[i] = lookupToUnsigned("serial_clock_source", i);
    cfg->io_clock_source[i] = lookupToUnsigned("io_clock_source", i);
    cfg->core_clock_source[i] = lookupToUnsigned("core_clock_source", i);
    cfg->dll_clock_source[i] = lookupToUnsigned("dll_clock_source", i);
    cfg->roll_over[i] = lookupToUnsigned("roll_over", i, 1);
    cfg->enable_matching[i] = lookupToUnsigned("enable_matching", i);
    cfg->enable_pair[i] = lookupToUnsigned("enable_pair", i);
    cfg->enable_ttl_serial[i] = lookupToUnsigned("enable_ttl_serial", i);
    cfg->enable_ttl_control[i] = lookupToUnsigned("enable_ttl_control", i);
    cfg->enable_ttl_reset[i] = lookupToUnsigned("enable_ttl_reset", i);
    cfg->enable_ttl_clock[i] = lookupToUnsigned("enable_ttl_clock", i);
    cfg->enable_ttl_hit[i] = lookupToUnsigned("enable_ttl_hit", i);
  }  
  for(int i=0;i<12;i++){
    setupCalib(cfg->calib, "Inl", 0, i);
    setupCalib(cfg->calib, "Dnl", 1, i);
  }
  delete m_moduleCfgFile;
}

void AFPHPTDCConfigFile::dump(const ipc::AFPHPTDCModuleConfig *cfg){
std::cout<<"FPGA Register Fields:"<<std::endl;
std::cout<<"====================="<<std::endl;
std::cout<<"idStr  "<<cfg->idStr<<std::endl;
std::cout<<"test  "<<(unsigned)cfg->test<<std::endl;
std::cout<<"tdcControl  "<<(unsigned)cfg->tdcControl<<std::endl;
std::cout<<"run  "<<(unsigned)cfg->run<<std::endl;
std::cout<<"bypassLut  "<<(unsigned)cfg->bypassLut<<std::endl;
std::cout<<"localClockEn  "<<(unsigned)cfg->localClockEn<<std::endl;
std::cout<<"calClockEn  "<<(unsigned)cfg->calClockEn<<std::endl;
std::cout<<"refEn  "<<(unsigned)cfg->refEn<<std::endl;
std::cout<<"hitTestEn  "<<(unsigned)cfg->hitTestEn<<std::endl;
std::cout<<"inputSel  "<<(unsigned)cfg->inputSel<<std::endl;
std::cout<<"address  "<<(unsigned)cfg->address<<std::endl;
std::cout<<"fanspeed  "<<(unsigned)cfg->fanspeed<<std::endl<<std::endl;
std::cout<<"channelEn "<<(unsigned)cfg->channelEn<<std::endl<<std::endl;

std::cout<<"TDC Configuration Fields:"<<std::endl;
std::cout<<"========================="<<std::endl;
for(int i=0;i<2;i++){
  std::cout<<"test_select  "<<(unsigned)cfg->test_select[0]<<"  "<<(unsigned)cfg->test_select[1]<<"  "<<(unsigned)cfg->test_select[2]<<std::endl;
  std::cout<<"enable_error_mark  "<<(unsigned)cfg->enable_error_mark[0]<<"  "<<(unsigned)cfg->enable_error_mark[1]<<"  "<<(unsigned)cfg->enable_error_mark[2]<<std::endl;
  std::cout<<"enable_error_bypass  "<<(unsigned)cfg->enable_error_bypass[0]<<"  "<<(unsigned)cfg->enable_error_bypass[1]<<"  "<<(unsigned)cfg->enable_error_bypass[2]<<std::endl;
  std::cout<<"enable_error  "<<(unsigned)cfg->enable_error[0]<<"  "<<(unsigned)cfg->enable_error[1]<<"  "<<(unsigned)cfg->enable_error[2]<<std::endl;
  std::cout<<"readout_single_cycle_speed  "<<(unsigned)cfg->readout_single_cycle_speed[0]<<"  "<<(unsigned)cfg->readout_single_cycle_speed[1]<<"  "<<(unsigned)cfg->readout_single_cycle_speed[2]<<std::endl;
  std::cout<<"serial_delay  "<<(unsigned)cfg->serial_delay[0]<<"  "<<(unsigned)cfg->serial_delay[1]<<"  "<<(unsigned)cfg->serial_delay[2]<<std::endl;
  std::cout<<"strobe_select  "<<(unsigned)cfg->strobe_select[0]<<"  "<<(unsigned)cfg->strobe_select[1]<<"  "<<(unsigned)cfg->strobe_select[2]<<std::endl;
  std::cout<<"readout_speed_select  "<<(unsigned)cfg->readout_speed_select[0]<<"  "<<(unsigned)cfg->readout_speed_select[1]<<"  "<<(unsigned)cfg->readout_speed_select[2]<<std::endl;
  std::cout<<"token_delay  "<<(unsigned)cfg->token_delay[0]<<"  "<<(unsigned)cfg->token_delay[1]<<"  "<<(unsigned)cfg->token_delay[2]<<std::endl;
  std::cout<<"enable_local_trailer  "<<(unsigned)cfg->enable_local_trailer[0]<<"  "<<(unsigned)cfg->enable_local_trailer[1]<<"  "<<(unsigned)cfg->enable_local_trailer[2]<<std::endl;
  std::cout<<"enable_local_header  "<<(unsigned)cfg->enable_local_header[0]<<"  "<<(unsigned)cfg->enable_local_header[1]<<"  "<<(unsigned)cfg->enable_local_header[2]<<std::endl;
  std::cout<<"enable_global_trailer  "<<(unsigned)cfg->enable_global_trailer[0]<<"  "<<(unsigned)cfg->enable_global_trailer[1]<<"  "<<(unsigned)cfg->enable_global_trailer[2]<<std::endl;
  std::cout<<"enable_global_header  "<<(unsigned)cfg->enable_global_header[0]<<"  "<<(unsigned)cfg->enable_global_header[1]<<"  "<<(unsigned)cfg->enable_global_header[2]<<std::endl;
  std::cout<<"keep_token  "<<(unsigned)cfg->keep_token[0]<<"  "<<(unsigned)cfg->keep_token[1]<<"  "<<(unsigned)cfg->keep_token[2]<<std::endl;
  std::cout<<"master  "<<(unsigned)cfg->master[0]<<"  "<<(unsigned)cfg->master[1]<<"  "<<(unsigned)cfg->master[2]<<std::endl;
  std::cout<<"enable_bytewise  "<<(unsigned)cfg->enable_bytewise[0]<<"  "<<(unsigned)cfg->enable_bytewise[1]<<"  "<<(unsigned)cfg->enable_bytewise[2]<<std::endl;
  std::cout<<"enable_serial  "<<(unsigned)cfg->enable_serial[0]<<"  "<<(unsigned)cfg->enable_serial[1]<<"  "<<(unsigned)cfg->enable_serial[2]<<std::endl;
  std::cout<<"enable_jtag_readout  "<<(unsigned)cfg->enable_jtag_readout[0]<<"  "<<(unsigned)cfg->enable_jtag_readout[1]<<"  "<<(unsigned)cfg->enable_jtag_readout[2]<<std::endl;
  std::cout<<"tdc_id  "<<(unsigned)cfg->tdc_id[0]<<"  "<<(unsigned)cfg->tdc_id[1]<<"  "<<(unsigned)cfg->tdc_id[2]<<std::endl;
  std::cout<<"select_bypass_inputs  "<<(unsigned)cfg->select_bypass_inputs[0]<<"  "<<(unsigned)cfg->select_bypass_inputs[1]<<"  "<<(unsigned)cfg->select_bypass_inputs[2]<<std::endl;
  std::cout<<"readout_fifo_size  "<<(unsigned)cfg->readout_fifo_size[0]<<"  "<<(unsigned)cfg->readout_fifo_size[1]<<"  "<<(unsigned)cfg->readout_fifo_size[2]<<std::endl;
  std::cout<<"reject_count_offset  "<<(unsigned)cfg->reject_count_offset[0]<<"  "<<(unsigned)cfg->reject_count_offset[1]<<"  "<<(unsigned)cfg->reject_count_offset[2]<<std::endl;
  std::cout<<"search_window  "<<(unsigned)cfg->search_window[0]<<"  "<<(unsigned)cfg->search_window[1]<<"  "<<(unsigned)cfg->search_window[2]<<std::endl;
  std::cout<<"match_window  "<<(unsigned)cfg->match_window[0]<<"  "<<(unsigned)cfg->match_window[1]<<"  "<<(unsigned)cfg->match_window[2]<<std::endl;
  std::cout<<"leading_resolution  "<<(unsigned)cfg->leading_resolution[0]<<"  "<<(unsigned)cfg->leading_resolution[1]<<"  "<<(unsigned)cfg->leading_resolution[2]<<std::endl;
  std::cout<<"fixed_pattern  "<<(unsigned)cfg->fixed_pattern[0]<<"  "<<(unsigned)cfg->fixed_pattern[1]<<"  "<<(unsigned)cfg->fixed_pattern[2]<<std::endl;
  std::cout<<"enable_fixed_pattern  "<<(unsigned)cfg->enable_fixed_pattern[0]<<"  "<<(unsigned)cfg->enable_fixed_pattern[1]<<"  "<<(unsigned)cfg->enable_fixed_pattern[2]<<std::endl;
  std::cout<<"max_event_size  "<<(unsigned)cfg->max_event_size[0]<<"  "<<(unsigned)cfg->max_event_size[1]<<"  "<<(unsigned)cfg->max_event_size[2]<<std::endl;
  std::cout<<"reject_readout_fifo_full  "<<(unsigned)cfg->reject_readout_fifo_full[0]<<"  "<<(unsigned)cfg->reject_readout_fifo_full[1]<<"  "<<(unsigned)cfg->reject_readout_fifo_full[2]<<std::endl;
  std::cout<<"enable_readout_occupancy  "<<(unsigned)cfg->enable_readout_occupancy[0]<<"  "<<(unsigned)cfg->enable_readout_occupancy[1]<<"  "<<(unsigned)cfg->enable_readout_occupancy[2]<<std::endl;
  std::cout<<"enable_readout_separator  "<<(unsigned)cfg->enable_readout_separator[0]<<"  "<<(unsigned)cfg->enable_readout_separator[1]<<"  "<<(unsigned)cfg->enable_readout_separator[2]<<std::endl;
  std::cout<<"enable_overflow_detect  "<<(unsigned)cfg->enable_overflow_detect[0]<<"  "<<(unsigned)cfg->enable_overflow_detect[1]<<"  "<<(unsigned)cfg->enable_overflow_detect[2]<<std::endl;
  std::cout<<"enable_relative  "<<(unsigned)cfg->enable_relative[0]<<"  "<<(unsigned)cfg->enable_relative[1]<<"  "<<(unsigned)cfg->enable_relative[2]<<std::endl;
  std::cout<<"enable_automatic_reject  "<<(unsigned)cfg->enable_automatic_reject[0]<<"  "<<(unsigned)cfg->enable_automatic_reject[1]<<"  "<<(unsigned)cfg->enable_automatic_reject[2]<<std::endl;
  std::cout<<"event_count_offset  "<<(unsigned)cfg->event_count_offset[0]<<"  "<<(unsigned)cfg->event_count_offset[1]<<"  "<<(unsigned)cfg->event_count_offset[2]<<std::endl;
  std::cout<<"trigger_count_offset  "<<(unsigned)cfg->trigger_count_offset[0]<<"  "<<(unsigned)cfg->trigger_count_offset[1]<<"  "<<(unsigned)cfg->trigger_count_offset[2]<<std::endl;
  std::cout<<"enable_set_counters_on_bunch_reset  "<<(unsigned)cfg->enable_set_counters_on_bunch_reset[0]<<"  "<<(unsigned)cfg->enable_set_counters_on_bunch_reset[1]<<"  "<<(unsigned)cfg->enable_set_counters_on_bunch_reset[2]<<std::endl;
  std::cout<<"enable_master_reset_code  "<<(unsigned)cfg->enable_master_reset_code[0]<<"  "<<(unsigned)cfg->enable_master_reset_code[1]<<"  "<<(unsigned)cfg->enable_master_reset_code[2]<<std::endl;
  std::cout<<"enable_master_reset_code_on_event_reset  "<<(unsigned)cfg->enable_master_reset_code_on_event_reset[0]<<"  "<<(unsigned)cfg->enable_master_reset_code_on_event_reset[1]<<"  "<<(unsigned)cfg->enable_master_reset_code_on_event_reset[2]<<std::endl;
  std::cout<<"enable_reset_channel_buffer_when_separator  "<<(unsigned)cfg->enable_reset_channel_buffer_when_separator[0]<<"  "<<(unsigned)cfg->enable_reset_channel_buffer_when_separator[1]<<"  "<<(unsigned)cfg->enable_reset_channel_buffer_when_separator[2]<<std::endl;
  std::cout<<"enable_separator_on_event_reset  "<<(unsigned)cfg->enable_separator_on_event_reset[0]<<"  "<<(unsigned)cfg->enable_separator_on_event_reset[1]<<"  "<<(unsigned)cfg->enable_separator_on_event_reset[2]<<std::endl;
  std::cout<<"enable_separator_on_bunch_reset  "<<(unsigned)cfg->enable_separator_on_bunch_reset[0]<<"  "<<(unsigned)cfg->enable_separator_on_bunch_reset[1]<<"  "<<(unsigned)cfg->enable_separator_on_bunch_reset[2]<<std::endl;
  std::cout<<"enable_direct_event_reset  "<<(unsigned)cfg->enable_direct_event_reset[0]<<"  "<<(unsigned)cfg->enable_direct_event_reset[1]<<"  "<<(unsigned)cfg->enable_direct_event_reset[2]<<std::endl;
  std::cout<<"enable_direct_bunch_reset  "<<(unsigned)cfg->enable_direct_bunch_reset[0]<<"  "<<(unsigned)cfg->enable_direct_bunch_reset[1]<<"  "<<(unsigned)cfg->enable_direct_bunch_reset[2]<<std::endl;
  std::cout<<"enable_direct_trigger  "<<(unsigned)cfg->enable_direct_trigger[0]<<"  "<<(unsigned)cfg->enable_direct_trigger[1]<<"  "<<(unsigned)cfg->enable_direct_trigger[2]<<std::endl;
  for(int j=31;j>=0;j--){
   std::cout<<"offset"<<j<<"  "<<(unsigned)cfg->offset[j][0]<<"  "<<(unsigned)cfg->offset[j][1]<<"  "<<(unsigned)cfg->offset[j][2]<<std::endl;
  }
  std::cout<<"coarse_count_offset  "<<(unsigned)cfg->coarse_count_offset[0]<<"  "<<(unsigned)cfg->coarse_count_offset[1]<<"  "<<(unsigned)cfg->coarse_count_offset[2]<<std::endl;
  std::cout<<"dll_tap_adjust3_0  "<<(unsigned)cfg->dll_tap_adjust3_0[0]<<"  "<<(unsigned)cfg->dll_tap_adjust3_0[1]<<"  "<<(unsigned)cfg->dll_tap_adjust3_0[2]<<std::endl;
  std::cout<<"dll_tap_adjust7_4  "<<(unsigned)cfg->dll_tap_adjust7_4[0]<<"  "<<(unsigned)cfg->dll_tap_adjust7_4[1]<<"  "<<(unsigned)cfg->dll_tap_adjust7_4[2]<<std::endl;
  std::cout<<"dll_tap_adjust11_8  "<<(unsigned)cfg->dll_tap_adjust11_8[0]<<"  "<<(unsigned)cfg->dll_tap_adjust11_8[1]<<"  "<<(unsigned)cfg->dll_tap_adjust11_8[2]<<std::endl;
  std::cout<<"dll_tap_adjust15_12  "<<(unsigned)cfg->dll_tap_adjust15_12[0]<<"  "<<(unsigned)cfg->dll_tap_adjust15_12[1]<<"  "<<(unsigned)cfg->dll_tap_adjust15_12[2]<<std::endl;
  std::cout<<"dll_tap_adjust19_16  "<<(unsigned)cfg->dll_tap_adjust19_16[0]<<"  "<<(unsigned)cfg->dll_tap_adjust19_16[1]<<"  "<<(unsigned)cfg->dll_tap_adjust19_16[2]<<std::endl;
  std::cout<<"dll_tap_adjust23_20  "<<(unsigned)cfg->dll_tap_adjust23_20[0]<<"  "<<(unsigned)cfg->dll_tap_adjust23_20[1]<<"  "<<(unsigned)cfg->dll_tap_adjust23_20[2]<<std::endl;
  std::cout<<"dll_tap_adjust27_24  "<<(unsigned)cfg->dll_tap_adjust27_24[0]<<"  "<<(unsigned)cfg->dll_tap_adjust27_24[1]<<"  "<<(unsigned)cfg->dll_tap_adjust27_24[2]<<std::endl;
  std::cout<<"dll_tap_adjust31_28  "<<(unsigned)cfg->dll_tap_adjust31_28[0]<<"  "<<(unsigned)cfg->dll_tap_adjust31_28[1]<<"  "<<(unsigned)cfg->dll_tap_adjust31_28[2]<<std::endl;
  std::cout<<"rc_adjust  "<<(unsigned)cfg->rc_adjust[0]<<"  "<<(unsigned)cfg->rc_adjust[1]<<"  "<<(unsigned)cfg->rc_adjust[2]<<std::endl;
  std::cout<<"not_used  "<<(unsigned)cfg->not_used[0]<<"  "<<(unsigned)cfg->not_used[1]<<"  "<<(unsigned)cfg->not_used[2]<<std::endl;
  std::cout<<"low_power_mode  "<<(unsigned)cfg->low_power_mode[0]<<"  "<<(unsigned)cfg->low_power_mode[1]<<"  "<<(unsigned)cfg->low_power_mode[2]<<std::endl;
  std::cout<<"width_select  "<<(unsigned)cfg->width_select[0]<<"  "<<(unsigned)cfg->width_select[1]<<"  "<<(unsigned)cfg->width_select[2]<<std::endl;
  std::cout<<"vernier_offset  "<<(unsigned)cfg->vernier_offset[0]<<"  "<<(unsigned)cfg->vernier_offset[1]<<"  "<<(unsigned)cfg->vernier_offset[2]<<std::endl;
  std::cout<<"dll_control  "<<(unsigned)cfg->dll_control[0]<<"  "<<(unsigned)cfg->dll_control[1]<<"  "<<(unsigned)cfg->dll_control[2]<<std::endl;
  std::cout<<"dead_time  "<<(unsigned)cfg->dead_time[0]<<"  "<<(unsigned)cfg->dead_time[1]<<"  "<<(unsigned)cfg->dead_time[2]<<std::endl;
  std::cout<<"test_invert  "<<(unsigned)cfg->test_invert[0]<<"  "<<(unsigned)cfg->test_invert[1]<<"  "<<(unsigned)cfg->test_invert[2]<<std::endl;
  std::cout<<"test_mode  "<<(unsigned)cfg->test_mode[0]<<"  "<<(unsigned)cfg->test_mode[1]<<"  "<<(unsigned)cfg->test_mode[2]<<std::endl;
  std::cout<<"enable_trailing  "<<(unsigned)cfg->enable_trailing[0]<<"  "<<(unsigned)cfg->enable_trailing[1]<<"  "<<(unsigned)cfg->enable_trailing[2]<<std::endl;
  std::cout<<"enable_leading  "<<(unsigned)cfg->enable_leading[0]<<"  "<<(unsigned)cfg->enable_leading[1]<<"  "<<(unsigned)cfg->enable_leading[2]<<std::endl;
  std::cout<<"mode_rc_compression  "<<(unsigned)cfg->mode_rc_compression[0]<<"  "<<(unsigned)cfg->mode_rc_compression[1]<<"  "<<(unsigned)cfg->mode_rc_compression[2]<<std::endl;
  std::cout<<"mode_rc  "<<(unsigned)cfg->mode_rc[0]<<"  "<<(unsigned)cfg->mode_rc[1]<<"  "<<(unsigned)cfg->mode_rc[2]<<std::endl;
  std::cout<<"dll_mode  "<<(unsigned)cfg->dll_mode[0]<<"  "<<(unsigned)cfg->dll_mode[1]<<"  "<<(unsigned)cfg->dll_mode[2]<<std::endl;
  std::cout<<"pll_control  "<<(unsigned)cfg->pll_control[0]<<"  "<<(unsigned)cfg->pll_control[1]<<"  "<<(unsigned)cfg->pll_control[2]<<std::endl;
  std::cout<<"serial_clock_delay  "<<(unsigned)cfg->serial_clock_delay[0]<<"  "<<(unsigned)cfg->serial_clock_delay[1]<<"  "<<(unsigned)cfg->serial_clock_delay[2]<<std::endl;
  std::cout<<"io_clock_delay  "<<(unsigned)cfg->io_clock_delay[0]<<"  "<<(unsigned)cfg->io_clock_delay[1]<<"  "<<(unsigned)cfg->io_clock_delay[2]<<std::endl;
  std::cout<<"core_clock_delay  "<<(unsigned)cfg->core_clock_delay[0]<<"  "<<(unsigned)cfg->core_clock_delay[1]<<"  "<<(unsigned)cfg->core_clock_delay[2]<<std::endl;
  std::cout<<"dll_clock_delay  "<<(unsigned)cfg->dll_clock_delay[0]<<"  "<<(unsigned)cfg->dll_clock_delay[1]<<"  "<<(unsigned)cfg->dll_clock_delay[2]<<std::endl;
  std::cout<<"serial_clock_source  "<<(unsigned)cfg->serial_clock_source[0]<<"  "<<(unsigned)cfg->serial_clock_source[1]<<"  "<<(unsigned)cfg->serial_clock_source[2]<<std::endl;
  std::cout<<"io_clock_source  "<<(unsigned)cfg->io_clock_source[0]<<"  "<<(unsigned)cfg->io_clock_source[1]<<"  "<<(unsigned)cfg->io_clock_source[2]<<std::endl;
  std::cout<<"core_clock_source  "<<(unsigned)cfg->core_clock_source[0]<<"  "<<(unsigned)cfg->core_clock_source[1]<<"  "<<(unsigned)cfg->core_clock_source[2]<<std::endl;
  std::cout<<"dll_clock_source  "<<(unsigned)cfg->dll_clock_source[0]<<"  "<<(unsigned)cfg->dll_clock_source[1]<<"  "<<(unsigned)cfg->dll_clock_source[2]<<std::endl;
  std::cout<<"roll_over  "<<(unsigned)cfg->roll_over[0]<<"  "<<(unsigned)cfg->roll_over[1]<<"  "<<(unsigned)cfg->roll_over[2]<<std::endl;
  std::cout<<"enable_matching  "<<(unsigned)cfg->enable_matching[0]<<"  "<<(unsigned)cfg->enable_matching[1]<<"  "<<(unsigned)cfg->enable_matching[2]<<std::endl;
  std::cout<<"enable_pair  "<<(unsigned)cfg->enable_pair[0]<<"  "<<(unsigned)cfg->enable_pair[1]<<"  "<<(unsigned)cfg->enable_pair[2]<<std::endl;
  std::cout<<"enable_ttl_serial  "<<(unsigned)cfg->enable_ttl_serial[0]<<"  "<<(unsigned)cfg->enable_ttl_serial[1]<<"  "<<(unsigned)cfg->enable_ttl_serial[2]<<std::endl;
  std::cout<<"enable_ttl_control  "<<(unsigned)cfg->enable_ttl_control[0]<<"  "<<(unsigned)cfg->enable_ttl_control[1]<<"  "<<(unsigned)cfg->enable_ttl_control[2]<<std::endl;
  std::cout<<"enable_ttl_reset  "<<(unsigned)cfg->enable_ttl_reset[0]<<"  "<<(unsigned)cfg->enable_ttl_reset[1]<<"  "<<(unsigned)cfg->enable_ttl_reset[2]<<std::endl;
  std::cout<<"enable_ttl_clock  "<<(unsigned)cfg->enable_ttl_clock[0]<<"  "<<(unsigned)cfg->enable_ttl_clock[1]<<"  "<<(unsigned)cfg->enable_ttl_clock[2]<<std::endl;
  std::cout<<"enable_ttl_hit  "<<(unsigned)cfg->enable_ttl_hit[0]<<"  "<<(unsigned)cfg->enable_ttl_hit[1]<<"  "<<(unsigned)cfg->enable_ttl_hit[2]<<std::endl;
}

  //Calibrations 
  std::cout<<"Calibration constants"<<std::endl;
  std::cout<<"---------------------"<<std::endl;
  for (int i=0;i<12;i++){
    std::cout<<"Inl_"<<i<<"  ";
    dumpCalib(cfg->calib, 0, i);
  }
  for (int i=0;i<12;i++){
    std::cout<<"Dnl_"<<i<<"  ";
    dumpCalib(cfg->calib, 1, i);
  }

}

void AFPHPTDCConfigFile::dumpCalib(const float cal[2][12][ipc::IPC_N_CALIBVALS], int i1, int i2){
  std::cout<<cal[i1][i2][0]<<" ... "<<(unsigned)cal[i1][i2][ipc::IPC_N_CALIBVALS-1]<<std::endl;
}
  
