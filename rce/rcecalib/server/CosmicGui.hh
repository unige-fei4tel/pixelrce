#ifndef COSMICGUI_HH
#define COSMICGUI_HH

#include "TGMdiMainFrame.h"
#include <TTimer.h>
#include <TTimeStamp.h>
#include <TGLabel.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGButtonGroup.h>
#include <TGComboBox.h>
#include "ConfigGui.hh"
#include <string>

class GlobalConfigGui;
class IPCController;
class CosmicDataReceiver;
class CosmicMonitoringGuiEmbedded;
class ModuleInfo;
class ScanLog;
namespace RCE{
  class PixScan;
}

class CosmicGui: public TGMainFrame {
  friend class CosmicDataReceiver;
public:
  enum Filemenu {LOAD, SAVE, QUIT, HISTOS, SCREENSHOT};
  CosmicGui(IPCController &c, bool autostart, const TGWindow *p,UInt_t w,UInt_t h, int exitAfterEvents=-1);
  virtual ~CosmicGui();
  void toggle();
  void synchSave1();
  void synchSave2();
  void timeouts();
  void timeoutm();
  void timeoutl();
  int startRun();
  void stopRun();
  void verifyConfiguration();
  void enableControls(bool on);
  void quit();
  void writeStdConfig();
  void handleFileMenu(Int_t);
  void readConfig(const char* filename);
  void writeConfig(const char* filename);
  void incrementNevents(){m_nevt++;}
  std::vector<ModuleInfo*>& getModuleInfo(){return m_moduleinfo;}
  std::vector<int>& getRces(){return m_rce;}
  int getL1AperTrig(){return m_ntrg->GetIntNumber();}
  int getRunNumber(){return m_runno->GetIntNumber();}
  IPCController* getController(){return &m_controller;}
  void allOffA(){switchOn(0,0);}
  void allOffC(){switchOn(1,0);}
  void allOffA2(){switchOn(2,0);}
  void allOffC2(){switchOn(3,0);}
  void allOnA(){switchOn(0,1);}
  void allOnC(){switchOn(1,1);}
  void allOnA2(){switchOn(2,1);}
  void allOnC2(){switchOn(3,1);}
  void switchOn(int panel, bool on);
  void setTaLogic0(Int_t i){m_tastate[0]=i;}
  void setTaLogic1(Int_t i){m_tastate[1]=i;}
  void setTbLogic0(Int_t i){m_tbstate[0]=i;}
  void setTbLogic1(Int_t i){m_tbstate[1]=i;}
  void setTabLogic0(Int_t i){m_tabstate[0]=i;}
  void setTabLogic1(Int_t i){m_tabstate[1]=i;}
  void setHbLogic(Int_t i){m_hbstate=i;}
  void setHsioLogic(Int_t i){m_hsiostate=i;}
  void setTriggerActivePhase(Int_t i){m_phasebusystate=i;}

private:
  void setRun(Bool_t on);
  bool running(){return m_isrunning;}
  int maxRunNum();
  void logScan();
  void finishLogFile();
  int openFile(RCE::PixScan*);

  IPCController& m_controller;
  ConfigGui* m_config[ConfigGui::MAX_MODULES];
  TGTextButton* m_start, *m_quit;
  TGLabel *m_nEvents, *m_time, *m_rate, *m_irate, *m_hpe;
  TGNumberEntry *m_delay0, *m_delay1, *m_delay2, *m_delay3, *m_l1alatency, *m_l1alatencytel, *m_trgdelay, *m_ntrg, *m_ntrgtel, *m_runno, *m_deadtime, *m_cperiod, *m_hbdelay, *m_hbdelaym, *m_maxevents;
  TGNumberEntry *m_hbrce[2];
  TGCheckButton *m_scint, *m_cyclic, *m_eudet, *m_HSIO1, *m_HSIO2, *m_hitbus, *m_NAND[4], *m_NANDlogic[4], *m_eudetfile, *m_rawfile, *m_writeRootFile, *m_maxtrue, *m_tempenable, *m_templog;
  TGCheckButton *m_tp[2][6], *m_phasebusyen;
  TGButtonGroup *m_talogic[2], *m_tblogic[2], *m_tablogic[2], *m_hbinput, *m_hsiologic, *m_phasebusysel;
  TGComboBox *m_tempfreq;
  int m_tastate[2], m_tbstate[2], m_tabstate[2], m_hbstate, m_hsiostate, m_phasebusystate;
  bool m_isrunning;
  bool m_realTime;
  TTimer *m_timers, *m_timerm, *m_timerl;
  TTimeStamp m_starttime;
  unsigned m_nevt;
  unsigned m_nevtMin;
  double m_oneminago;
  std::string m_oldRunName;
  CosmicDataReceiver * m_dataReceiver;
  CosmicMonitoringGuiEmbedded* monitoringGui;
  std::vector<ModuleInfo*> m_moduleinfo;
  std::vector<int> m_rce;
  std::string m_fullpath, m_rootfile, m_tempfile;
  ScanLog *m_scanLog;
  GlobalConfigGui* m_globalconf;
  bool m_oneRunAndExit;
  
ClassDef (CosmicGui,0)
};
#endif
