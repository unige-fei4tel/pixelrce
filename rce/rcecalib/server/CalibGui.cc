#include <ipc/partition.h>
#include <ipc/core.h>
#include "rcecalib/server/FEI4AConfigFile.hh"
#include "rcecalib/server/PixScan.hh"
#include "rcecalib/server/CalibGui.hh"
#include "rcecalib/server/PrimListGui.hh"
#include "rcecalib/server/PrimList.hh"
#include "rcecalib/server/ScanGui.hh"
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/IPCHistoController.hh"
#include "rcecalib/server/IPCGuiCallback.hh"
#include "rcecalib/analysis/CalibAnalysis.hh"
#include "rcecalib/analysis/AnalysisFactory.hh"
#include "rcecalib/server/DataExporter.hh"
#include "rcecalib/server/ScanLog.hh"
#include "rcecalib/util/VerifyErrors.hh"
#include "server/CallbackInfo.hh"

//#include "rcecalib/server/atlasimage.hh"
#include "ScanOptions.hh"
#include "TApplication.h"
#include "TGMsgBox.h"
#include "TGIcon.h"
#include "TGTab.h"
#include "TGMenu.h"
#include "TCanvas.h"
#include "TGCanvas.h"
#include "TGListTree.h"
#include "TRootEmbeddedCanvas.h"
#include <TGFileDialog.h>
#include <TROOT.h>
#include <TH2F.h>
#include <TStyle.h>
#include <cmdl/cmdargs.h>
#include <boost/regex.hpp>
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <sys/stat.h> 
#include <fstream>
#include <list>
#include <pthread.h>
#include <time.h>

#include <is/infoT.h>
#include <is/infodictionary.h>

using namespace RCE;

  


void* CalibGui::runloop( void *ptr ){
  CalibGui* calibGui=(CalibGui*)ptr;
  calibGui->runloop();
  return 0;
}

  
void CalibGui::runloop(){
//Execute shell script before scan, if available
if(m_primList->isLoaded() && m_primList->getCurrentPreScript().compare("") != 0)
{
	std::string script = m_primList->getCurrentPreScript();
	if(script.find("RUNNUM")!=(unsigned)-1)script.replace(script.find("RUNNUM"), 6, 
						    Form("%06d", m_globalconf->getRunNumber()));
	
	char directory[512];
	getcwd(directory,512);
	int retval=system(script.c_str());
	chdir(directory);
	if(retval!=0){
	  std::cout<<"****Script "<<script<<" failed. Aborting run."<<std::endl;
	  m_primList->setFailed(true);
	  stopRun();
	  return;
	}

}
  ipc::ScanOptions *options;
  PixScan *pixscan=currentScan();
  options=currentScanConfig();
  std::vector<float>loop1vals=pixscan->getLoopVarValues(1);
  std::vector<float>loop2vals=pixscan->getLoopVarValues(2);
  size_t loopsize1=loop1vals.size();
  size_t loopsize2=loop2vals.size();
  //even if the loops don't exist we have to run through once
  if(pixscan->getLoopActive(1)==false || pixscan->getDspProcessing(1)==true)loopsize1=1;
  if(pixscan->getLoopActive(2)==false || pixscan->getDspProcessing(2)==true)loopsize2=1;
  char txt[128];
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  TGListTreeItem *current1=0, *current2;
  TGListTreeItem *old1;
  old1=0;
  m_dir="";
  current2=0;
  std::string dir2="";
  PixLib::EnumScanParam parlist;

  for(size_t i=0;i<loopsize2;i++){
    m_nLoop2->SetText(i);
    if(pixscan->getDspProcessing(2)==false && pixscan->getLoopActive(2)==true){
      sprintf(txt, "loop2_%d",i);
      if(saveChecked()){
	m_file->cd();
	m_file->mkdir(txt,"Loop 2 directory");
	dir2=txt;
      }
      current2=m_tree->AddItem(root,txt);
      updateTree();
    }else{
      current2=root;
    }
    for(size_t j=0;j<loopsize1;j++){
      m_nLoop1->SetText(j);
      if(pixscan->getDspProcessing(1)==false && pixscan->getLoopActive(1)==true){
	sprintf(txt, "loop1_%d",j);
	if(saveChecked()){
	  m_file->cd(dir2.c_str());
	  gDirectory->mkdir(txt,"Loop 1 directory");
	  if(dir2!="") m_dir=dir2+"/"+txt;
	  else m_dir=txt;
	}
	if(j!=0 || i!=0)old1=current1;
	current1=m_tree->AddItem(current2,txt);
	updateTree();
      }else{
	current1=current2;
      }

      m_controller.downloadScanConfig(*options);
      if(pixscan->getDspProcessing(2)==false && pixscan->getLoopActive(2)==true){
	loopAction(2, i);
	m_controller.setupParameter(parlist.lookup(pixscan->getLoopParam(2)).c_str(), (int)loop2vals[i]);
      }
      if(pixscan->getDspProcessing(1)==false && pixscan->getLoopActive(1)==true){
	loopAction(1, j);
	m_controller.setupParameter(parlist.lookup(pixscan->getLoopParam(1)).c_str(), (int)loop1vals[j]);
      }
      m_cb=new IPCGuiCallback(m_cbinfo);
	
      runScan(pixscan->getCallbackPriority());
      if(m_cbinfo->failed()){ //Scan failed
	m_status=FAILED;
	m_primList->setFailed(true);
	stopRun();
	return;
      }
      if(saveChecked()){
	
	m_timers->Stop(); //TFile->Write() crashes sometimes when the timer is running
	saveHistos();
	m_timers->Start(); 
      }
      if(old1!=0 && saveChecked()==false){
	  m_tree->DeleteChildren(old1);
	  updateTree();
      }
      fillHistoTree(current1);
      updateTree();
      if(m_primList->isLoaded() && m_primList->getCurrentPostScript().compare("") != 0)
       {
	 std::string script = m_primList->getCurrentPostScript();
	 if(script.find("RUNNUM")!=(unsigned)-1)script.replace(script.find("RUNNUM"), 6, 
							       Form("%06d", m_globalconf->getRunNumber()));
	 int retval=system(script.c_str());
	 if(retval!=0){
	   std::cout<<"****Script "<<script<<" failed. Aborting run."<<std::endl;
	   m_primList->setFailed(true);
	   stopRun();
	   return;
	 }
       }
      if(m_isrunning==false){ //Scan was aborted
	m_primList->setFailed(true);
	stopRun();
	return;
      }
    }
  }
  if(currentScan()->verifyConfig()==true)verifyConfiguration();
  if(saveChecked()&&std::string(pixscan->getAnalysisType())!="NONE"){
    m_file->ReOpen("read"); //re-open file for reading
    analyze();
  }
  if(exportChecked()){
    m_file->ReOpen("read"); //re-open file for reading
    std::string topconfigname=m_globalconf->getConfigName()+".cfg";
    m_dataExporter->exportData(m_exportdir, topconfigname, m_rcdir, m_file, m_anfile);
  }
  //End of successful run loop
  if(m_primList->isLoaded())//Running primlist, so check and see if there is another item
	EndOfPrimListScan();	
  else //Using ScanGui, so stop run
 	stopRun();

}

CalibGui::~CalibGui(){
  Cleanup();
  delete m_dataExporter;
}

CalibGui::CalibGui(IPCPartition *partition, const TGWindow *p,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h), m_partition(partition), m_controller(*new IPCController(*partition)),
    m_hcontroller(*new IPCHistoController(*partition)),
    m_histo(0), m_file(0), m_anfile(0), m_delhisto(false) {
  // connect x icon on window manager
  ISInfoDictionary dict(*partition);
  ISInfoInt gui_running(0);
  try{
    dict.getValue("RceIsServer.GUI_running", gui_running);
  }catch(daq::is::RepositoryNotFound){
    std::cout<<"RceIsServer not running in the partition"<<std::endl;
    exit(0);
  }catch(daq::is::InfoNotFound){
    //std::cout<<"No entry for GUI_running"<<std::endl;
  }
  if (gui_running){
    int retcode;
    char msg[512];
    sprintf(msg, "Another GUI is already running in partition %s. Continue?", m_partition->name().c_str());
    new TGMsgBox(gClient->GetRoot(), 0, "Attention", msg,
		 kMBIconExclamation, kMBYes | kMBCancel,&retcode); 
    if(retcode!=kMBYes){
      exit(0);
    }
  }
  gui_running=1;
  dict.checkin("RceIsServer.GUI_running", gui_running);
  Connect("CloseWindow()","CalibGui",this,"quit()");

  TGMenuBar *menubar=new TGMenuBar(this,1,1,kHorizontalFrame | kRaisedFrame);
  TGLayoutHints *menubarlayout=new TGLayoutHints(kLHintsTop|kLHintsLeft,0,4,0,0);
  // menu "File"
  TGPopupMenu* filepopup=new TGPopupMenu(gClient->GetRoot());
  filepopup->AddEntry("&Load Config",LOAD);
  filepopup->AddEntry("&Save Config",SAVE);
  filepopup->AddSeparator();
  filepopup->AddEntry("&Quit",QUIT);
  menubar->AddPopup("&File",filepopup,menubarlayout);

  filepopup->Connect("Activated(Int_t)","CalibGui",this,"handleFileMenu(Int_t)");

  TGPopupMenu* plotpopup=new TGPopupMenu(gClient->GetRoot());
  plotpopup->AddEntry("&Save as gif",GIF);
  plotpopup->AddEntry("&Savefd as pdf",PDF);
  menubar->AddPopup("&Plot",plotpopup,menubarlayout);

  
  plotpopup->Connect("Activated(Int_t)","CalibGui",this,"handlePlotMenu(Int_t)");

  AddFrame(menubar, new TGLayoutHints(kLHintsTop | kLHintsExpandX, 0,0,0,2));

  //TGVerticalFrame* datapanel=new TGVerticalFrame(this,1,1, kSunkenFrame);
  TGTab* fTab = new TGTab(this);
  AddFrame(fTab,new TGLayoutHints(kLHintsExpandX|kLHintsExpandY )) ;
  TGCompositeFrame *datapanelt1 = fTab->AddTab("Scan");
  TGCompositeFrame *datapanelt2 = fTab->AddTab("Config Halfstave A");
  TGCompositeFrame *datapanelt2b = fTab->AddTab("Config Halfstave C");
  TGCompositeFrame *datapanelt2x = fTab->AddTab("Config Halfstave A (2)");
  TGCompositeFrame *datapanelt2bx = fTab->AddTab("Config Halfstave C (2)");
  TGCompositeFrame *datapanelt3 = fTab->AddTab("Plots");
  TGVerticalFrame *scanpanel=new TGVerticalFrame(datapanelt1);
  datapanelt1->AddFrame(scanpanel,new TGLayoutHints(kLHintsExpandX |kLHintsExpandY));

 // TGVerticalFrame *primlistpanel = new TGVerticalFrame(datapanelt1);
 // datapanelt1->AddFrame(primlistpanel, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  
  // scan panel
  TGHorizontalFrame *datapanel1a = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanel1a,new TGLayoutHints(kLHintsExpandX ));
  m_datapaneldd = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(m_datapaneldd, new TGLayoutHints(kLHintsExpandX, 2, 2, 0, 10));
  TGHorizontalFrame *datapanel1 = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanel1,new TGLayoutHints(kLHintsExpandX ));
  TGHorizontalFrame *datapanel2aa = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanel2aa,new TGLayoutHints(kLHintsExpandX ));
  TGHorizontalFrame *fwpanel = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(fwpanel, new TGLayoutHints(kLHintsExpandX));
  TGHorizontalFrame *datapanel2a = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanel2a,new TGLayoutHints(kLHintsExpandX ));
  TGHorizontalFrame *datapanelprim = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanelprim, new TGLayoutHints(kLHintsExpandX));
  TGHorizontalFrame *datapanel2 = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanel2,new TGLayoutHints(kLHintsExpandX ));
  TGHorizontalFrame *datapanel3 = new TGHorizontalFrame(scanpanel, 2, 2, kSunkenFrame);
  scanpanel->AddFrame(datapanel3,new TGLayoutHints(kLHintsExpandX));
  TGVerticalFrame *datapanel4[4];
  datapanel4[0] = new TGVerticalFrame(datapanelt2, 2, 2, kSunkenFrame);
  datapanelt2->AddFrame(datapanel4[0],new TGLayoutHints(kLHintsExpandX));
  datapanel4[1] = new TGVerticalFrame(datapanelt2b, 2, 2, kSunkenFrame);
  datapanelt2b->AddFrame(datapanel4[1],new TGLayoutHints(kLHintsExpandX));
  datapanel4[2] = new TGVerticalFrame(datapanelt2x, 2, 2, kSunkenFrame);
  datapanelt2x->AddFrame(datapanel4[2],new TGLayoutHints(kLHintsExpandX));
  datapanel4[3] = new TGVerticalFrame(datapanelt2bx, 2, 2, kSunkenFrame);
  datapanelt2bx->AddFrame(datapanel4[3],new TGLayoutHints(kLHintsExpandX));
  TGHorizontalFrame *datapanel5 = new TGHorizontalFrame(datapanelt3, 2, 2, kSunkenFrame);
  datapanelt3->AddFrame(datapanel5,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  //AddFrame(datapanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));
  m_scan=new ScanGui("Scan Config", datapanel1, 1, 1, kSunkenFrame);
  datapanel1->AddFrame(m_scan,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,0,0));

 //HSIO Firmware

  TGTextButton *mddd=new TGTextButton(fwpanel,"Print HSIO Firmware Version");
  mddd->Connect("Clicked()", "CalibGui", this,"getFirm()"); //todo: find and print FW
  fwpanel->AddFrame(mddd,new TGLayoutHints(kLHintsCenterY | kLHintsExpandX ,2,2,5,5));
  
  //primList Panel
 
  m_primList = new PrimListGui("Prim List", datapanelprim, 1, 1, kSunkenFrame);
  m_primList->setScanTypes(m_scan->getScanTypes());
  datapanelprim->AddFrame(m_primList, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,0,0));  
  m_globalconf=new GlobalConfigGui(m_config, ".calibDaq.rc",datapanel1a, 1, 1, kSunkenFrame);
  datapanel1a->AddFrame(m_globalconf,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,2,2));

  int modperpanel=ConfigGui::MAX_MODULES/4;
  const char* fenames[]={"A1-1", "A1-2", "A2-1", "A2-2", "A3-1", "A3-2",
			 "A4-1", "A4-2", "A5-1", "A5-2", "A6-1", "A6-2",
			 "A7-1", "A7-2", "A8-1", "A8-2",
			 "C1-1", "C1-2", "C2-1", "C2-2", "C3-1", "C3-2",
			 "C4-1", "C4-2", "C5-1", "C5-2", "C6-1", "C6-2",
			 "C7-1", "C7-2", "C8-1", "C8-2"};
				       
  for(int j=0;j<4;j++){
    TGHorizontalFrame *datapanelb=new TGHorizontalFrame(datapanel4[j]);
    datapanel4[j]->AddFrame(datapanelb,new TGLayoutHints(kLHintsExpandX ));
    TGTextButton *alloff=new TGTextButton(datapanelb,"All Off");
    alloff->SetMargins(15,25,0,0);
    datapanelb->AddFrame(alloff,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,2,2,2,2));
    TGTextButton *allon=new TGTextButton(datapanelb,"All On");
    allon->SetMargins(15,25,0,0);
    datapanelb->AddFrame(allon,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,2,2,2,2));
    if(j==0){
      alloff->Connect("Clicked()", "CalibGui", this,"allOffA()");
      allon->Connect("Clicked()", "CalibGui", this,"allOnA()");
    }else if(j==1){
      alloff->Connect("Clicked()", "CalibGui", this,"allOffC()");
      allon->Connect("Clicked()", "CalibGui", this,"allOnC()");
    }else if(j==2){
      alloff->Connect("Clicked()", "CalibGui", this,"allOffA2()");
      allon->Connect("Clicked()", "CalibGui", this,"allOnA2()");
    }else if(j==3){
      alloff->Connect("Clicked()", "CalibGui", this,"allOffC2()");
      allon->Connect("Clicked()", "CalibGui", this,"allOnC2()");
    }
    TGHorizontalFrame *datapanelc[4];
    for(int i=0;i<4;i++){
      datapanelc[i] = new TGHorizontalFrame(datapanel4[j]);
      datapanel4[j]->AddFrame(datapanelc[i],new TGLayoutHints(kLHintsExpandX |kLHintsExpandY));
    }
    char modname[128];
    for (int i=0;i<modperpanel;i++){
      sprintf(modname, "%s", fenames[(j%2)*modperpanel+i]);
      m_config[i+modperpanel*j]=new ConfigGui(modname, datapanelc[i/(modperpanel/4)],1,1, kSunkenFrame);
      datapanelc[i/(modperpanel/4)]->AddFrame(m_config[i+modperpanel*j],new TGLayoutHints(kLHintsExpandX | kLHintsExpandY,2,2,1,1));
    } 
  }
  m_nevt=0;
  m_timers=new TTimer;
  m_timers->Connect("Timeout()","CalibGui",this,"timeouts()");
  m_start=new TGTextButton(datapanel2,"Start Run");
  FontStruct_t labelfont;
  labelfont = gClient->GetFontByName("-adobe-helvetica-medium-r-*-*-18-*-*-*-*-*-iso8859-1");
  m_start->SetFont(labelfont);
  m_start->SetMargins(10,40,10,10);
  datapanel2->AddFrame(m_start,new TGLayoutHints(kLHintsCenterY | kLHintsLeft  ,5,5,5,5));
  m_start->Connect("Clicked()", "CalibGui", this, "toggle()");
  m_save=new TGCheckButton(datapanel2,"Analyze/Save Histos");
  m_save->SetOn(true);
  m_save->Connect("Clicked()", "CalibGui", this, "clearTree()");
  m_save->Connect("Clicked()", "CalibGui", this, "synchSave2()");
  datapanel2->AddFrame(m_save,new TGLayoutHints(kLHintsCenterY | kLHintsLeft ,10,2,5,5));
  m_export=new TGCheckButton(datapanel2,"Export Data");
  m_export->SetOn(false);
  m_export->Connect("Clicked()", "CalibGui", this, "synchSave1()");
  datapanel2->AddFrame(m_export,new TGLayoutHints(kLHintsCenterY |kLHintsLeft, 0, 2, 5, 5)); 
  m_debug=new TGCheckButton(datapanel2,"Debugging Run");
  m_debug->SetOn(false);
  datapanel2->AddFrame(m_debug,new TGLayoutHints(kLHintsCenterY |kLHintsLeft, 0, 2, 5, 5)); 
  //TGTextButton *quit=new TGTextButton(datapanel2,"&Quit","gApplication->Terminate(0)");
  m_quit=new TGTextButton(datapanel2,"Quit");
  m_quit->Connect("Clicked()", "CalibGui", this,"quit()");
  m_quit->SetFont(labelfont);
  m_quit->SetMargins(15,25,10,10);
  datapanel2->AddFrame(m_quit,new TGLayoutHints(kLHintsCenterY | kLHintsRight ,2,2,5,5));

  m_print=new TGTextButton(datapanel2aa,"Print Scan");
  datapanel2aa->AddFrame(m_print,new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX ,2,2,5,0));
  m_print->Connect("Clicked()", "CalibGui", this, "printFromGui()");

  TGLabel *commentlabel=new TGLabel(datapanel2a,"Comment:");
  datapanel2a->AddFrame(commentlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_comment=new TGTextEntry(datapanel2a);
  datapanel2a->AddFrame(m_comment,new TGLayoutHints(kLHintsLeft|kLHintsCenterY|kLHintsExpandX, 2, 2, 10, 0));
  
  TGLabel *timelabel=new TGLabel(datapanel3,"Time:");
  datapanel3->AddFrame(timelabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_time=new TGLabel(datapanel3,"0 s            ");
  datapanel3->AddFrame(m_time,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 30, 0, 0));
  m_time->SetTextFont(labelfont);
  
  TGLabel *nevlabel=new TGLabel(datapanel3,"Mask Stage:");
  datapanel3->AddFrame(nevlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 2, 10, 0));
  m_nEvents=new TGLabel(datapanel3,"0                        ");
  datapanel3->AddFrame(m_nEvents,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 30, 0, 0));
  m_nEvents->SetTextFont(labelfont);
  m_cbinfo=new CallbackInfo(m_nEvents);
  
  nevlabel=new TGLabel(datapanel3,"Loop 1:");
  datapanel3->AddFrame(nevlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 20, 2, 10, 0));
  m_nLoop1=new TGLabel(datapanel3,"0              ");
  datapanel3->AddFrame(m_nLoop1,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 30, 0, 0));
  m_nLoop1->SetTextFont(labelfont);
  
  nevlabel=new TGLabel(datapanel3,"Loop 2:");
  datapanel3->AddFrame(nevlabel,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 20, 2, 10, 0));
  m_nLoop2=new TGLabel(datapanel3,"0              ");
  datapanel3->AddFrame(m_nLoop2,new TGLayoutHints(kLHintsLeft|kLHintsCenterY, 2, 30, 0, 0));
  m_nLoop2->SetTextFont(labelfont);
  
  TGVerticalFrame *treepanel = new TGVerticalFrame(datapanel5, 150, 2, kSunkenFrame);
  datapanel5->AddFrame(treepanel,new TGLayoutHints(kLHintsExpandY));
  TGVerticalFrame *plotpanel = new TGVerticalFrame(datapanel5, 2, 2, kSunkenFrame);
  datapanel5->AddFrame(plotpanel,new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));



  m_tgc=new TGCanvas(treepanel, 300,100);
  m_tree=new TGListTree(m_tgc, kHorizontalFrame);
  m_tree->AddItem(0,"Histos");
  m_tree->Connect("Clicked(TGListTreeItem*, Int_t)","CalibGui", this, "displayHisto(TGListTreeItem*, Int_t)");
  treepanel->AddFrame(m_tgc,new TGLayoutHints(kLHintsExpandY));
  m_canvas=new TRootEmbeddedCanvas("Canvas",plotpanel,100,100);
  m_canvas->GetCanvas()->GetPad(0)->SetRightMargin(0.15);
  plotpanel->AddFrame(m_canvas,new TGLayoutHints(kLHintsExpandY|kLHintsExpandX));
  
  m_dataExporter=new DataExporter(m_config);
  m_homedir=getenv("HOME");
  m_homedir+="/";
  readGuiConfig((m_homedir+".calibDaq.rc").Data());
  m_scanLog=new ScanLog;

  m_thp = gClient->GetPicture("h1_t.xpm");

  SetWindowName("Calibration GUI");
  Resize(w,h);
  Layout();
  MapSubwindows();
  MapWindow();
}
void CalibGui::toggle(){
  setRun(!running());
}
void CalibGui::synchSave1(){
  if(m_export->IsOn()==true && m_save->IsOn()==false){
    m_save->SetOn(true);
  }
}
void CalibGui::synchSave2(){
  if(m_export->IsOn()==true && m_save->IsOn()==false){
    m_export->SetOn(false);
  }
}
void CalibGui::setRun(Bool_t on){

  if (on){

    //load new top config, change includes for primlist
    if(m_primList->isLoaded()){
      for(int i=0;i<ConfigGui::MAX_MODULES;i++){
	m_config[i]->enableControls(true);
      }
      if(m_primList->getCurrentTopConfig()!=""){
	std::cout<<"Loading top config "<<m_primList->getCurrentTopConfig().c_str()<<std::endl;
	m_globalconf->load(m_primList->getCurrentTopConfig().c_str());
      }
      m_primList->changeIncludes(m_config);
    }
    enableControls(false);
    m_globalconf->incrementRunNumber();
    m_globalconf->updateAvailable(false);
    int ret=setupScan();
    if(ret==-1){
      enableControls(true);
      return;
    }
    if(saveChecked()){
      int stat=openFile();
      if(stat){
	enableControls(true);
	return;
      }
    }
    if(m_primList->isLoaded())//set Primlist update info 
    {
	
	if(!m_globalconf->oldFileLoaded())//if no global conf loaded, prompt user where to save
	{
		m_globalconf->filePrompt();
		
	}
	if(!m_globalconf->oldFileLoaded())//file loading canceled or failed
	{
		setRun(false);
		m_primList->updateStatus("PrimList will not run because no valid cfg directory chosen!");
		return;
	}	
	else
	{
		std::stringstream buf;
 		buf << "Running " << m_primList->getCurrScanName() << " Scan (" << m_primList->currentScan + 1<< "/"<< m_primList->getNumScans() << ")" ;
 		m_primList->updateStatus(buf.str());
		m_primList->setFailed(false);
	}
    }
    m_timers->Start(1000,kFALSE);  // update every second
    m_starttime.Set();
    m_nevt=0;
    m_start->SetDown(true);
    m_isrunning=true;
    m_status=OK;
    m_start->SetText("Running");
    m_cbinfo->clear();
    m_cbinfo->addToMask(CallbackInfo::RUNNING);
    clearTree();
    startRun();
  } else{
    std::cout<<"Aborting Scan"<<std::endl;
    m_controller.abortScan();
    m_isrunning=false;
    m_controller.resetFE();
    /*
    m_timers->Stop();
    m_start->SetDown(false);
    m_start->SetText("Start Run");
    if(m_save->IsDisabledAndSelected()){
      m_file->ReOpen("read"); //re-open file for reading
    }
    enableControls();
    */
  
  }
}
void CalibGui::stopRun(){
  m_comment->Clear();
  finishLogFile();
  //m_controller.sendHWcommand(0, 111);
  m_controller.resetFE();
  if(saveChecked()){
    m_file->ReOpen("read"); //re-open file for reading
  }
  if(m_primList->isLoaded())
  {
	std::stringstream buf;
	if(m_primList->failed()){
	  buf << "Primlist failed in scan "<<m_primList->currentScan +1<<". Aborted Run."<<std::endl;
	}else{
	  buf << "Finished running " << m_primList->getNumScans() << " scans! ";
	}
 	m_primList->updateStatus(buf.str());
  }
  m_primList->currentScan=0;//reset PrimList to beginning
  m_timers->Stop();
  m_start->SetDown(false);
  m_start->SetText("Start Run");
  m_isrunning=false;
  m_cbinfo->addToMask(CallbackInfo::DONE);
  m_cbinfo->updateGui();
  enableControls(true);
  std::cout<<"Done"<<std::endl;
}
void CalibGui::timeouts(){
  // 1 s timeout, update number of events
  char labelstring[128];
  TTimeStamp currenttime;
  currenttime.Set();
  int timediff=(int)(currenttime.AsDouble()-m_starttime.AsDouble()+.5);
  //std::cout<<"starttime "<<m_starttime.AsDouble()<<std::endl;
  //std::cout<<"currenttime "<<currenttime.AsDouble()<<std::endl;
  //std::cout<<"Timediff "<<timediff<<std::endl;
  sprintf(labelstring,"%d s",timediff);
  //std::cout<<"Labelstring "<<labelstring<<std::endl;
  m_time->SetText(labelstring);
  m_cbinfo->updateGui();
}

int CalibGui::startRun(){

  //clear is server
  if( m_hcontroller.clearISServer(".*", false) == 0 ){
    std::cout<<"Clearing histograms off IS server at beginning of run failed! Not continuing"<<std::endl;
    assert(0);
  }

  //configure all valid modules
  //set up RCEs first

  std::map<int,unsigned int> rcemap;
  m_controller.removeAllRces();
  m_hcontroller.removeAllRces();
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      m_controller.addRce(m_config[i]->getRce());
      m_hcontroller.addRce(m_config[i]->getRce());
      rcemap[m_config[i]->getRce()]=0; //value will be the phase word
    }
  }
  // set up the trigger
  
  PixScan *pixscan=currentScan();
  //pixscan->dump(std::cout, *m_scan->getScanConfig());
 if(pixscan==0){ // no scan selected
    stopRun();
    return 1;
  }

  m_controller.setupTrigger(pixscan->getTriggerType());
  //now set up modules
  m_controller.removeAllModules();
  bool valid=false;
  std::map<int,unsigned int> linkmap;
  std::map<int,unsigned int> idmap;
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      valid=true;
      unsigned rce=m_config[i]->getRce();
      unsigned outlink=m_config[i]->getOutLink();
      if(linkmap.find(outlink+1000*rce)!=linkmap.end()){
	int retcode;
	new TGMsgBox(gClient->GetRoot(), 0, "Attention",Form("Outlink %d used by more than one frontend. Fix in config tab.", outlink),
		     kMBIconExclamation, kMBClose,&retcode); 
	stopRun();
	return 1;
      }
      linkmap[outlink+1000*rce]=1;
      unsigned inlink=m_config[i]->getInLink();
      //rcemap[rce]|=1<<(m_config[i]->getPhase()+outlink*4);
      int id=m_config[i]->getId();
      if(idmap.find(id)!=idmap.end()){
	int retcode;
	new TGMsgBox(gClient->GetRoot(), 0, "Attention",Form("ID %d used by more than one frontend. Fix configuration files.", id),
		 kMBIconExclamation, kMBClose,&retcode); 
	stopRun();
	return 1;
      }
      idmap[id]=1;
      const char* modname=m_config[i]->getName();
      std::string formatter=pixscan->getFormatterType();
      if(m_config[i]->getType()=="FEI4A"){
	if(formatter=="FEI4")formatter+="A";
	printf("FEI4A: addModule (%s, %d, %d, %d, %d, %s)\n",modname, id, inlink, outlink, rce,formatter.c_str());
	m_controller.addModule(modname, "FEI4A",id, inlink, outlink, rce, formatter.c_str());
	ipc::PixelFEI4AConfig *cfg=m_config[i]->getFEI4AConfig();
	m_controller.downloadModuleConfig(rce, id ,*cfg);
      } else if(m_config[i]->getType()=="FEI4B"){
	if(formatter=="FEI4")formatter+="B";
	printf("FEI4B: addModule (%s, %d, %d, %d, %d, %s)\n",modname, id, inlink, outlink, rce,formatter.c_str());
	m_controller.addModule(modname, "FEI4B",id, inlink, outlink, rce, formatter.c_str());
	ipc::PixelFEI4BConfig *cfg=m_config[i]->getFEI4BConfig();
	m_controller.downloadModuleConfig(rce, id ,*cfg);
      } else if(m_config[i]->getType()=="FEI3"){
        printf("FEI3: addModule (%s, %d, %d, %d, %d, %s)\n",modname, id, inlink, outlink, rce,formatter.c_str());
	m_controller.addModule(modname, "FEI3",id, inlink, outlink, rce, formatter.c_str());
	ipc::PixelModuleConfig *cfg=m_config[i]->getModuleConfig();
	m_controller.downloadModuleConfig(rce, id ,*cfg);
      } else if(m_config[i]->getType()=="Hitbus"){
        printf("Hitbus: addModule (%s, %d, %d, %d, %d %s)\n",modname, id, inlink, outlink, rce,formatter.c_str());
	m_controller.addModule(modname, "Hitbus",id, inlink, outlink, rce, "");
	ipc::HitbusModuleConfig *cfg=m_config[i]->getHitbusConfig();
	m_controller.downloadModuleConfig(rce, id ,*cfg);
      } else {
	std::cout<<"Unknown config type"<<std::endl;
	assert(0);
      }
    }
  }
  if(valid==false){
    int retcode;
    new TGMsgBox(gClient->GetRoot(), 0, "Attention", "No pixel module included in the calibration. Continue?",
		 kMBIconExclamation, kMBYes | kMBCancel,&retcode); 
    if(retcode!=kMBYes){
      stopRun();
      return 1;
    }
  }
  unsigned goodHSIOconnection;
  for (std::map <int, unsigned int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    goodHSIOconnection=m_controller.writeHWregister(rce, 11,0x0); //Trigger mask 1=scint 2=cyclic 4=Eudet 8=HSIO
    assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 3,0); //Mode 0=normal 1=tdccalib 2=eudaq
    assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 27,0); //regular L1A
    assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 22,0); //don't optimize multiplexer
    assert(goodHSIOconnection==0);
    goodHSIOconnection=m_controller.writeHWregister(rce, 23,0); //don't optimize multiplexer
    assert(goodHSIOconnection==0);
    // Reset delays
    goodHSIOconnection=m_controller.writeHWregister(rce, 7,0);
    assert(goodHSIOconnection==0);
    //assert(m_controller.writeHWregister(rce, 15,0x280)==0); //setup mux
    //assert(m_controller.writeHWregister(rce, 20,0x0)==0); //no BPM encoding
    //else assert(m_controller.writeHWregister(rce, 20,0x1)==0); //BPM encoding
    //assert(m_controller.writeHWregister(rce,17,it->second)==0); // set up phase
    //Delay 0 -- Delay for disc 0 
    // for(int i=0;i<m_phase->GetIntNumber();i++){
        //  serstat=m_controller.writeHWregister(rce, 5,0);
        //  assert(serstat==0);
        //}
  
  }
  if (currentScanConfig()==0){
    stopRun();
    return 1;
  }
  logScan();
  pthread_t mthread;
  pthread_create( &mthread, 0, CalibGui::runloop, (void*)this);
  pthread_detach(mthread);

  
  return 0;
}

void CalibGui::runScan(int pr){
    m_controller.runScan((ipc::Priority)pr, m_cb);
}
  
void CalibGui::verifyConfiguration(){
  std::cout<<"Verifying HW configuration"<<std::endl;
  m_cbinfo->addToMask(CallbackInfo::VERIFY);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      unsigned rce=m_config[i]->getRce();
      unsigned id=m_config[i]->getId();
      std::cout<<"Frontend "<<id<<" on RCE "<<rce<<" Outlink "<<m_config[i]->getOutLink()<<":"<<std::endl;
      int retval=m_controller.verifyModuleConfigHW(rce, id);
      if(retval&ModuleVerify::NO_FORMATTER)std::cout<<"No formatter defined. Skipping register readback."<<std::endl;
      if(retval&ModuleVerify::GLOBAL_READBACK_FAILED)std::cout<<"Global register readback failed for unknown reasons."<<std::endl;
      if(retval&ModuleVerify::GLOBAL_READBACK_DIFFERENT)std::cout<<"Global register readback differs from original configuration."<<std::endl;
      if(retval&ModuleVerify::PIXEL_WRONG_N_WORDS)std::cout<<"Pixel register read back wrong number of words."<<std::endl;
      if(retval&ModuleVerify::PIXEL_READBACK_DIFFERENT)std::cout<<"Pixel register readback differs from original configuration."<<std::endl;
      if(retval==ModuleVerify::OK)std::cout<<"Register verification successful."<<std::endl;
      if(retval!=ModuleVerify::OK)m_status=FAILED;
    }
  }
}
void CalibGui::enableControls(bool on){
  m_print->SetEnabled(on);
  m_quit->SetEnabled(on);
  m_save->SetEnabled(on);
  m_export->SetEnabled(on);
  m_debug->SetEnabled(on);
  m_comment->SetEnabled(on);
  for(int i=0;i<ConfigGui::MAX_MODULES;i++)
    m_config[i]->enableControls(on);
  m_scan->enableControls(on);
  m_globalconf->enableControls(on);
}

void CalibGui::quit(){
  writeGuiConfig((m_homedir+".calibDaq.rc").Data());
  ISInfoDictionary dict(*m_partition);
  ISInfoInt gui_running(0);
  dict.checkin("RceIsServer.GUI_running", gui_running);
  gApplication->Terminate(0);
}


void CalibGui::handlePlotMenu(int item){
  if(item==GIF){
    m_canvas->GetCanvas()->SaveAs("c1.gif");
  }else if(item==PDF){
    m_canvas->GetCanvas()->SaveAs("c1.pdf");
  }
}
void CalibGui::getFirm(){
  std::map<int,unsigned int> rcemap;
  m_controller.removeAllRces();
  bool foundRce=false; 
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      foundRce=true;
      m_controller.addRce(m_config[i]->getRce());
      rcemap[m_config[i]->getRce()]=0; //value will be the phase word
    }
  }
  unsigned firm;
  for (std::map <int, unsigned int>::const_iterator it = rcemap.begin(); it != rcemap.end(); ++it){
    int rce=it->first;
    unsigned value;
    firm=m_controller.readHWregister(rce, 13, value);
    if(firm==0){
      std::cout<<"RCE "<<rce<<": HSIO firmware version is "<<std::hex<<value<<std::dec<<"\n";
    }else{
      std::cout<<"RCE "<<rce<<": Firmware readout failed"<<"\n";
    }
  }
  if(foundRce==false){
    std::cout<<"***NO RCEs FOUND!***\n"<<std::endl;
  }else{printf("Done\n");}
}

void CalibGui::switchOn(int panel, bool on){
  int modperpanel=ConfigGui::MAX_MODULES/4;
  for (int i=panel*modperpanel;i<(panel+1)*modperpanel;i++){
    m_config[i]->setIncluded(on);
  }
}
  

void CalibGui::handleFileMenu(int item){
  TGFileInfo fileinfo;
  const char* types[]={"Teststand config", "*.tcf", "All files", "*", 0, 0};
  fileinfo.fFileTypes=types;
  if(item==QUIT)quit();
  else if(item==LOAD){
    new TGFileDialog(gClient->GetRoot(), 0, kFDOpen, &fileinfo);
    if(!fileinfo.fFilename){
      printf("Scheisse\n");
      return;
    }
    readGuiConfig(fileinfo.fFilename);
  }
  else if(item==SAVE){
    new TGFileDialog(gClient->GetRoot(), 0, kFDSave, &fileinfo);
    if(!fileinfo.fFilename){
      printf("Scheisse\n");
      return;
    }
    writeGuiConfig(fileinfo.fFilename);
  }
}
void CalibGui::clearTree(){
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  if(root) m_tree->DeleteChildren(root);
  root->SetOpen(false);
  updateTree();
}
void CalibGui::updateTree(){
  int x=m_tgc->GetViewPort()->GetX();
  int y=m_tgc->GetViewPort()->GetY();
  int w=m_tgc->GetViewPort()->GetWidth();
  int h=m_tgc->GetViewPort()->GetHeight();
  m_tree->DrawRegion(x,y,w,h);
}
  
void CalibGui::fillHistoTree(TGListTreeItem* branch){
  std::vector<std::string> names=m_hcontroller.getPublishedHistoNames();
  if(names.size()==0){
    std::cout<<"No Histograms available"<<std::endl;
    return;
  }
  for(size_t i=0;i<names.size();i++){
    names[i]=CalibAnalysis::addPosition(names[i].c_str(), m_config);
  }
  std::sort(names.begin(), names.end());
  for(size_t i=0;i<names.size();i++){
    TGListTreeItem* item=m_tree->AddItem(branch,names[i].c_str());
    item->SetPictures(m_thp,m_thp);
  }
}
void CalibGui::displayHisto(TGListTreeItem* item, int b){
  TGListTreeItem *parent=item->GetParent();
  if(parent==0)return;
  std::string histonames=item->GetText();
  if(histonames[5]==':')histonames=histonames.substr(6); //remove position information
  const char* histoname=histonames.c_str();
  if(std::string(histoname).substr(0,4)=="loop")return;
  if(std::string(histoname).substr(0,8)=="Analysis")return;
  //std::cout<<"Text "<<histoname<<std::endl;
  if(std::string(parent->GetText())=="Analysis"){
    if(m_delhisto==true) delete m_histo;
    m_delhisto=false;
    m_anfile->cd();
    m_histo=(TH1*)gDirectory->Get(histoname);
    //assert(m_histo!=0);
    if(m_histo==0){
	std::cout<<"Histogram does not exist"<<std::endl;
	return;
    }
  }else if(saveChecked()==false || 
     (m_save->IsEnabled()==true && m_save->IsOn()==false)){
    std::vector<TH1*> his=m_hcontroller.getHistosFromIS(histoname);
    if(his.size()!=1)return;
    if(m_delhisto==true) delete m_histo;
    m_histo=his[0];
    m_delhisto=true;
    //std::cout<<"Got histo from IS."<<std::endl;
  }else{
    TGListTreeItem *parent1=parent->GetParent();
    if(parent1==0)m_file->cd();
    else{
      TGListTreeItem *parent2=parent1->GetParent();
      if(parent2==0)m_file->cd(parent->GetText());
      else{
	char dir[128];
	sprintf(dir, "%s/%s",parent1->GetText(), parent->GetText());
	m_file->cd(dir);
      }
    }
    if(m_delhisto==true) delete m_histo;
    m_delhisto=false;
    m_histo=(TH1*)gDirectory->Get(histoname);
    //assert(m_histo!=0);
    if(m_histo==0){
	std::cout<<"Histogram does not exist"<<std::endl;
	return;
    }
    //std::cout<<"Got histo from file."<<std::endl;
  }
  m_histo->SetMinimum(0);
  if(m_histo->GetDimension()==1)m_histo->Draw();
  else {
    std::cout<<"Drawing 2-d histogram"<<std::endl;
    m_histo->Draw("colz");
  }
  m_canvas->GetCanvas()->Update();
}

int CalibGui::openFile(){
  if(currentScan()==0){
    std::cout<<"No Scan selected."<<std::endl;
    return 1;
  }
  TGFileInfo fileinfo;
  char runnum[128];
  sprintf(runnum,"%06d",m_globalconf->getRunNumber());
  TString runname;
  runname = (std::string(runnum)+"_"+getScanName());
  TString rcdir("/"+runname);
  rcdir=m_globalconf->getDataDir()+rcdir;
  m_rcdir=std::string(rcdir.Data());
  m_exportdir=std::string(runname.Data());
  TString filename(rcdir+"/histos.root");
  TString anfilename(rcdir+"/analysis.root");
  TString rcconfig(rcdir+"/globalconfig.txt");
  std::string topconfigname=m_globalconf->getConfigName()+".cfg";
  TString topconfig(rcdir+"/"+topconfigname);
  TString scanconfig(rcdir+"/scanconfig_"+m_exportdir+".txt");
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes
 
  intStat = stat(rcdir.Data(),&stFileInfo);
  if(intStat == 0) { //File exists
    int retcode;

    new TGMsgBox(gClient->GetRoot(), 0, "Attention", "Data file exists. Overwrite?",
		 kMBIconExclamation, kMBYes | kMBCancel,&retcode); 
    if(retcode==kMBYes){
      char command[512];
      sprintf(command, "rm -rf %s",rcdir.Data());
      system(command);
    }
    else 
    {
	if (!m_primList->isLoaded())
		return 1;
	else
	{
		m_isrunning=false;
		return 1;
	}
    }
  }
  // make new run directory
  mkdir (rcdir.Data(),0777);
  // copy configuration
  writeGuiConfig(rcconfig.Data());
  m_globalconf->copyConfig(topconfig.Data());
  std::ofstream sc(scanconfig.Data());
  if (m_primList->isLoaded()){
    m_primList->getCurrentScan()->dump(sc, *m_primList->getCurrentScanConfig());
  }else{
    m_scan->print(sc);
  }
  std::string oldcfg=std::string(rcdir.Data())+"/config";
  std::string updcfg=std::string(rcdir.Data())+"/configUpdate/";
  mkdir (oldcfg.c_str(),0777);
  mkdir (updcfg.c_str(),0777);
  m_globalconf->setConfigDir(updcfg.c_str());
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      m_config[i]->copyConfig(oldcfg.c_str());
    }
  }
  delete m_file;
  TDatime dt;
  m_file=new TFile(filename.Data(),"recreate", Form("Scan: %s Date: %s", runname.Data(), dt.AsString()) );
  delete m_anfile;
  if(std::string(currentScan()->getAnalysisType())!="NONE") { 
    m_anfile=new TFile(anfilename.Data(),"recreate",Form("Scan: %s Date: %s", runname.Data(), dt.AsString()) ); 
  }
  else m_anfile=0;
  return 0;
}

void CalibGui::writeGuiConfig(const char *filename){
  std::ofstream of(filename);
  m_globalconf->writeGuiConfig(of);
  of.close();
}
void CalibGui::readGuiConfig(const char* filename){
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes
  intStat = stat(filename,&stFileInfo);
  if(intStat == 0) { //File exists
    std::ifstream fin(filename);
    m_globalconf->readGuiConfig(fin);
    fin.close();
  }else{
    std::cout<<"File "<<filename<<" does not exist"<<std::endl;
  }
}

void CalibGui::printFromGui(){
  setupScan();
  m_scan->print(std::cout);
}
  
int CalibGui::setupScan(){ 
  if(m_primList->isLoaded()){//already setup when loaded primlist
    m_primList->setRunNumber(m_globalconf->getRunNumber());
    return 0;
  }
  std::string fetype="";
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if(m_config[i]->isIncluded()){
      std::string type=m_config[i]->getType();
      if(fetype!="" && type!="Hitbus" && type.substr(0,4) != fetype.substr(0,4)){
	std::cout<<"No mixing of frontend flavors allowed."<<std::endl;
	return -1;
      }
      if(type!="Hitbus" && fetype!="FEI4A")fetype=type; //FEI4A overrules FEI4B
    }
  }
  m_scan->setupScan(fetype, m_globalconf->getRunNumber());
  return 0;
}

void CalibGui::saveHistos(){
  m_cbinfo->addToMask(CallbackInfo::SAVE);
  m_cbinfo->updateGui();
  m_file->cd(m_dir.c_str());

  std::vector<TH1*> his = m_hcontroller.getHistosFromIS(".*");
  if(his.size()==0)
    {
      std::cout<<"CalibGui::saveHistos - Error, no histograms found on IS server"<<std::endl;
      return;
    }
  for(size_t i=0;i<his.size();i++)
    {
      if(his.at(i)==0){
	std::cout<<"CalibGui::saveHistos - Error, empty histogram retrieved from IS server"<<std::endl;
	continue;
      }
      //do we need to set histogram name?
      his.at(i)->Write();
      delete his.at(i);
    }
}

void CalibGui::loopAction(int i, int j){
  PixScan* pixscan=currentScan();
  switch(pixscan->getLoopAction(i)){
    case PixScan::T0_SET:
      m_controller.setupParameter( "CHARGE", pixscan->getTotTargetCharge());
      break;
    case PixScan::TDAC_TUNING:
      setupTdacTuning(j);
      break;
    case PixScan::TDAC_FAST_TUNING:
      setupTdacFastTuning(j);
      break;
    case PixScan::GDAC_TUNING:
      if(i==2)setupGdacTuningOuterLoop();
      else setupGdacTuning(j);
      break;
     case PixScan::OFFSET:
      setupOffsetScan(j);
      break;
    case PixScan::GDAC_FAST_TUNING:
      setupGdacFastTuning(j);
      break;
    case PixScan::GDAC_COARSE_FAST_TUNING:
      setupGdacCoarseFastTuning(j);
      break;
    case PixScan::SETMASK:
      setupMask(i,j);
      break;
    case PixScan::NO_ACTION:
      break;
    default:
      break;
  }
}

void CalibGui::analyze(){
  std::cout<<"Analyzing"<<std::endl;
  m_cbinfo->addToMask(CallbackInfo::ANALYZE);
  std::string type=currentScan()->getAnalysisType();
  AnalysisFactory af;
  CalibAnalysis* ana=af.getAnalysis(type);
  if(ana){
    ana->analyze(m_file, m_anfile, currentScan(), m_globalconf->getRunNumber(), m_config);
    m_anfile->ReOpen("read"); //re-open file for reading
    TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
    TGListTreeItem* anroot=m_tree->AddItem(root,"Analysis");
    m_anfile->cd();
    TList* histos=gDirectory->GetListOfKeys();
    std::vector<std::string> namevec;
    for(int i=0;i<histos->GetEntries();i++){
      std::string hname=((TH1*)histos->At(i))->GetName();
      hname=CalibAnalysis::addPosition(hname.c_str(), m_config);
      namevec.push_back(hname);
    }
    std::sort(namevec.begin(), namevec.end());
    for(size_t i=0;i<namevec.size();i++){
      TGListTreeItem* item=m_tree->AddItem(anroot,namevec[i].c_str());
      item->SetPictures(m_thp,m_thp);
    }
    updateTree();
    if(ana->configUpdate()==true){
      m_globalconf->updateAvailable(true);
    }
    delete ana;
  }
}

void CalibGui::setupGdacTuningOuterLoop(){
  std::vector<float> varValues=currentScan()->getLoopVarValues(2);
  if(varValues.size()<1){
    std::cout<<"No outer loop for GDAC tuning. Not setting up TDAC values"<<std::endl;
    return;
  }
  int val=(int)varValues[0];
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip;
    if(m_config[i]->getType().substr(0,4)=="FEI4"){
      ncol=80;
      nrow=336;
      nchip=1;
    }else{
      ncol=18;
      nrow=160;
      nchip=16;
    }
    for (int chip=0;chip<nchip;chip++){
      for (int col=0;col<ncol;col++){
        for (int row=0;row<nrow;row++){
          if(m_config[i]->getType()=="FEI4A")m_config[i]->getFEI4AConfig()->FETrims.dacThresholdTrim[col][row]=val;
            else if(m_config[i]->getType()=="FEI4B")m_config[i]->getFEI4BConfig()->FETrims.dacThresholdTrim[col][row]=val;
            else m_config[i]->getModuleConfig()->FEConfig[chip].FETrims.dacThresholdTrim[row][col]=val;
        }
      }
    }
  }
}

void CalibGui::setupGdacTuning(int j){ 
  if(!saveChecked()){
    std::cout<<"**** Check Save/Analyze before running this scan. ***"<<std::endl;
    return;
  }
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  char subdir[128];
  sprintf(subdir, "loop1_%d", j);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip, maxval, maxstage;
    if(m_config[i]->getType().substr(0,4)=="FEI4"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=120;
    }else{
      ncol=18;
      nrow=160;
      nchip=16;
      maxval=32;
      maxstage=32;
    }
    std::vector<float> varValues=currentScan()->getLoopVarValues(1);
    int occlast=currentScan()->getLoopVarValues(0).size()-3;
    int target=currentScan()->getThresholdTargetValue();
    int nstages=currentScan()->getMaskStageSteps();
    int npixtotal=nchip*ncol*nrow;
    float npixused=float(nstages)/float(maxstage) * npixtotal;
    float npixunused=(1.-float(nstages)/float(maxstage))*npixtotal;
    TH2 *mean=0, *chi2=0, *occ0=0, *occmax=0;
    char name[128];
    sprintf (name, "GDAC_settings_Mod_%d_it_%d", m_config[i]->getId(), j);
    TH1F* threshhist=new TH1F(name, Form("GDAC settings module %d step %d", m_config[i]->getId(), j), 
			      nchip, 0, nchip);
    threshhist->SetMinimum(0);
    threshhist->GetYaxis()->SetTitle("GDAC setting");
    threshhist->GetXaxis()->SetTitle("Chip");
    if(j!=0){
      mean=(TH2*)m_file->Get(Form( "loop2_0/loop1_%d/Mod_%d_Mean", j-1, m_config[i]->getId()));
      chi2=(TH2*)m_file->Get(Form( "loop2_0/loop1_%d/Mod_%d_ChiSquare", j-1, m_config[i]->getId()));
      occ0=(TH2*)m_file->Get(Form("loop2_0/loop1_%d/Mod_%d_Occupancy_Point_002", j-1, m_config[i]->getId()));
      occmax=(TH2*)m_file->Get(Form( "loop2_0/loop1_%d/Mod_%d_Occupancy_Point_%03d", j-1, m_config[i]->getId(),occlast));
      if(mean==0 || chi2==0 || occ0==0 || occmax==0){
	std::cout<<"Did not find histo "<<mean<<" "<<chi2<<" "<<occ0<<" "<<occmax<<std::endl;
	std::cout<<"occlast "<<occlast<<std::endl;
	assert(0);
	return;
      }
    }
    for (int chip=0;chip<nchip;chip++){
      CORBA::Octet val=int(varValues[0]); //initial setting
      if(varValues[0]==-1 || j!=0){ //val==-1 means retuning, use GDAC values from file
	if(m_config[i]->getType()=="FEI4A") val=m_config[i]->getFEI4AConfig()->FEGlobal.Vthin_AltFine;
	else if(m_config[i]->getType()=="FEI4B") val=m_config[i]->getFEI4BConfig()->FEGlobal.Vthin_AltFine;
	else val=m_config[i]->getModuleConfig()->FEConfig[chip].FEGlobal.gdac;
      }
      if(j!=0){ //modify TDAC values appropriately
	int repetitions = currentScan()->getRepetitions();
	int diffval=int(varValues[j]);
	float empty=0;
	float noisy=0;
	float good=0;
        float meanf=0;
	for (int col=0;col<ncol;col++){
	  for (int row=0;row<nrow;row++){
	    if(occ0->GetBinContent(chip*ncol+col+1, row+1)!=0)noisy++;
	    else if (occmax->GetBinContent(chip*ncol+col+1, row+1)<repetitions*0.9)empty++;
	    else if (chi2->GetBinContent(chip*ncol+col+1, row+1)!=0){
	      good++;
	      meanf+=mean->GetBinContent(chip*ncol+col+1, row+1);
	    }
	  }
	}
	if(good>0)meanf/=good;
	if(noisy>npixused*0.50){ //more than 50 % of pixels in the noise
	  if((int)val+diffval<maxval)val+=diffval; 
	  else {
	    val=maxval-1;
	    std::cout<<"Module "<< m_config[i]->getId()<<": GDAC already at "<<maxval-1<<std::endl;
	  }
	  std::cout<<"Module "<< m_config[i]->getId()<<": More than 50 % of the pixels are in the noise. Moving to higher threshold."<<std::endl;
        }else if (empty>npixunused+npixused*0.5){ //more than 50 % never reach threshold
          if((int)val-diffval>0)val-=diffval;
	  else {
	    std::cout<<"Module "<< m_config[i]->getId()<<": GDAC already at 0"<<std::endl;
	    val=0;
	  }
	  std::cout<<"Module "<< m_config[i]->getId()<<": More than 50 % of the pixels never reach threshold. Moving to lower threshold."<<std::endl;
	}else if(good>0.3*npixused){
	  if(meanf>target){
	    if((int)val-diffval>0)val-=diffval;
	    else {
	      std::cout<<"Module "<< m_config[i]->getId()<<": GDAC already at 0"<<std::endl;
	      val=0;
	    }
	  } else {
	    if((int)val+diffval<maxval)val+=diffval; 
	    else {
	      val=maxval-1;
	      std::cout<<"Module "<< m_config[i]->getId()<<": GDAC already at "<<maxval-1<<std::endl;
	    }
	  }
	}else{
	  std::cout<<"Module "<<m_config[i]->getId()<<": GDAC Tuning: No criteria match. Don't know what to do. good="<<good/npixused<<" empty="<<(empty-npixunused)/npixused<<" noisy="<<noisy/npixused<<std::endl;
	}
      }
      std::cout<<"Setting GDAC to "<<(int)val<<std::endl;
      if(m_config[i]->getType()=="FEI4A")m_config[i]->getFEI4AConfig()->FEGlobal.Vthin_AltFine=val;
      else if(m_config[i]->getType()=="FEI4B")m_config[i]->getFEI4BConfig()->FEGlobal.Vthin_AltFine=val;
      else m_config[i]->getModuleConfig()->FEConfig[chip].FEGlobal.gdac=val;
      threshhist->SetBinContent(1, val);
    }
    if(saveChecked()){
      TGListTreeItem* branch2=m_tree->FindChildByName(root,"loop2_0");
      TGListTreeItem* branch=m_tree->FindChildByName(branch2, subdir);
      TGListTreeItem* item=m_tree->AddItem(branch,CalibAnalysis::addPosition(name, m_config).c_str());
      item->SetPictures(m_thp,m_thp);
      if(m_config[i]->getType()=="FEI4A") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4AConfig());
      else if(m_config[i]->getType()=="FEI4B") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4BConfig());
      else m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getModuleConfig());
      m_file->cd(Form("loop2_0/%s", subdir));
      threshhist->Write();
      threshhist->SetDirectory(gDirectory);
    }
  }
  m_controller.downloadScanConfig(*(currentScanConfig()));
}

void CalibGui::setupGdacFastTuning(int j){ 
  if(!saveChecked()){
    std::cout<<"**** Check Save/Analyze before running this scan. ***"<<std::endl;
    return;
  }
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  char subdir[128];
  sprintf(subdir, "loop1_%d", j);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip, maxval, maxstage;
    if(m_config[i]->getType()=="FEI4A"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=120;
    }
    else if(m_config[i]->getType()=="FEI4B"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=24;
    }
    else{
      ncol=18;
      nrow=160;
      nchip=16;
      maxval=32;
      maxstage=32;
    }
    currentScanConfig()->scanLoop[0].dataPoints[0] = currentScan()->getThresholdTargetValue();
    int nstages=currentScan()->getMaskStageSteps();
    int npixtotal=nchip*ncol*nrow;
    float npixused=float(nstages)/float(maxstage)*npixtotal;
    std::vector<float> varValues=currentScan()->getLoopVarValues(1);
    int repetitions = currentScan()->getRepetitions();
    double targetEff = 0.5;
    TH2  *occ0=0;
    char name[128];
    sprintf (name, "GDAC_settings_Mod_%d_it_%d", m_config[i]->getId(), j);
    TH1F* threshhist=new TH1F(name, Form("GDAC settings module %d step %d", m_config[i]->getId(), j), 
			      nchip, 0, nchip);
    threshhist->SetMinimum(0);
    threshhist->GetYaxis()->SetTitle("GDAC setting");
    threshhist->GetXaxis()->SetTitle("Chip");
    if(j!=0){
      occ0=(TH2*)m_file->Get(Form("loop2_0/loop1_%d/Mod_%d_Occupancy_Point_000", j-1, m_config[i]->getId()));
      if(occ0==0){
	std::cout<<"Did not find occupancy histo "<<occ0<<std::endl;
	assert(0);
	return;
      }
    }
    for (int chip=0;chip<nchip;chip++){
       CORBA::Octet val=int(varValues[0]); //initial setting
      if(varValues[0]==-1 || j!=0){ //val==-1 means retuning, use GDAC values from file
	if(m_config[i]->getType()=="FEI4A") val=m_config[i]->getFEI4AConfig()->FEGlobal.Vthin_AltFine;
	else if(m_config[i]->getType()=="FEI4B") val=m_config[i]->getFEI4BConfig()->FEGlobal.Vthin_AltFine;
	else val=m_config[i]->getModuleConfig()->FEConfig[chip].FEGlobal.gdac;
      }
      if(j!=0){ //modify GDAC values appropriately
	float meanEff = 0.0;
	int diffval=int(varValues[j]);
	for (int col=0;col<ncol;col++){
	  for (int row=0;row<nrow;row++) meanEff += occ0->GetBinContent(chip*ncol+col+1, row+1) / repetitions;
	}
	meanEff /= npixused;
	if (meanEff < targetEff) {
	  if ((int)val-diffval > 0) {
	    val-=diffval;
	  }
	  else {
	    std::cout<<"Module "<< m_config[i]->getId()<<": GDAC already at 0"<<std::endl;
	    val=0;
	  }
	}
	else if (meanEff > targetEff) {
	  if ((int)val+diffval < maxval){
	    val+=diffval;
	  }
	  else {
	    val=maxval-1;
	    std::cout<<"Module "<< m_config[i]->getId()<<": GDAC already at "<<maxval-1<<std::endl;
	  }
	}
      }
      std::cout<<"Setting GDAC for module " << m_config[i]->getId() << " to " << (int)val << std::endl;
      if(m_config[i]->getType()=="FEI4A")m_config[i]->getFEI4AConfig()->FEGlobal.Vthin_AltFine=val;
      else if(m_config[i]->getType()=="FEI4B")m_config[i]->getFEI4BConfig()->FEGlobal.Vthin_AltFine=val;
      else m_config[i]->getModuleConfig()->FEConfig[chip].FEGlobal.gdac=val;
      threshhist->SetBinContent(1, val);
    }
    if(saveChecked()){
      TGListTreeItem* branch2=m_tree->FindChildByName(root,"loop2_0");
      TGListTreeItem* branch=m_tree->FindChildByName(branch2, subdir);
      TGListTreeItem* item=m_tree->AddItem(branch,CalibAnalysis::addPosition(name, m_config).c_str());
      item->SetPictures(m_thp,m_thp);
      if(m_config[i]->getType()=="FEI4A") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4AConfig());
      else if(m_config[i]->getType()=="FEI4B") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4BConfig());
      else m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getModuleConfig());
      m_file->cd(Form("loop2_0/%s", subdir));
      threshhist->Write();
      threshhist->SetDirectory(gDirectory);
    }
  }
  m_controller.downloadScanConfig(*(currentScanConfig()));
}

void CalibGui::setupGdacCoarseFastTuning(int j){ 
  if(!saveChecked()){
    std::cout<<"**** Check Save/Analyze before running this scan. ***"<<std::endl;
    return;
  }
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  char subdir[128];
  sprintf(subdir, "loop1_%d", j);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip, maxval, maxstage;
    if(m_config[i]->getType()=="FEI4A"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=120;
    }
    else {
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=256;
      maxstage=24;
    }
    currentScanConfig()->scanLoop[0].dataPoints[0] = currentScan()->getThresholdTargetValue();
    std::vector<float> varValues=currentScan()->getLoopVarValues(1);
    char name[128];
    sprintf (name, "GDACCoarse_settings_Mod_%d_it_%d", m_config[i]->getId(), j);
    TH1F* threshhist=new TH1F(name, Form("GDACCoarse settings module %d step %d", m_config[i]->getId(), j), 
			      nchip, 0, nchip);
    threshhist->SetMinimum(0);
    threshhist->GetYaxis()->SetTitle("GDACCoarse setting");
    threshhist->GetXaxis()->SetTitle("Chip");
    for (int chip=0;chip<nchip;chip++) {
      CORBA::Octet val = int(varValues[j]); //initial setting
      CORBA::Octet fineval = int(maxval-1);
      if ((int)val > maxval-1) {
	val = maxval-1;
	std::cout << "Module " << m_config[i]->getId() << ": GDACCoarse cannot be higher than " << (int)val << std::endl;
      }
      else if ((int)val < 0) {
	val = int(0);
	std::cout << "Module " << m_config[i]->getId() << ": GDACCoarse cannot be lower than " << (int)val << std::endl;
      }
      std::cout<<"Setting GDACCoarse for module " << m_config[i]->getId() << " to " << (int)val << std::endl;
      if (m_config[i]->getType() == "FEI4A") {
	m_config[i]->getFEI4AConfig()->FEGlobal.Vthin_AltCoarse = val;
	m_config[i]->getFEI4AConfig()->FEGlobal.Vthin_AltFine = fineval;
      } 
      else if (m_config[i]->getType() == "FEI4B") {
	m_config[i]->getFEI4BConfig()->FEGlobal.Vthin_AltCoarse = val;
	m_config[i]->getFEI4BConfig()->FEGlobal.Vthin_AltFine = fineval;
      }
      threshhist->SetBinContent(1, val);
    }
    if(saveChecked()){
      TGListTreeItem* branch2=m_tree->FindChildByName(root,"loop2_0");
      TGListTreeItem* branch=m_tree->FindChildByName(branch2, subdir);
      TGListTreeItem* item=m_tree->AddItem(branch,CalibAnalysis::addPosition(name, m_config).c_str());
      item->SetPictures(m_thp,m_thp);
      if(m_config[i]->getType()=="FEI4A") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4AConfig());
      else if(m_config[i]->getType()=="FEI4B") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4BConfig());
      m_file->cd(Form("loop2_0/%s", subdir));
      threshhist->Write();
      threshhist->SetDirectory(gDirectory);
    }
  }
  m_controller.downloadScanConfig(*(currentScanConfig()));
}

void CalibGui::setupTdacTuning(int j){ 
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  char subdir[128];
  sprintf(subdir, "loop1_%d", j);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip, maxval;
    if(m_config[i]->getType().substr(0,4)=="FEI4"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=32;
    }else{
      ncol=18;
      nrow=160;
      nchip=16;
      maxval=128;
    }
      //ipc::PixelFEI4Config *cfg=m_config[i]->getFEI4Config();
    std::vector<float> varValues=currentScan()->getLoopVarValues(1);
    int occlast=currentScan()->getLoopVarValues(0).size()-3;
    int target=currentScan()->getThresholdTargetValue();
    TH2 *mean=0, *chi2=0, *occ0=0, *occmax=0;
    char name[128];
    sprintf (name, "TDAC_settings_Mod_%d_it_%d", m_config[i]->getId(), j);
    TH2F* threshhist=new TH2F(name, Form("TDAC settings module %d step %d", m_config[i]->getId(), j), 
			      nchip*ncol, 0, nchip*ncol, nrow, 0, nrow);
    threshhist->SetMinimum(0);
    threshhist->GetXaxis()->SetTitle("Column");
    threshhist->GetYaxis()->SetTitle("Row");
    if(j!=0){
      mean=(TH2*)m_file->Get(Form( "loop1_%d/Mod_%d_Mean", j-1, m_config[i]->getId()));
      chi2=(TH2*)m_file->Get(Form( "loop1_%d/Mod_%d_ChiSquare", j-1, m_config[i]->getId()));
      occ0=(TH2*)m_file->Get(Form("loop1_%d/Mod_%d_Occupancy_Point_002", j-1, m_config[i]->getId()));
      occmax=(TH2*)m_file->Get(Form( "loop1_%d/Mod_%d_Occupancy_Point_%03d", j-1, m_config[i]->getId(),occlast));
      if(mean==0 || chi2==0 || occ0==0 || occmax==0){
	std::cout<<"Did not find histo "<<mean<<" "<<chi2<<" "<<occ0<<" "<<occmax<<std::endl;
	std::cout<<"occlast "<<occlast<<std::endl;
	assert(0);
	return;
      }
    }
    for (int chip=0;chip<nchip;chip++){
      for (int col=0;col<ncol;col++){
	for (int row=0;row<nrow;row++){
	  CORBA::Octet val=int(varValues[0]); //initial setting
	  if(varValues[0]==-1 || j!=0){ //val==-1 means retuning, use TDAC values from file
	    if(m_config[i]->getType()=="FEI4A") val=m_config[i]->getFEI4AConfig()->FETrims.dacThresholdTrim[col][row];
	    else if(m_config[i]->getType()=="FEI4B") val=m_config[i]->getFEI4BConfig()->FETrims.dacThresholdTrim[col][row];
	    else val=m_config[i]->getModuleConfig()->FEConfig[chip].FETrims.dacThresholdTrim[row][col];
	  }
	  if(j!=0){ //modify TDAC values appropriately
	    int diffval=int(varValues[j]);
	    if(m_config[i]->getType()=="FEI3"){
	      if(chi2->GetBinContent(chip*ncol+col+1, row+1)!=0){
		double m=mean->GetBinContent(chip*ncol+col+1, row+1);
		if(m<target && val+diffval<128)val+=diffval;
		else if(m>target && val>=diffval)val-=diffval;
	      }else if (occ0->GetBinContent(chip*ncol+col+1, row+1)!=0 && occmax->GetBinContent(chip*ncol+col+1, row+1)!=0
			&& val+diffval<128){
		val+=diffval;
	      }else if (occ0->GetBinContent(chip*ncol+col+1, row+1)==0 && occmax->GetBinContent(chip*ncol+col+1, row+1)==0 && val!=0
			&& val>=diffval){
		val-=diffval;
	      }
	    }else{
	      if(chi2->GetBinContent(col+1, row+1)!=0){
		double m=mean->GetBinContent(col+1, row+1);
		if(m>target && val+diffval<32)val+=diffval;
		else if(m<target && val>=diffval)val-=diffval;
	      }else if (occ0->GetBinContent(col+1, row+1)==0 && occmax->GetBinContent(col+1, row+1)==0
			&& val+diffval<32){
		val+=diffval;
	      }else if (occ0->GetBinContent(col+1, row+1)!=0 && occmax->GetBinContent(col+1, row+1)!=0 && val!=0
			&& val>=diffval){
		val-=diffval;
	      }
	    }
	  }
	  if(m_config[i]->getType()=="FEI4A")m_config[i]->getFEI4AConfig()->FETrims.dacThresholdTrim[col][row]=val;
	  else if(m_config[i]->getType()=="FEI4B")m_config[i]->getFEI4BConfig()->FETrims.dacThresholdTrim[col][row]=val;
	  else m_config[i]->getModuleConfig()->FEConfig[chip].FETrims.dacThresholdTrim[row][col]=val;
	  threshhist->SetBinContent(chip*ncol+col+1, row+1, val);
	}
      }
    }
    if(saveChecked()){
      TGListTreeItem* branch=m_tree->FindChildByName(root,subdir);
      TGListTreeItem* item=m_tree->AddItem(branch,CalibAnalysis::addPosition(name, m_config).c_str());
      item->SetPictures(m_thp,m_thp);
      if(m_config[i]->getType()=="FEI4A") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4AConfig());
      else if(m_config[i]->getType()=="FEI4B") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4BConfig());
      else m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getModuleConfig());
      m_file->cd(subdir);
      threshhist->Write();
      threshhist->SetDirectory(gDirectory);
    }
  }
  m_controller.downloadScanConfig(*(currentScanConfig()));
}

void CalibGui::setupTdacFastTuning(int j){
  TGListTreeItem* root=m_tree->FindChildByName(0,"Histos");
  char subdir[128];
  sprintf(subdir, "loop1_%d", j);
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip, maxval;
    if(m_config[i]->getType().substr(0,4)=="FEI4"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=32;
    }else{
      ncol=18;
      nrow=160;
      nchip=16;
      maxval=128;
    }
    //ipc::PixelFEI4Config *cfg=m_config[i]->getFEI4Config();
    currentScanConfig()->scanLoop[0].dataPoints[0] = currentScan()->getThresholdTargetValue();
    std::vector<float> varValues=currentScan()->getLoopVarValues(1);
    int repetitions = currentScan()->getRepetitions();
    double targetEff = 0.5;
    TH2  *occ0=0;
    char name[128];
    sprintf (name, "TDAC_settings_Mod_%d_it_%d", m_config[i]->getId(), j);
    TH2F* threshhist=new TH2F(name, Form("TDAC settings module %d step %d", m_config[i]->getId(), j), 
			      nchip*ncol, 0, nchip*ncol, nrow, 0, nrow);
    threshhist->SetMinimum(0);
    threshhist->GetXaxis()->SetTitle("Column");
    threshhist->GetYaxis()->SetTitle("Row");
    if(j!=0){
      occ0=(TH2*)m_file->Get(Form("loop1_%d/Mod_%d_Occupancy_Point_000", j-1, m_config[i]->getId()));
      if(occ0==0){
	std::cout<<"Did not find occupancy histo "<<occ0<<std::endl;
	assert(0);
	return;
      }
    }
    for (int chip=0;chip<nchip;chip++){
      for (int col=0;col<ncol;col++){
	for (int row=0;row<nrow;row++){
	  CORBA::Octet val=int(varValues[0]); //initial setting
	  if(varValues[0]==-1 || j!=0){ //val==-1 means retuning, use TDAC values from file
	    if(m_config[i]->getType()=="FEI4A") val=m_config[i]->getFEI4AConfig()->FETrims.dacThresholdTrim[col][row];
	    else if(m_config[i]->getType()=="FEI4B") val=m_config[i]->getFEI4BConfig()->FETrims.dacThresholdTrim[col][row];
	    else val=m_config[i]->getModuleConfig()->FEConfig[chip].FETrims.dacThresholdTrim[row][col];
	  }
	  if(j!=0){ //modify TDAC values appropriately
	    int diffval=int(varValues[j]);
	    if(m_config[i]->getType()=="FEI3"){
	      double m= occ0->GetBinContent(chip*ncol+col+1, row+1) / repetitions;
	      if(m>targetEff && val+diffval<128)val+=diffval;
	      else if(m<targetEff && val>diffval)val-=diffval;
	    }else{
	      double m=occ0->GetBinContent(col+1, row+1) / repetitions;
	      if(m<targetEff && val+diffval<32)val+=diffval;
	      else if(m>targetEff && val>diffval)val-=diffval;
	    }
	  }
	  if(m_config[i]->getType()=="FEI4A")m_config[i]->getFEI4AConfig()->FETrims.dacThresholdTrim[col][row]=val;
	  else if(m_config[i]->getType()=="FEI4B")m_config[i]->getFEI4BConfig()->FETrims.dacThresholdTrim[col][row]=val;
	  else m_config[i]->getModuleConfig()->FEConfig[chip].FETrims.dacThresholdTrim[row][col]=val;
	  threshhist->SetBinContent(chip*ncol+col+1, row+1, val);
	}
      }
    }
    if(saveChecked()){
      TGListTreeItem* branch=m_tree->FindChildByName(root,subdir);
      TGListTreeItem* item=m_tree->AddItem(branch,CalibAnalysis::addPosition(name, m_config).c_str());
      item->SetPictures(m_thp,m_thp);
      if(m_config[i]->getType()=="FEI4A") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4AConfig());
      else if(m_config[i]->getType()=="FEI4B") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4BConfig());
      else m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getModuleConfig());
      m_file->cd(subdir);
      threshhist->Write();
      threshhist->SetDirectory(gDirectory);
    }
  }
  m_controller.downloadScanConfig(*(currentScanConfig()));
}

void CalibGui::setupOffsetScan(int j){
  ipc::ScanOptions* scanPar=currentScanConfig();
  if(j==0){
    scanPar->stagingMode = CORBA::string_dup("FEI4_ENA_SCAP");
    for(unsigned int k=0;k<scanPar->scanLoop[0].nPoints;k++)
      scanPar->scanLoop[0].dataPoints[k]= k*3;
  }else{
    scanPar->stagingMode = CORBA::string_dup("FEI4_ENA_BCAP");
    for(unsigned int k=0;k<scanPar->scanLoop[0].nPoints;k++)
	scanPar->scanLoop[0].dataPoints[k]= k;
  }
  //m_controller.downloadScanConfig(*m_scan->getScanConfig());
  m_controller.downloadScanConfig(*(scanPar));
}

void CalibGui::setupMask(int loop_i, int j){
  //scanLoop loop_i loops through the different mask stages we want to cycle over  
  
  std::vector<float> varValues;
  if(loop_i==1){
    varValues=currentScan()->getLoopVarValues(1);
  }
  else if(loop_i==2){
    varValues=currentScan()->getLoopVarValues(2);
  }
  else{
    std::cout<<"CalibGui::setupMask error:  invalid value loop_i = "<<loop_i<<std::endl;
    return;
  }
  int stage = varValues[j];
  
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    if (m_config[i]->isIncluded()==false)continue;
    int ncol, nrow, nchip, maxval;
    if(m_config[i]->getType().substr(0,4)=="FEI4"){
      ncol=80;
      nrow=336;
      nchip=1;
      maxval=32;
    }else{
      //Not supporting FEI3, because the masking bits are different in that case 
      std::cout<<"CalibGui::setupMask ERROR:only FEI4 supported"<<std::endl;
      return;
      //ncol=18;
      //nrow=160;
      //nchip=16;
      //maxval=128;
    }
    
    // stage = row + 336*col + 1 (row from 0-335, col from 0-79.  We add 1 because we reserve stage==0 for special meaning)
    int rowInj = (stage-1)%336;
    int colInj = (stage-1)/336;
    
    for (int col=0;col<ncol;col++){
      for (int row=0;row<nrow;row++){
	
	//enable: bit 0, largeCap: bit 1, smallCap: bit 2
	
	//set enable for all bits on FE
	int val = (1<<ipc::enable);
	//set smallCap and largeCap just for the pixel we are injecting
	if(col==colInj && row==rowInj){
	  val |= (1<<ipc::largeCap);
	  val |= (1<<ipc::smallCap);
	}
	//	std::cout<<"row "<<row<<" col "<<col<<"  ; val = "<<val<<std::endl;
	
	if(m_config[i]->getType()=="FEI4A")m_config[i]->getFEI4AConfig()->FEMasks[col][row] = val;
	else if(m_config[i]->getType()=="FEI4B")m_config[i]->getFEI4BConfig()->FEMasks[col][row] = val;
	
      }
    }
    
    if(m_config[i]->getType()=="FEI4A") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4AConfig());
    else if(m_config[i]->getType()=="FEI4B") m_controller.downloadModuleConfig(m_config[i]->getRce(), m_config[i]->getId(), *m_config[i]->getFEI4BConfig());
    
  } //end loop over i
  
  m_controller.downloadScanConfig(*(currentScanConfig()));
  //This sets up mask stage, which sets Colpr_Mode to 3.  This is necessary, since
  //Colpr_Addr gets overwritten during configuration, so we need to be able to inject
  //any arbitrary pixel
  m_controller.setupMaskStage(stage);
    
}

PixScan* CalibGui::currentScan()
{
	if(m_primList->isLoaded())
		return m_primList->getCurrentScan();
	return m_scan->getPixScan();
}
std::string CalibGui::getScanName(){
  if(m_primList->isLoaded())return m_primList->getCurrScanName();
  else return m_scan->getTypeString();
}
ipc::ScanOptions* CalibGui::currentScanConfig()
{
	if(m_primList->isLoaded())
		return m_primList->getCurrentScanConfig();
	return m_scan->getScanConfig();
}
void CalibGui::EndOfPrimListScan()//Called from the end of run loop, either move to next item 
{
	if(!m_isrunning)
	{
		stopRun();
		return;
	}
	if(m_primList->getCurrentPause()!=0){
           m_cbinfo->addToMask(CallbackInfo::WAIT);
           sleep(m_primList->getCurrentPause());
        }
	if(m_primList->getCurrentUpdateConfig())
        {
	  m_globalconf->update();	
	}
	if(m_primList->currentScan+1 < m_primList->getNumScans() && m_isrunning){
	  finishLogFile();
	  m_primList->currentScan++;
	  setRun(true);
	}
	else{
	  stopRun();
	}
}
void CalibGui::logScan(){
  LogData logdata;
  m_scanLog->setAuthor("calibGui");
  m_scanLog->setScanName(getScanName().c_str());
  m_scanLog->setRunNumber(m_globalconf->getRunNumber());
  m_scanLog->setDebugging(debugChecked());
  logdata.comment=m_comment->GetText();
  logdata.exported=exportChecked();
  logdata.saved=saveChecked();
  if(saveChecked())logdata.datapath=m_globalconf->getDataDir();
  for (int i=0;i<ConfigGui::MAX_MODULES;i++){
    std::ostringstream ss;
    if(m_config[i]->isIncluded()){
      ss<<"Module: "<<m_config[i]->getModuleId()<<" FE: "<< m_config[i]->getId()<<" Cfg: "<< m_config[i]->getFilename();
      logdata.configs.push_back(ss.str());
    }
  }
  m_scanLog->logScan(logdata);
}
void CalibGui::finishLogFile(){
  std::string runstatus;
  if(isRunning()==false)runstatus="Aborted";
  else if(getStatus()==CalibGui::OK)runstatus="OK";
  else runstatus="Failed";
  std::string lstatus;
  if(isRunning()==false)lstatus="Aborted"; 
  else if(getStatus()==CalibGui::OK)lstatus="Done";
  else lstatus="Failed";
  m_scanLog->finishLogFile(runstatus, lstatus);
}

//====================================================================
int main(int argc, char **argv){
  //
  // Initialize command line parameters with default values
  //
  try {
    IPCCore::init( argc, argv );
  }
  catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
    return 1;
  }
  
//
// Declare command object and its argument-iterator
//       
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in.");
  CmdLine  cmd(*argv, &partition_name, NULL);
  CmdArgvIter  arg_iter(--argc, ++argv);
//
// Parse arguments
//
  cmd.parse(arg_iter);
  const char *p_name=getenv("TDAQ_PARTITION");
  if(p_name==NULL) p_name="rcetest";
  if(! partition_name.isNULL()) p_name= (const char*)partition_name;
  IPCPartition* partition=new IPCPartition(p_name );
  //check if somebody else is using the partition
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetPalette(1);

  TApplication theapp("app",&argc,argv);
  new CalibGui(partition, gClient->GetRoot(),800,735);
  theapp.Run();
  return 0;
}


