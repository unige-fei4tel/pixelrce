#ifndef IPCREGULARCALLBACK_HH
#define IPCREGULARCALLBACK_HH

#include "rcecalib/server/IPCCallback.hh"

class IPCRegularCallback : public IPCCallback {
public:
  IPCRegularCallback(): IPCCallback(){}
  void notify( const ipc::CallbackParams & msg );
  void stopServer();
};

#endif
