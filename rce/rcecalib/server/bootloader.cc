/// @file core.cc
/// @brief As the last part of the initialization of the system after
/// a reboot, load from flash, relocate and run the application module
/// selected by the RCE front panel.


#include "rce/debug/Debug.hh"
#include "rce/debug/Print.hh"
#include "rce/shell/ShellCommon.hh"
#include "rce/gdbstub/rtems-gdb-stub.h"


#include "rce/service/Thread.hh"
#include "rceusr/init/NetworkConfig.hh"
#include "rceusr/tool/DebugHandler.hh"
#include "rcecalib/server/EthPrimitive.hh"
#include "rcecalib/server/CmdDecoder.hh"
#include "rce/dynalink/Linker.hh"

#include "rce/pgp/DriverList.hh"

#include "rcecalib/server/BootLoaderPort.hh"

extern "C"{
  #include <librtemsNfs.h>
  #include <rtems/malloc.h>
}

//#include <omnithread.h>
#include <pthread.h>

#include <exception>
#include <stdio.h>
#include <string>

#include <iostream>


// We need to include <iostream> so that symbolic references to
// std::cout et al. are made.  The weak definitions will therefore be
// picked up from libstdc++.  Modules are not linked against libstdc++
// so they rely on the definitions in the core.
#include <iostream>

/*
// for speed test
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h> 
*/

using std::exception;
using std::string;

using RCE::Shell::startShell;

using RceDebug::printv;

using RceInit::configure_network_from_dhcp;

using RceSvc::Exception;

using RCE::Dynalink::Linker;
using RCE::Dynalink::LinkingError;

// use global IP address of bootp server in RTEMS
extern struct in_addr rtems_bsdnet_bootp_server_address;


namespace {
  int majorVersion=1;
  int minorVersion=4;
  string branch("devel");
}
extern "C" {

  // This symbol is set by the static linker (ld) to the start if the
  // "dynamic section" which the linker uses to find the system symbol
  // table.
  extern const char _DYNAMIC;
  
  void *posix_init( void *argument );
#if RCE_INTERFACE == 1
#warning RCE_INTERFACE=1
#elif RCE_INTERFACE == 0
#warning RCE_INTERFACE=0
#else
#error invalid RCE_INTERFACE
#endif


  unsigned interface = RCE_INTERFACE; 
  void init_executive()
  {
    try {
         RceInit::configure_network_from_dhcp(interface);
	 RcePgp::init_pgp();

	 (void)Linker::instance(&_DYNAMIC, majorVersion, minorVersion, branch);

         printf("starting shell\n");	
	 startShell();
	 // Start a debugger daemon
	 // First arg = 0 -->  use socket IO over TCP.
	 // Second arg = 0 --> Priority is chosen by the debugger daemon 
	 rtems_gdb_start(0, 0);
	 //rpcUdpInit();
	 		 // nfsInit( 0, 0 );
	 		 // printf("BOOTP server is %s. Use as NFS server.\n",inet_ntoa( rtems_bsdnet_bootp_server_address));
	 		 // nfsMount(inet_ntoa( rtems_bsdnet_bootp_server_address), NFSDIR ,"/nfs"); //NFSDIR comes from constituents.mk
         pthread_t mthread;
         pthread_attr_t attr;
         int stacksize;
         int ret;
         // setting a new size
         stacksize = (PTHREAD_STACK_MIN + 0x20000);
        // void* stackbase = (void *) malloc(size);
         ret = pthread_attr_init(&attr);
         ret = pthread_attr_setstacksize(&attr, stacksize);
	 struct sched_param sparam;
	 sparam.sched_priority = 5;
	 pthread_attr_setschedparam(&attr, &sparam);
	 pthread_create( &mthread, &attr , posix_init, NULL);
         rtems_malloc_statistics_t stats;      	
         malloc_get_statistics(&stats);
         printf("*** malloc statistics\n");
	 printf("space available: %uk\n",(unsigned int)stats.space_available/1024);
         printf("space allocated: %uk\n",(unsigned int)(stats.lifetime_allocated-stats.lifetime_freed)/1024);
	 
    } catch (Exception& e) {
      printv("*** RCE exception %s", e.what());
    } catch (exception& e) {
      printv("*** C++ exception %s", e.what());
    }
    printf("Done with init_executive");
  }
void *posix_init( void *argument ){
  std::cout<<"ATLAS Pixel RCE boot loader (built on " << BUILDDATE << " by " << BUILDUSER <<")" <<   std::endl;
  // std::cout<<"ATLAS Pixel RCE boot loader." <<   std::endl;	
  EthPrimitive eth(BootloaderPort);
  CmdDecoder cd(&eth);
  cd.run();
  
  //this is for speed testing
  /*
  EthPrimitive eth(3434);
  int length=1000000;
  char *buffer=new char[1000000];
  eth.receiveCommand();
  sleep(7);
 std::cout<<"Start"<<std::endl;
  eth.reply(buffer, 270256);
  std::cout<<"Stop"<<std::endl;
  */

//   more speed testing
/*
  int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char *buffer=new char[251000];
    portno = 3434;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        printf("ERROR opening socket\n");
    server = gethostbyname("172.21.6.45");
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        printf("ERROR connecting\n");
    int flag = 1;

    int ret = setsockopt( sockfd, IPPROTO_TCP, TCP_NODELAY, (char *)&flag, sizeof(flag) );
    for(int i=0;i<100;i++)n = write(sockfd,buffer,100);
    usleep(50000);
    n = write(sockfd,buffer,212);
    n = write(sockfd,buffer,10000);
    
    if (n < 0) 
      printf("Scheisse\n");
    close(sockfd);
*/
  return 0;
}

}
