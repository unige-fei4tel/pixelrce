#ifndef IPCGUICALLBACK_HH
#define IPCGUICALLBACK_HH

#include "rcecalib/server/IPCCallback.hh"

class CallbackInfo;

class IPCGuiCallback : public IPCCallback {
public:
  IPCGuiCallback(CallbackInfo* info): IPCCallback(),m_cbinfo(info){
  }
  void notify( const ipc::CallbackParams & msg );
  void stopServer();
private:
  CallbackInfo* m_cbinfo;
};

#endif
