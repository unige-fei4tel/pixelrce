#include "TurboDaqFile.hh"
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <typeinfo>
#include <fstream>
#include "PixelModuleConfig.hh"
#include "rcecalib/util/exceptions.hh"
#include <sys/stat.h> 
typedef unsigned int uint;

const int numberofcolumns = 18; // number of columns read in the readoutenable, pream, strobe and hitbus config file
const int numberofrows = 160; // number of rows read in the tdacs and fdacs config file
const int nofchip = 16; // number of chips per module

void TurboDaqFile::dump(const ipc::PixelModuleConfig &config) 
{ 
  std::cout<<"config.maskEnableFEConfig "<<config.maskEnableFEConfig<<std::endl;
  std::cout<<"config.maskEnableFEScan "<<config.maskEnableFEScan<<std::endl;
  std::cout<<"config.maskEnableFEDacs "<<config.maskEnableFEDacs<<std::endl;
  std::cout<<"config.feFlavour "<<(unsigned)config.feFlavour<<std::endl;
  std::cout<<"config.mccFlavour "<<(unsigned)config.mccFlavour<<std::endl;
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    std::cout<<"Chip "<<i<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEIndex "<<(unsigned) config.FEConfig[i].FEIndex<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.latency "<<(unsigned) config.FEConfig[i].FEGlobal.latency<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacIVDD2 "<<(unsigned) config.FEConfig[i].FEGlobal.dacIVDD2<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacIP2 "<<(unsigned) config.FEConfig[i].FEGlobal.dacIP2<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacID "<<(unsigned) config.FEConfig[i].FEGlobal.dacID<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacIP "<<(unsigned) config.FEConfig[i].FEGlobal.dacIP<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacITRIMTH "<<(unsigned) config.FEConfig[i].FEGlobal.dacITRIMTH<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacIF "<<(unsigned)config.FEConfig[i].FEGlobal.dacIF <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacITH1 "<<(unsigned) config.FEConfig[i].FEGlobal.dacITH1<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacITH2 "<<(unsigned)config.FEConfig[i].FEGlobal.dacITH2 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacIL "<<(unsigned) config.FEConfig[i].FEGlobal.dacIL<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacIL2 "<<(unsigned) config.FEConfig[i].FEGlobal.dacIL2<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacITRIMIF "<<(unsigned) config.FEConfig[i].FEGlobal.dacITRIMIF<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacSpare "<<(unsigned) config.FEConfig[i].FEGlobal.dacSpare<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.threshTOTMinimum "<<(unsigned) config.FEConfig[i].FEGlobal.threshTOTMinimum<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.threshTOTDouble "<<(unsigned) config.FEConfig[i].FEGlobal.threshTOTDouble<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.hitbusScaler "<<(unsigned) config.FEConfig[i].FEGlobal.hitbusScaler<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.capMeasure "<<(unsigned) config.FEConfig[i].FEGlobal.capMeasure<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.gdac "<<(unsigned)config.FEConfig[i].FEGlobal.gdac <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.selfWidth "<<(unsigned) config.FEConfig[i].FEGlobal.selfWidth<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.selfLatency "<<(unsigned)config.FEConfig[i].FEGlobal.selfLatency <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.muxTestPixel "<<(unsigned) config.FEConfig[i].FEGlobal.muxTestPixel<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.aregTrim "<<(unsigned) config.FEConfig[i].FEGlobal.aregTrim<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.aregMeas "<<(unsigned)config.FEConfig[i].FEGlobal.aregMeas <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dregTrim "<<(unsigned) config.FEConfig[i].FEGlobal.dregTrim<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dregMeas "<<(unsigned)config.FEConfig[i].FEGlobal.dregMeas <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.parity "<<(unsigned)config.FEConfig[i].FEGlobal.parity <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacMonLeakADC "<<(unsigned) config.FEConfig[i].FEGlobal.dacMonLeakADC<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.dacVCAL "<<(unsigned) config.FEConfig[i].FEGlobal.dacVCAL<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.widthSelfTrigger "<<(unsigned) config.FEConfig[i].FEGlobal.widthSelfTrigger<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.muxDO "<<(unsigned) config.FEConfig[i].FEGlobal.muxDO<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.muxMonHit "<<(unsigned)config.FEConfig[i].FEGlobal.muxMonHit <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.muxEOC "<<(unsigned) config.FEConfig[i].FEGlobal.muxEOC<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.frequencyCEU "<<(unsigned) config.FEConfig[i].FEGlobal.frequencyCEU<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.modeTOTThresh "<<(unsigned) config.FEConfig[i].FEGlobal.modeTOTThresh<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableTimestamp "<<(unsigned) config.FEConfig[i].FEGlobal.enableTimestamp<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableSelfTrigger "<<(unsigned) config.FEConfig[i].FEGlobal.enableSelfTrigger<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableHitParity "<<(unsigned) config.FEConfig[i].FEGlobal.enableHitParity<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monMonLeakADC "<<(unsigned)config.FEConfig[i].FEGlobal.monMonLeakADC <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monADCRef "<<(unsigned)config.FEConfig[i].FEGlobal.monADCRef <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableMonLeak "<<(unsigned)config.FEConfig[i].FEGlobal.enableMonLeak <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.statusMonLeak "<<(unsigned)config.FEConfig[i].FEGlobal.statusMonLeak <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCapTest "<<(unsigned) config.FEConfig[i].FEGlobal.enableCapTest<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableBuffer "<<(unsigned)config.FEConfig[i].FEGlobal.enableBuffer <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableVcalMeasure "<<(unsigned) config.FEConfig[i].FEGlobal.enableVcalMeasure<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableLeakMeasure "<<(unsigned) config.FEConfig[i].FEGlobal.enableLeakMeasure<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableBufferBoost "<<(unsigned)config.FEConfig[i].FEGlobal.enableBufferBoost <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP8 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP8 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monIVDD2 "<<(unsigned) config.FEConfig[i].FEGlobal.monIVDD2<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monID "<<(unsigned)config.FEConfig[i].FEGlobal.monID <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP7 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP7 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monIP2 "<<(unsigned)config.FEConfig[i].FEGlobal.monIP2 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monIP "<<(unsigned) config.FEConfig[i].FEGlobal.monIP<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP6 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP6 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monITRIMTH "<<(unsigned)config.FEConfig[i].FEGlobal.monITRIMTH <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monIF "<<(unsigned) config.FEConfig[i].FEGlobal.monIF<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP5 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP5 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monITRIMIF "<<(unsigned) config.FEConfig[i].FEGlobal.monITRIMIF<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monVCAL "<<(unsigned)config.FEConfig[i].FEGlobal.monVCAL <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP4 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP4 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCinjHigh "<<(unsigned)config.FEConfig[i].FEGlobal.enableCinjHigh <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableExternal "<<(unsigned)config.FEConfig[i].FEGlobal.enableExternal <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableTestAnalogRef "<<(unsigned) config.FEConfig[i].FEGlobal.enableTestAnalogRef<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableDigital "<<(unsigned)config.FEConfig[i].FEGlobal.enableDigital <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP3 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP3 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monITH1 "<<(unsigned) config.FEConfig[i].FEGlobal.monITH1<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monITH2 "<<(unsigned) config.FEConfig[i].FEGlobal.monITH2<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP2 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP2 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monIL "<<(unsigned) config.FEConfig[i].FEGlobal.monIL<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monIL2 "<<(unsigned)config.FEConfig[i].FEGlobal.monIL2 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP1 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP1 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableCP0 "<<(unsigned)config.FEConfig[i].FEGlobal.enableCP0 <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableHitbus "<<(unsigned)config.FEConfig[i].FEGlobal.enableHitbus <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.monSpare "<<(unsigned)config.FEConfig[i].FEGlobal.monSpare <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableAregMeas "<<(unsigned)config.FEConfig[i].FEGlobal.enableAregMeas <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableAreg "<<(unsigned)config.FEConfig[i].FEGlobal.enableAreg <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableLvdsRegMeas "<<(unsigned)config.FEConfig[i].FEGlobal.enableLvdsRegMeas <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableDregMeas "<<(unsigned) config.FEConfig[i].FEGlobal.enableDregMeas<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableTune "<<(unsigned)config.FEConfig[i].FEGlobal.enableTune <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableBiasComp "<<(unsigned) config.FEConfig[i].FEGlobal.enableBiasComp<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEGlobal.enableIpMonitor "<<(unsigned) config.FEConfig[i].FEGlobal.enableIpMonitor<<std::endl;
    for (int j=0;j<5;j++){
      for (int k=0;k<ipc::IPC_N_PIXEL_COLUMNS;k++){
	std::cout<<"config.FEConfig["<<i<<"].FEMasks.maskEnable["<<j<<"]["<<k<<"]"<< config.FEConfig[i].FEMasks.maskEnable[j][k]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEMasks.maskSelect["<<j<<"]["<<k<<"] "<< config.FEConfig[i].FEMasks.maskSelect[j][k]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEMasks.maskPreamp["<<j<<"]["<<k<<"] "<< config.FEConfig[i].FEMasks.maskPreamp[j][k]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FEMasks.maskHitbus["<<j<<"]["<<k<<"] "<<config.FEConfig[i].FEMasks.maskHitbus[j][k] <<std::endl;
      }
    }
    for (int j=0;j<ipc::IPC_N_PIXEL_ROWS;j++){
      for (int k=0;k<ipc::IPC_N_PIXEL_COLUMNS;k++){
	std::cout<<"config.FEConfig["<<i<<"].FETrims.dacThresholdTrim["<<j<<"]["<<k<<"] "<< (unsigned)config.FEConfig[i].FETrims.dacThresholdTrim[j][k]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FETrims.dacFeedbackTrim["<<j<<"]["<<k<<"] "<< (unsigned)config.FEConfig[i].FETrims.dacFeedbackTrim[j][k]<<std::endl;
      }
    }
    std::cout<<"config.FEConfig["<<i<<"].FECalib.cinjLo "<<config.FEConfig[i].FECalib.cinjLo <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.cinjHi "<< config.FEConfig[i].FECalib.cinjHi<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.vcalCoeff[0] "<<config.FEConfig[i].FECalib.vcalCoeff[0] <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.vcalCoeff[1] "<< config.FEConfig[i].FECalib.vcalCoeff[1]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.vcalCoeff[2] "<< config.FEConfig[i].FECalib.vcalCoeff[2]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.vcalCoeff[3] "<< config.FEConfig[i].FECalib.vcalCoeff[3]<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.chargeCoeffClo "<< config.FEConfig[i].FECalib.chargeCoeffClo<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.chargeCoeffChi "<<config.FEConfig[i].FECalib.chargeCoeffChi <<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.chargeOffsetClo "<< config.FEConfig[i].FECalib.chargeOffsetClo<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.chargeOffsetChi "<< config.FEConfig[i].FECalib.chargeOffsetChi<<std::endl;
    std::cout<<"config.FEConfig["<<i<<"].FECalib.monleakCoeff "<<config.FEConfig[i].FECalib.monleakCoeff <<std::endl;
  }
    std::cout<<"config.MCCRegisters.regCSR "<<config.MCCRegisters.regCSR <<std::endl;
    std::cout<<"config.MCCRegisters.regLV1 "<<config.MCCRegisters.regLV1 <<std::endl;
    std::cout<<"config.MCCRegisters.regFEEN "<<config.MCCRegisters.regFEEN <<std::endl;
    std::cout<<"config.MCCRegisters.regWFE "<<config.MCCRegisters.regWFE <<std::endl;
    std::cout<<"config.MCCRegisters.regWMCC "<< config.MCCRegisters.regWMCC<<std::endl;
    std::cout<<"config.MCCRegisters.regCNT "<<config.MCCRegisters.regCNT <<std::endl;
    std::cout<<"config.MCCRegisters.regCAL "<< config.MCCRegisters.regCAL<<std::endl;
    std::cout<<"config.MCCRegisters.regPEF "<<config.MCCRegisters.regPEF <<std::endl;
    std::cout<<"config.MCCRegisters.regWBITD "<< config.MCCRegisters.regWBITD<<std::endl;
    std::cout<<"config.MCCRegisters.regWRECD "<<config.MCCRegisters.regWRECD <<std::endl;
    std::cout<<"config.MCCRegisters.regSBSR "<<config.MCCRegisters.regSBSR <<std::endl;

}
      

void TurboDaqFile::getLineDos(std::ifstream& is, std::string& str){
  bool readline = false;
  while(readline != true){
    if(!is.good()){
      std::ostringstream errorstring;
      std::cout << "Error TurboDaqFile::getLineDos: not good file status" << std::endl;
      throw rcecalib::Config_File_Error(ERS_HERE);
    }
    std::getline(is,str);
    //std::cout<<str<<std::endl;
    if(str.c_str()[str.size()-1] == '\r')
      str.erase(str.size()-1, 1);
    std::stringstream s(str);
    std::string field;
    s >> field;
    if(field != "//"){
      readline = true;
    }
  }
  // cout << str.c_str() << endl;
}

int TurboDaqFile::columnPairMaskDecode(int mask, int columnPairNumber){

  switch(columnPairNumber){
  case 0:
    return ((mask & 0x001)>>0);
    break;
  case 1:
    return ((mask & 0x002)>>1);
    break;
  case 2:
    return ((mask & 0x004)>>2);
    break;
  case 3:
    return ((mask & 0x008)>>3);
    break;
  case 4:
    return ((mask & 0x010)>>4);
    break;
  case 5:
    return ((mask & 0x020)>>5);
    break;
  case 6:
    return ((mask & 0x040)>>6);
    break;
  case 7:
    return ((mask & 0x080)>>7);
    break;
  case 8:
    return ((mask & 0x100)>>8);
    break;
  default:
    std::stringstream a;
    a << "Request for decoding column pair number illegal in TurboDaqFile::columnPairMaskDecode. Mask value: " << mask;
    a << "Column Pair value: " << columnPairNumber << std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }    
  return 1;
}

void TurboDaqFile::intMaskFileDecode(CORBA::Octet arr[][ipc::IPC_N_PIXEL_COLUMNS], int valflag, std::ifstream* file){
  if(valflag == 128){ // Turbo Daq alternative file found
    int j;
    for(j = 0; j < numberofrows; j++){
      std::string readstring;
      getLineDos(*file,readstring);
      std::stringstream a(readstring);
      int ncols;
      for(ncols = 0; ncols < numberofcolumns; ncols++){
	int val;
	a>> val;
	arr[j][ncols]=val;
      }      
    }
  }
  else{
    for(int j = 0; j < numberofrows; j++){
      for(int ncols = 0; ncols < numberofcolumns; ncols++){
	arr[j][ncols]=valflag;
      }
    }   
  }
}

void TurboDaqFile::boolMaskFileDecode(CORBA::ULong arr[][ipc::IPC_N_PIXEL_COLUMNS] , int valflag, std::ifstream* file){
  if(valflag == 0) { // Turbo Daq alternative file found
    for(int j = 0; j < numberofcolumns; j++){
      std::string readstring;
      getLineDos(*file,readstring);
      std::stringstream a(readstring);
      unsigned column;
      a>>std::dec>>column;
      for(int nrows = 0; nrows <5; nrows++){
	unsigned columnmask;
	a >> std::hex >>columnmask;
	arr[4-nrows][column]=columnmask;
      }
    }
  }
  else{
    unsigned val;
    if(valflag == 1)val=0; // TurboDaq all off found
    else if(valflag == 2)val=0xffffffff; // TurboDaq all on found
    else{
      std::stringstream a;
      a << "TurboDaqFile::intMaskFileDecode: Turbo daq code not correct (0, 1 or 2 allowed) " << valflag; 
      throw rcecalib::Config_File_Error(ERS_HERE);
    }
    for(int j = 0; j < numberofcolumns; j++){
      for(int nrows = 0; nrows < 5; nrows++){
	arr[nrows][j]=val;
      }
    }
  }  
}


void TurboDaqFile::openTurboDaqFiles(std::string filename){
  m_moduleCfgFile=new std::ifstream(filename.c_str());
  m_moduleCfgFilePath = filename;
  if(!m_moduleCfgFile->good()){
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
}

void TurboDaqFile::readModuleConfig(ipc::PixelModuleConfig* mod, std::string namefile){
  //std::cout<<namefile<<std::endl;
  
  std::string readstring;
  std::stringstream modstream;
  int modrunnint;
  //clear structure
  char* ccfg=(char*)mod;
  for (unsigned int i=0;i<sizeof(ipc::PixelModuleConfig);i++)ccfg[i]=0;
  openTurboDaqFiles(namefile);
  std::string readString;
  getLineDos(*m_moduleCfgFile,readString);
  getLineDos(*m_moduleCfgFile,readString);
  getLineDos(*m_moduleCfgFile,readString);
  getLineDos(*m_moduleCfgFile,readString);

  if(readString == "1"){ // module
  }
  else if(readString == "0"){ // single chip
    //  readSingleChipConfig(m_rootRecord);
    std::cout<<"Single Chip assembly record, not implemented"<<std::endl;
    throw rcecalib::Config_File_Error(ERS_HERE);
  }
  else throw rcecalib::Config_File_Error(ERS_HERE);

  getLineDos(*m_moduleCfgFile, readstring);
  getLineDos(*m_moduleCfgFile, readstring); // here we read the MCC type
  modstream.str(readstring); 
  modstream >> modrunnint; modstream.clear();
  mod->mccFlavour=modrunnint;
  // MCC 
  //mod->MCCRegisters.regFEEN=0xffff;

  getLineDos(*m_moduleCfgFile, readstring);
  getLineDos(*m_moduleCfgFile, readstring); // here we read the FE type
  modstream.str(readstring); 
  modstream >> modrunnint; modstream.clear();
  mod->feFlavour=modrunnint;
  int feflavour=modrunnint;

  // variables used to temporary store the information from config file
  std::vector<std::string> rmname; rmname.resize(nofchip); 
  std::vector<std::string> smname; smname.resize(nofchip); 
  std::vector<std::string> pkname; pkname.resize(nofchip); 
  std::vector<std::string> hmname; hmname.resize(nofchip); 
  std::vector<std::string> tdname; tdname.resize(nofchip); 
  std::vector<std::string> fdname; fdname.resize(nofchip); 


  int runnInt;
  std::string runString;
  std::stringstream a(readstring); // Chip 0 geographical address (0-15)

  // scan the config file
  for(int i = 0; i < nofchip; i++){

    for(int j = 0; j < 4; j++) getLineDos(*m_moduleCfgFile,readstring);

    getLineDos(*m_moduleCfgFile,readstring);
    a.str(readstring); 
    a>>runnInt; a.clear();
    //mod->FEConfig[number].FECommand.address=runnInt;
    mod->FEConfig[i].FEIndex=runnInt;
    mod->FEConfig[i].FEGlobal.latency=255;
    mod->FEConfig[i].FEGlobal.frequencyCEU=2;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 global configuration enable (0 = off, 1 = on)
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->maskEnableFEConfig &= (~(0x1<<i));  mod->maskEnableFEConfig |= (runnInt<<i);

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 global scan/readout enable (0 = off, 1 = on)
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->maskEnableFEScan   &= (~(0x1<<i));  mod->maskEnableFEScan   |= (runnInt<<i);

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 DACs enable (0 = off, 1 = on)
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->maskEnableFEDacs   &= (~(0x1<<i));  mod->maskEnableFEDacs   |= (runnInt<<i);

	if(feflavour == 1){
		getLineDos(*m_moduleCfgFile,readstring); 
		getLineDos(*m_moduleCfgFile,readstring); // Chip 0 Global Threshold DAC
		a.str(readstring);
		a>>runnInt; a.clear();
		mod->FEConfig[i].FEGlobal.gdac=runnInt;
	}

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 IVDD2 DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacIVDD2=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 ID DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacID=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 IP2 DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacIP2=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 IP DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacIP=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 TRIMT DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacITRIMTH =runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 IF DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacIF=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 TRIMF DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacITRIMIF=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 ITH1 DAC 
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacITH1=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 ITH2 DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacITH2=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 IL DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacIL=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 IL2 DAC
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.dacIL2=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 column-pair mask parameter
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.enableCP0= columnPairMaskDecode(runnInt,0);
    mod->FEConfig[i].FEGlobal.enableCP1= columnPairMaskDecode(runnInt,1);
    mod->FEConfig[i].FEGlobal.enableCP2= columnPairMaskDecode(runnInt,2);
    mod->FEConfig[i].FEGlobal.enableCP3= columnPairMaskDecode(runnInt,3);
    mod->FEConfig[i].FEGlobal.enableCP4= columnPairMaskDecode(runnInt,4);
    mod->FEConfig[i].FEGlobal.enableCP5= columnPairMaskDecode(runnInt,5);
    mod->FEConfig[i].FEGlobal.enableCP6= columnPairMaskDecode(runnInt,6);
    mod->FEConfig[i].FEGlobal.enableCP7= columnPairMaskDecode(runnInt,7);
    mod->FEConfig[i].FEGlobal.enableCP8= columnPairMaskDecode(runnInt,8);

    mod->FEConfig[i].FEGlobal.hitbusScaler          = 0; // : 8; ???
    mod->FEConfig[i].FEGlobal.selfWidth             = 0; // : 4; ???
    mod->FEConfig[i].FEGlobal.selfLatency           = 0; // : 4; ???
    mod->FEConfig[i].FEGlobal.aregTrim              = 1; // : 2;
    mod->FEConfig[i].FEGlobal.aregMeas              = 0; // : 2;
    mod->FEConfig[i].FEGlobal.dregTrim              = 1; // : 2;
    mod->FEConfig[i].FEGlobal.dregMeas              = 0; // : 2;
    mod->FEConfig[i].FEGlobal.parity                = 0; // : 1;                
    mod->FEConfig[i].FEGlobal.enableHitParity       = 0; // : 1;                    
    mod->FEConfig[i].FEGlobal.enableMonLeak         = 0; // : 1;            
    mod->FEConfig[i].FEGlobal.enableHitbus          = 1; // : 1;                 
    mod->FEConfig[i].FEGlobal.monSpare              = 0; // : 1; ???                     
    mod->FEConfig[i].FEGlobal.enableAregMeas        = 0; // : 1;
    mod->FEConfig[i].FEGlobal.enableAreg            = 0; // : 1;
    mod->FEConfig[i].FEGlobal.enableLvdsRegMeas     = 0; // : 1; ???
    mod->FEConfig[i].FEGlobal.enableDregMeas        = 0; // : 1;
    mod->FEConfig[i].FEGlobal.enableTune            = 0; // : 1;
    mod->FEConfig[i].FEGlobal.enableBiasComp        = 1; // : 1;
    mod->FEConfig[i].FEGlobal.enableIpMonitor       = 0; // : 1; ???

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 Timestamp enable (0 = OFF, 1 = ON)
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.enableTimestamp=runnInt;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 Decoupling capacitor enable (0 = OFF, 1 = ON)
    a.str(readstring);
    a>>runnInt; a.clear();
    mod->FEConfig[i].FEGlobal.enableCapTest=runnInt;

    std::ifstream * ifp = 0;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 readout mask mode (see end for details)
    a.str(readstring);
    a>>runnInt; a.clear();
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 readout mask file (if appl.)
    a.str(readstring);
    a>>rmname[i]; a.clear(); //cout << rmname[i].c_str() << endl;

    if(runnInt == 0){

      std::string full_path = getFullPath(rmname[i]);
      ifp = new std::ifstream;

      ifp->open(full_path.c_str());

      if(!ifp->good()){

	    std::stringstream a;
	    a << "Chip " << i << " ";
	    a << "readout mask file not open as good: " << full_path.c_str();

	    throw rcecalib::Config_File_Error(ERS_HERE);

      }
    }

    //ENABLE
    boolMaskFileDecode(mod->FEConfig[i].FEMasks.maskEnable, runnInt, ifp);
    if(runnInt == 0) delete ifp;
    

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 strobe mask mode (see end for details)
    a.str(readstring);
    a>>runnInt; a.clear();
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 strobe mask file (if appl.)
    a.str(readstring);
    a>>smname[i]; a.clear();
    if(runnInt == 0){
      std::string full_path = getFullPath(smname[i]);

      ifp = new std::ifstream;
      ifp->open(full_path.c_str());
      if(!ifp->good()){
	std::stringstream a;
	a << "Chip " << i << " ";
	a << "strobe mask file not open as good: " << full_path.c_str();
	throw rcecalib::Config_File_Error(ERS_HERE);
      }
    }    


    //SELECT
    boolMaskFileDecode(mod->FEConfig[i].FEMasks.maskSelect, runnInt, ifp);
    if (runnInt == 0) delete ifp;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 preamp kill mask mode (FE-I only)
    a.str(readstring);
    a>>runnInt; a.clear();
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 preamp mask file (if appl.)
    a.str(readstring);
    a>>pkname[i]; a.clear();
    if(runnInt == 0){
      std::string full_path = getFullPath(pkname[i]);
      ifp = new std::ifstream;
      ifp->open(full_path.c_str());
      if(!ifp->good()){
	std::stringstream a;
	a << "Chip " << i << " ";
	a << "preamp mask file not open as good: " << full_path.c_str();
	throw rcecalib::Config_File_Error(ERS_HERE);
      }
    }
    //PREAMP
    boolMaskFileDecode(mod->FEConfig[i].FEMasks.maskPreamp, runnInt, ifp);
    if (runnInt == 0) delete ifp;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 hitbus mask mode (FE-I only)
    a.str(readstring);
    a>>runnInt; a.clear();
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 hitbus mask file (if appl.)
    a.str(readstring);
    a>>hmname[i]; a.clear();
    if(runnInt == 0){
      std::string full_path = getFullPath(hmname[i]);
      ifp = new std::ifstream;
      ifp->open(full_path.c_str());
      if(!ifp->good()){
	std::stringstream a;
	a << "Chip " << i << " ";
	a << "hitbus mask file not open as good: " << full_path.c_str();
	throw rcecalib::Config_File_Error(ERS_HERE);
      }
    }
    //HITBUS
    boolMaskFileDecode(mod->FEConfig[i].FEMasks.maskHitbus, runnInt, ifp);
    if (runnInt == 0) delete ifp;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 TDAC mode (n = all n (for n = 0-31), 32 = alternative file)
    a.str(readstring);
    a>>runnInt; a.clear();
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 TDAC file
    a.str(readstring);
    a>>tdname[i]; a.clear();
    if(runnInt == 128){
      std::string full_path = getFullPath(tdname[i]);
      ifp = new std::ifstream;
      ifp->open(full_path.c_str());
      if(!ifp->good()){
	std::stringstream a;
	a << "Chip " << i << " ";
	a << "TDAC file not open as good: " << full_path.c_str();
	std::cout<<"Could not open file "<<full_path.c_str()<<std::endl;
	throw rcecalib::Config_File_Error(ERS_HERE);
      }
    }
    //TDAC
    intMaskFileDecode(mod->FEConfig[i].FETrims.dacThresholdTrim, runnInt, ifp);
    if (runnInt == 128) delete ifp;

    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 FDAC mode (n = all n (for n = 0-31), 32 = alternative file)
    a.str(readstring);
    a>>runnInt; a.clear();
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 FDAC file
    a.str(readstring);
    a>>fdname[i]; a.clear();
    if(runnInt == 128){
      std::string full_path = getFullPath(fdname[i]);
      ifp = new std::ifstream;
      ifp->open(full_path.c_str());
      if(!ifp->good()){
	std::stringstream a;
	a << "Chip " << i << " ";
	a << "FDAC file not open as good: " << full_path.c_str();
	throw rcecalib::Config_File_Error(ERS_HERE);
      }
    }
    // FDAC
    intMaskFileDecode(mod->FEConfig[i].FETrims.dacFeedbackTrim, runnInt, ifp);
    if (runnInt == 128) delete ifp;
  }

  getLineDos(*m_moduleCfgFile, readstring);
  getLineDos(*m_moduleCfgFile, readstring);
  getLineDos(*m_moduleCfgFile, readstring); 

  float runnFloat;
  {for(int k = 0; k < nofchip; k++){
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip k Cinj-LO (fF)
    a.str(readstring);
    a>>runnFloat; a.clear();
    mod->FEConfig[k].FECalib.cinjLo = runnFloat;
  }}    

  {for(int k = 0; k < nofchip; k++){
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip k Cinj-HI (fF)
    a.str(readstring);
    a>>runnFloat; a.clear();
    mod->FEConfig[k].FECalib.cinjHi = runnFloat;
  }}    


  {for(int k = 0; k < nofchip; k++){
    bool isCubicFit=false;
    getLineDos(*m_moduleCfgFile,readstring); 
    if((int)readstring.find("VCAL-FE coefficients")!=(int)std::string::npos)
      isCubicFit = true;
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 VCAL-FE gradient (mV/count)
    a.str(readstring);
    float c0, c1, c2, c3;
    if(isCubicFit){ // new TurboDAQ format has slope, cubic and quadratic fit pars in one row
      a.str(readstring);
      a>>c3>>c2>>c1>>c0; a.clear();
    } else { 
      a>>c1; a.clear();
      c0 = c2 = c3 = 0.0;
    }
    mod->FEConfig[k].FECalib.vcalCoeff[0]=c0;
    mod->FEConfig[k].FECalib.vcalCoeff[1]=c1;
    mod->FEConfig[k].FECalib.vcalCoeff[2]=c2;
    mod->FEConfig[k].FECalib.vcalCoeff[3]=c3;
  }}    

  {for(int k = 0; k < nofchip; k++){
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // Chip 0 Internal-injection offset correction (VCAL-FE counts)
    a.str(readstring);
    a>>runnFloat; a.clear();
    //fefi = new TurboDaqFileField("OffsetCorrection", FLOAT);
  }}    

  // two empty lines and TPLL XCKr phase
  getLineDos(*m_moduleCfgFile,readstring); 
  getLineDos(*m_moduleCfgFile,readstring); 
  getLineDos(*m_moduleCfgFile,readstring); 
  getLineDos(*m_moduleCfgFile,readstring); 

  {for(int k = 0; k < 16; k++){
    getLineDos(*m_moduleCfgFile,readstring); 
    getLineDos(*m_moduleCfgFile,readstring); // MCC CAL strobe-delay range 0: calibration factor (ns/count)
    a.str(readstring);
    a>>runnFloat; a.clear();
    std::string name = "DELAY_";
    std::stringstream b;
    b << k;
    name += b.str(); 
    // fefi = new TurboDaqFileField(name.c_str(), FLOAT);
        //fefi->m_floatContent = runnFloat;
        //insertField(*it, fefi);
  }}    
  getLineDos(*m_moduleCfgFile,readstring);
  getLineDos(*m_moduleCfgFile,readstring);
  getLineDos(*m_moduleCfgFile,readstring);
  sprintf((char*)mod->idStr, readstring.c_str());
  delete m_moduleCfgFile;
}

//FS: writing out config file...


void TurboDaqFile::writeMaskFile(CORBA::ULong arr[][ipc::IPC_N_PIXEL_COLUMNS], std::string path){
  std::ofstream file(path.c_str());
  for (int j = 0; j < ipc::IPC_N_PIXEL_COLUMNS; j++) {
    if (j<10)
      file<<std::dec<<j<<"      ";
    else
      file<<std::dec<<j<<"     ";
    for (int i = 0; i < 5; i++){
      file<<std::hex<<arr[4-i][j]<<" ";
    }
    file<<std::endl;
  }
  file.close();
}

void TurboDaqFile::writeMaskFileFT(CORBA::Octet arr[][ipc::IPC_N_PIXEL_COLUMNS], std::string path){
  std::ofstream file(path.c_str());
  for (int i = 0; i < numberofrows; i++) {
    for (int j = 0; j < numberofcolumns; j++){
        file<<(int) arr[i][j]<<" ";
    }
    file<<std::endl;
  }
  file.close();
}

void TurboDaqFile::writeModuleConfig(ipc::PixelModuleConfig* config, const std::string &base, const std::string &confdir, const std::string &configname, const std::string &key){
  struct stat stFileInfo;
  int intStat;
  // Attempt to get the file attributes
  intStat = stat(base.c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Directory "<<base<<" does not exist. Not writing config file"<<std::endl;
    return;
  }

  intStat = stat((base+"/"+confdir).c_str(),&stFileInfo);
  if(intStat != 0) { //File does not exist
    std::cout<<"Directory "<<base<<"/"<<confdir<<" does not exist. Creating."<<std::endl;
    mkdir (base.c_str(),0777);
    mkdir ((base+"/"+confdir).c_str(),0777);
    mkdir ((base+"/"+confdir+"/configs").c_str(),0777);
    mkdir ((base+"/"+confdir+"/masks").c_str(),0777);
    mkdir ((base+"/"+confdir+"/tdacs").c_str(),0777);
    mkdir ((base+"/"+confdir+"/fdacs").c_str(),0777);
  }

  std::string cfgname=configname;
  if(key.size()!=0) cfgname+="__"+key;
  std::string fullpath=base+"/"+confdir+"/configs/"+cfgname+".cfg";
    std::cout<<fullpath<<std::endl;
  std::ofstream cfgfile(fullpath.c_str());
  cfgfile<<"TurboDAQ VERSION 6.6"<<std::endl;
  cfgfile<<std::endl;
  cfgfile<<"Assembly type (0 = Single-chip, 1 = Module)"<<std::endl;
  cfgfile<<"1"<<std::endl;
  cfgfile<<"MCC flavour if applicable (1 = MCC-I1, 2 = MCC-I2)"<<std::endl;
  cfgfile<<"2"<<std::endl;
  cfgfile<<"FE flavour (0 = FE-I1, 1 = FE-I2)"<<std::endl;
  cfgfile<<"1"<<std::endl;
  cfgfile<<std::endl;
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    std::stringstream iStringstream;
    std::string iString;
    iStringstream << i;
    if (i <10)
      iString = "0" + iStringstream.str();
    else
      iString = iStringstream.str();
      
    cfgfile<<"CHIP "<<i<<":"<<std::endl;
    cfgfile<<std::endl;
    cfgfile<<"Chip "<<i<<" geographical address (0-15)"<<std::endl<<(unsigned) config->FEConfig[i].FEIndex<<std::endl;
    
    int enable = config->maskEnableFEConfig & (0x1<<i);
    bool isEnable = (enable!=0);
    cfgfile<<"Chip "<<i<<" global configuration enable (0 = off, 1 = on)"<<std::endl<<isEnable<<std::endl;
    
    enable = config->maskEnableFEScan & (0x1<<i);
    isEnable = (enable!=0);
    cfgfile<<"Chip "<<i<<" global scan/readout enable (0 = off, 1 = on)"<<std::endl<<isEnable<<std::endl;

	enable = config->maskEnableFEDacs & (0x1<<i);
    isEnable = (enable!=0);
    cfgfile<<"Chip "<<i<<" DACs enable (0 = off, 1 = on)"<<std::endl<<isEnable<<std::endl;
    cfgfile<<"Chip "<<i<<" Global Threshold DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.gdac<<std::endl;
    cfgfile<<"Chip "<<i<<" IVDD2 DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacIVDD2<<std::endl;
    cfgfile<<"Chip "<<i<<" ID DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacID<<std::endl;
    cfgfile<<"Chip "<<i<<" IP2 DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacIP2<<std::endl;
    cfgfile<<"Chip "<<i<<" IP DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacIP<<std::endl;
    cfgfile<<"Chip "<<i<<" TRIMT DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacITRIMTH<<std::endl;
    cfgfile<<"Chip "<<i<<" IF DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacIF<<std::endl;
    cfgfile<<"Chip "<<i<<" TRIMF DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacITRIMIF<<std::endl;
    cfgfile<<"Chip "<<i<<" ITH1 DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacITH1<<std::endl;
    cfgfile<<"Chip "<<i<<" ITH2 DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacITH2<<std::endl;
    cfgfile<<"Chip "<<i<<" IL DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacIL<<std::endl;
    cfgfile<<"Chip "<<i<<" IL2 DAC"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.dacIL2<<std::endl;
    
    // FS: Encoding mask parameter from enableCPi's
    int mask = 0;
    mask = mask | config->FEConfig[i].FEGlobal.enableCP0;
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP1<<1);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP2<<2);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP3<<3);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP4<<4);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP5<<5);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP6<<6);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP7<<7);
    mask = mask | (config->FEConfig[i].FEGlobal.enableCP8<<8);
    cfgfile<<"Chip "<<i<<" column-pair mask parameter"<<std::endl<<mask<<std::endl;
    
    cfgfile<<"Chip "<<i<<" Timestamp enable (0 = OFF, 1 = ON)"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.enableTimestamp<<std::endl;
	cfgfile<<"Chip "<<i<<" Decoupling capacitor enable (0 = OFF, 1 = ON)"<<std::endl<<(unsigned) config->FEConfig[i].FEGlobal.enableCapTest<<std::endl;
	std::string path = confdir+"/masks/enable_"+iString+"_"+cfgname+".dat";
	cfgfile<<"Chip "<<i<<" readout mask mode (see end for details)"<<std::endl<<"0"<<std::endl;
	cfgfile<<"Chip "<<i<<" readout mask file (if appl.)"<<std::endl<<path<<std::endl;
	path  = base+"/"+confdir+"/masks/enable_"+iString+"_"+cfgname+".dat";
	writeMaskFile(config->FEConfig[i].FEMasks.maskEnable, path);

	path=confdir+"/masks/strobe_"+iString+"_"+cfgname+".dat";
	cfgfile<<"Chip "<<i<<" strobe mask mode (see end for details)"<<std::endl<<"0"<<std::endl;
	cfgfile<<"Chip "<<i<<" strobe mask file (if appl.)"<<std::endl<<path<<std::endl;
	path=base+"/"+confdir+"/masks/strobe_"+iString+"_"+cfgname+".dat";
	writeMaskFile(config->FEConfig[i].FEMasks.maskSelect, path);
	
	path=confdir+"/masks/preamp_"+iString+"_"+cfgname+".dat";
	cfgfile<<"Chip "<<i<<" preamp kill mask mode (FE-I only)"<<std::endl<<"0"<<std::endl;
	cfgfile<<"Chip "<<i<<" preamp mask file (if appl.)"<<std::endl<<path<<std::endl;
	path=base+"/"+confdir+"/masks/preamp_"+iString+"_"+cfgname+".dat";
	writeMaskFile(config->FEConfig[i].FEMasks.maskPreamp, path);
	
	path=confdir+"/masks/hitbus_"+iString+"_"+cfgname+".dat";
	cfgfile<<"Chip "<<i<<" hitbus mask mode (FE-I only)"<<std::endl<<"0"<<std::endl;
	cfgfile<<"Chip "<<i<<" hitbus mask file (if appl.)"<<std::endl<<path<<std::endl;
	path=base+"/"+confdir+"/masks/hitbus_"+iString+"_"+cfgname+".dat";
	writeMaskFile(config->FEConfig[i].FEMasks.maskHitbus, path);
	
	path=confdir+"/tdacs/tdac_"+iString+"_"+cfgname+".dat";
	cfgfile<<"Chip "<<i<<" TDAC mode (n = all n (for n = 0-31), 128 = alternative file)"<<std::endl<<"128"<<std::endl;
	cfgfile<<"Chip "<<i<<" TDAC file"<<std::endl<<path<<std::endl;
	path=base+"/"+confdir+"/tdacs/tdac_"+iString+"_"+cfgname+".dat";
	writeMaskFileFT(config->FEConfig[i].FETrims.dacThresholdTrim, path);
	
	path=confdir+"/fdacs/fdac_"+iString+"_"+cfgname+".dat";
	cfgfile<<"Chip "<<i<<" FDAC mode (n = all n (for n = 0-31), 128 = alternative file)"<<std::endl<<"128"<<std::endl;
	cfgfile<<"Chip "<<i<<" FDAC file"<<std::endl<<path<<std::endl;
	path=base+"/"+confdir+"/fdacs/fdac_"+iString+"_"+cfgname+".dat";
	writeMaskFileFT(config->FEConfig[i].FETrims.dacFeedbackTrim, path);
	cfgfile<<std::endl;
  }
  
  cfgfile<<"Calibration Parameters:"<<std::endl;
  cfgfile<<std::endl;
  
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    cfgfile<<"Chip "<<i<<" Cinj-LO (fF)"<<std::endl<<(float) config->FEConfig[i].FECalib.cinjLo<<std::endl;
  }
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    cfgfile<<"Chip "<<i<<" Cinj-HI (fF)"<<std::endl<<(float) config->FEConfig[i].FECalib.cinjHi<<std::endl;
  }
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    cfgfile<<"Chip "<<i<<" VCAL-FE coefficients (V(mV) = [0]*vcal**3+[1]*vcal**2+[2]*vcal+[3])"<<std::endl;
    cfgfile<<(float) config->FEConfig[i].FECalib.vcalCoeff[3]<<" "<<(float) config->FEConfig[i].FECalib.vcalCoeff[2]<<" "<<(float) config->FEConfig[i].FECalib.vcalCoeff[1]<<" "<<(float) config->FEConfig[i].FECalib.vcalCoeff[0]<<std::endl;
  }
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    // FS: Is not used by read-in function, thus being set to 0
    cfgfile<<"Chip "<<i<<" Internal-injection offset correction (VCAL-FE counts)"<<std::endl<<"0"<<std::endl; 
  }
  cfgfile<<std::endl;
  cfgfile<<"TPLL XCKr phase"<<std::endl;
  cfgfile<<"0"<<std::endl; // FS: Is not used by read-in function, thus being set to 0
  cfgfile<<std::endl;
  
  for(int i=0;i<ipc::IPC_N_PIXEL_FE_CHIPS;i++){
    // FS: Is not used by read-in function, thus being set to 0
    cfgfile<<"MCC CAL strobe-delay range "<<i<<": calibration factor (ns/count)"<<std::endl<<"0"<<std::endl;
  }
  cfgfile<<std::endl;
  cfgfile<<"Module string identifier"<<std::endl;
  cfgfile<<config->idStr<<std::endl;
}

TurboDaqFile::TurboDaqFile() {}


std::string TurboDaqFile::getFullPath(std::string relPath){
  std::string newPath = relPath, basePath=m_moduleCfgFilePath, testName;
  unsigned int pos;
  // skip config file-name part of base path
  pos = basePath.find_last_of('/');
  if(pos!=std::string::npos) basePath.erase(pos,basePath.length()-pos);
  // skip "config" part of base path
  pos = basePath.find_last_of('/');
  if(pos!=std::string::npos) basePath.erase(pos,basePath.length()-pos);
  // now skip module part of base path, but keep last "/"
  pos = basePath.find_last_of('/');
  if(pos!=std::string::npos) basePath.erase(pos+1,basePath.length()-pos);
  else basePath="";
  // then add relative path of DAC or mask file
  newPath = basePath + newPath;
  return newPath;
}

