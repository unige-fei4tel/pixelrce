#include <stdio.h>

#include <ers/ers.h>

#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <owl/timer.h>
#include <cmdl/cmdargs.h>
#include <unistd.h>

#include "rcecalib/server/TurboDaqFile.hh"
#include "rcecalib/server/IPCController.hh"
#include "rcecalib/server/IPCHistoController.hh"
#include "rcecalib/server/PixScan.hh"

#include "IPCScanAdapter.hh"
#include "IPCConfigIFAdapter.hh"
#include "IPCFEI3Adapter.hh"
#include "ScanOptions.hh"
#include "PixelModuleConfig.hh"

#include <TApplication.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH2F.h>

using namespace RCE;

void create_1d_hist(TH2F *h,const char *title)
{
  int max_val=(int) h->GetBinContent(h->GetMaximumBin())+1;
  std::cout << "max " << max_val;
  TH1F *h1d=new TH1F(title,title,max_val,0,max_val);
  TCanvas *c=new TCanvas(title,title,600,600);
  int nx=h->GetNbinsX();
  int ny=h->GetNbinsY();  
  for(int x=0;x<nx;x++) {
     for(int y=0;y<ny;y++) {
       h1d->Fill(h->GetBinContent(x+1,y+1));
     }
  }
  c->Draw();
  h1d->Draw();
}


int main( int argc, char ** argv )
{	
 
  TApplication *tapp;
    CmdArgStr	partition_name ('p', "partition", "partition-name", "partition to work in.");
    CmdArgInt	channel ('c', "channel", "channel-number", "channel modules is connected too.");
    CmdArgStr	config_name ('m', "configuration", "module-configuration", "module configuation file.");

//
// Initialize command line parameters with default values
//
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
	ers::fatal( ex );
	return 1;
    }

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmd(*argv, &partition_name,&channel,&config_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);
  
//
// Parse arguments
//
    cmd.parse(arg_iter);
  
    IPCPartition   p( (const char*)partition_name );
    IPCController controller(p);
    IPCHistoController hcontroller(p);
    std::cout << "Partition    : " <<(const char*)partition_name <<std::endl;
    std::cout << "Channel      : " <<channel <<std::endl;
    std::cout << "Configuration: " <<(const char*)config_name <<std::endl;
    tapp=new TApplication("bla", &argc, argv);
    gROOT->SetStyle("Plain");
    gStyle->SetOptStat(0);
    gStyle->SetPalette(1);
    TH2F* occ=new TH2F("occ","occ", 8*18,0,8*18,2*160,0,2*160);	
    occ->SetMinimum(0);
    TCanvas *c1=new TCanvas("occupancy","occupancy",600,600);
    c1->Draw();
    occ->Draw("colz");
    
    TH2F* mean=new TH2F("threshold mean","threshold mean", 8*18,0,8*18,2*160,0,2*160);	
    mean->SetMinimum(0);
    TCanvas *c2=new TCanvas("threshold mean","threshold mean",600,600);
    c2->Draw();
    mean->Draw("colz");

    TH2F* sigma=new TH2F("threshold sigma","threshold sigma", 8*18,0,8*18,2*160,0,2*160);     
    sigma->SetMinimum(0);
    TCanvas *c3=new TCanvas("threshold sigma","threshold sigma",600,600);
    c3->Draw();
    sigma->Draw("colz");

    TH2F* chi2=new TH2F("threshold chi2","threshold chi2", 8*18,0,8*18,2*160,0,2*160);  
    chi2->SetMinimum(0);
    TCanvas *c4=new TCanvas("threshold chi2","threshold chi2",600,600);
    c4->Draw();
    chi2->Draw("colz");
    
    
    TH2F* iter=new TH2F("threshold iter","thrshold iter", 8*18,0,8*18,2*160,0,2*160);  
    iter->SetMinimum(0);
    TCanvas *c5=new TCanvas("threshold iter","threshold iter",600,600);
    c5->Draw();
    iter->Draw("colz");

    TH2F* totmean=new TH2F("tot mean","tot mean", 8*18,0,8*18,2*160,0,2*160);  
    totmean->SetMinimum(0);
    TCanvas *c6=new TCanvas("tot mean","tot mean",600,600);
    c6->Draw();
    totmean->Draw("colz");
   
    TH2F* totsigma=new TH2F("tot sigma","tot sigma", 8*18,0,8*18,2*160,0,2*160);  
    totsigma->SetMinimum(0);
    TCanvas *c7=new TCanvas("tot sigma","tot sigma",600,600);
    c7->Draw();
    totsigma->Draw("colz");

    TH2F* bcidmean=new TH2F("bcid mean","bcid mean", 8*18,0,8*18,2*160,0,2*160);  
    bcidmean->SetMinimum(0);
    TCanvas *c8=new TCanvas("bcid mean","bcid mean",600,600);
    c8->Draw();
    bcidmean->Draw("colz");
    
    TH2F* bcidsigma=new TH2F("bcid sigma","bcid sigma", 8*18,0,8*18,2*160,0,2*160);  
    bcidsigma->SetMinimum(0);
    TCanvas *c9=new TCanvas("bcid sigma","bcid sigma",600,600);
    c9->Draw();
    bcidsigma->Draw("colz");

    
    ipc::PixelModuleConfig cfg;
    TurboDaqFile turbo;
    turbo.readModuleConfig(&cfg,(const char*)config_name);
    //    turbo.dump(cfg);
    
    controller.removeAllRces();
    controller.addRce(0);
    controller.removeAllModules();
    controller.addModule("module 0","FEI3",0,channel, channel, 0, "JJ");
    controller.setupTrigger();
    controller.downloadModuleConfig(0, 0, cfg);
   
    PixScan* scn = new PixScan(PixScan::THRESHOLD_SCAN,PixLib::EnumFEflavour::PM_FE_I2 );
 
    scn->resetScan();
    scn->setLoopVarValues(0, 0, 200, 101);
    scn->setRepetitions(50); 
    scn-> setHistogramFilled(PixScan::TOT_MEAN,true);
    scn-> setHistogramFilled(PixScan::TOT_SIGMA,true);
    scn-> setHistogramFilled(PixScan::BCID_MEAN,true);
    scn-> setHistogramFilled(PixScan::BCID_SIGMA,true);
    
    ipc::ScanOptions options;
    scn->convertScanConfig(options);
    //scn->dump(options);

    controller.downloadScanConfig(options);
    
    controller.runScan(ipc::LOW);

    //std::vector<TH1*> his=controller.getHistosByName("Mod_0_Occupancy_Point_075");
    std::vector<TH1*> his=hcontroller.getHistosFromIS("Mod_0_Occupancy_Point_075");
    assert(his.size()==1);
    TH1* histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	occ->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1));
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_Occupancy_Point_075"<<std::endl;
    //his=controller.getHistosByName("Mod_0_ToTmean_Point_075");
    his=hcontroller.getHistosFromIS("Mod_0_ToTmean_Point_075");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	totmean->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1)/128);
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_ToTmean_Point_075" <<std::endl;
    his=hcontroller.getHistosFromIS("Mod_0_ToTsigma_Point_075");
    //his=controller.getHistosByName("Mod_0_ToTsigma_Point_075");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	totsigma->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1)/128);
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_ToTsigma_Point_075" <<std::endl;

    his=hcontroller.getHistosFromIS("Mod_0_BCIDmean_Point_075");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	bcidmean->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1)/128);
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_BCIDmean_Point_075" <<std::endl;

    his=hcontroller.getHistosFromIS("Mod_0_BCIDsigma_Point_075");
    //his=controller.getHistosByName("Mod_0_BCIDsigma_Point_075");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	bcidsigma->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1)/128);
      }
    }
    
    delete histo;
    std::cout<<"Updated Mod_0_BCIDsigma_Point_075" <<std::endl;

    his=hcontroller.getHistosFromIS("Mod_0_Mean");
    //his=controller.getHistosByName("Mod_0_Mean");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
        int chip=i/18;
	int col=i%18;
	int row=j;
	mean->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1));
      }
    }
    delete histo;

    std::cout<<"Updated Mod_0_Mean"<<std::endl;
    his=hcontroller.getHistosFromIS("Mod_0_Sigma");
    //his=controller.getHistosByName("Mod_0_Sigma");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	sigma->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1));
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_Sigma"<<std::endl;
    his=hcontroller.getHistosFromIS("Mod_0_ChiSquare");
    //his=controller.getHistosByName("Mod_0_ChiSquare");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	int row=j;
	chi2->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1));
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_ChiSquare"<<std::endl;
    his=hcontroller.getHistosFromIS("Mod_0_Iter");
    //his=controller.getHistosByName("Mod_0_Iter");
    assert(his.size()==1);
    histo=his[0];
    for(int i=0;i<histo->GetNbinsX();i++){
      for(int j=0;j<histo->GetNbinsY();j++){
	int chip=i/18;
	int col=i%18;
	    int row=j;
	    iter->Fill((chip%8)*18+col,chip/8*160+row,histo->GetBinContent(i+1,j+1));
      }
    }
    delete histo;
    std::cout<<"Updated Mod_0_Iter"<<std::endl;


    create_1d_hist(mean,"threshold mean 1d");
    create_1d_hist(sigma,"threshold sigma 1d");
    create_1d_hist(chi2,"thresshold chi2 1d");
    create_1d_hist(iter,"threshold iter 1d");
    create_1d_hist(totsigma,"tot sigma 1d");
    create_1d_hist(totmean,"tot mean 1d");
    create_1d_hist(bcidsigma,"bcid sigma 1d");
    create_1d_hist(bcidmean,"bcid mean 1d");
  
    controller.resetFE();
    tapp->Run();
}
