#ifndef IPCHISTOCONTROLLER_HH
#define IPCHISTOCONTROLLER_HH

#include "ipc/partition.h"
#include "IPCScanRootAdapter.hh"
#include <vector>

#include <TH1.h>


class IPCHistoController{
public:
  IPCHistoController() {};
  IPCHistoController(IPCPartition &p);
  void addRce(int rce);
  void removeAllRces();
  
  std::vector<TH1*> getHistosFromIS(const char* reg); 
  std::vector<std::string> getHistoNames(const char* reg);
  std::vector<std::string> getPublishedHistoNames();

  std::string getIPCPartitionName() { return m_partition.name(); }

  bool clearISServer(std::string regex="RCE.*", bool verbose=false);

  IPCPartition m_partition;
  std::vector<int> m_rces;
};

#endif
