#include "CosmicDataReceiver.hh"

#include "CosmicGui.hh"

#include "eudaq/TransportFactory.hh"
#include "eudaq/Event.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"

#include "CosmicMonitoringGuiEmbedded.hh"

void * CosmicDataReceiver_thread(void * arg)
{
  CosmicDataReceiver * cdr = static_cast<CosmicDataReceiver *>(arg);
  cdr->DataThread();
  return 0;
}

CosmicDataReceiver::CosmicDataReceiver(CosmicGui * gui,
				       IPCController* controller,
				       std::vector<ModuleInfo*> modinfo,
				       std::vector<int> rcesAll,
				       unsigned runnum, 
				       const std::string & listenAddress, 
				       const std::string& outputFile,
				       bool writeEudet, bool writeRaw) :
m_gui(gui),
m_dataserver(eudaq::TransportFactory::CreateServer(listenAddress)),
m_thread(),
m_done(false),
m_listening(true)
{
  m_dataproc=new CosmicOfflineDataProc(controller, modinfo, rcesAll, runnum, outputFile, writeEudet, writeRaw);
  m_dataserver->SetCallback(eudaq::TransportCallback(this, &CosmicDataReceiver::DataHandler));
  pthread_attr_init(&m_threadattr);
  pthread_create(&m_thread, &m_threadattr, CosmicDataReceiver_thread, this);
}

CosmicDataReceiver::~CosmicDataReceiver()
{
  usleep(250000); //wait for buffer to flush
  m_done = true;
  pthread_join(m_thread, 0);
  delete m_dataproc;
  if (m_connection.size()>0){
    for(size_t i_connect =0; i_connect<m_connection.size(); i_connect++){
      delete m_connection[i_connect];
    }
    m_connection.clear();
  }
  delete m_dataserver;
  
}

void CosmicDataReceiver::OnReceive(counted_ptr<eudaq::Event> ev)
{
  // std::cout << "<CosmicDataReceiver::OnReceive> : Function called!" << std::endl;
  // ev->Print(std::cout);

  // counted_ptr<eudaq::DetectorEvent> event((eudaq::DetectorEvent*)eudaq::EventFactory::Create(ser));
  eudaq::DetectorEvent* dev = (eudaq::DetectorEvent*)ev.get();
  m_gui->monitoringGui->addEvent(dev);
  if(dev->GetEventNumber()!=0)m_gui->incrementNevents();

  return;
}

void CosmicDataReceiver::DataThread()
{
  try
  {
    while (!m_done) m_dataserver->Process(100000);
  }
  catch (const std::exception & e)
  {
    std::cout << "Error: Uncaught exception: " << e.what() << "\n"
              << "DataThread is dying..." << std::endl;
  }
  catch (...)
  {
    std::cout << "Error: Uncaught unrecognised exception: \n"
              << "DataThread is dying..." << std::endl;
  }

  return;
}

void CosmicDataReceiver::DataHandler(eudaq::TransportEvent & ev)
{
  static int cout_count; // keep track of how many RECEIVE cout printout has been printed
  static const int cout_limit = 3;

  // std::vector<unsigned char> begdata(ev.packet.begin(), ev.packet.end());
  // std::cout<<"received data packet with size = "<<begdata.size()<<std::endl;

  
  // Note: the type of ev.id is "eudaq::ConnectionInfo"
  switch (ev.etype)
  {
    case (eudaq::TransportEvent::CONNECT):
      std::cout << "<CosmicDataReceiver::DataHandler> : CONNECT" << std::endl;
      if (m_listening)
      {
        m_dataserver->SendPacket("OK EUDAQ DATA DataCollector", ev.id, true);
      }
      else
      {
        m_dataserver->SendPacket("ERROR EUDAQ DATA CosmicDataReceiver not accepting connections", 
                                 ev.id, true);
        m_dataserver->Close(ev.id);
      }
      break;

    case (eudaq::TransportEvent::DISCONNECT):
      std::cout << "<CosmicDataReceiver::DataHandler> : DISCONNECT" << std::endl;
      if (m_connection.size()==0)
      {
        std::cout << "<CosmicDataReceiver::DataHandler> : Warning: disconnect attempted when no "
                  << "m_connection is set." << std::endl;
      }
      else {
	
	int id_connect=-1;
	int nConnections = m_connection.size();
	for(int i_connect = 0; i_connect<nConnections; i_connect++){
	  if (ev.id.Matches(*m_connection[i_connect])){
	    id_connect = i_connect;
	    break;
	  }
	}

	if(id_connect>=0){
	  delete m_connection[id_connect];
	  m_connection.erase(m_connection.begin() + id_connect); //remove this element from vector
	}
	else{
	  std::cout << "<CosmicDataReceiver::DataHandler> : Warning: attempted to disconnect "
		    << "from "<<ev.id<<", but this does not exist in m_connection list." << std::endl;
	}
      
      }
      break;

    case (eudaq::TransportEvent::RECEIVE):
      if (cout_count < cout_limit)
      {
        cout_count++;
        std::cout << "<CosmicDataReceiver::DataHandler> : RECEIVE" << std::endl;
      }
      else if (cout_count == cout_limit)
      {
        cout_count++;
        std::cout << "<CosmicDataReceiver::DataHandler> : RECEIVE (output limit reached, no more cout for future events)." << std::endl;
      }
      
      
      if (ev.id.GetState() == 0) // Waiting for identification
      {
	
        size_t i0 = 0, i1 = ev.packet.find(' ');
        if (i1 == std::string::npos) break;
        std::string part(ev.packet, i0, i1);
        if (part != "OK") break;
        i0 = i1+1;
        i1 = ev.packet.find(' ', i0);
        if (i1 == std::string::npos) break;
        part = std::string(ev.packet, i0, i1-i0);
        if (part != "EUDAQ") break;
        i0 = i1+1;
        i1 = ev.packet.find(' ', i0);
        if (i1 == std::string::npos) break;
        part = std::string(ev.packet, i0, i1-i0);
        if (part != "DATA") break;
        i0 = i1+1;
        i1 = ev.packet.find(' ', i0);
        part = std::string(ev.packet, i0, i1-i0);
        ev.id.SetType(part);
        i0 = i1+1;
        i1 = ev.packet.find(' ', i0);
        part = std::string(ev.packet, i0, i1-i0);
        ev.id.SetName(part);

        m_dataserver->SendPacket("OK", ev.id, true);
        ev.id.SetState(1);
        std::string name = ev.id.GetName();
        if (name != std::string("CosmicGuiDataSender"))
        {
          // This is not the data sender for the cosmic gui; do nothing
          std::cout << "name is: " << name << std::endl;
          break;
        }
        else
        {
	  //Check to see if this connection is already made; if it's not, add it.
	  int id_connect=-1;
	  int nConnections = m_connection.size();
	  for(int i_connect = 0; i_connect<nConnections; i_connect++){
	    if (ev.id.Matches(*m_connection[i_connect])){
	      id_connect = i_connect;
	      break;
	    }
	  }
	  
	  if(id_connect<0){
	    // eudaq::ConnectionInfo* newConnection = ev.id.Clone();
	    m_connection.push_back(0);
	    size_t nConnections = m_connection.size();
	    m_connection[nConnections-1] = ev.id.Clone();
	  }
	  else{
	    std::cout << "<CosmicDataReceiver::DataHandler> : Request to connect "
		      << "to "<<ev.id<<", but this connection already exists.  Ignoring." << std::endl;
  	  }
	  
        }
      }
      else
      {
	
        if (m_connection.size() == 0)
        {
          std::cout << "<CosmicDataReceiver::DataHandler> : Receiving but m_connection is not set." 
                    << std::endl;
        }
        else
        {
	  
	  int id_connect=-1;
          int nConnections = m_connection.size();
          for(int i_connect = 0; i_connect<nConnections; i_connect++){
            if (ev.id.Matches(*m_connection[i_connect])){
              id_connect = i_connect;
              break;
            }
          }
	  
          if (id_connect < 0){
	    std::cout << "<CosmicDataReceiver::DataHandler> : Received data, but do not recognize the "
		      << " ev.id : "<<ev.id<<" , so ignoring data."<<std::endl;
	    break;
	  }
	  
	  std::vector<unsigned char> data(ev.packet.begin(), ev.packet.end());
	  size_t i=0;
	  while(i<data.size()){
	    //	    int length=data[i]<<8|data[i+1];
	    unsigned short length=*(unsigned short*)&data[i];
	    if(length==0){
	      std::cout<<"Length 0 event"<<std::endl;
	      break;
	    }
	    unsigned short link=*(unsigned short*)&data[i+2];
	    //unsigned short link=data[i+2]<<8|data[i+3];
	    int retval=m_dataproc->processData(link, &data[i+4], length);
	    if(retval==1)break;//resynch
	    i+=length+4;
	  }

        }
      }
      break;

    default:
      std::cout << "<CosmicDataReceiver::DataHandler> : Unknown event type: " << ev.id << std::endl;
  }
  
  return;
}
