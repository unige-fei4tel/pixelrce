// -*-Mode: C++;-*-
/*!
*
* @brief   Functions for bootstrapping the RCE system
*
* @author  R. Claus -- REG/DRD - (claus@slac.stanford.edu)
*
* @date    March 17, 2012 -- Created
*
* $Revision: 966 $
*
* @verbatim
*                               Copyright 2012
*                                     by
*                        The Board of Trustees of the
*                      Leland Stanford Junior University.
*                             All rights reserved.
* @endverbatim
*/

#include <cstring>
#include <pthread.h>
#include <iostream>
#include <memory>
#include "rcecalib/server/EthPrimitive.hh"
#include "rcecalib/server/CmdDecoder.hh"
#include "rcecalib/server/BootLoaderPort.hh"
using std::auto_ptr;

#include "datCode.hh"
//#include DAT_PUBLIC( configuration, rtems,       prod.hh)
#include DAT_PRIVATE(platform,      startup,     Init.hh)
#include DAT_PRIVATE(platform,      startup,     TaskThread.hh)

#if !tgt_board_rce440
#include DAT_PUBLIC( service,       shell,       ShellCommon.hh)
#include DAT_PUBLIC( service,       dynalink,    Linker.hh)
#include DAT_PUBLIC( oldPpi,       pgp,    DriverList.hh)
using service::dynalink::Linker;

#include DAT_PUBLIC( service, fci,         Task.hh)
using service::fci::Task;

// The address of the dynamic section created by ld.
extern "C" char _DYNAMIC[0];
#endif
extern "C"{
  #include <rtems/malloc.h>
}

void *posix_init( void *argument );

// GDB stub is compiled with C bindings
extern "C" int rtems_gdb_start(int pri, char *ttyName);


namespace platform {

  namespace startup {

    void initialize()
    {
      // Output some identification information
      splash("prod");

#if !tgt_board_rce440
      // Add our custom commands first
      service::shell::addCommands();
#endif

      // Do the board-common initialization of the core
      brdInitialize();
      // Initialize PGP
      oldPpi::pgp::init_pgp();
#if !tgt_board_rce440
      // Allow users to log in via telnet and execute interactive commands.
      service::shell::initialize();

      // Start a debugger daemon
      // First  arg = 0 --> Use socket IO over TCP.
      // Second arg = 0 --> Priority is chosen by the debugger daemon
      rtems_gdb_start(0, 0);

      // Establish the system image version for use by following code.
      const uint majorVersion(rtems_majorv_macro);
      const uint minorVersion(rtems_minorv_macro);

      // We must use the 4-argument form of Linker::instance() to
      // create the linker instance that will be fetched by the
      // no-argument form.
      Linker::instance(_DYNAMIC, majorVersion, minorVersion, "prod");

      pthread_t mthread;
      pthread_attr_t attr;
      int stacksize;
      int ret;
      // setting a new size
      stacksize = (PTHREAD_STACK_MIN + 0x20000);
      // void* stackbase = (void *) malloc(size);
      ret = pthread_attr_init(&attr);
      ret = pthread_attr_setstacksize(&attr, stacksize);
      struct sched_param sparam;
      sparam.sched_priority = 5;
      pthread_attr_setschedparam(&attr, &sparam);
      pthread_create( &mthread, &attr , posix_init, NULL);
      rtems_malloc_statistics_t stats;      	
      malloc_get_statistics(&stats);
      printf("*** malloc statistics\n");
      printf("space available: %uk\n",(unsigned int)stats.space_available/1024);
      printf("space allocated: %uk\n",(unsigned int)(stats.lifetime_allocated-stats.lifetime_freed)/1024);
      
      printf("Done with initialize\n");
 

#endif
    }

  } // startup

} // platform

void *posix_init( void *argument ){
  std::cout<<"ATLAS Pixel RCE boot loader (built on " << BUILDDATE << " by " << BUILDUSER <<")" <<   std::endl;
  // std::cout<<"ATLAS Pixel RCE boot loader." <<   std::endl;	
  EthPrimitive eth(BootloaderPort);
  CmdDecoder cd(&eth);
  cd.run();
  return 0;
}
 
