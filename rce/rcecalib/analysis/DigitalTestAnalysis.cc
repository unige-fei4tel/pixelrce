#include "rcecalib/analysis/DigitalTestAnalysis.hh"
#include "rcecalib/server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>

namespace{
  const double threshold=0;
}
using namespace RCE;

void DigitalTestAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"Digital test analysis"<<std::endl;
  if(file->Get("loop1_0"))file->cd("loop1_0");
  TIter nextkey(gDirectory->GetListOfKeys());
  TKey *key;
  std::vector<float> masks;
  std::vector<float> digimaskdistros;
  std::vector<std::string> pos;
  boost::regex re("_(\\d+)_Occupancy");
  double nHits=(double)scan->getRepetitions();
  
    
  while ((key=(TKey*)nextkey())) {
    std::string name(key->GetName());
    boost::cmatch matches;
    if(boost::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      std::string match2=addPosition(name.c_str(), cfg).substr(0,5);
      pos.push_back(match2);

      TH2* histo = (TH2*)key->ReadObj();
      TH2D* mhis=new TH2D(Form("Mask_Mod_%d",id), Form("Mask Mod %d at %s", id, findFieldName(cfg, id)), 
	      		  histo->GetNbinsX(), histo->GetXaxis()->GetXmin(), histo->GetXaxis()->GetXmax(),
	      		  histo->GetNbinsY(), histo->GetYaxis()->GetXmin(), histo->GetYaxis()->GetXmax());
      mhis->GetXaxis()->SetTitle("Column");
      mhis->GetYaxis()->SetTitle("Row");
      unsigned char (*masks)[ipc::IPC_N_I4_PIXEL_ROWS]=0;
      ipc::PixelFEI4AConfig* conf=findFEI4AConfig(cfg, id);
      if(conf)masks=conf->FEMasks;
      else {
	ipc::PixelFEI4BConfig* confb=findFEI4BConfig(cfg, id);
	if(confb)masks=confb->FEMasks;
      }
      if(masks && scan->clearMasks()==true)clearFEI4Masks(masks);
      for (int i=0;i<histo->GetNbinsX();i++){
	for(int j=0;j<histo->GetNbinsY();j++){
	  if(histo->GetBinContent(i+1, j+1)!=nHits){
	    mhis->SetBinContent(i+1,j+1,1);
	    if(masks){
	      masks[i][j]&=0xfe; //reset bit 0 (enable)
	      masks[i][j]|=0x8; //reset bit 3 (hitbus)
	    }
	  }
	}
      }
      digimaskdistros.push_back(mhis->Integral());
      if(masks) writeFEI4Config(anfile, runno);
      m_fw->writeMaskFile(Form("%sMask_Mod_%d", m_fw->getPath(anfile).c_str(), id), mhis);
      delete histo;
      anfile->cd();
      mhis->Write();
      mhis->SetDirectory(gDirectory);
    }
  }

  TH1I* mask_distro[2];
  mask_distro[0]=new TH1I(Form("1-Mask"), Form("Mask Distribution 1st stave"), 32, 0, 32);
  mask_distro[1]=new TH1I(Form("2-Mask"), Form("Mask Distribution 2nd stave"), 32, 0, 32);
  char position[10];
  
  for(int i=0;i<4;i++){ //half stave
      if(i%2==0)position[0]='A';
      else position[0]='C';
        //mhis[i]->GetYaxis()->SetTitle("Threshold (e)");
      for(int j=1;j<=8;j++){ //module
          for(int k=1;k<=2;k++){ //FE
              sprintf(&position[1], "%d-%d", j,k);
              int bin=0;
              if(i%2==0)bin=17-((j-1)*2+k); // A side
              else bin=16+(j-1)*2+k;
              mask_distro[i/2]->GetXaxis()->SetBinLabel(bin,position);
              for(size_t l=0;l<pos.size();l++){
                  if(std::string(position)==pos[l].substr(1,4) &&
                     pos[l].substr(0,1)==Form("%d", i/2+1)){
                     mask_distro[i/2]->SetBinContent(bin, digimaskdistros[l]);
                  }
            }
         }
        
      }
      if(i%2==1){
          //anfile->cd();
          mask_distro[i/2]->SetFillColor(kRed);
          mask_distro[i/2]->Write();
          mask_distro[i/2]->SetDirectory(gDirectory);
      }

  }
    
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

