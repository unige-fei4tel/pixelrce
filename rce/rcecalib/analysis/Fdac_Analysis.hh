#ifndef FDAC_ANALYSIS_HH
#define FDAC_ANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include "rcecalib/analysis/CfgFileWriter.hh"
#include "rcecalib/server/PixScan.hh"

class ConfigGui;
class TFile;
class TH2;
namespace RCE{
  class PixScan;
}

class FdacAnalysis: public CalibAnalysis{
public:
  struct odata{
    TH2* occ;
    TH2* tot;
    TH2* tot2;
  };
  FdacAnalysis(CfgFileWriter* fw): CalibAnalysis(), m_fw(fw){}
  ~FdacAnalysis(){delete m_fw;}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
private:
  CfgFileWriter* m_fw;
};


#endif
