#include "rcecalib/analysis/StuckPixelAnalysis.hh"
#include "rcecalib/server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>
using namespace RCE;

void StuckPixelAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  TIter nextkey(file->GetListOfKeys());
  TKey *key;
  boost::regex re("_(\\d+)_Hitor");
  while ((key=(TKey*)nextkey())) {
    file->cd();
    std::string name(key->GetName());
    std::cout<<name<<std::endl;
    boost::cmatch matches;
    if(boost::regex_search(name.c_str(), matches, re)){
      assert(matches.size()>1);
      std::string match(matches[1].first, matches[1].second);
      int id=strtol(match.c_str(),0,10);
      TH2* histo = (TH2*)key->ReadObj();
      m_fw->writeMaskFile(Form("%sStuckPixelMask_Mod_%d", m_fw->getPath(anfile).c_str(), id), histo);
      unsigned char (*masks)[ipc::IPC_N_I4_PIXEL_ROWS]=0;
      ipc::PixelFEI4AConfig* conf=findFEI4AConfig(cfg, id);
      if(conf)masks=conf->FEMasks;
      else {
	ipc::PixelFEI4BConfig* confb=findFEI4BConfig(cfg, id);
	if(confb)masks=confb->FEMasks;
      }
      if(masks){
      if(scan->clearMasks()==true)clearFEI4Masks(masks);
	for (int i=0;i<histo->GetNbinsX();i++){
	  for(int j=0;j<histo->GetNbinsY();j++){
	    if(histo->GetBinContent(i+1, j+1)==1){
	      masks[i][j]&=0xfe; //reset bit 0 (enable)
	      masks[i][j]|=0x8; //reset bit 3 (hitbus)
	    }
	  }
	}
	writeFEI4Config(anfile, runno);
      }
      delete histo;
    }
  }  
  if(configUpdate())writeTopFile(cfg, anfile, runno);
}

