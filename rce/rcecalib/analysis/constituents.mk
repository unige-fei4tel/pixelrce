ifeq ($(profiler),y)
CPPFLAGS+='-D__PROFILER_ENABLED__'
profiler_lib=rcecalib/profiler
endif


ifneq ($(findstring linux,$(tgt_os)),)
libnames := analysis 
libsrcs_analysis:= NoiseAnalysis.cc \
                    GdacAnalysis.cc \
                    GdacFastAnalysis.cc \
                    GdacCoarseFastAnalysis.cc \
                    TdacAnalysis.cc \
                    TdacFastAnalysis.cc \
                    Fdac_Analysis.cc \
                    ThresholdAnalysis.cc \
                    Iff_Analysis.cc \
	            TotAnalysis.cc \
	            TotCalibAnalysis.cc \
	            DigitalTestAnalysis.cc \
	            ModuleCrosstalkAnalysis.cc \
	            OffsetAnalysis.cc \
	            CrosstalkAnalysis.cc \
	            T0Analysis.cc \
	            TimeWalkAnalysis.cc \
                    AnalysisFactory.cc \
                    CalibAnalysis.cc \
                    CfgFileWriter.cc \
                    Fei4CfgFileWriter.cc \
                    Fei3CfgFileWriter.cc \
	            StuckPixelAnalysis.cc \
	            MultiTrigAnalysis.cc \
	            MultiTrigNoiseAnalysis.cc \
	            SerialNumberAnalysis.cc \
	            RegisterTestAnalysis.cc \
	            TemperatureAnalysis.cc

libincs_analysis := \
		rcecalib


LXFLAGS +=-pthread -lm -ldl -rdynamic 
rootlibs:= \
    $(RELEASE_DIR)/build/root/lib/Gui \
    $(RELEASE_DIR)/build/root/lib/Thread \
    $(RELEASE_DIR)/build/root/lib/MathCore \
    $(RELEASE_DIR)/build/root/lib/Physics \
    $(RELEASE_DIR)/build/root/lib/Matrix \
    $(RELEASE_DIR)/build/root/lib/Postscript \
    $(RELEASE_DIR)/build/root/lib/Rint \
    $(RELEASE_DIR)/build/root/lib/Tree \
    $(RELEASE_DIR)/build/root/lib/Gpad \
    $(RELEASE_DIR)/build/root/lib/Graf3d \
    $(RELEASE_DIR)/build/root/lib/Graf \
    $(RELEASE_DIR)/build/root/lib/Hist \
    $(RELEASE_DIR)/build/root/lib/Net \
    $(RELEASE_DIR)/build/root/lib/RIO \
    $(RELEASE_DIR)/build/root/lib/Cint \
    $(RELEASE_DIR)/build/root/lib/Core

tgtnames := analysisGui mergeMaskFilesFei4 histoViewer

GUIHEADERSDEP = AnalysisGui.hh ../server/ConfigGui.hh HistoViewerGui.hh

#--------------------------------------------
tgtsrcs_analysisGui := AnalysisGui.cc \
                       ../server/PixScan.cc \
                       ../server/ConfigGui.cc \
                       ../server/FEI4AConfigFile.cc \
                       ../server/FEI4BConfigFile.cc \
                       ../server/TurboDaqFile.cc \
                       ../server/HitbusConfigFile.cc \
                       ../server/AFPHPTDCConfigFile.cc \
                       analysisGui_rootDict.cc  

guiheaders_analysisGui := AnalysisGui.hh ../server/ConfigGui.hh


tgtincs_analysisGui := \
		rcecalib \
                rcecalib/analysis 



tgtlibs_analysisGui := \
    rcecalib/analysis 

tgtslib_analysisGui := \
    dl\
    $(z_lib) \
    $(rootlibs) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(ers_lib) \
    $(boost_regex_lib)
#--------------------------------------------
tgtsrcs_histoViewer := HistoViewerGui.cc \
                       histoViewer_rootDict.cc  

guiheaders_histoViewer := HistoViewerGui.hh 


tgtincs_histoViewer := \
		rcecalib \
                rcecalib/analysis 



tgtlibs_histoViewer := 

tgtslib_histoViewer := \
    dl\
    $(z_lib) \
    $(rootlibs) \
    $(omniorb_lib) \
    $(omnithread_lib) \
    $(ers_lib) \
    $(boost_regex_lib)
tgtsrcs_mergeMaskFilesFei4 := MergeMaskFilesFei4.cc Fei4CfgFileWriter.cc
tgtincs_mergeMaskFilesFei4 := $(root_include_path) $(boost_include_path)
tgtslib_mergeMaskFilesFei4 := $(rootlibs)

endif

