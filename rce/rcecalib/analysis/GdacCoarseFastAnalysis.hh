#ifndef GDACCOARSEFASTANALYSIS_HH
#define GDACCOARSEFASTANALYSIS_HH

#include "rcecalib/analysis/CalibAnalysis.hh"
#include <map>
#include <vector>

class ConfigGui;
class TFile;
class TH2;
class TH1;

namespace RCE{
  class PixScan;
}
namespace{
  struct hdatagdaccoarsefast{
    TH2* occupancy;
    TH1* gdac;
  };
}

class GdacCoarseFastAnalysis: public CalibAnalysis{
public:
  GdacCoarseFastAnalysis(): CalibAnalysis(){}
  ~GdacCoarseFastAnalysis(){}
  void analyze(TFile* file, TFile* anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]);
};


#endif
