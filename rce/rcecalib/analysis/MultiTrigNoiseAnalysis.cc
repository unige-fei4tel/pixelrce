#include "rcecalib/analysis/MultiTrigNoiseAnalysis.hh"
#include "rcecalib/server/PixScan.hh"

#include <TFile.h>
#include <TH2.h>
#include <TH2D.h>
#include <TH1.h>
#include <TKey.h>
#include <boost/regex.hpp>
#include <iostream>
#include <fstream>
#include "TH1D.h"
#include "TF1.h"
#include "TStyle.h"


void MultiTrigNoiseAnalysis::analyze(TFile* file, TFile *anfile, RCE::PixScan* scan, int runno, ConfigGui* cfg[]){
  std::cout<<"MultiTrigNoise analysis"<<std::endl;
  
  const int totalScans=scan->getLoopVarValues(1).size();
  
  gStyle->SetOptFit(10);
  gStyle->SetOptStat(0);
  
  float xVals[totalScans];
  
  std::map<int, std::vector<TH1D*> > histoNumhitsScan;
  std::map<int, std::vector<float> > numhitsVals;
  
  float xMin = scan->getLoopVarValues(1).at(0);
  float xMax = scan->getLoopVarValues(1).at(totalScans-1);
  if(xMax<xMin){
    float xtemp = xMax;
    xMax = xMin;
    xMin = xtemp;
  }
  
  int nScanBins = (xMax - xMin + 3);
    
  
  //loop over the different values of multitrig_interval
  for(int iScan = 0; iScan<totalScans; iScan++){
    file->cd(Form("loop1_%d",iScan));

    std::map<int, TH1D*> histmap;
    boost::regex re0("_(\\d+)_Trig0_Occupancy_Point_000");
    boost::regex re1("_(\\d+)_Trig1_Occupancy_Point_000");
    boost::regex re2("Mean");
    
    int trigId=-1;
    TIter nextkey(gDirectory->GetListOfKeys()); // histograms
    TKey *key;
    
    while ((key=(TKey*)nextkey())) {
      std::string hname(key->GetName());
      boost::cmatch matches;
      
      bool foundMatch=false;
      if(boost::regex_search(hname.c_str(), matches, re0)){
	trigId=0;
	foundMatch=true;
      }
      else if(boost::regex_search(hname.c_str(), matches, re1)){
	trigId=1;
	foundMatch=true;
      }

      if(foundMatch){

	assert(matches.size()>1);
	std::string match(matches[1].first, matches[1].second);
	//  std::cout<<"Match = "<<match.c_str()<<std::endl;
	// std::cout<<"name = "<<hname.c_str()<<std::endl;
	
	int id=strtol(match.c_str(),0,10);
	
	TH2* occHisto = (TH2*)key->ReadObj();
	
	double totalNumHits = occHisto->GetSumOfWeights();
	
	xVals[iScan] = scan->getLoopVarValues(1).at(iScan);

	
	//check if the hits vs. interval histos have already been initialized for this Module ID
	if(histoNumhitsScan.find(id)==histoNumhitsScan.end()){
	  
	  std::vector<TH1D*> temphistHits(2);
	  
	  for(int iTrig=0; iTrig<2; iTrig++){
	    temphistHits[iTrig] = new TH1D(Form("HitsScan_Mod%d_Trig%d",id,iTrig),Form("Total Hits versus Interval for Module %d, Trigger %d",id,iTrig+1), nScanBins,xMin-1,xMax+1);
	    temphistHits[iTrig]->GetXaxis()->SetTitle("Interval between 1st and 2nd inject");
	    temphistHits[iTrig]->GetYaxis()->SetTitle("Total Hits");
	    temphistHits[iTrig]->SetOption("P");
	    temphistHits[iTrig]->SetMarkerStyle(2);
	    temphistHits[iTrig]->SetMarkerSize(2);
	    	    
	  }
	  
	  histoNumhitsScan[id] = temphistHits;
	  std::vector<float> tempVecHits(2*totalScans,0);
	  numhitsVals[id] = tempVecHits;
	  
	}//end if id not in histoNumhitsScan map
		
	
	int myIndex = 2*iScan + trigId;
	numhitsVals[id].at(myIndex) = totalNumHits;
		
	std::cout<<"Trigger "<<trigId+1<<" , interval "<<xVals[iScan]<<" : Total hits = "
		 <<numhitsVals[id].at(myIndex)<<std::endl;
	
	
	file->cd(Form("loop1_%d",iScan));

      }//if foundmatch
    
    }//end loop over keys
    
  } //end loop over iScan
    
  for(std::map<int, std::vector<TH1D*> >::iterator it=histoNumhitsScan.begin();it!=histoNumhitsScan.end();it++){
    int id=it->first;
    
    for(int iScan = 0; iScan<totalScans; iScan++){
      
      int myIndex = 2*iScan;
      histoNumhitsScan[id].at(0)->Fill(xVals[iScan], numhitsVals[id].at(myIndex+0) );
      histoNumhitsScan[id].at(1)->Fill(xVals[iScan], numhitsVals[id].at(myIndex+1) );
      
    }
    
    anfile->cd();
    
    for(int iTrig=0; iTrig<2; iTrig++){
      histoNumhitsScan[id].at(iTrig)->Write();
      histoNumhitsScan[id].at(iTrig)->SetDirectory(gDirectory);
    }    
    
  } //end iterate over histoNumhitsScan
  
}

      
  
