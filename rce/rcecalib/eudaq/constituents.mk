ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
modlibnames := eudaq
endif

libsrcs_eudaq := BufferSerializer.cc \
                    DetectorEvent.cc \
                    Event.cc \
                    Exception.cc \
                    FileSerializer.cc \
                    RawDataEvent.cc \
                    Time.cc \
                    Utils.cc \
                    Producer.cc \
                    CommandReceiver.cc \
                    DataSender.cc \
                    DataSenderIF.cc \
                    TransportClient.cc \
                    Configuration.cc \
                    Status.cc \
                    TransportBase.cc \
	            TransportFactory.cc \
                    TransportTCP.cc \
                    TransportServer.cc \
                    TransportNULL.cc \
                    ProducerIF.cc \
                    Mutex.cc

libincs_eudaq := rcecalib 


ifneq ($(findstring linux,$(tgt_os)),)

libnames := eudaq
tgtnames := testread testreadp testwrite

tgtsrcs_testread := testread.cc
tgtincs_testread := rcecalib  


tgtlibs_testread := \
    rcecalib/eudaq
tgtslib_testread := $(boost_regex_lib)

tgtsrcs_testreadp := testreadp.cc
tgtincs_testreadp := rcecalib  


tgtlibs_testreadp := \
    rcecalib/eudaq
tgtslib_testreadp := $(boost_regex_lib)

tgtsrcs_testwrite := testwrite.cc
tgtincs_testwrite := rcecalib 

tgtlibs_testwrite := \
    rcecalib/eudaq
tgtslib_testwrite := $(boost_regex_lib)

LXFLAGS +=-L$(RELEASE_DIR)/build/root/lib -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lGui -pthread -lm -ldl -rdynamic 
endif

