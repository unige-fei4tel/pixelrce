#include "eudaq/Event.hh"
#include "config/FormattedRecord.hh"
#include "eudaq/DetectorEvent.hh"
#include "eudaq/RawDataEvent.hh"
#include "eudaq/FileSerializer.hh"
#include "eudaq/counted_ptr.hh"

#include <TApplication.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>

unsigned indx=0;

unsigned nextcurrent(unsigned char* p){
  unsigned current=*(unsigned*)&p[indx];
  //unsigned current=(p[indx]<<24) | (p[indx+1]<<16) | (p[indx+2]<<8) | p[indx+3];
  indx+=4;
  return current;
}
unsigned getWord(unsigned char *p, int word){
  return *(unsigned*)&p[word*4];
  //return (p[word*4]<<24) | (p[word*4+1]<<16) | (p[word*4+2]<<8) | p[word*4+3];
}

int main(int argc, char **argv){
  int linkmap[]={4,0,0,0,0,0,0,0,0,0,0,0,1,2,3};
  TApplication *tapp;
  const char* filename=argv[1];
  int nevt=100000000;
  if(argc==3)nevt=atoi(argv[2]);
  tapp=new TApplication("bla", &argc, argv);
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1);
  gStyle->SetPalette(1);
  gStyle->SetOptFit(111);
  gStyle->SetErrorX(0);
    
  unsigned noisy[5][16][160][18];
  unsigned hitmap[5][16][160][18];
  for(int i=0;i<5;i++){
    for(int j=0;j<16;j++){
      for(int k=0;k<160;k++){
	for(int l=0;l<18;l++){
	  hitmap[i][j][k][l]=0;
	  noisy[i][j][k][l]=0;
	}
      }
    }
  }
  //  std::ifstream nos("noisypixels3.txt");
    //int md, chp, r, c;
    //while(!nos.eof()){
    //  nos>>md>>chp>>r>>c;
  //  noisy[md][chp][r][c]=1;
    //}
  TCanvas *co[5];
  TCanvas *ct[5];
  TH2F* occ[5];
  TH1F* toth[5];
  TCanvas *tdcc;
  TH1F* tdc;
  char name[128], title[128];
  for (int i=0;i<5;i++){
    sprintf(name,"co%d",i);
    co[i]=new TCanvas(name,"Canvas",600,600);
    sprintf(name,"occ%d",i);
    sprintf(title,"Occupancy module %d",i+1);
    //occ[i]=new TH2F(name,title, 18,-.5,17.5,160,-.5,159.5);	
    if(i==4)
      occ[i]=new TH2F(name,title, 80,0,80,336,0, 336);	//FEI4
    else
      occ[i]=new TH2F(name,title, 8*18,0,8*18,2*160,0, 2*160);	
    co[i]->Draw();
    occ[i]->Draw("colz");
    occ[i]->SetMinimum(0);
    sprintf(name,"ct%d",i);
    ct[i]=new TCanvas(name,"Canvas",600,600);
    sprintf(name,"tot%d",i);
        sprintf(title,"ToT module %d",i+1);
    toth[i]=new TH1F(name,title, 256,-.5,255.5);
    //sprintf(title,"Timing module %d",i+1);
    //toth[i]=new TH1F(name,title, 16,-.5,15.5);
    ct[i]->Draw();
    toth[i]->Draw();
  }
  tdcc=new TCanvas("TDCC","Canvas",600,600);
  tdc=new TH1F("TDC","TDC",64,-.5,63.5);
  tdcc->Draw();
  tdc->Draw();

  int l1id=0;
  int link=0;
  int bxid=0;
  int firstbxid=0;
  int highesttot=0;
  int ntrg=0;
  int nev=0;
  eudaq::FileDeserializer fs(filename);
  while(fs.HasData()){
    eudaq::DetectorEvent* dev=(eudaq::DetectorEvent*)eudaq::EventFactory::Create(fs);
    nev++;
    if(nev>nevt)break;
    //if(nev==10)break;
  //  dev->Print(std::cout);
    if(dev->IsEORE() || dev->IsBORE())continue;
    eudaq::RawDataEvent::data_t pixblock;
    int modtot[5];
    for(int i=0;i<5;i++)modtot[i]=0;
    for(unsigned k=0;k<dev->NumEvents();k++){
      int oldl1id=-1;
      eudaq::RawDataEvent* rev=(eudaq::RawDataEvent*)dev->GetEvent(k);
      if(rev->GetSubType()=="APIX-CT"){
	eudaq::RawDataEvent::data_t block0=rev->GetBlock(0);
	//	std::cout<<"Block 0 size: "<<std::dec<<block0.size()<<std::endl;
		//for(int i=0;i<9;i++){
		//  std::cout<<std::hex<<getWord(&block0[0],i)<<std::endl;
		//}
	unsigned counter1=getWord(&block0[0],2);
	unsigned counter2=getWord(&block0[0],3);
	unsigned long long counter=counter2;
	counter=(counter<<32) | counter1;
	//printf("%llx\n",counter);
	int bitpos=-1;
	for (int j=0;j<64;j++){
	  if(((counter>>j)&1)==0){
	    bitpos=j;
	    break;
	  }
	} 
	//	if(bitpos!=-1)std::cout<<"Phase is " <<bitpos*.4<<" ns"<<std::endl;
	if(bitpos!=-1)tdc->Fill(bitpos);
	unsigned trgtime1=getWord(&block0[0],4);
	unsigned trgtime2=getWord(&block0[0],5);
	unsigned deadtime1=getWord(&block0[0],6);
	unsigned deadtime2=getWord(&block0[0],7);
	unsigned long long trgtime=trgtime1;
	trgtime=(trgtime<<32) | trgtime2;
	unsigned long long deadtime=deadtime1;
	deadtime=(deadtime<<32) | deadtime2;
	//	std::cout<<"Trgtime: "<<trgtime<<std::endl;
		//std::cout<<"Deadtime: "<<deadtime<<std::endl;
	//unsigned eudaqtrg=getWord(&block0[0],8);
	//        std::cout<<"Eudaq trigger word: "<<eudaqtrg<<std::endl;

	pixblock=rev->GetBlock(1);
      }else if(rev->GetSubType()=="CTEL"){
	pixblock=rev->GetBlock(0);
      }else{
	//std::cout<<"Unknown data block. Skipping."<<std::endl;
	continue;
      }
      //      std::cout<<"Pixblock size "<<pixblock.size()<<std::endl;
      indx=0;
      while (indx<pixblock.size()){
	unsigned currentu=nextcurrent(&pixblock[0]);
	FormattedRecord current(currentu);
	if(current.isHeader()){
	  ntrg++;
	  link=current.getLink();
	  //std::cout<<"Data from link "<<link<<std::endl;
	  l1id=current.getL1id();
	  bxid=current.getBxid();
	  if(l1id!=oldl1id){
	    oldl1id=l1id;
	    firstbxid=bxid;
	  }
	}else if(current.isData()){
	  int chip=current.getFE();
	  //std::cout<<"Chip "<<chip<<std::endl;
	  int tot=current.getToT();
	  if(tot>highesttot)highesttot=tot;
	  int col=current.getCol();
	  int row=current.getRow();
	  //if(col>17)std::cout<<"Bad column "<<col<<" in module "<<link<<" chip "<<chip<<std::endl;
	  //      	  if(chip!=8 || row<150){
	  //	  if(chip!=6 || col<16){
	  int diffbx=bxid-firstbxid;
	  //std::cout<<diffbx<<std::endl;
	  if (diffbx<0)diffbx+=256;
	  //  if(chip!=8||col>0){
	  //	  if(row<156){
	  	  //if(diffbx>5&&diffbx<9){
	  if(noisy[linkmap[link]][chip][row][col]==0){// && diffbx>5&&diffbx<12){
	  float x,y;
	  if (chip<8){
	    x=18*chip+col;
	    y=row;
	  } else {
	    x=(15-chip)*18+17-col;
	    y= 319-row;
	  }
	  // occ[linkmap[link]]->Fill(chip/2*18+col,(chip%2)*160+row);
	    
	  occ[linkmap[link]]->Fill(x,y);
	  hitmap[linkmap[link]][chip][row][col]++;
	  //std::cout<<chip/2*18+col<<" "<<(chip%2)*160+row<<" "<<tot<<" "<<l1id<<" "<<firstbxid<<" "<<bxid<<std::endl;
	  if(link==0&&col<78)std::cout<<col<<" "<<row<<" "<<tot<<" "<<l1id<<" "<<firstbxid<<" "<<bxid<<std::endl;
	  
	  if(tot!=14&&col<78)modtot[linkmap[link]]+=tot;
	  //toth[linkmap[link]]->Fill(tot);
	  //if(col<78)toth[linkmap[link]]->Fill(diffbx);
	  //if(link==8 && col<6)std::cout<<"Tot "<<diffbx<<std::endl;
	  //toth->Fill(diffbx);
	  //std::cout<<chip<<" "<<row<<" "<<col<<" "<<tot<<std::endl;
	  }
	  //  }
	  	  // }
	}
      }
    }
    for(int i=0;i<5;i++)if(modtot[i]!=0)toth[i]->Fill(modtot[i]);
  }
  //  std::cout<<"Highest ToT: "<<highesttot<<std::endl;
  for (int i=0;i<5;i++){
    occ[i]->Draw("colz");
    toth[i]->Draw();
    //toth[i]->Fit("gaus");
    co[i]->Update();
    ct[i]->Update();
  }
  tdc->Draw();
  tdcc->Update();
  std::cout<<nev<<" events and "<<ntrg<<" triggers."<<std::endl;
  std::cout<<"Noisy pixels:"<<std::endl;
  for(int i=0;i<5;i++){
    for(int j=0;j<16;j++){
      for(int k=0;k<160;k++){
	for(int l=0;l<18;l++){
	  if( hitmap[i][j][k][l]>5){
	    std::cout<<i<<" "<<j<<" "<<k<<" "<<l<<" "<<std::endl;
	  }
	}
      }
    }
  }
//  TFile a("out.root","recreate");
//  tdc->Write();
 // a.Close();
  tapp->Run();
}

