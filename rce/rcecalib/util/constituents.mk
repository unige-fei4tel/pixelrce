
ifdef SWAP_DATA
CPPFLAGS += '-DSWAP_DATA'
endif

ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)
modlibnames := util 
endif
ifneq ($(findstring linux,$(tgt_os)),)
libnames := util 
endif

libsrcs_util := IPCHistoManager.cc \
	            RceName.cc \
                    AbsRceHisto.cc \
		    RceMath.cc


libincs_util := rcecalib \
                    $(ers_include_path) \
                    $(owl_include_path) \
                    $(ipc_include_path) \
                    $(is_include_path) \
                    $(oh_include_path) \
                    $(boost_include_path) \
                    $(omniorb_include_path)


