#ifndef SCANCTRLEXCEPTIONS_HH
#define SCANCTRLEXCEPTIONS_HH

#include "ers/ers.h"

ERS_DECLARE_ISSUE(    rcecalib,                                    // namespace name 
                        Bad_ptree_param,                              // issue name 
                        "Bad scan parameter: " << reason,       // message 
                        ((const char *)reason )              // first attribute 
                   )

ERS_DECLARE_ISSUE(    rcecalib,                                    // namespace name 
                        Param_exists,                              // issue name 
		      "Parameter " << reason <<" was previously defined",       // message 
                        ((const char *)reason )              // first attribute 
                   )
ERS_DECLARE_ISSUE(    rcecalib,                                    // namespace name 
                        Unknown_Module_Type,                              // issue name 
		      "No such module type: " << reason,       // message 
                        ((const char *)reason )              // first attribute 
                   )
ERS_DECLARE_ISSUE(    rcecalib,                                    // namespace name 
                        Unknown_Scan_Type,                              // issue name 
		      "No such scan type: " << reason,       // message 
                        ((const char *)reason )              // first attribute 
                   )
ERS_DECLARE_ISSUE(    rcecalib,                                    // namespace name 
                        Config_File_Error,                              // issue name 
		      "Error in config file " ,       // message 
                   )


#endif
