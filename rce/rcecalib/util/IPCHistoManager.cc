
#include "rcecalib/util/IPCHistoManager.hh"
#include "ipc/partition.h"
#include "rcecalib/util/AbsRceHisto.hh"
#include "IPCScanRootAdapter.hh"
#include "oh/OHRawProvider.h"


#include <boost/regex.hpp>

IPCHistoManager* IPCHistoManager::m_manager=0;

IPCHistoManager::IPCHistoManager(IPCPartition& p, const char* servername, const char* providername){
    m_provider=new OHRawProvider<>(p, servername, providername, 0);
    m_manager=this;
} 
IPCHistoManager::~IPCHistoManager(){
  delete m_provider;
  m_manager=0;
}
void IPCHistoManager::addToInventory(AbsRceHisto* histo){
  std::string name=histo->name();
  if(m_inventory.find(name)!=m_inventory.end()){
    std::cout<<"Histogram with name "<<name<<" exists already. Replacing..."<<std::endl;
  }
  m_inventory[name]=histo;
}
void IPCHistoManager::removeFromInventory(AbsRceHisto* histo){
  std::map<std::string, AbsRceHisto*>::iterator it;
  it=m_inventory.find(histo->name());
  if(it==m_inventory.end()){
    std::cout<<"Histogram with name "<<histo->name()<<" was not in repository."<<std::endl;
  }else{
    m_inventory.erase(it);
  }
  std::map<std::string, AbsRceHisto*>::iterator it2=m_published.find(histo->name());
  if(it2!=m_published.end()){
    m_published.erase(it2);
  }
}

void IPCHistoManager::publish(const char* rege){
  boost::regex re(rege);
  std::map<std::string,AbsRceHisto*>::iterator it;
  for(it=m_inventory.begin();it!=m_inventory.end();it++){
    if(boost::regex_match((*it).first,re)){
      (*it).second->publish(m_provider);
      m_published[(*it).first]=(*it).second;
    }
  }
}
std::vector<std::string> IPCHistoManager::getHistoNames(const char *rege){
  std::vector<std::string> retvec;
  boost::regex re(rege);
  std::map<std::string,AbsRceHisto*>::iterator it;
  for(it=m_inventory.begin();it!=m_inventory.end();it++){
    if(boost::regex_match((*it).first,re)){
      retvec.push_back((*it).first);
    }
  }
  return retvec;
}
std::vector<std::string> IPCHistoManager::getPublishedHistoNames(){
  std::vector<std::string> retvec;
  std::map<std::string,AbsRceHisto*>::iterator it;
  for(it=m_published.begin();it!=m_published.end();it++){
    retvec.push_back((*it).first);
  }
  return retvec;
}
