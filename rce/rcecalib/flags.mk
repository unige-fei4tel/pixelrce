include $(RELEASE_DIR)/make/sw/flags.mk

BOOST_VERSION:=1_42
ifeq ($(TDAQ_VERSION),4)
CPPFLAGS+=-DTDAQ_RELEASE_4
BOOST_VERSION=1_44
endif

ifneq ($(findstring ppc-rtems-rce,$(tgt_arch)),)

DEPFLAGS+= -D__powerpc__  -D__GNU__ -D__GLIBC__=2 -DBOOST_THREAD_POSIX
DEFINES+= -D__powerpc__  -D__GNU__ -D__GLIBC__=2 -DBOOST_THREAD_POSIX
MDEFINES+= -D__powerpc__ -D__GNU__ -D__GLIBC__=2 -DBOOST_THREAD_POSIX -DERS_NO_DEBUG -mlongcall

endif

ifneq ($(findstring x86_64-linux,$(tgt_arch)),)
DEFINES+= -D__LITTLE_ENDIAN__
DEPFLAGS+= -D__LITTLE_ENDIAN__
endif

CPPFLAGS += -I$(RELEASE_DIR)/build/rcecalib/obj/$(tgt_arch)/idl
CPPFLAGS += -I$(RELEASE_DIR)/build/rcecalib/modobj/$(tgt_arch)/idl
CPPFLAGS += -I$(RELEASE_DIR)/build/rcecalib/obj/$(tgt_arch)/idl
CPPFLAGS += -I$(RELEASE_DIR)/build/rcecalib/modobj/$(tgt_arch)/idl
CPPFLAGS += -I$(RCE_CORE)/include
CPPFLAGS += -I$(PIXLIBINTERFACE)
LXFLAGS  += -L$(RCE_CORE)/$(tgt_arch)/lib

rdb_lib:=
ers_lib:=$(TDAQC_INST_PATH)/$(tgt_arch)/lib/ers
ers_stream_lib:=
owl_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/owl
ipc_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/ipc
oh_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/oh
ifneq ($(findstring i686,$(tgt_arch)),)
oh_root_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/ohroot
else
oh_root_lib:=
endif
is_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/is
tdaq_cmdline_lib=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/cmdline
ifneq ($(findstring i686,$(tgt_arch)),)
boost_thread_lib:=$(TDAQ_BOOST)/$(tgt_arch)/lib/boost_thread-gcc43-mt-$(BOOST_VERSION)
boost_date_time_lib:=$(TDAQ_BOOST)/$(tgt_arch)/lib/boost_date_time-gcc43-mt-$(BOOST_VERSION)
boost_regex_lib:=$(TDAQ_BOOST)/$(tgt_arch)/lib/boost_regex-gcc43-mt-$(BOOST_VERSION)
else
boost_thread_lib:=$(TDAQ_BOOST)/$(tgt_arch)/lib/boost_thread
boost_date_time_lib:=$(TDAQ_BOOST)/$(tgt_arch)/lib/boost_date_time
boost_regex_lib:=$(TDAQ_BOOST)/$(tgt_arch)/lib/boost_regex
endif
omniorb_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/omniORB4
omnithread_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/omnithread
mysql_lib:=$(TDAQ_MYSQL)/$(tgt_arch)/lib/mysqlclient
#zlib hack for rtems - avoid clash with internal rtems zlib
ifneq ($(findstring i686,$(tgt_arch)),)
z_lib:=$(TDAQC_EXT_PATH)/$(tgt_arch)/lib/z
else
z_lib:=/daq/slc5/sw/lcg/external/zlib/1.2.3p1/ppc-rtems-rce405-opt/lib/zz
ifeq ($(TDAQ_VERSION),4)
rdb_lib:=$(TDAQ_INST_PATH)/$(tgt_arch)/lib/rdb
ers_lib=$(TDAQC_INST_PATH)/$(tgt_arch)/lib/ers 
ers_stream_lib:=$(TDAQC_INST_PATH)/$(tgt_arch)/lib/ErsBaseStreams
endif
endif

ARCH=$(tgt_arch)
ifneq ($(findstring i686,$(tgt_arch)),)
CPPFLAGS+=-I$(TDAQ_BOOST)/$(ARCH)/include/boost-$(BOOST_VERSION)
else
CPPFLAGS+=-I$(TDAQ_BOOST)/$(ARCH)/include
endif
CPPFLAGS+=-I$(TDAQC_INST_PATH)/include
CPPFLAGS+=-I$(TDAQ_INST_PATH)/include -I$(TDAQ_INST_PATH)/$(ARCH)/include
CPPFLAGS+=-I$(TDAQ_INST_PATH)/$(ARCH)/include/ipc
CPPFLAGS+=-I$(ROOTSYS)/include
CPPFLAGS+=-I$(TDAQ_MYSQL)/$(ARCH)/include
IDLFLAGS+=-I$(TDAQ_INST_PATH)/include 
ifneq ($(findstring i686,$(tgt_arch)),)
LXFLAGS+=-Wl,-rpath=$(TDAQC_INST_PATH)/$(ARCH) 
LXFLAGS+=-Wl,-rpath=$(TDAQ_INST_PATH)/$(ARCH)/lib
LXFLAGS+=-Wl,-rpath=$(TDAQ_BOOST)/$(ARCH)/lib
LXFLAGS+=-Wl,-rpath=$(TDAQ_MYSQL)/$(ARCH)/lib
endif


