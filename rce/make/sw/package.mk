# Package level makefile
# ----------------------
Makefile:;

# Symbols
# -------
SHELL := /bin/bash
RM    := rm -f
MV    := mv -f
empty :=
space := $(empty) $(empty)

cwd := $(call reverse,$(subst /, ,$(shell pwd)))
pkg_name := $(word 1,$(cwd))
prj_name := $(word 2,$(cwd))

# Defines which directories are being created by this makefile
libdir  := $(RELEASE_DIR)/build/$(prj_name)/lib/$(tgt_arch)
bindir  := $(RELEASE_DIR)/build/$(prj_name)/bin/$(tgt_arch)
objdir  := $(RELEASE_DIR)/build/$(prj_name)/obj/$(tgt_arch)/$(pkg_name)
moddir  := $(RELEASE_DIR)/build/$(prj_name)/mod/$(tgt_arch)
modobjdir  := $(RELEASE_DIR)/build/$(prj_name)/modobj/$(tgt_arch)/$(pkg_name)
modlibdir := $(RELEASE_DIR)/build/$(prj_name)/modlib/$(tgt_arch)
depdir  := $(RELEASE_DIR)/build/$(prj_name)/dep/$(tgt_arch)/$(pkg_name)
moddepdir  := $(RELEASE_DIR)/build/$(prj_name)/moddep/$(tgt_arch)/$(pkg_name)
prod_dirs := $(strip $(bindir) $(libdir) $(moddir) $(modlibdir))
temp_dirs  = $(strip $(sort $(foreach o,$(depends) $(objects),$(dir $(o)))))

# Dummy shared library search (RTEMS only)
# ----------------------------------------

# In order to get a dynamic symbol table into a target it's necessary
# (but not sufficient) to search at least one shared library. Why a
# dynamic symbol table rather than a regular one? A dynamic symbol
# table is part of a loadable segment (and section) so ELF-loaders
# will automatically copy the table into memory; no painful hacks are
# required to extract the table and find a safe place for it. As a
# bonus we get a hash table to speed our lookups.

# Make each target (executable) depend on the "dummy" library of
# package rce/ldtools. We can just append to the list of libraries
# for a target because the command line arguments generated
# for the final linking of the target don't depend on the type
# of library. It's up to rce/ldtools to make sure that the dummy
# library is indeed a shared library.

ifeq ($(tgt_os),rtems)

define adddummylib
ifndef tgtlibs_$(tgt)
tgtlibs_$(tgt) :=
endif
ifeq ($(RCE_CORE_VERSION),2.2)
tgtlibs_$(tgt) += service/dummy
else
tgtlibs_$(tgt) += rce/dummy
endif
endef

$(foreach tgt,$(tgtnames),$(eval $(adddummylib)))
endif


# Procedures
# ----------

# Define some procedures and create (different!) rules for libraries
# and targets. Note that 'eval' needs gmake >= 3.80.
libraries :=
modules   :=
targets   :=
objects   :=
depends   :=
getobjects = $(strip \
	$(patsubst %.cc,$(1)/%.o,$(filter %.cc,$(2))) \
	$(patsubst %.cpp,$(1)/%.o,$(filter %.cpp,$(2))) \
	$(patsubst %.c,$(1)/%.o, $(filter %.c,$(2))) \
	$(patsubst %.s,$(1)/%.o, $(filter %.s,$(2))))
getprj = $(word 1,$(subst /, ,$(1)))
getlib = $(word 2,$(subst /, ,$(1)))
getproject = $(RELEASE_DIR)/build/$(1)/lib/$(tgt_arch)
getlibrary = $(call getproject,$(call getprj,$(1)))/lib$(call getlib,$(1)).$(LIBEXTNS)
getlibraries = $(foreach prjlib,$(1),$(call getlibrary,$(prjlib)))
getprojects  = $(foreach prjlib,$(1),$(call getprj,$(prjlib)))
getlinkdirs  = $(addprefix -L, $(sort $(foreach prj,$(call getprojects,$(1)),$(call getproject,$(prj)))))
getlinksdir  = $(addprefix -L, $(sort $(dir $(1))))
getlinklibs  = $(addprefix -l,$(foreach prjlib,$(1),$(call getlib,$(prjlib))))
getlinkslib  = $(addprefix -l,$(notdir $(1)))
getrpath  = $$ORIGIN/../../../$(1)/lib/$(tgt_arch)
getrpaths = $(subst $(space),:,$(strip $(foreach prj,$(call getprojects,$(1)),$(call getrpath,$(prj)))))

getmodname = $(word 1,$(subst ., ,$(1)))
getmodlibproject = $(RELEASE_DIR)/build/$(1)/modlib/$(tgt_arch)
getmodproject = $(RELEASE_DIR)/build/$(1)/mod/$(tgt_arch)
getmodlibrary = $(call getmodlibproject,$(call getprj,$(1)))/lib$(call getlib,$(1)).a
getmodlibraries = $(foreach prjlib,$(1),$(call getmodlibrary,$(prjlib)))
getmodlinkdirs  = $(addprefix -L, $(sort $(foreach prj,$(call getprojects,$(1)),$(call getmodlibproject,$(prj)))))
expandneededmods = $(foreach needed,$(1),$(call getmodproject,$(call getprj,$(needed)))/$(call getlib,$(needed)).so)


define object_template
  incdirs_$(1) := $$(addprefix -I$(RELEASE_DIR)/,$(2))
  incdirs_$(1) += -I$(RELEASE_DIR)
  incdirs_$(1) += $$(addprefix -I$(RELEASE_DIR)/build/,$(2))
  incdirs_$(1) += -I$(RELEASE_DIR)/build
  incdirs_$(1) += $$(addprefix -I,$(3))
endef

define library_template
  library_$(1) := $$(libdir)/lib$(1).$(LIBEXTNS)
  libobjs_$(1) := $$(call getobjects,$$(objdir),$$(libsrcs_$(1)))
  libraries    += $$(library_$(1))
  objects      += $$(libobjs_$(1))
  depends      += $$(libobjs_$(1):$$(objdir)/%.o=$$(depdir)/%.d)
  libraries_$(1) := $$(call getlibraries,$$(liblibs_$(1)))
  linkdirs_$(1)  := $$(call getlinkdirs,$$(liblibs_$(1)))
  linkdirs_$(1)  += $$(call getlinksdir,$$(libslib_$(1)))
ifneq ($$(liblibs_$(1)),)
  linklibs_$(1)  := $$(call reverse,$$(call getlinklibs,$$(liblibs_$(1))))
endif
ifneq ($$(libslib_$(1)),)
  linklibs_$(1)  += $$(call reverse,$$(call getlinkslib,$$(libslib_$(1))))
endif
ifeq ($$(LIBEXTNS),so)
ifneq ($$(ifversn_$(1)),)
  ifversnflags_$(1) := -Wl,--version-script=$$(ifversn_$(1))
endif
endif
  linkflags_$(1) := $$(linkdirs_$(1)) $$(linklibs_$(1))
$$(library_$(1)): $$(libobjs_$(1))
endef

$(foreach lib,$(libnames),$(eval $(call library_template,$(lib))))
$(foreach lib,$(libnames),$(foreach obj,$(libsrcs_$(lib)),$(eval $(call object_template,$(obj),$(libincs_$(lib)),$(libsinc_$(lib))))))

define target_template
  target_$(1)  := $$(bindir)/$(1)
  tgtobjs_$(1) := $$(call getobjects,$$(objdir),$$(tgtsrcs_$(1)))
  targets      += $$(target_$(1))
  objects      += $$(tgtobjs_$(1))
  depends      += $$(tgtobjs_$(1):$$(objdir)/%.o=$$(depdir)/%.d)
  libraries_$(1) := $$(call getlibraries,$$(tgtlibs_$(1)))
  linkdirs_$(1)  := $$(call getlinkdirs,$$(tgtlibs_$(1)))
  linkdirs_$(1)  += $$(call getlinksdir,$$(tgtslib_$(1)))
ifneq ($$(tgtlibs_$(1)),)
  linklibs_$(1)  := $$(call reverse,$$(call getlinklibs,$$(tgtlibs_$(1))))
endif
ifneq ($$(tgtslib_$(1)),)
  linklibs_$(1)  += $$(call reverse,$$(call getlinkslib,$$(tgtslib_$(1))))
endif
ifeq ($$(LIBEXTNS),so)
  rpaths_$(1)    := -Wl,-rpath='$$(call getrpaths,$$(tgtlibs_$(1)))'
endif
  linkflags_$(1) := $$(linkdirs_$(1)) $$(linklibs_$(1)) $$(rpaths_$(1))
ifneq ($$(MANAGERS),)
  nomanagrs_$(1) := $$(filter-out $$(managrs_$(1)),$$(MANAGERS))
  nomanagrs_$(1) := $$(nomanagrs_$(1):%=$$(RTEMSDIR)/no-%.rel)
  tgtobjs_$(1)   += $$(nomanagrs_$(1))
endif
$$(target_$(1)): $$(tgtobjs_$(1)) $$(libraries_$(1))
endef

$(foreach tgt,$(tgtnames),$(eval $(call target_template,$(tgt))))
$(foreach tgt,$(tgtnames),$(foreach obj,$(tgtsrcs_$(tgt)),$(eval $(call object_template,$(obj),$(tgtincs_$(tgt)),$(tgtsinc_$(tgt))))))


define module_library_template
  library_$(1) := $$(modlibdir)/lib$(1).a
  libobjs_$(1) := $$(call getobjects,$$(modobjdir),$$(libsrcs_$(1)))
  libraries    += $$(library_$(1))
  objects      += $$(libobjs_$(1))
  depends      += $$(libobjs_$(1):$$(modobjdir)/%.o=$$(moddepdir)/%.d)
  libraries_$(1) := $$(call getmodlibraries,$$(liblibs_$(1)))
  linkdirs_$(1)  := $$(call getmodlinkdirs,$$(liblibs_$(1)))
  linkdirs_$(1)  += $$(call getmodlinksdir,$$(libslib_$(1)))
ifneq ($$(liblibs_$(1)),)
  linklibs_$(1)  := $$(call reverse,$$(call getlinklibs,$$(liblibs_$(1))))
endif
ifneq ($$(libslib_$(1)),)
  linklibs_$(1)  += $$(call reverse,$$(call getlinkslib,$$(libslib_$(1))))
endif
  linkflags_$(1) := $$(linkdirs_$(1)) $$(linklibs_$(1))
$$(library_$(1)): $$(libobjs_$(1))
endef

$(foreach lib,$(modlibnames),$(eval $(call module_library_template,$(lib))))
$(foreach lib,$(modlibnames),$(foreach obj,$(libsrcs_$(lib)),$(eval $(call object_template,$(obj),$(libincs_$(lib)),$(libsinc_$(lib))))))


define module_template
  module_$(1) := $$(moddir)/$(1).$(majorv_$(1)).$(minorv_$(1)).$(branch_$(1)).so
  modobjs_$(1) := $$(call getobjects,$$(modobjdir),$$(modsrcs_$(1)))
  modules      += $$(module_$(1))
  objects      += $$(modobjs_$(1))
  depends      += $$(modobjs_$(1):$$(modobjdir)/%.o=$$(moddepdir)/%.d)
  libraries_$(1) := $$(call getmodlibraries,$$(modlibs_$(1)))
  linkdirs_$(1)  := $$(call getmodlinkdirs,$$(modlibs_$(1)))
  linkdirs_$(1)  += $$(call getlinksdir,$$(modslib_$(1)))
ifneq ($$(modlibs_$(1)),)
  linklibs_$(1)  := $$(call reverse,$$(call getlinklibs,$$(modlibs_$(1))))
endif
ifneq ($$(modslib_$(1)),)
  linklibs_$(1)  += $$(call reverse,$$(call getlinkslib,$$(modslib_$(1))))
endif
  linkflags_$(1) := $$(linkdirs_$(1)) $$(linklibs_$(1))
$$(module_$(1)): $$(modobjs_$(1)) $$(libraries_$(1)) $$(call expandneededmods,$$(modsneeded_$(1)))
endef

$(foreach mod,$(modnames),$(eval $(call module_template,$(mod))))
$(foreach mod,$(modnames),$(foreach obj,$(modsrcs_$(mod)),$(eval $(call object_template,$(obj),$(modincs_$(mod)),$(modsinc_$(mod))))))


# Rules
# -----
rules := all dir obj lib bin clean cleanall userall userclean print

.PHONY: $(rules) $(libnames) $(tgtnames)

.SUFFIXES:  # Kills all implicit rules

all: bin userall;

obj: $(objects);

lib: $(libraries);

bin: lib $(targets) $(modules);

dir: $(prod_dirs) $(temp_dirs);

print:
	@echo	"bindir    = $(bindir)"
	@echo	"moddir    = $(moddir)"
	@echo	"libdir    = $(libdir)"
	@echo	"objdir    = $(objdir)"
	@echo	"modobjdir = $(modobjdir)"
	@echo	"depdir    = $(depdir)"
	@echo	"moddepdir = $(moddepdir)"
	@echo   "targets   = $(targets)"
	@echo	"libraries = $(libraries)"
	@echo   "modules   = $(modules)"
	@echo	"depends   = $(depends)"
	@echo	"objects   = $(objects)"
	@echo	"managers  = $(MANAGERS)"

clean: userclean
ifneq ($(objects),)
	@echo "[RO] Removing object files"
	$(quiet)$(RM) $(objects)
endif
ifneq ($(depends),)
	@echo "[RD] Removing depend files"
	$(quiet)$(RM) $(depends)
endif
ifneq ($(libraries),)
	@echo "[RL] Removing libraries: $(notdir $(libraries))"
	$(quiet)$(RM) $(libraries)
endif
ifneq ($(targets),)
	@echo "[RT] Removing targets: $(notdir $(targets))"
	$(quiet)$(RM) $(targets)
endif
ifneq ($(modules),)
	@echo "[RM] Removing modules: $(notdir $(modules))"
	$(quiet)$(RM) $(modules)
endif

cleanall: userclean
	$(quiet)$(RM) -r $(temp_dirs)


# Directory structure
$(prod_dirs) $(temp_dirs):
	$(quiet)mkdir -p $@


# Libraries
$(libdir)/lib%.$(LIBEXTNS):
	@echo "[LD] Build library $*"
	$(quiet)$(LD) $(LDFLAGS) $(ifversnflags_$*) $(linkflags_$*) $^ -o $@


# Exceutables
$(bindir)/%:
	@echo "[LT] Linking target $*"
	$(quiet)$(LX) $(DEFINES) $(tgtobjs_$*) $(linkflags_$*) $(LXFLAGS) -o $@

# Libraries for modules
$(modlibdir)/lib%.a:
	@echo "[MD] Linking lib-for-modules $*"
	$(quiet)$(LD) $(MLDFLAGS) $(linkflags_$*) $^ -o $@

# Modules
ifeq ($(RCE_CORE_VERSION),2.2)
ldtoolsobj := $(LDTOOLSD)
else
ldtoolsobj := $(RELEASE_DIR)/build/rce/modobj/$(tgt_arch)/ldtools
endif

$(moddir)/%:
	@echo "[LM] Linking module $* (modname = $(call getmodname,$*))"
	$(quiet)$(LX) $(MLXFLAGS) -o $@ \
            -Wl,-soname,$(*:.so=) \
            -Wl,-Map,$(moddir)/$*.map \
            $(ldtoolsobj)/modbegin.o \
            $(ldtoolsobj)/rcework.o \
            $(modobjs_$(call getmodname,$*)) $(linkflags_$(call getmodname,$*)) \
            $(ldtoolsobj)/modend.o \
            $(modsneeded_$(call getmodname,$*))
	$(quiet)$(OBJCOPY) --set-section-flags .bss=alloc,contents,load $@

# Objects for C, C++ and assembly files not intended for modules
$(objdir)/%.o: %.c
	@echo "[CC] Compiling $<"
	$(quiet)$(CC) $(incdirs_$<) $(CPPFLAGS) $(DEFINES) $(CFLAGS) -c $< -o $@

$(objdir)/%.o: %.cc
	@echo "[CX] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$<) $(CPPFLAGS) $(DEFINES) $(CXXFLAGS) -c $< -o $@

$(objdir)/%.o: %.cpp
	@echo "[CX] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$<) $(CPPFLAGS) $(DEFINES) $(CXXFLAGS) -c $< -o $@

$(objdir)/%.o: %.s
	@echo "[CS] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$<) $(CPPFLAGS) $(DEFINES) $(CASFLAGS) -c $< -o $@


# Module objects for C, C++ and assembly files
$(modobjdir)/%.o: %.c
	@echo "[MC] Compiling $<"
	$(quiet)$(CC) $(incdirs_$<) $(CPPFLAGS) $(MDEFINES) $(MCFLAGS) -c $< -o $@

$(modobjdir)/%.o: %.cc
	@echo "[MX] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$<) $(CPPFLAGS) $(MDEFINES) $(MCXXFLAGS) -c $< -o $@

$(modobjdir)/%.o: %.cpp
	@echo "[MX] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$<) $(CPPFLAGS) $(MDEFINES) $(MCXXFLAGS) -c $< -o $@

$(modobjdir)/%.o: %.s
	@echo "[MS] Compiling $<"
	$(quiet)$(CXX) $(incdirs_$<) $(CPPFLAGS) $(MDEFINES) $(MCASFLAGS) -c $< -o $@


# Defines rules to (re)build dependency files
DEPSED = sed '\''s!$(notdir $*)\.o!$(objdir)/$*\.o $@!g'\''
CXXDEP = $(CXX) $(incdirs_$<) $(CPPFLAGS) $(DEPFLAGS)
CCDEP  = $(CC)  $(incdirs_$<) $(CPPFLAGS) $(DEPFLAGS)

$(depdir)/%.d: %.c
	@echo "[DC] Dependencies for $<"
	$(quiet)$(SHELL) -ec '$(CCDEP) $< | $(DEPSED)  > $@'

$(depdir)/%.d: %.cc
	@echo "[DX] Dependencies for $<"
	$(quiet)$(SHELL) -ec '$(CXXDEP) $< | $(DEPSED) > $@'

$(depdir)/%.d: %.cpp
	@echo "[DX] Dependencies for $<"
	$(quiet)$(SHELL) -ec '$(CXXDEP) $< | $(DEPSED) > $@'

$(depdir)/%.d: %.s
	@echo "[DS] Dependencies for $<" 
	$(quiet)$(SHELL) -ec '$(CCDEP) $< | $(DEPSED) > $@'

MODDEPSED = sed '\''s!$(notdir $*)\.o!$(modobjdir)/$*\.o $@!g'\''

$(moddepdir)/%.d: %.c
	@echo "[MDC] Dependencies for $<"
	$(quiet)$(SHELL) -ec '$(CCDEP) $< | $(MODDEPSED)  > $@'

$(moddepdir)/%.d: %.cc
	@echo "[MDX] Dependencies for $<"
	$(quiet)$(SHELL) -ec '$(CXXDEP) $< | $(MODDEPSED) > $@'

$(moddepdir)/%.d: %.cpp
	@echo "[MDX] Dependencies for $<"
	$(quiet)$(SHELL) -ec '$(CXXDEP) $< | $(MODDEPSED) > $@'

$(moddepdir)/%.d: %.s
	@echo "[MDS] Dependencies for $<" 
	$(quiet)$(SHELL) -ec '$(CCDEP) $< | $(MODDEPSED) > $@'

# Include the dependency files.  If one of the .d files in depends
# does not exist, then make invokes one of the rules [Dn] above to
# rebuild the missing .d file.  This can be short-circuited by
# defining the symbol 'no_depends'.

ifneq ($(depends),)
ifeq  ($(no_depends),)
-include $(depends)
endif
endif
