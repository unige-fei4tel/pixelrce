-- MySQL dump 10.13  Distrib 5.1.67, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: 
-- ------------------------------------------------------
-- Server version	5.1.67

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `DCSTests`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `DCSTests` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `DCSTests`;

--
-- Table structure for table `dcs_results`
--

DROP TABLE IF EXISTS `dcs_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dcs_results` (
  `dcs_uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stave_num` int(11) NOT NULL,
  `staves_uid` int(10) unsigned NOT NULL,
  `stave_group` varchar(5) NOT NULL,
  `dcs_iv_file` varchar(200) NOT NULL,
  `dcs_iv_plot` varchar(200) NOT NULL,
  `dcs_iv_bd` float NOT NULL,
  `dcs_iv_T` float NOT NULL,
  `dcs_iv_pass` tinyint(1) DEFAULT NULL,
  `dcs_lv_powerup` int(11) DEFAULT '-1',
  `dcs_lv_config1` int(11) DEFAULT '-1',
  `dcs_lv_config2` int(11) DEFAULT '-1',
  PRIMARY KEY (`dcs_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `elec_readout`
--

DROP TABLE IF EXISTS `elec_readout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elec_readout` (
  `chip_uid` int(10) unsigned NOT NULL,
  `config_uid` int(11) NOT NULL,
  `eye_pic` varchar(100) NOT NULL,
  `eye_open_V` float DEFAULT NULL,
  `eye_open_t` float DEFAULT NULL,
  `eye_amplitude` float DEFAULT NULL,
  `eye_pass` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lv_tests`
--

DROP TABLE IF EXISTS `lv_tests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lv_tests` (
  `lv_uid` int(10) unsigned NOT NULL,
  `test_type` varchar(20) NOT NULL,
  `config_uid` int(11) NOT NULL,
  `lv_I_max` float NOT NULL,
  `lv_I_min` float NOT NULL,
  `lv_I_file` varchar(100) NOT NULL,
  `lv_I_plot` varchar(100) NOT NULL,
  `lv_T_max` float NOT NULL,
  `lv_T_min` float NOT NULL,
  `lv_T_file` varchar(100) NOT NULL,
  `lv_T_plot` varchar(100) NOT NULL,
  `lv_V_set` float NOT NULL,
  `lv_V_mon` float NOT NULL,
  `lv_pass` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`lv_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `RCECalibRuns`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `RCECalibRuns` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `RCECalibRuns`;

--
-- Table structure for table `CalibRunLog`
--

DROP TABLE IF EXISTS `CalibRunLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CalibRunLog` (
  `URL` varchar(200) DEFAULT NULL,
  `RunNum` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `User` varchar(50) DEFAULT NULL,
  `Host` varchar(50) DEFAULT NULL,
  `ScanType` varchar(50) DEFAULT NULL,
  `Debugging` bit(1) DEFAULT NULL,
  `DataExported` bit(1) DEFAULT NULL,
  `DataSaved` bit(1) DEFAULT NULL,
  `DataPath` varchar(200) DEFAULT NULL,
  `StartTime` timestamp NULL DEFAULT NULL,
  `EndTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Comment` text,
  `Status` varchar(10) NOT NULL DEFAULT 'Failed',
  `Quality` varchar(25) NOT NULL DEFAULT 'Unknown' COMMENT 'Quality of the run: Unkown, Green, Yellow, Red',
  PRIMARY KEY (`RunNum`)
) ENGINE=InnoDB AUTO_INCREMENT=6931 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `LogView`
--

DROP TABLE IF EXISTS `LogView`;
/*!50001 DROP VIEW IF EXISTS `LogView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `LogView` (
  `RunNum` int(10) unsigned,
  `StaveId` varchar(255),
  `Position` varchar(10),
  `User` varchar(50),
  `Host` varchar(50),
  `ScanType` varchar(50),
  `Debugging` int(1) unsigned,
  `DataExported` int(1) unsigned,
  `DataSaved` int(1) unsigned,
  `DataPath` varchar(200),
  `ModuleId` varchar(50),
  `FEId` varchar(50),
  `CfgName` varchar(100),
  `StartTime` timestamp,
  `EndTime` timestamp,
  `Comment` text,
  `Status` varchar(10),
  `Quality` varchar(10)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ModuleLog`
--

DROP TABLE IF EXISTS `ModuleLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleLog` (
  `RunNum` int(10) unsigned NOT NULL,
  `ModuleId` varchar(50) DEFAULT NULL,
  `FEId` varchar(50) DEFAULT NULL,
  `CfgName` varchar(100) DEFAULT NULL,
  `Quality` varchar(10) NOT NULL DEFAULT 'Unknown',
  KEY `RunNum` (`RunNum`),
  CONSTRAINT `ModuleLog_ibfk_1` FOREIGN KEY (`RunNum`) REFERENCES `CalibRunLog` (`RunNum`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `StaveMap`
--

DROP TABLE IF EXISTS `StaveMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `StaveMap` (
  `FEId` int(11) NOT NULL,
  `Position` varchar(10) NOT NULL,
  `StaveId` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `RCECalibRunsDev`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `RCECalibRunsDev` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `RCECalibRunsDev`;

--
-- Table structure for table `CalibRunLog`
--

DROP TABLE IF EXISTS `CalibRunLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CalibRunLog` (
  `URL` varchar(200) DEFAULT NULL,
  `RunNum` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `User` varchar(50) DEFAULT NULL,
  `Host` varchar(50) DEFAULT NULL,
  `ScanType` varchar(50) DEFAULT NULL,
  `Debugging` bit(1) DEFAULT NULL,
  `DataExported` bit(1) DEFAULT NULL,
  `DataSaved` bit(1) DEFAULT NULL,
  `DataPath` varchar(200) DEFAULT NULL,
  `StartTime` timestamp NULL DEFAULT NULL,
  `EndTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Comment` text,
  `Status` varchar(10) NOT NULL DEFAULT 'Failed',
  PRIMARY KEY (`RunNum`)
) ENGINE=InnoDB AUTO_INCREMENT=748 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `LogView`
--

DROP TABLE IF EXISTS `LogView`;
/*!50001 DROP VIEW IF EXISTS `LogView`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `LogView` (
  `RunNum` int(10) unsigned,
  `User` varchar(50),
  `Host` varchar(50),
  `ScanType` varchar(50),
  `Debugging` int(1) unsigned,
  `DataExported` int(1) unsigned,
  `DataSaved` int(1) unsigned,
  `DataPath` varchar(200),
  `ModuleId` varchar(50),
  `FEId` varchar(50),
  `CfgName` varchar(100),
  `StartTime` timestamp,
  `EndTime` timestamp,
  `Comment` text,
  `Status` varchar(10)
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ModuleLog`
--

DROP TABLE IF EXISTS `ModuleLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModuleLog` (
  `RunNum` int(10) unsigned NOT NULL,
  `ModuleId` varchar(50) DEFAULT NULL,
  `FEId` varchar(50) DEFAULT NULL,
  `CfgName` varchar(100) DEFAULT NULL,
  KEY `RunNum` (`RunNum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

